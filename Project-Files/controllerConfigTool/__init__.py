from flask_bootstrap import Bootstrap
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from controllerConfigTool.configFile import getConfigFile
from flask_migrate import Migrate
from flask_wtf.csrf import CSRFProtect

csrf = CSRFProtect()

app = Flask(__name__)

# Get database info from Jetvent.ini File
db_pass, db_ip = getConfigFile()
db_uri = 'mysql+pymysql://jetvent:' + str(db_pass) + '@'+ str(db_ip) + '/jetvent'
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config.from_pyfile('config.cfg')
bootstrap = Bootstrap(app)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# csrf.init_app(app)

from controllerConfigTool.salesorder.routes import salesorder_blueprint
from controllerConfigTool.salesorderstatus.routes import salesorderstatus_blueprint
from controllerConfigTool.itemconfigurations.routes import itemconfigurations_blueprint
from controllerConfigTool.inputoutputconfigurations.routes import inputoutputconfigurations_blueprint
from controllerConfigTool.expansionboardconfigurations.routes import expansionboardconfigurations_blueprint
from controllerConfigTool.controllerconfigurations.routes import controllerconfigurations_blueprint
from controllerConfigTool.controllerassignments.routes import controllerassignments_blueprint 
from controllerConfigTool.analogsensorassignment.routes import analogsensorassignment_blueprint
from controllerConfigTool.addressrangecreation.routes import addressrangecreation_blueprint
from controllerConfigTool.admin.routes import admin_blueprint
from controllerConfigTool.choosefilepath.routes import choosefilepath_blueprint
from controllerConfigTool.createdefinitions.routes import createdefinitions_blueprint
from controllerConfigTool.searchsalesorder.routes import searchsalesorder_blueprint
from controllerConfigTool.viewdefinitions.routes import viewdefinitions_blueprint
from controllerConfigTool.programcontrollers.routes import programcontrollers_blueprint
from controllerConfigTool.zoneconfigure.routes import zone_blueprint


app.register_blueprint(salesorder_blueprint)
app.register_blueprint(salesorderstatus_blueprint)
app.register_blueprint(itemconfigurations_blueprint)
app.register_blueprint(inputoutputconfigurations_blueprint)
app.register_blueprint(controllerconfigurations_blueprint)
app.register_blueprint(controllerassignments_blueprint)
app.register_blueprint(analogsensorassignment_blueprint)
app.register_blueprint(addressrangecreation_blueprint)
app.register_blueprint(admin_blueprint)
app.register_blueprint(choosefilepath_blueprint)
app.register_blueprint(createdefinitions_blueprint)
app.register_blueprint(searchsalesorder_blueprint)
app.register_blueprint(viewdefinitions_blueprint)
app.register_blueprint(expansionboardconfigurations_blueprint)
app.register_blueprint(programcontrollers_blueprint)
app.register_blueprint(zone_blueprint)