from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField
from wtforms.validators import ValidationError, Optional
from wtforms.widgets import HiddenInput

def addressRangeCreationForm(default_mixer_fan_ending_address, default_fieldbus_sensor_quantity):

    class AddressRangeCreation(FlaskForm):
        def checkFanRange(form, field):
            modbus_address = field.data
            if modbus_address:
                if modbus_address < 1 or modbus_address > 247:
                    raise ValidationError(f'Invalid Modbus Address Value: {modbus_address}. Values must be within range 1-247.')

        fs_count = 100 + default_fieldbus_sensor_quantity if default_fieldbus_sensor_quantity else 0

        mixer_fan_starting_address = IntegerField('Starting Address', default=1, validators=[Optional(), checkFanRange])
        mixer_fan_ending_address = IntegerField('Ending Address', default=default_mixer_fan_ending_address, validators=[Optional(), checkFanRange])

        fieldbus_sensor_starting_address = IntegerField('Starting Address', validators=[Optional(), checkFanRange], default= 101)
        fieldbus_sensor_ending_address = IntegerField('Ending Address', validators=[Optional(), checkFanRange], default=fs_count)

        analog_sensor_starting_address = IntegerField('Starting Address', validators=[Optional(), checkFanRange])
        analog_sensor_ending_address = IntegerField('Ending Address', validators=[Optional(), checkFanRange])

        supply_exhaust_starting_address = IntegerField('Starting Address', validators=[Optional(), checkFanRange])
        supply_exhuast_ending_address = IntegerField('Ending Address', validators=[Optional(), checkFanRange])

        submit_button = SubmitField('Submit Address Range(s)')
        return_button= SubmitField("Return") #Added return button CG changes 03/04/2023
        # edit_button= SubmitField("Edit Address Range") #Added edit button CG changes 03/04/2023

    
    return AddressRangeCreation()



def addressRangeDisplayForm(default_mixer_fan_ending_address, default_fieldbus_sensor_quantity):

    class AddressRangeDisplay(FlaskForm):
        def checkFanRange(form, field):
            modbus_address = field.data
            if modbus_address:
                if modbus_address < 1 or modbus_address > 247:
                    raise ValidationError(f'Invalid Modbus Address Value: {modbus_address}. Values must be within range 1-247.')
        
        
        fs_count = 100 + default_fieldbus_sensor_quantity if default_fieldbus_sensor_quantity else 0

        mixer_fan_starting_address = IntegerField('Starting Address', default=1, validators=[Optional(), checkFanRange], render_kw={'readonly': True} )
        mixer_fan_ending_address = IntegerField('Ending Address', default=default_mixer_fan_ending_address, validators=[Optional(), checkFanRange], render_kw={'readonly': True})

        fieldbus_sensor_starting_address = IntegerField('Starting Address', validators=[Optional(), checkFanRange], render_kw={'readonly': True}, default=101)
        fieldbus_sensor_ending_address = IntegerField('Ending Address', validators=[Optional(), checkFanRange], render_kw={'readonly': True}, default=fs_count)

        analog_sensor_starting_address = IntegerField('Starting Address', validators=[Optional(), checkFanRange], render_kw={'readonly': True})
        analog_sensor_ending_address = IntegerField('Ending Address', validators=[Optional(), checkFanRange], render_kw={'readonly': True})

        supply_exhaust_starting_address = IntegerField('Starting Address', validators=[Optional(), checkFanRange], render_kw={'readonly': True})
        supply_exhuast_ending_address = IntegerField('Ending Address', validators=[Optional(), checkFanRange], render_kw={'readonly': True})

        # submit_button = SubmitField('Submit Address Range(s)')
        return_button= SubmitField("Return") #Added return button CG changes 03/04/2023
        edit_button= SubmitField("Edit Address Range") #Added edit button CG changes 03/04/2023
        dict_string =  StringField(widget=HiddenInput(), render_kw={'readonly': True})

    
    return AddressRangeDisplay()

