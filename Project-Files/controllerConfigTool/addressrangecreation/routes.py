'''CG Changes, This file is modified by CG developers on 07/04/2023'''

from flask import render_template, url_for, flash, redirect, request
from flask import Blueprint
from controllerConfigTool import app, db
from controllerConfigTool.models import SalesOrders
from controllerConfigTool.getInfoFromDatabase import findUnaddressedItems
from controllerConfigTool.addressrangecreation.forms import addressRangeCreationForm, addressRangeDisplayForm
from controllerConfigTool.addressrangecreation.utils import createFans, createFieldbusSensors, findaddressedItems
from controllerConfigTool.models import Job, FieldbusSensors
# import numpy as np



addressrangecreation_blueprint = Blueprint('addressrangecreation', __name__)


@addressrangecreation_blueprint.route('/address_range_display/<int:sales_order_id>', methods=['GET', 'POST'])
def address_range_display(sales_order_id):
    # pass
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    default_mixer_fan_ending_address = sales_order_object.mixer_fan_quantity
    default_fieldbus_sensor_quantity = sales_order_object.fieldbus_sensor_quantity

    address_string_dictionary = findaddressedItems(sales_order_id)
    unaddressed_items_dict = findUnaddressedItems(sales_order_id)
    unaddressed_fbsensor_count = unaddressed_items_dict['Fieldbus Sensors']
    unaddressed_fans_count = unaddressed_items_dict['Fans']
    total_remaining_items = unaddressed_fbsensor_count + unaddressed_fans_count

    # create address range creation form
    address_range_edition_form = addressRangeDisplayForm(unaddressed_fans_count, unaddressed_fbsensor_count)
    # get existing addressed items
  
    

    if request.method == 'POST':
        if address_range_edition_form.validate_on_submit():

            if address_range_edition_form.edit_button.data:
                if total_remaining_items == 0:
                    return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id))
                else:
                    return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id))
            elif address_range_edition_form.return_button.data:
                    return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
        else:
            flash('Something Went Wrong', 'danger')

    return render_template('address_range_display.html', address_range_creation_form=address_range_edition_form, unaddressed_items_dict=unaddressed_items_dict,
                address_string_dictionary=address_string_dictionary)



@addressrangecreation_blueprint.route('/address_range_creation/<int:sales_order_id>', methods=['GET', 'POST'])
def address_range_creation(sales_order_id):
    # find sales order object
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    entered_by = sales_order_object.input_by
    
    # find unassigned items
    unaddressed_items_dict = findUnaddressedItems(sales_order_id)

    unaddressed_fbsensor_count = unaddressed_items_dict['Fieldbus Sensors']
    # unaddressed_analog_count = unaddressed_items_dict['Analog Sensors']
    unaddressed_fans_count = unaddressed_items_dict['Fans']
    
    total_remaining_items = unaddressed_fbsensor_count + unaddressed_fans_count
    # Added condition and update the item dict for edit
    if total_remaining_items == 0:
        sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
        initial_mixer_fans_count = sales_order_object.mixer_fan_quantity
        initial_fieldbus_sensors_count = sales_order_object.fieldbus_sensor_quantity
        unaddressed_fans_count, unaddressed_fbsensor_count = initial_mixer_fans_count, initial_fieldbus_sensors_count
        unaddressed_items_dict['Fans'] = initial_mixer_fans_count
        unaddressed_items_dict['Fieldbus Sensors'] = initial_fieldbus_sensors_count
       
    default_mixer_fan_ending_address = sales_order_object.mixer_fan_quantity
    default_fieldbus_sensor_quantity = sales_order_object.fieldbus_sensor_quantity
    
    # create address range creation form
    address_range_creation_form = addressRangeCreationForm(unaddressed_fans_count, unaddressed_fbsensor_count)


    # get existing addressed items
    address_string_dictionary = findaddressedItems(sales_order_id)
    
    if request.method == 'POST':

        if address_range_creation_form.validate_on_submit():
            if address_range_creation_form.submit_button.data:
                
                mixer_fan_starting_address = address_range_creation_form.mixer_fan_starting_address.data
                mixer_fan_ending_address = address_range_creation_form.mixer_fan_ending_address.data
                fieldbus_sensor_starting_address = address_range_creation_form.fieldbus_sensor_starting_address.data
                fieldbus_sensor_ending_address = address_range_creation_form.fieldbus_sensor_ending_address.data
                # analog_sensor_starting_address = address_range_creation_form.analog_sensor_starting_address.data
                # analog_sensor_ending_address = address_range_creation_form.analog_sensor_ending_address.data
                # supply_exhaust_starting_address = address_range_creation_form.supply_exhaust_starting_address.data
                # supply_exhuast_ending_address = address_range_creation_form.supply_exhuast_ending_address.data
            
                # flash message boolean
                flash_message_boolean = False
                flash_message_string = ''

                # mixer fans
                submitted_mixerfan_count = 0
                if mixer_fan_starting_address is not None and mixer_fan_ending_address is not None:
                    submitted_mixerfan_count = mixer_fan_ending_address - mixer_fan_starting_address + 1
                    if submitted_mixerfan_count > unaddressed_fans_count:
                        flash(f'Invalid submission. You tried addressing {submitted_mixerfan_count} Mixer Fans while only {unaddressed_fans_count} Mixer Fans are available', 'danger')
                    else:
                        # get jobs number
                        sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                        sales_order_number = sales_order_object.sales_order_number
                        # Added condition and sql query for deleting data previous data from database
                        if total_remaining_items == 0 and submitted_mixerfan_count <= unaddressed_fans_count:
                            Job.query.filter_by(sales_order_id=sales_order_id).delete()
                            db.session.commit()
                        # Create row for each Fan (in Jobs table)
                        createFans(sales_order_id, sales_order_number, mixer_fan_starting_address, mixer_fan_ending_address, entered_by)
                        flash_message_boolean = True
                        flash_message_string += f'Mixer Fans {mixer_fan_starting_address} - {mixer_fan_ending_address} were created. '


                # fieldbus sensors
                submitted_fieldbus_sensor_count = 0
                if fieldbus_sensor_starting_address is not None and fieldbus_sensor_ending_address is not None:
                    submitted_fieldbus_sensor_count = fieldbus_sensor_ending_address - fieldbus_sensor_starting_address + 1
                    if submitted_fieldbus_sensor_count > unaddressed_fbsensor_count:
                        flash(f'Invalid submission. You tried addressing {submitted_fieldbus_sensor_count} Fieldbus sensors while only {unaddressed_fbsensor_count} Fieldbus sensors are available', 'danger')
                    else:
                        # Added condition and sql query for deleting data previous data from database
                        if total_remaining_items == 0 and submitted_fieldbus_sensor_count <= unaddressed_fbsensor_count:
                            FieldbusSensors.query.filter_by(sales_order_id=sales_order_id).delete()
                            db.session.commit()
                        # Create row for each Fieldbus sensor (in Fieldbus Sensors table)
                        createFieldbusSensors(sales_order_id, fieldbus_sensor_starting_address, fieldbus_sensor_ending_address)
                        flash_message_boolean = True
                        flash_message_string += f'Fieldbus Sensors {fieldbus_sensor_starting_address} - {fieldbus_sensor_ending_address} were created. '


                # analog sensors
                # submitted_analog_sensor_count = 0
                # if analog_sensor_starting_address is not None and analog_sensor_ending_address is not None:
                #     submitted_analog_sensor_count = analog_sensor_ending_address - analog_sensor_starting_address + 1
                #     if submitted_analog_sensor_count > unaddressed_analog_count:
                #         flash(f'Invalid submission. You tried addressing {submitted_analog_sensor_count} Analog sensors while only {unaddressed_analog_count} Analog sensors are available', 'danger')
                #     else:
                #         # Create row for each Analog sensor (in Analog Sensors table)
                #         createAnalogSensors(sales_order_id, analog_sensor_starting_address, analog_sensor_ending_address)
                #         flash_message_boolean = True
                #         flash_message_string += f'Analog Sensors {analog_sensor_starting_address} - {analog_sensor_ending_address} were created. '


                # supply exhuast fans
                # submitted_exhaust_supply_fan_count = 0
                # if (supply_exhaust_starting_address is not None and supply_exhuast_ending_address is not None):
                #     submitted_exhaust_supply_fan_count = submitted_exhaust_supply_fan_count + (supply_exhuast_ending_address-supply_exhaust_starting_address + 1)
                #     if submitted_exhaust_supply_fan_count > unaddressed_supply_exhaust_count:
                #         flash(f'Invalid submission. You tried addressing {submitted_exhaust_supply_fan_count} Supply/Exhaust fans while only {unaddressed_supply_exhaust_count} Supply/Exhaust are available', 'danger')
                #     else:
                #         # Create row for each fan (in Supply Exhaust Fans table)
                #         createExhaustSupplyFans(sales_order_id, supply_exhaust_starting_address, supply_exhuast_ending_address)
                #         flash_message_boolean = True
                #         flash_message_string += f'Supply & Exhaust Fans {supply_exhaust_starting_address} - {supply_exhuast_ending_address} were created. '


                if flash_message_boolean:
                    flash(flash_message_string, 'success')

                # Determine unaddressed items; if 0 items remaining, flash finish and redirect to salesorderstatus
                # If > 0 items, redirect back to this route (address range creation) and flash to user they must configure all items before leaving
                unaddressed_items_dict = findUnaddressedItems(sales_order_id)

                unaddressed_fbsensor_count = unaddressed_items_dict['Fieldbus Sensors']
                unaddressed_fans_count = unaddressed_items_dict['Fans']

                total_remaining_items = unaddressed_fbsensor_count + unaddressed_fans_count

                # Added flash condition for success
                if total_remaining_items == 0 and flash_message_boolean:
                    flash('All items have been addressed. Redirecting to SalesOrder Status','success')
                    # show_edit_button = True
                    return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
                else:
                    flash('Please finish addressing all items', 'success')
                    return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id))
 
            elif address_range_creation_form.return_button.data:
                    return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
            
            # elif address_range_creation_form.edit_button.data:
            #         return redirect(url_for('addressrangecreation.address_range_edit', sales_order_id=sales_order_id))

        else:
            flash('Changes failed to save', 'danger')

    return render_template('address_range_creation.html', address_range_creation_form=address_range_creation_form, unaddressed_items_dict=unaddressed_items_dict,
                address_string_dictionary=address_string_dictionary)


