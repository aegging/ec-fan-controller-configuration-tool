from controllerConfigTool.getInfoFromDatabase import getDateAndTime
from controllerConfigTool.models import Job, FieldbusSensors, AnalogSensors
from controllerConfigTool import db
from controllerConfigTool.dataCalculations import createModbusRangesString
from controllerConfigTool.models import Job
from controllerConfigTool import db


def createFieldbusSensors(sales_order_id, sensor_starting_address, sensor_ending_address):
    zone=1
    for current_address in range(sensor_starting_address, sensor_ending_address + 1):
        fbsensor = FieldbusSensors(
                        sales_order_id = sales_order_id,
                        modbus_address = current_address,
                        zone= zone

                    )
        db.session.add(fbsensor)
    
    db.session.commit()


def createCompleteAddressList(fan_range_1_starting_address, fan_range_1_ending_address,
                                fan_range_2_starting_address, fan_range_2_ending_address):
    complete_address_list = []
    # if fan range 1 was given
    if (fan_range_1_starting_address is not None and fan_range_1_ending_address is not None):
        for address in range(fan_range_1_starting_address, fan_range_1_ending_address+1):
            complete_address_list.append(address)

    # if fan range 2 was given
    if (fan_range_2_starting_address is not None and fan_range_2_ending_address is not None):
        for address in range(fan_range_2_starting_address, fan_range_2_ending_address+1):
            complete_address_list.append(address)

    return complete_address_list


def createFans(sales_order_id, current_sales_order_number, fan_range_1_starting_address, fan_range_1_ending_address, entered_by):

    complete_address_list = createCompleteAddressList(fan_range_1_starting_address, fan_range_1_ending_address,
                                                      None, None)

    """ Initialize Variables --> """
    comms = ""
    no_sensors = 0
    sensor_1_type = ''
    sensor_1_unit = ''
    sensor_1_min = 0
    sensor_1_max = 0
    sensor_2_type= ''
    sensor_2_unit = ''
    sensor_2_min = 0
    sensor_2_max = 0
    command_value = 0
    control = ''
    minimum_speed = 0
    maximum_speed = 0
    p_val = 0
    i_val = 0
    control_min = 0
    control_max = 0
    smoke_detector = 0
    emergency = ''
    emer_set_speed = 0
    emer_external_signal = ''
    emer_start_stop_signal = ''
    timeout = 60
    timeout_speed = 0
    programmed = 'False'
    input_by = 'None'
    input_on = getDateAndTime()
    fan_type = ''
    zone = 1
    fault_relay = ''
    """ --> Initialize Variables """

    # Initialize rows in Controller table
    for address_index in range(0, len(complete_address_list)):

        # handle job number when column in jobs table when there are overlapping fan ranges
        if address_index > (fan_range_1_ending_address - fan_range_1_starting_address):
            job_number = str(current_sales_order_number) + "-" + str("{0:03}".format(fan_range_1_starting_address + address_index))
        else:
            job_number = str(current_sales_order_number) + "-" + str("{0:03}".format(complete_address_list[address_index]))

        fan = Job(
            sales_order_id=sales_order_id,
            Job_number=job_number,
            Comms=comms,
            Address=complete_address_list[address_index],
            No_Sensors=no_sensors,
            Sensor_1_type=sensor_1_type,
            Sensor_1_unit=sensor_1_unit,
            Sensor_1_min=sensor_1_min,
            Sensor_1_max=sensor_1_max,
            Sensor_2_type=sensor_2_type,
            Sensor_2_unit=sensor_2_unit,
            Sensor_2_min=sensor_2_min,
            Sensor_2_max=sensor_2_max,
            Command_Value=command_value,
            Control=control,
            Minimum_Speed=minimum_speed,
            Maximum_Speed=maximum_speed,
            P_val=p_val,
            I_val=i_val,
            Control_Min=control_min,
            Control_Max=control_max,
            SmokeDetector=smoke_detector,
            emergancy=emergency,
            emer_set_speed=emer_set_speed,
            emer_external_signal=emer_external_signal,
            emer_start_stop_signal=emer_start_stop_signal,
            Timeout=timeout,
            TimeoutSpeed=timeout_speed,
            Programmed=programmed,
            InputBy=entered_by,
            InputOn=input_on,
            FanType=fan_type,
            Zone=zone,
            FaultRelay=fault_relay        
        )

        # add each fan
        db.session.add(fan)

    # commit after all fans have been added
    db.session.commit()



""" find addressed items in sales order
    find remaining by comparing count in sales order table w/ how how many have actually been created (look for this sales order id in appropriate table)
    controllers_count, mixer_fans_count, sensors_count, expansion_boards_count
"""
def findaddressedItems(sales_order_id):

    #find how many have modbus address and sales order id

    mixer_fan_objects = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.Address!=None).order_by(Job.Index).all()

    fieldbus_sensors_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
        filter(FieldbusSensors.modbus_address!=None).order_by(FieldbusSensors.id).all()

    analog_sensors_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
        filter(AnalogSensors.modbus_address!=None).order_by(AnalogSensors.id).all()

    # exhaust_supply_fan_objects = db.session.query(SupplyExhaustFans).filter(SupplyExhaustFans.sales_order_id==sales_order_id).\
    #     filter(SupplyExhaustFans.modbus_address!=None).order_by(SupplyExhaustFans.id).all()


    # build array of [address, id] for each item
    mixer_fan_address_list = []
    fieldbus_sensors_list = []
    analog_sensors_list = []
    exhaust_supply_fan_list = []


    if mixer_fan_objects:
        for mixer_fan_object in mixer_fan_objects:
            mixer_fan_address_list.append(int(mixer_fan_object.Address))

    if fieldbus_sensors_objects:
        for fieldbus_sensor_object in fieldbus_sensors_objects:
            fieldbus_sensors_list.append(int(fieldbus_sensor_object.modbus_address))

    if analog_sensors_objects:
        for analog_sensors_object in analog_sensors_objects:
            analog_sensors_list.append(int(analog_sensors_object.modbus_address))

    # if exhaust_supply_fan_objects:
    #     for exhaust_supply_fan_object in exhaust_supply_fan_objects:
    #         exhaust_supply_fan_list.append(int(exhaust_supply_fan_object.modbus_address))



    # create modbus address string for each array
    mixer_fan_address_string = createModbusRangesString(mixer_fan_address_list)
    fieldbus_sensors_address_string = createModbusRangesString(fieldbus_sensors_list)
    analog_sensors_address_string = createModbusRangesString(analog_sensors_list)
    # exhaust_supply_address_string = createModbusRangesString(exhaust_supply_fan_list)


    address_string_dictionary = {}
    
    address_string_dictionary['Mixer Fans'] = mixer_fan_address_string
    address_string_dictionary['Fieldbus Sensors'] = fieldbus_sensors_address_string
    address_string_dictionary['Analog Sensors'] = analog_sensors_address_string
    # address_string_dictionary['Exhaust Supply Fans'] = exhaust_supply_address_string


    return address_string_dictionary