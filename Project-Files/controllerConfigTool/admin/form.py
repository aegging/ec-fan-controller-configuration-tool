
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, StringField, BooleanField
from controllerConfigTool.models import Settings
from wtforms_sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, ValidationError



def initializeVersionForm():
    # find current selection
    currently_selected_object = Settings.query.filter_by(currently_selected = True).first()
    if currently_selected_object:
        settings_id = currently_selected_object.id
    # if none are selected, default to 3.2.7 version
    else:
        settings_id = 5

    # find all enabled versions
    settings_query_objects = Settings.query.filter_by(enable_version = True).all()
    settings_enabled_version_list = []
    for settings_object in settings_query_objects:
        settings_enabled_version_list.append([settings_object.id, settings_object.version_number])

    class SelectVersionForm(FlaskForm):
        version_number = SelectField('Version Number', choices = settings_enabled_version_list, default=settings_id)
        save_settings = SubmitField('Save Settings')
        edit_settings = SubmitField('Edit Settings')
    return SelectVersionForm()


class AdminForm(FlaskForm):
    add_version_button = SubmitField('Add New')
    edit_version_button = SubmitField('Edit Existing')
    create_standard_fan_definition_button = SubmitField('Add New')



def initializeSettingsForm(version_id):
    are_there_settings = bool(Settings.query.filter_by(id=version_id).first())
    if are_there_settings:
        settings_object = Settings.query.filter_by(id=version_id).first()
        version_number_value = settings_object.version_number
        cfactory_path_value = settings_object.cfactory_path
        mini_http_folder_directory_value = settings_object.mini_http_folder_directory
        max_http_folder_directory_value = settings_object.max_http_folder_directory
        mini_ap1_directory_value = settings_object.mini_ap1_directory
        max_ap1_directory_value = settings_object.max_ap1_directory
        time_zone_value = settings_object.time_zone
        enable_version_value = settings_object.enable_version
        valid_with_max_controller_value = settings_object.valid_with_max_controller
        valid_with_mini_controller_value = settings_object.valid_with_mini_controller
    else:
        version_number_value = None
        cfactory_path_value = None
        mini_http_folder_directory_value = None
        max_http_folder_directory_value = None
        mini_ap1_directory_value = None
        max_ap1_directory_value = None
        time_zone_value = None
        enable_version_value = True
        valid_with_max_controller_value = False
        valid_with_mini_controller_value = False


    class SettingsForm(FlaskForm):
        if are_there_settings:
            version_number=StringField('Enter Version Number', default=version_number_value, render_kw={'readonly': True}, validators=[DataRequired()])
            cfactory_path=StringField('Enter Path for C.factory.exe', default=cfactory_path_value)
            mini_http_folder_directory=StringField('Enter Path for Mini HTTP Folder', default=mini_http_folder_directory_value)
            max_http_folder_directory=StringField('Enter Path for Max HTTP Folder', default=max_http_folder_directory_value)
            mini_ap1_directory=StringField('Enter Path for Mini Ap1 file', default=mini_ap1_directory_value)
            max_ap1_directory=StringField('Enter Path for Max Ap1 file', default=max_ap1_directory_value)
            enable_version = BooleanField('Enable Version To Allow Access To Users', default=enable_version_value)
            valid_with_max_controller = BooleanField('Enable for Max Controllers', default=valid_with_max_controller_value)
            valid_with_mini_controller = BooleanField('Enable for Mini Controllers', default=valid_with_mini_controller_value)
            time_zone = SelectField('Time Zone: ', choices=[('11','Chicago'),('9','Denver'),('15','New York'),('6','Los Angeles'),('7','Phoenix'),('90','SYD/MELB')], validate_choice=False, default=time_zone_value)
        else:
            version_number=StringField('Enter Version Number', validators=[DataRequired()])
            cfactory_path=StringField('Enter Path For C.factory.exe')
            mini_http_folder_directory=StringField('Enter Path For Mini HTTP Folder')
            max_http_folder_directory=StringField('Enter Path For Max HTTP Folder')
            mini_ap1_directory=StringField('Enter Path For Mini Ap1 File')
            max_ap1_directory=StringField('Enter Path For Max Ap1 File')
            enable_version = BooleanField('Enable Version To Allow Access To Users', default=False)
            valid_with_max_controller = BooleanField('Enable for Max Controllers', default=False)
            valid_with_mini_controller = BooleanField('Enable for Mini Controllers', default=False)
            time_zone = SelectField('Time Zone: ', choices=[('11','Chicago'),('9','Denver'),('15','New York'),('6','Los Angeles'),('7','Phoenix'),('90','SYD/MELB')], validate_choice=False)
     
        submit = SubmitField('Save Settings')

        def validate_version_number(self, version_number):
            if len(version_number.data) > 7:
                raise ValidationError('The Maximum length for version number is 7 characters')

    return SettingsForm

