from flask import Blueprint

from flask import render_template, url_for, flash, redirect, request

from controllerConfigTool.admin.form import AdminForm, initializeVersionForm, initializeSettingsForm
from controllerConfigTool.admin.utils import editSettings, saveSettingsToDatabase, saveVersionSelection
from controllerConfigTool.getInfoFromDatabase import getSelectedVersion
from controllerConfigTool.models import Settings

admin_blueprint = Blueprint('admin', __name__)


@admin_blueprint.route("/addversion/<view_state>/<int:version_id>/", methods=['GET', 'POST'])
def addversion(view_state, version_id):
    SettingsForm = initializeSettingsForm(version_id=version_id)
    settings_form = SettingsForm()

    if request.method == 'POST':
        if view_state == 'Add Version':
            if settings_form.validate_on_submit():
                saveSettingsToDatabase(
                    cfactory_path = settings_form.cfactory_path.data,
                    mini_http_folder_directory = settings_form.mini_http_folder_directory.data,
                    max_http_folder_directory = settings_form.max_http_folder_directory.data,
                    mini_ap1_directory = settings_form.mini_ap1_directory.data,
                    max_ap1_directory = settings_form.max_ap1_directory.data,
                    version_number = settings_form.version_number.data,
                    time_zone=settings_form.time_zone.data,
                    enable_version = settings_form.enable_version.data, 
                    valid_with_max_controller = settings_form.valid_with_max_controller.data,
                    valid_with_mini_controller = settings_form.valid_with_mini_controller.data
                )
                # Flash message to show controller has been successfully saved into database
                flash(f'Settings have been successfully stored in the database', 'success')

        elif view_state == 'Edit Version':
            if settings_form.validate_on_submit():
                editSettings(
                    version_id = version_id,
                    cfactory_path = settings_form.cfactory_path.data,
                    mini_http_folder_directory = settings_form.mini_http_folder_directory.data,
                    max_http_folder_directory = settings_form.max_http_folder_directory.data,
                    mini_ap1_directory = settings_form.mini_ap1_directory.data,
                    max_ap1_directory = settings_form.max_ap1_directory.data,
                    version_number = settings_form.version_number.data,
                    time_zone=settings_form.time_zone.data,
                    enable_version = settings_form.enable_version.data,
                    valid_with_max_controller = settings_form.valid_with_max_controller.data,
                    valid_with_mini_controller = settings_form.valid_with_mini_controller.data
                )


    return render_template('addversion.html', title=view_state, settings_form=settings_form)


@admin_blueprint.route("/selectversion/<view_state>/", methods=['GET', 'POST'])
def selectversion(view_state):
    # show current version being used
    selected_version = getSelectedVersion()

    select_version_form = initializeVersionForm()

    if request.method == 'POST':
        version_id = select_version_form.version_number.data
        settings_object = Settings.query.filter_by(id=version_id).first()
        version_number = settings_object.version_number
        if 'save_settings' in request.form:
            saveVersionSelection(version_id)
            flash(f'Version {version_number} has been selected', 'success')
            return redirect(url_for('admin.selectversion', view_state='Select Version'))
        elif 'edit_settings' in request.form:
            # redirect to add settings with prepopulate form
            return redirect(url_for('admin.addversion', view_state='Edit Version', version_id=version_id))

    return render_template('selectversion.html', title=view_state, view_state=view_state, select_version_form=select_version_form, selected_version=selected_version)


# admin panel giving option to add new software version
@admin_blueprint.route('/admin_view/', methods=['GET', 'POST'])
def admin_view():
    admin_form = AdminForm()

    if request.method == 'POST':
        # if button add version button is pressed, redirect to addversion
        if 'add_version_button' in request.form:
            return redirect(url_for('admin.addversion', view_state='Add Version', version_id=0))
        elif 'edit_version_button' in request.form:
            return redirect(url_for('admin.selectversion', view_state='Edit Version'))
        elif 'create_standard_fan_definition_button' in request.form:
            return redirect(url_for('createdefinitions.initializestandardfandefinition'))
    
    return render_template('admin.html', title='Admin Panel', admin_form=admin_form)
