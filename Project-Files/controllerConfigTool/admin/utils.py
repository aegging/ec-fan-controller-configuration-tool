from controllerConfigTool.getInfoFromDatabase import getDateAndTime
from controllerConfigTool.models import Settings
from controllerConfigTool import db




def editSettings(version_id, cfactory_path, mini_http_folder_directory, max_http_folder_directory,
                mini_ap1_directory, max_ap1_directory, version_number, time_zone, enable_version,
                valid_with_max_controller, valid_with_mini_controller):
    settings_object = Settings.query.filter_by(id=version_id).first()

    settings_object.cfactory_path = cfactory_path
    settings_object.mini_http_folder_directory = mini_http_folder_directory
    settings_object.max_http_folder_directory = max_http_folder_directory
    settings_object.mini_ap1_directory = mini_ap1_directory
    settings_object.max_ap1_directory = max_ap1_directory
    settings_object.version_number = version_number
    settings_object.time_zone = time_zone
    settings_object.input_on = getDateAndTime()
    settings_object.enable_version = enable_version
    settings_object.valid_with_max_controller = valid_with_max_controller
    settings_object.valid_with_mini_controller = valid_with_mini_controller

    db.session.add(settings_object)
    db.session.commit()


def saveSettingsToDatabase(cfactory_path, mini_http_folder_directory, max_http_folder_directory,
                mini_ap1_directory, max_ap1_directory, version_number, time_zone, enable_version, 
                valid_with_max_controller, valid_with_mini_controller):

    configurations = Settings(
        cfactory_path = cfactory_path,
        mini_http_folder_directory = mini_http_folder_directory,
        max_http_folder_directory = max_http_folder_directory,
        mini_ap1_directory = mini_ap1_directory,
        max_ap1_directory = max_ap1_directory,
        input_on = getDateAndTime(),
        currently_selected = True,
        version_number = version_number,
        time_zone = time_zone,
        enable_version = enable_version,
        valid_with_max_controller = valid_with_max_controller,
        valid_with_mini_controller = valid_with_mini_controller
    )
    
    # add each fan
    db.session.add(configurations)

    # commit after all fans have been added
    db.session.commit()


def saveVersionSelection(version_id):
    # clear out all previous selections
    settings_object = Settings.query.filter_by(currently_selected=True).all()
    for current_object in settings_object:
        current_object.currently_selected = False
        db.session.add(current_object)
        db.session.commit()

    # set new version to True in database
    new_settings_object = Settings.query.filter_by(id=version_id).first()
    new_settings_object.currently_selected = True
    db.session.add(new_settings_object)
    db.session.commit()
