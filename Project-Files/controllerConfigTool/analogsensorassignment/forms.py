from controllerConfigTool.getInfoFromDatabase import getCarelKeyValues_AnalogFans
from controllerConfigTool.models import ControllerInputOutputs
from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms.widgets import HiddenInput
from wtforms import SubmitField, SelectField, IntegerField
from wtforms.widgets import HiddenInput



def buildAnalogSensorDroppableForm(sensor_count, controller_id_value):

    analog_output_choices = getCarelKeyValues_AnalogFans()

    # default ControllerInputOutputs values if no data exist
    y1_analog_fan_type_default_value = y2_analog_fan_type_default_value = None
    y3_analog_fan_type_default_value = y4_analog_fan_type_default_value = None

    y1_min_fan_speed_percent_default_value = y2_min_fan_speed_percent_default_value = y3_min_fan_speed_percent_default_value = 0
    y4_min_fan_speed_percent_default_value = y1_min_demand_range_percent_default_value = y2_min_demand_range_percent_default_value = 0
    y3_min_demand_range_percent_default_value = y4_min_demand_range_percent_default_value = 0

    y1_max_fan_speed_percent_default_value = y2_max_fan_speed_percent_default_value = y3_max_fan_speed_percent_default_value = 100
    y4_max_fan_speed_percent_default_value = y1_max_demand_range_percent_default_value = y2_max_demand_range_percent_default_value = 100
    y3_max_demand_range_percent_default_value = y4_max_demand_range_percent_default_value = 100


    # find default values if there is existing data for this controller id
    existing_controller_input_output_data = bool(ControllerInputOutputs.query.filter_by(controller_id=controller_id_value).first())
    if existing_controller_input_output_data:
        controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id_value).first()


        if controller_input_output_object.y1_analog_fan_type!= None:
            y1_analog_fan_type_default_value = controller_input_output_object.y1_analog_fan_type
        if controller_input_output_object.y2_analog_fan_type!= None:
            y2_analog_fan_type_default_value = controller_input_output_object.y2_analog_fan_type
        if controller_input_output_object.y3_analog_fan_type!= None:
            y3_analog_fan_type_default_value = controller_input_output_object.y3_analog_fan_type
        if controller_input_output_object.y4_analog_fan_type!= None:
            y4_analog_fan_type_default_value = controller_input_output_object.y4_analog_fan_type
        if controller_input_output_object.y1_min_fan_speed_percent!= None:
            y1_min_fan_speed_percent_default_value = controller_input_output_object.y1_min_fan_speed_percent
        if controller_input_output_object.y2_min_fan_speed_percent!= None:
            y2_min_fan_speed_percent_default_value = controller_input_output_object.y2_min_fan_speed_percent
        if controller_input_output_object.y3_min_fan_speed_percent!= None:
            y3_min_fan_speed_percent_default_value = controller_input_output_object.y3_min_fan_speed_percent
        if controller_input_output_object.y4_min_fan_speed_percent!= None:
            y4_min_fan_speed_percent_default_value = controller_input_output_object.y4_min_fan_speed_percent
        if controller_input_output_object.y1_min_demand_range_percent!= None:
            y1_min_demand_range_percent_default_value = controller_input_output_object.y1_min_demand_range_percent
        if controller_input_output_object.y2_min_demand_range_percent!= None:
            y2_min_demand_range_percent_default_value = controller_input_output_object.y2_min_demand_range_percent
        if controller_input_output_object.y3_min_demand_range_percent!= None:
            y3_min_demand_range_percent_default_value = controller_input_output_object.y3_min_demand_range_percent
        if controller_input_output_object.y4_min_demand_range_percent!= None:
            y4_min_demand_range_percent_default_value = controller_input_output_object.y4_min_demand_range_percent
        if controller_input_output_object.y1_max_fan_speed_percent!= None:
            y1_max_fan_speed_percent_default_value = controller_input_output_object.y1_max_fan_speed_percent
        if controller_input_output_object.y2_max_fan_speed_percent!= None:
            y2_max_fan_speed_percent_default_value = controller_input_output_object.y2_max_fan_speed_percent
        if controller_input_output_object.y3_max_fan_speed_percent!= None:
            y3_max_fan_speed_percent_default_value = controller_input_output_object.y3_max_fan_speed_percent
        if controller_input_output_object.y4_max_fan_speed_percent!= None:
            y4_max_fan_speed_percent_default_value = controller_input_output_object.y4_max_fan_speed_percent
        if controller_input_output_object.y1_max_demand_range_percent!= None:
            y1_max_demand_range_percent_default_value = controller_input_output_object.y1_max_demand_range_percent
        if controller_input_output_object.y2_max_demand_range_percent!= None:
            y2_max_demand_range_percent_default_value = controller_input_output_object.y2_max_demand_range_percent
        if controller_input_output_object.y3_max_demand_range_percent!= None:
            y3_max_demand_range_percent_default_value = controller_input_output_object.y3_max_demand_range_percent
        if controller_input_output_object.y4_max_demand_range_percent!= None:
            y4_max_demand_range_percent_default_value  = controller_input_output_object.y4_max_demand_range_percent




    class ControllerInputOutputAndExpansionBoardForm(FlaskForm):
        submit_button = SubmitField('Save')
        save_to_all_controllers_button = SubmitField('Save to All Controllers')

        controller_id = IntegerField(default=controller_id_value, widget=HiddenInput(), render_kw={'readonly': True})

        y1_analog_fan_type = SelectField('Fan Type', default=y1_analog_fan_type_default_value, choices=analog_output_choices)
        y1_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y1_min_fan_speed_percent_default_value)
        y1_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y1_max_fan_speed_percent_default_value)
        y1_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y1_min_demand_range_percent_default_value)
        y1_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y1_max_demand_range_percent_default_value)

        y2_analog_fan_type = SelectField('Fan Type', default=y2_analog_fan_type_default_value, choices=analog_output_choices)
        y2_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y2_min_fan_speed_percent_default_value)
        y2_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y2_max_fan_speed_percent_default_value)
        y2_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y2_min_demand_range_percent_default_value)
        y2_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y2_max_demand_range_percent_default_value)

        y3_analog_fan_type = SelectField('Fan Type', default=y3_analog_fan_type_default_value, choices=analog_output_choices)
        y3_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y3_min_fan_speed_percent_default_value)
        y3_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y3_max_fan_speed_percent_default_value)
        y3_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y3_min_demand_range_percent_default_value)
        y3_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y3_max_demand_range_percent_default_value)

        y4_analog_fan_type = SelectField('Fan Type', default=y4_analog_fan_type_default_value, choices=analog_output_choices)
        y4_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y4_min_fan_speed_percent_default_value)
        y4_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y4_max_fan_speed_percent_default_value)
        y4_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y4_min_demand_range_percent_default_value)
        y4_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y4_max_demand_range_percent_default_value)

        available_sensor_count = IntegerField(default=sensor_count, widget=HiddenInput() ,render_kw={'readonly': True})




    return ControllerInputOutputAndExpansionBoardForm()



