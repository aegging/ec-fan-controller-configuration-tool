from flask import Blueprint

from flask import render_template, url_for, redirect, request, flash
from controllerConfigTool.models import AnalogSensorDefinitions, AnalogSensors,\
     ControllerInputOutputs, OutputZones, Controller

from controllerConfigTool import db
from controllerConfigTool.getInfoFromDatabase import getControllerID, findTagPrefixGivenSalesOrderID,\
    getSalesOrderValues
from controllerConfigTool.analogsensorassignment.utils import getListControllersWithAnalogSensorAssignment,\
    getListAvailableAnalogSensors
from controllerConfigTool.analogsensorassignment.forms import buildAnalogSensorDroppableForm
from sqlalchemy import null
from copy import deepcopy


analogsensorassignment_blueprint = Blueprint('analogsensorassignment', __name__)



@analogsensorassignment_blueprint.route('/analog_sensor_assignment/<int:sales_order_id>/<int:current_controller>', methods=['GET', 'POST'])
def analog_sensor_assignment(sales_order_id, current_controller):

    # Get controller_id
    controller_id = getControllerID(current_controller, sales_order_id)

    # get tag prefix
    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    # find finished controllers
    list_index_of_finished_controllers = getListControllersWithAnalogSensorAssignment(sales_order_id, tag_prefix)


    # get all values from sales order table for this sales order
    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)

    # get available sensors
    dictionary_available_sensors, list_available_senors_ids = getListAvailableAnalogSensors(sales_order_id, controller_id)

    for sensor_index in dictionary_available_sensors:
        for sensor_address, sensor_definition_name in dictionary_available_sensors[sensor_index]:
            print(f'index: {sensor_index} || address: {sensor_address} || def name: {sensor_definition_name}')

    # return list of True/False depending if U1-U4 have been assigned a sensor id
    list_assigned_sensors = {}

    u1_u4_availability_boolean_list = [True, True, True, True]
    controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
    if controller_input_output_object:
        u1_analog_sensor_id = controller_input_output_object.u1_analog_sensor_id
        u2_analog_sensor_id = controller_input_output_object.u2_analog_sensor_id
        u3_analog_sensor_id = controller_input_output_object.u3_analog_sensor_id
        u4_analog_sensor_id = controller_input_output_object.u4_analog_sensor_id

        if u1_analog_sensor_id:
            u1_u4_availability_boolean_list[0] = u1_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u1_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u1_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

        if u2_analog_sensor_id:
            u1_u4_availability_boolean_list[1] = u2_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u2_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u2_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

        if u3_analog_sensor_id:
            u1_u4_availability_boolean_list[2] = u3_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u3_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u3_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

        if u4_analog_sensor_id:
            u1_u4_availability_boolean_list[3] = u4_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u4_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u4_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

    print(f'list_available_senors_ids: {list_available_senors_ids}')
    controller_input_output_and_expansion_board_form = buildAnalogSensorDroppableForm(len(list_available_senors_ids), controller_id)

    print(list_assigned_sensors)

    # Handle Controller Input Output Table Fields
    controller_input_output_variable_list = [
    'y1_analog_fan_type','y2_analog_fan_type', 'y3_analog_fan_type', 'y4_analog_fan_type',
    'y1_min_fan_speed_percent', 'y2_min_fan_speed_percent', 'y3_min_fan_speed_percent', 'y4_min_fan_speed_percent',
    'y1_max_fan_speed_percent', 'y2_max_fan_speed_percent', 'y3_max_fan_speed_percent', 'y4_max_fan_speed_percent',
    'y1_min_demand_range_percent', 'y2_min_demand_range_percent', 'y3_min_demand_range_percent', 'y4_min_demand_range_percent',
    'y1_max_demand_range_percent', 'y2_max_demand_range_percent', 'y3_max_demand_range_percent', 'y4_max_demand_range_percent']



    if request.method == "POST":
        # if a Controller button on the left side of page is clicked, navigate to that controller
        for i in range(1, sales_order_controller_quantity + 1):
            controller_name = 'controller' + str(i)
            if controller_name in request.form:
                # before redirect, if fans have been selected, consider current controller configuration as complete. Thus save finished modbus range data into the controller table
                # if controller doesn't exist, create it
                controller_exist = bool(ControllerInputOutputs.query.filter_by(controller_id=controller_id).first())
                if controller_exist:
                    return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=i))
                else:
                    # create controller
                    controller_object = ControllerInputOutputs(
                        controller_id = controller_id,
                        sales_order_id = sales_order_id
                    )
                    db.session.add(controller_object)
                    db.session.commit()
                    return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=i))
                
        if "clear_button_assignments" in request.form:
            # clear all existing assignments
            controller_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()

            if controller_object:
                controller_object.u1_analog_sensor_id = null()
                controller_object.u2_analog_sensor_id = null()
                controller_object.u3_analog_sensor_id = null()
                controller_object.u4_analog_sensor_id = null()

                db.session.add(controller_object)

            analog_sensor_objects = AnalogSensors.query.filter_by(controller_id=controller_id).all()
        
            if analog_sensor_objects:
                for analog_sensor_object in analog_sensor_objects:
                    analog_sensor_object.controller_id = null()

                    db.session.add(analog_sensor_object)

            db.session.commit()

            return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=current_controller))

        elif "navigate_sales_order_status" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif "submit_button" in request.form:
            if controller_input_output_and_expansion_board_form.validate_on_submit():
                print(f'inside from submit')
                controller_input_output_and_expansion_board_form_dictionary = controller_input_output_and_expansion_board_form.data
                controller_input_output_and_expansion_board_form_dictionary.pop('submit_button')
                controller_input_output_and_expansion_board_form_dictionary.pop('csrf_token')
                controller_input_output_and_expansion_board_form_dictionary.pop('available_sensor_count')
                controller_input_output_and_expansion_board_form_dictionary.pop('save_to_all_controllers_button')
            
                # make a dictionary for ControllerInputOutput and ExpansionBoardInputOutput
                # seperate the data such that it can be pushed to 2 different tables
                controller_input_output_dictionary = deepcopy(controller_input_output_and_expansion_board_form_dictionary)

                # give empty variables sqlalchemy valid null value
                for variable_name in controller_input_output_variable_list:
                    if controller_input_output_dictionary[variable_name] == 'None':
                        controller_input_output_dictionary[variable_name] = null()


                # place sales order id inside dictionary
                controller_input_output_dictionary['sales_order_id'] = sales_order_id
                controller_input_output_dictionary['analog_outputs_saved'] = True
                # controller_input_output_dictionary['Y1'] = 1
                # controller_input_output_dictionary['Y2'] = 1
                # controller_input_output_dictionary['Y3'] = 1
                # controller_input_output_dictionary['Y4'] = 1

                output_objects = OutputZones.query.filter_by(controller_id=controller_id).all()
                for output_object in output_objects:
                    output_object.y1_zone = 1
                    output_object.y2_zone = 1
                    output_object.y3_zone = 1
                    output_object.y4_zone = 1
                    db.session.add(output_object)
                controller_inout = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
                if controller_inout and output_objects:
                    for output_object in output_objects:
                        output_object.controller_inout_id = controller_inout.id
                        db.session.add(output_object)

                db.session.commit()


                print(controller_input_output_dictionary, 'I/O')

                # Save Controller Input Output Data
                # is there an existing Controller Input Output data object?
                existing_controller_input_output_object = bool(ControllerInputOutputs.query.filter_by(controller_id=controller_id).first())
                if existing_controller_input_output_object:
                    # existing one found, use this object
                    # controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
                    db.session.query(ControllerInputOutputs).filter(ControllerInputOutputs.controller_id==controller_id).update(controller_input_output_dictionary)
                    db.session.commit()
                else:
                    # create one
                    controller_input_output_object = ControllerInputOutputs(**controller_input_output_dictionary)
                    db.session.add(controller_input_output_object)
                    db.session.commit()

                flash('Changes have been saved', 'success')
                return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=current_controller))

        elif "save_to_all_controllers_button" in request.form:
            if controller_input_output_and_expansion_board_form.validate_on_submit():
                controller_input_output_and_expansion_board_form_dictionary = controller_input_output_and_expansion_board_form.data
                controller_input_output_and_expansion_board_form_dictionary.pop('submit_button')
                controller_input_output_and_expansion_board_form_dictionary.pop('csrf_token')
                controller_input_output_and_expansion_board_form_dictionary.pop('available_sensor_count')
                controller_input_output_and_expansion_board_form_dictionary.pop('save_to_all_controllers_button')
                controller_input_output_and_expansion_board_form_dictionary.pop('controller_id')
            
                # make a dictionary for ControllerInputOutput and ExpansionBoardInputOutput
                # seperate the data such that it can be pushed to 2 different tables
                controller_input_output_dictionary = deepcopy(controller_input_output_and_expansion_board_form_dictionary)

                print(controller_input_output_dictionary)

                # give empty variables sqlalchemy valid null value
                for variable_name in controller_input_output_variable_list:
                    if controller_input_output_dictionary[variable_name] == 'None':
                        controller_input_output_dictionary[variable_name] = null()


                # place sales order id inside dictionary
                controller_input_output_dictionary['sales_order_id'] = sales_order_id
                controller_input_output_dictionary['analog_outputs_saved'] = True

                all_controllers= Controller.query.filter_by(sales_order_id=sales_order_id).all()
                for each_Controller in all_controllers:
                    output_object = db.session.query(OutputZones).filter(OutputZones.controller_id==each_Controller.id).filter(OutputZones.controller_inout_id==None).first()
                    if output_object:
                        controller_inout = ControllerInputOutputs.query.filter_by(controller_id=each_Controller.id).first()
                        output_object.controller_inout_id = controller_inout.id
                        output_object.y1_zone = 1
                        output_object.y2_zone = 1
                        output_object.y3_zone = 1
                        output_object.y4_zone = 1
                        db.session.add(output_object)
                        db.session.commit()

                

                db.session.query(ControllerInputOutputs).filter(ControllerInputOutputs.sales_order_id==sales_order_id).update(controller_input_output_dictionary)
                db.session.commit()

                flash('Changes have been saved', 'success')
                return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=current_controller))



    return render_template('droppable-template.html', controller_input_output_and_expansion_board_form=controller_input_output_and_expansion_board_form, 
            dictionary_available_sensors=dictionary_available_sensors, u1_u4_availability_boolean_list=u1_u4_availability_boolean_list,
            list_assigned_sensors=list_assigned_sensors, list_available_senors_ids=list_available_senors_ids,
            sales_order_id=sales_order_id, current_controller=current_controller,
            sales_order_controller_quantity=sales_order_controller_quantity,
            list_index_of_finished_controllers=list_index_of_finished_controllers, tag_prefix=tag_prefix,
            controller_input_output_variable_list=controller_input_output_variable_list)



@analogsensorassignment_blueprint.route("/redirectanalogsensorassignment/<int:sales_order_id>/<int:current_controller>/<controllerPort>/<int:sensor_id>", methods=['GET', 'POST'])
def redirectanalogsensorassignment(sales_order_id, current_controller, controllerPort, sensor_id):

    # Get data to show fans selected for currently selected controller
    controller_id = getControllerID(current_controller, sales_order_id)
    print(f'controller id: {controller_id}')
    controller_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
    if controller_object:
        if controllerPort == 'u1_analog_sensor_id':
            controller_object.u1_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.controller_id = controller_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()

        elif controllerPort == 'u2_analog_sensor_id':
            controller_object.u2_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.controller_id = controller_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()

        elif controllerPort == 'u3_analog_sensor_id':
            controller_object.u3_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.controller_id = controller_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()

        elif controllerPort == 'u4_analog_sensor_id':
            controller_object.u4_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.controller_id = controller_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()
        
        db.session.add(controller_object)
        db.session.commit()
    else:
        controller_object = ControllerInputOutputs(controller_id=controller_id, sales_order_id=sales_order_id)
        db.session.add(controller_object)
        db.session.commit()
        return redirect(url_for('analogsensorassignment.redirectanalogsensorassignment', sales_order_id=sales_order_id, current_controller=current_controller,
                controllerPort=controllerPort, sensor_id=sensor_id))

    return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=current_controller))



