from controllerConfigTool.models import Controller, AnalogSensorDefinitions, AnalogSensors, ControllerInputOutputs
from controllerConfigTool import db


def getListControllersWithAnalogSensorAssignment(sales_order_id, tag_prefix):
    """ return a list of controllers with any items assignment to them """

    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)

    controller_id_fans_found_list = []

    # for each controller id, search FieldbusSensors table for a match, if there is a match add to list as finished
    for this_controller_id in controller_id_list:
        found_analog_sensor_controller_id_boolean = bool(AnalogSensors.query.filter_by(controller_id=this_controller_id).first())

        controller_input_output_boolean = False
        controller_inputoutput_object = ControllerInputOutputs.query.filter_by(controller_id=this_controller_id).first()
        if controller_inputoutput_object:
            controller_input_output_boolean = controller_inputoutput_object.analog_outputs_saved

        if found_analog_sensor_controller_id_boolean or controller_input_output_boolean:
            controller_id_fans_found_list.append(this_controller_id)


    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)

    return controller_number_list



def getListAvailableAnalogSensors(sales_order_id, controller_id):
    # find all analog sensors available (null controller id and expansion board id)
    available_sensors_boolean = bool(db.session.query(AnalogSensors).filter(AnalogSensors.controller_id==None).\
                                        filter(AnalogSensors.expansion_board_id==None).\
                                            filter(AnalogSensors.fan_id==None).\
                                                filter(AnalogSensors.sales_order_id==sales_order_id).\
                                                    filter(AnalogSensors.analog_sensor_definitions_id!=None).\
                                                        first())

    analog_sensor_id_dictionary = {}
    analog_sensor_id_list = []
    # analog_sensor_id_list.setdefault(None, list()).append([None, None])
    
    # add analog sensors to list that are currently selected, on current controller, or else we can't display they are selected (default values) in the dropdown
    analog_sensors_need_added_list = []

    # if any exist
    if available_sensors_boolean:
        available_analog_sensors_objects = db.session.query(AnalogSensors).filter(AnalogSensors.controller_id==None).\
                                        filter(AnalogSensors.expansion_board_id==None).\
                                            filter(AnalogSensors.fan_id==None).\
                                                filter(AnalogSensors.sales_order_id==sales_order_id).\
                                                    filter(AnalogSensors.analog_sensor_definitions_id!=None).\
                                                        all()

        # for each available sensors, add to list
        for analog_sensor_object in available_analog_sensors_objects:
            # find the address and definition name
            current_sensor_address = analog_sensor_object.modbus_address
            
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name

            current_sensor_definition_standard_boolean = analog_sensor_definition_object.standard

            if current_sensor_definition_standard_boolean:
                analog_sensor_id_dictionary.setdefault(analog_sensor_object.id, list()).append([int(current_sensor_address), current_sensor_definition_name])
                analog_sensor_id_list.append(analog_sensor_object.id)
            else:
                analog_sensor_id_dictionary.setdefault(analog_sensor_object.id, list()).append([int(current_sensor_address), f'**{current_sensor_definition_name}'])
                analog_sensor_id_list.append(analog_sensor_object.id)



    
        
    # controller_input_output_exist_boolean = bool(ControllerInputOutputs.query.filter_by(controller_id=controller_id).first())
    # if controller_input_output_exist_boolean:
    #     controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()

    #     analog_sensors_need_added_list.append(controller_input_output_object.u1_analog_sensor_id)
    #     analog_sensors_need_added_list.append(controller_input_output_object.u2_analog_sensor_id)
    #     analog_sensors_need_added_list.append(controller_input_output_object.u3_analog_sensor_id)
    #     analog_sensors_need_added_list.append(controller_input_output_object.u4_analog_sensor_id)

        # for sensor_id in analog_sensors_need_added_list:
        #     if sensor_id:
        #         analog_sensor_object = AnalogSensors.query.filter_by(id=sensor_id).first()
        #         current_sensor_address = analog_sensor_object.modbus_address
        #         analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
        #         current_sensor_definition_name = analog_sensor_definition_object.definition_name

        #         analog_sensor_id_dictionary.append([sensor_id, f'MB Address: {current_sensor_address}; {current_sensor_definition_name}'])


    # find expansion board ids
    # expansion_boards_exist_boolean = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).first())

    # if expansion_boards_exist_boolean:
    #     expansion_board_input_output_objects = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).all()

    #     for expansion_board_input_output_object in expansion_board_input_output_objects:
    #         analog_sensors_need_added_list.append(expansion_board_input_output_object.u1_analog_sensor_id)
    #         analog_sensors_need_added_list.append(expansion_board_input_output_object.u2_analog_sensor_id)
    #         analog_sensors_need_added_list.append(expansion_board_input_output_object.u3_analog_sensor_id)
    #         analog_sensors_need_added_list.append(expansion_board_input_output_object.u4_analog_sensor_id)

    
    # add sensors to list
    # if analog_sensors_need_added_list:
    #     for sensor_id in analog_sensors_need_added_list:
    #         if sensor_id:
    #             analog_sensor_object = AnalogSensors.query.filter_by(id=sensor_id).first()
    #             current_sensor_address = analog_sensor_object.modbus_address
    #             analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
    #             current_sensor_definition_name = analog_sensor_definition_object.definition_name

    #             analog_sensor_id_dictionary.setdefault(sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])



    return analog_sensor_id_dictionary, analog_sensor_id_list