import os
from controllerConfigTool import app
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, TextField



def findDirectories(root_directory):
    allowed_folders = app.config['ARCHIVE_ROOT_DIRECTORY_ALLOWED_FOLDERS']
    archive_root_directory=app.config['ARCHIVE_ROOT_DIRECTORY']

    # if root directory equals archive root directory, then limit results to allowed folder names
    if archive_root_directory.lower() == root_directory.lower():
        mask_folders_boolean = True
    else:
        mask_folders_boolean = False

    folders = []

    for entry in os.scandir(root_directory):
        if entry.is_dir():

            # only show folders declared in config file
            if mask_folders_boolean:
                for allowed_folder in allowed_folders:
                    if allowed_folder.lower() == entry.name.lower():
                        folders.append(entry.path)
                        folders.append(entry.name)
            else:
                folders.append(entry.path)
                folders.append(entry.name)


    folders_list_tuple = [x for x in zip(*[iter(folders)]*2)]

    # print(f'folders list tuple: {folders_list_tuple}')
    return folders_list_tuple



def initChooseFilePathForm(root_directory):

    folders = findDirectories(root_directory)
    folders.insert(0, ["",""])


    class ChooseFilePathForm(FlaskForm):

        file_path = SelectField('Choose Archive Directory', default=folders, choices=[(path, name) for path, name in folders])
        current_path = TextField('Current Path Selected', default=root_directory, render_kw={'readonly': True})
        submit_button = SubmitField('Save')

    return ChooseFilePathForm


class ClearFilePathForm(FlaskForm):
    clear_path = SubmitField('Clear')


