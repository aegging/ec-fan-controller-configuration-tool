from flask import Blueprint

from flask import render_template, url_for, flash, redirect, request
from controllerConfigTool import db, app
from controllerConfigTool.choosefilepath.form import ClearFilePathForm, initChooseFilePathForm
from controllerConfigTool.models import SalesOrders


choosefilepath_blueprint = Blueprint('choosefilepath', __name__)


# accept data from javascript and save current archive directory to database and reroute back to choosefilepath route
@choosefilepath_blueprint.route('/redirectchoosefilepath_/<int:sales_order_id>/<int:current_controller>/<tag_prefix>/<path:root_directory>', methods=['GET','POST'])
def redirectchoosefilepath_(sales_order_id,current_controller,tag_prefix, root_directory):

    # save archive directory system path to Sales Order Table
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    sales_order_object.system_path = root_directory

    db.session.add(sales_order_object)
    db.session.commit()

    return redirect(url_for('choosefilepath.choosefilepath_', sales_order_id=sales_order_id, 
                            tag_prefix=tag_prefix, current_controller=current_controller))


@choosefilepath_blueprint.route('/choosefilepath_/<int:sales_order_id>/<int:current_controller>/<tag_prefix>', methods=['GET','POST'])
def choosefilepath_(sales_order_id,current_controller,tag_prefix):
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    root_directory = sales_order_object.system_path

    ChooseFilePathForm = initChooseFilePathForm(root_directory)
    choose_file_path_form = ChooseFilePathForm()

    clear_file_path_form = ClearFilePathForm()
 

    # Display dropdown for user to select a folder
    # upon selection, redirect to this same route with the new root selected by user
    if request.method == 'POST':
        if "clear_path" in request.form:
            return redirect(url_for('choosefilepath.redirectchoosefilepath_', sales_order_id=sales_order_id, 
                            tag_prefix=tag_prefix, current_controller=current_controller, root_directory=app.config['ARCHIVE_ROOT_DIRECTORY']))

        if choose_file_path_form.validate_on_submit():
            current_path = choose_file_path_form.current_path.data

            # save filepath to sales order table
            sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
            sales_order_object.system_path = current_path

            db.session.add(sales_order_object)
            db.session.commit()

            flash(f'Archive Directory Saved','success')
            # redirect to controllers route 
            # Orignal
            # return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id, 
            #                         tag_prefix=tag_prefix, current_controller=current_controller 
            #                         ))

            # CG Change 31/03/2023 page redirect to salesorderstatus page start from choose filepath
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
             # end 
            
        



    return render_template('choosefilepath.html', title='Choose Directory to Archive Files', choose_file_path_form=choose_file_path_form,
            sales_order_id=sales_order_id, current_controller=current_controller, tag_prefix=tag_prefix, clear_file_path_form=clear_file_path_form)


# calling from salesorderstatus
@choosefilepath_blueprint.route('/choosearchivedirectory/<int:sales_order_id>', methods=['GET','POST'])
def choosearchivedirectory(sales_order_id):
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    root_directory = sales_order_object.system_path

    ChooseFilePathForm = initChooseFilePathForm(root_directory)
    choose_file_path_form = ChooseFilePathForm()

    clear_file_path_form = ClearFilePathForm()
 

    # Display dropdown for user to select a folder
    # upon selection, redirect to this same route with the new root selected by user
    if request.method == 'POST':
        if "clear_path" in request.form:
            return redirect(url_for('choosefilepath.redirectchoosefilepath', sales_order_id=sales_order_id, 
                            root_directory=app.config['ARCHIVE_ROOT_DIRECTORY']))

        if choose_file_path_form.validate_on_submit():
            current_path = choose_file_path_form.current_path.data

            # save filepath to sales order table
            sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
            sales_order_object.system_path = current_path

            db.session.add(sales_order_object)
            db.session.commit()

            flash(f'Archive Directory Saved','success')
            # redirect to controllers route 
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))



    return render_template('choosefilepath2.html', title='Choose Directory to Archive Files', choose_file_path_form=choose_file_path_form,
            sales_order_id=sales_order_id, clear_file_path_form=clear_file_path_form)


# accept data from javascript and save current archive directory to database and reroute back to choosefilepath route
@choosefilepath_blueprint.route('/redirectchoosefilepath/<int:sales_order_id>/<path:root_directory>', methods=['GET','POST'])
def redirectchoosefilepath(sales_order_id, root_directory):

    # save archive directory system path to Sales Order Table
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    sales_order_object.system_path = root_directory

    db.session.add(sales_order_object)
    db.session.commit()

    return redirect(url_for('choosefilepath.choosearchivedirectory', sales_order_id=sales_order_id))

