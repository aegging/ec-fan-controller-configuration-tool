import configparser

JETVENT_INI_PATH = 'C:\Program Files\Fantech\JetVent\JetVent.ini'

class JetventIni:
    def __init__(self):
        self.config_file = str(JETVENT_INI_PATH)
        self.config = configparser.ConfigParser()
        self.config.read(self.config_file)

    def convertIntList(self, severuser):
        temp_map = map(int, severuser)
        int_list = list(temp_map)
        return int_list

    def decode(self, testvar):
        password = ''
        key = [65, 108, 108, 101, 110, 32, 65, 115, 116, 102, 97, 108, 99, 107]

        for i in range(0, len(testvar)):
            password = password + chr(testvar[i] + key[i])

        return password


    def getDBPass(self):
        serveruser = self.config['DEFAULT']['serveruser'].split(',')
        int_list = self.convertIntList(serveruser)
        db_pass = self.decode(int_list)
        return db_pass


    def getDBIP(self):
        ip = self.config['DEFAULT']['server'].split(',')
        db_ip = self.convertIntList(ip)
        return db_ip


    def convertListToString(self, list):
        final_string = ''
        for item in list:
            final_string = final_string + str(item) + '.'
        final_string = final_string[:len(final_string)-1]
        return final_string

    def start(self):
        db_pass = self.getDBPass()
        db_ip = self.getDBIP()
        db_ip = self.convertListToString(db_ip)

        return db_pass, db_ip

def getConfigFile():
    c = JetventIni()
    db_pass, db_ip = c.start()
    return db_pass, db_ip