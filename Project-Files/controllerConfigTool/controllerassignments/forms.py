from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField
from wtforms.validators import DataRequired
from wtforms import StringField,  SubmitField, IntegerField
from wtforms.validators import DataRequired, ValidationError
from wtforms.widgets import HiddenInput
from wtforms.widgets.core import SubmitInput
from controllerConfigTool.dataCalculations import getCountOfModbusRange



class FBSensorSelectionForm(FlaskForm):

    fbsensor_quantity = IntegerField('Fieldbus Sensor Quantity', render_kw={'readonly': True}, validators=[DataRequired()])
    fbsensor_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
    fbsensor_address_ranges = StringField('Fieldbus Sensor Addresses', render_kw={'readonly': True}, validators=[DataRequired()])
    fbsensor_selection_form_submit = SubmitField('Add Sensors')

    def validate_fbsensor_index_ranges(self, fbsensor_index_ranges):
        if fbsensor_index_ranges.data:
            modbus_range_count = getCountOfModbusRange(fbsensor_index_ranges.data)
            if modbus_range_count < 0 or modbus_range_count > 50:
                raise ValidationError(f'Invalid number of FBSensors selected ({modbus_range_count}). Max number of Sensors allowed per controller is 50.')  

    # def validate_fan_type(self, fan_type):
    #     if fan_type.data == "Blank" or fan_type.data == "":
    #         raise ValidationError(f'Sensor type cannot be blank')
            


class FanSelectionForm(FlaskForm):

    fan_quantity = IntegerField('Fan Quantity', render_kw={'readonly': True}, validators=[DataRequired()])
    fan_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
    fan_address_ranges = StringField('Fan Addresses', render_kw={'readonly': True}, validators=[DataRequired()])
    fan_selection_form_submit = SubmitField('Add Fans')
    fan_add_and_next_button = SubmitField('Add & Next')

    def validate_fan_index_ranges(self, fan_index_ranges):
        if fan_index_ranges.data:
            modbus_range_count = getCountOfModbusRange(fan_index_ranges.data)
            if modbus_range_count < 0 or modbus_range_count > 50:
                raise ValidationError(f'Invalid number of Fans selected ({modbus_range_count}). Max number of Fans allowed per controller is 50.')  

    # def validate_fan_type(self, fan_type):
    #     if fan_type.data == "Blank" or fan_type.data == "":
    #         raise ValidationError(f'Sensor type cannot be blank')
            


class ExpansionBoardSelectionForm(FlaskForm):

    expansionboard_quantity = IntegerField('Quantity', render_kw={'readonly': True}, validators=[DataRequired()])
    expansionboard_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
    expansionboard_address_ranges = StringField('Indexes', render_kw={'readonly': True}, validators=[DataRequired()])
    expansionboard_selection_form_submit = SubmitField('Add Expansion Boards')

    def validate_expansionboard_index_ranges(self, expansionboard_index_ranges):
        if expansionboard_index_ranges.data:
            modbus_range_count = getCountOfModbusRange(expansionboard_index_ranges.data)
            if modbus_range_count < 0 or modbus_range_count > 50:
                raise ValidationError(f'Invalid number of Expansion Boards selected ({modbus_range_count}). Max number of Expansion Boards allowed per controller is 50.')  

    # def validate_fan_type(self, fan_type):
    #     if fan_type.data == "Blank" or fan_type.data == "":
    #         raise ValidationError(f'Sensor type cannot be blank')
            
