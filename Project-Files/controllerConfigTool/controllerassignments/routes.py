import re
from flask import Blueprint
from controllerConfigTool.controllerassignments.forms import ExpansionBoardSelectionForm, FBSensorSelectionForm, FanSelectionForm
from controllerConfigTool.controllerassignments.utils import edit_expansion_boards, edit_fan_in_jobs_table, edit_fieldbus_sensors, findAvailableExpansionBoards, findFBSensorsWithFBSensorDefinition, findFansWithFanDefinition, getAssignedExpansionBoardsList, getFBSensorAddressDefininitionDictionary, getListControllersWithAssignment, removeControllerIDFromExpansionBoards, removeControllerIDFromFBSensors, removeControllerIDFromFans
from controllerConfigTool.models import Controller, ExpansionBoardInputOutput, OutputZones
from flask import render_template, url_for, flash, redirect, request
from controllerConfigTool.getInfoFromDatabase import findFirstNonConfiguredController, findTagPrefixGivenSalesOrderID, getControllerID, getControllerValues, getDateAndTime,\
    getSalesOrderValues, getTotalControllers
from controllerConfigTool import db


controllerassignments_blueprint = Blueprint('controllerassignments', __name__)


# Fieldbus Sensor Assignment
@controllerassignments_blueprint.route("/controller_assignments/<int:sales_order_id>/<int:current_controller>", methods=['GET', 'POST'])
def controller_assignments(sales_order_id,current_controller):

    # get tag prefix
    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    # get archived directory to display to user
    # archived_directory = getArchiveDirectory(sales_order_id)

    # find finished controllers
    list_index_of_finished_controllers = getListControllersWithAssignment(sales_order_id, tag_prefix)
    # CG
    # print("------------------list_index_of_finished_controllers----------------",list_index_of_finished_controllers)


    # get controller object for current controller
    current_controller_object = Controller.query.filter_by(sales_order_id=sales_order_id).order_by(Controller.id).first()
    
    # get all values from sales order table for this sales order
    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)

    # get total controllers for sales order so that we know how many controller buttons to create
    total_countroller_buttons = getTotalControllers(sales_order_id)

    # Get data to show fans selected for currently selected controller
    controller_id = getControllerID(current_controller, sales_order_id)

    # create array of available sensors with this sales order e.g.-> [[address,id],[address,id]]
    # id is necessary for sensors with overlapping fan ranges 
    available_fb_sensors = findFBSensorsWithFBSensorDefinition(sales_order_id)

    existing_saved_expansion_boards = getAssignedExpansionBoardsList(controller_id)
    
    address_sensortype_dictionary = getFBSensorAddressDefininitionDictionary(controller_id)

    fbsensor_selection_form = FBSensorSelectionForm()
    fan_selection_form = FanSelectionForm()
    expansionboard_selection_form = ExpansionBoardSelectionForm()

    # create array of available fans with this sales order e.g.-> [[address,index],[address,index]]
    # index is necessary for jobs with overlapping fan ranges 
    available_fans = findFansWithFanDefinition(sales_order_id)

    controller_type, controller_serial_number, address_fantype_dictionary, grouped_list_fan_types, version_number_id = getControllerValues(controller_id)


    # find available expansion boards (expansion boards with no controller_id assignment)
    available_expansion_board_list = findAvailableExpansionBoards(sales_order_id)

    # get values from jobs (fan) table for each fan_type that is being used w/ a given address
    # allow user to edit these fields, and then press a button to confirm values are good
    # create data structure involving address_fantype_dictionary
    # where it has something like [address, fantype, {number_of_sensors, sensor_1_type, sensor_1_unit, etc, etc.}]

    # create button to allow changing of settings after selecting a given fan type
    
    if request.method == 'POST':
        # if a Controller button on the left side of page is clicked, navigate to that controller
        for i in range(1, total_countroller_buttons + 1):
            controller_name = 'controller' + str(i)
            if controller_name in request.form:
                # before redirect, if fans have been selected, consider current controller configuration as complete. Thus save finished modbus range data into the controller table
                return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=i))
                
        # clear fieldbus sensors
        if "clear_all_fieldbus_sensors" in request.form:

            # remove all controller assignments from fans created and then redirect back to this page
            removeControllerIDFromFBSensors(sales_order_id, controller_id)
            return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))
        # clear fieldbus sensors
        elif "clear_all_fans" in request.form:

            # remove all controller assignments from fans created and then redirect back to this page
            removeControllerIDFromFans(sales_order_id, controller_id)
            return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))
        elif "clear_all_expansion_boards" in request.form:
            removeControllerIDFromExpansionBoards(sales_order_id, controller_id)
            return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))
            
        elif "navigate_sales_order_status_button" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        # create view for viewing fbsensor definitions
        elif "view_fan_definition_details" in request.form:
            return redirect(url_for('viewfandefinition', sales_order_id=sales_order_id))  

        # elif "navigate_archived_directory" in request.form:
        #     return redirect(url_for('choosefilepath.redirectchoosefilepath', sales_order_id=sales_order_id, 
        #                         tag_prefix=tag_prefix, current_controller=current_controller, root_directory=archived_directory))
                                
        elif "navigate_sales_order_status" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        # when add sensors button is pressed, if form data is valid
        elif "fbsensor_selection_form_submit" in request.form:
            if fbsensor_selection_form.validate_on_submit():
                # save controller data from form
                current_controller_object = Controller.query.filter_by(id=controller_id).first()
                current_controller_object.controller_type=None
                current_controller_object.controller_serial_number=None
                current_controller_object.programmed = False
                current_controller_object.input_by = entered_by
                current_controller_object.input_on = getDateAndTime()

                # save data from controller form to database
                db.session.add(current_controller_object)
                db.session.commit()

                # Flash message to show controller has been successfully saved into database
                flash(f'Controller {current_controller} data successfully saved into database', 'success')

                print(f'---------------{fbsensor_selection_form.fbsensor_index_ranges.data}')
                # when controller is configured, add associated controller id and fan type to fan rows in jobs table
                edit_fieldbus_sensors(sales_order_id, current_controller_object.id, fbsensor_selection_form.fbsensor_index_ranges.data)

                return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))


        # when add fans button is pressed, if form data is valid
        elif "fan_selection_form_submit" in request.form:
            if fan_selection_form.validate_on_submit():

                # save controller data from form
                current_controller_object = Controller.query.filter_by(id=controller_id).first()
                current_controller_object.controller_type=None
                current_controller_object.controller_serial_number=None
                current_controller_object.programmed = False
                current_controller_object.input_by = entered_by
                current_controller_object.input_on = getDateAndTime()

                # save data from controller form to database
                db.session.add(current_controller_object)
                db.session.commit()

                # Flash message to show controller has been successfully saved into database
                flash(f'Controller {current_controller} data successfully saved into database', 'success')

                print(f'---------------{fan_selection_form.fan_index_ranges.data}')
                # when controller is configured, add associated controller id and fan type to fan rows in jobs table
                edit_fan_in_jobs_table(sales_order_id, current_controller_object.id, fan_selection_form.fan_index_ranges.data)

                return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))

        elif "fan_add_and_next_button" in request.form:
            if fan_selection_form.validate_on_submit():
            # save controller data from form
                current_controller_object = Controller.query.filter_by(id=controller_id).first()
                current_controller_object.controller_type=None
                current_controller_object.controller_serial_number=None
                current_controller_object.programmed = False
                current_controller_object.input_by = entered_by
                current_controller_object.input_on = getDateAndTime()

                # save data from controller form to database
                db.session.add(current_controller_object)
                db.session.commit()

                # Flash message to show controller has been successfully saved into database
                flash(f'Controller {current_controller} data successfully saved into database', 'success')

                print(f'---------------{fan_selection_form.fan_index_ranges.data}')
                # when controller is configured, add associated controller id and fan type to fan rows in jobs table
                edit_fan_in_jobs_table(sales_order_id, current_controller_object.id, fan_selection_form.fan_index_ranges.data)

                # find next controller to configure
                next_controller_index = findFirstNonConfiguredController(sales_order_id, tag_prefix)

                return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=next_controller_index))

        # when add fans button is pressed, if form data is valid
        elif "expansionboard_selection_form_submit" in request.form:
            if expansionboard_selection_form.validate_on_submit():

                # Don't allow more than 5 expansion_boards to be submitted
                existing_expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()

                new_expansion_board_count = existing_expansion_board_count + expansionboard_selection_form.expansionboard_quantity.data
                if new_expansion_board_count > 5:
                    flash(f'Invalid Expansion Board selection. Maximum allowed per Controller is 5.', 'danger')
                    return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))

                # save controller data from form
                current_controller_object = Controller.query.filter_by(id=controller_id).first()
                current_controller_object.controller_type=None
                current_controller_object.controller_serial_number=None
                current_controller_object.programmed = False
                current_controller_object.input_by = entered_by
                current_controller_object.input_on = getDateAndTime()

                # save data from controller form to database
                db.session.add(current_controller_object)



                db.session.commit()

                # Flash message to show controller has been successfully saved into database
                flash(f'Controller {current_controller} data successfully saved into database', 'success')
                # when controller is configured, add associated controller id and fan type to fan rows in jobs table
                edit_expansion_boards(sales_order_id, current_controller_object.id, 
                                        expansionboard_selection_form.expansionboard_index_ranges.data)
                
                output_objects = OutputZones.query.filter_by(controller_id=controller_id).all()
                if output_objects:
                    for output_object in output_objects:
                        output_object.u8_zone = 1
                        output_object.u9_zone = 1
                        output_object.u10_zone = 1
                        db.session.add(output_object)
                    db.session.commit()

                return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=current_controller))



        # else:

        #     # get list of all sensor type names
        #     fbsensor_definition_objects = FieldbusSensorDefinitions.query.filter_by(sales_order_id=sales_order_id).all()
        #     fbsensor_definitions_name_list = []
        #     for fbsensor_definition_object in fbsensor_definition_objects:
        #         fbsensor_definitions_name_list.append(fbsensor_definition_object.definition_name)

        #     for i in range(0, len(fbsensor_definitions_name_list)):
        #         # create button name
        #         definition_name = fbsensor_definitions_name_list[i]
        #         clear_button_name = "clear_button_" + str(definition_name)
                
        #         # if clear button was pressed, remove controller assocation from fieldbus sensors with this definition name
        #         if clear_button_name in request.form:
        #             # find definition id using definition name
        #             fbsensor_definition_object = FieldbusSensorDefinitions.query.filter_by(definition_name=definition_name).first()
        #             fbsensor_definition_id = fbsensor_definition_object.id

        #             # find all fieldbus sensors with this sales_order_id, controller_id, and fieldbus_sensor_definitions_id
        #             fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.fieldbus_sensor_definitions_id==fbsensor_definition_id).\
        #                 filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==controller_id).all()

        #             # loop through each fb sensor
        #             for fbsensor_object in fbsensor_objects:
        #                 fbsensor_object.controller_id = None

        #                 db.session.add(fbsensor_object)
                        
        #             # commit to database
        #             db.session.commit()
        #             return redirect(url_for('fb_sensor_assignment', sales_order_id=sales_order_id, current_controller=current_controller))

    return render_template(
                        'controller_assignments.html', title='Fieldbus Sensor Assignment', 
                        fbsensor_selection_form=fbsensor_selection_form,sales_order_number=current_sales_order_number, 
                        address_sensortype_dictionary=address_sensortype_dictionary,
                        sales_order_controller_quantity=sales_order_controller_quantity, 
                        current_controller=current_controller, fan_selection_form = fan_selection_form,
                        available_fb_sensors=available_fb_sensors, tag_prefix=tag_prefix,
                        list_index_of_finished_controllers=list_index_of_finished_controllers,
                        # archived_directory=archived_directory, 
                        available_fans=available_fans,
                        address_fantype_dictionary=address_fantype_dictionary,
                        available_expansion_board_list=available_expansion_board_list,
                        expansionboard_selection_form=expansionboard_selection_form,
                        existing_saved_expansion_boards=existing_saved_expansion_boards
                        )




