from controllerConfigTool import db
from controllerConfigTool.dataCalculations import decodeModbusRange
from controllerConfigTool.getInfoFromDatabase import getDateAndTime
from controllerConfigTool.models import Controller, ControllerInputOutputs, FanDefinitions, FieldbusSensors, Job, ExpansionBoardInputOutput,\
    FieldbusSensorDefinitions, OutputZones



def findFansWithFanDefinition(sales_order_id):
    """ find all fans that have been assigned a fan definition """
    all_fans_object = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).\
            filter(Job.controller_id==None).order_by(Job.Index).all()
    list_address_of_selected_fans = []
    fan_definitions_name = "None"
    for fan in all_fans_object:
        current_fan_address = int(fan.Address)
        fan_index = fan.Index

        # get fan defintion name
        fan_definition_id = fan.fan_definitions_id
        fan_definitions_object_boolean = bool(FanDefinitions.query.filter_by(id=fan_definition_id).first())
        if fan_definitions_object_boolean:
            fan_definitions_object = FanDefinitions.query.filter_by(id=fan_definition_id).first()
            fan_definitions_name = fan_definitions_object.name

        list_address_of_selected_fans.append([current_fan_address, fan_definitions_name, fan_index])

    result = {}

    for fan_address, definition_name, fan_index in list_address_of_selected_fans:
        result.setdefault(definition_name, list()).append([int(fan_address), int(fan_index)])

    return result



def findAvailableExpansionBoards(sales_order_id):
    expansion_board_objects = db.session.query(ExpansionBoardInputOutput).\
                            filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).\
                                filter(ExpansionBoardInputOutput.controller_id==None).all()
    expansion_board_list = []

    for index, expansion_board_object in enumerate(expansion_board_objects):
        expansion_board_list.append([expansion_board_object.id, index+1])
    
    return expansion_board_list



def getListControllersWithAssignment(sales_order_id, tag_prefix):
    """ return a list of controllers with any items assignment to them """

    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)

    controller_id_fans_found_list = []

    # for each controller id, search FieldbusSensors, Fans, and Expansion Board tables for a match, if there is a match add to list as finished
    for this_controller_id in controller_id_list:
        found_fbsensor_controller_id_boolean = bool(FieldbusSensors.query.filter_by(controller_id=this_controller_id).first())
        found_fan_controller_id_boolean = bool(Job.query.filter_by(controller_id=this_controller_id).first())
        found_expansion_board_id_boolean = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=this_controller_id).first())
        
        # found a fan with this controller id, add to list
        if found_fbsensor_controller_id_boolean or found_fan_controller_id_boolean or found_expansion_board_id_boolean:
            controller_id_fans_found_list.append(this_controller_id)
    # CG
    print("-----------------controller_id_fans_found_list--------------------",controller_id_fans_found_list)

    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)
    # CG
    print("------------------------------controller_number_list-----------------------",controller_number_list)

    return controller_number_list


def  findFBSensorsWithFBSensorDefinition(sales_order_id):
    """ find all fbsensors that have been assigned a fbsensor definition """
    all_fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
            filter(FieldbusSensors.controller_id==None).order_by(FieldbusSensors.id).all()
    list_address_of_selected_fbsensors = []
    fbsensor_definitions_name = "None"
    for fbsensor in all_fbsensor_objects:
        current_fbsensor_address = int(fbsensor.modbus_address)
        fbsensor_id = fbsensor.id

        # get fieldbus defintion name
        fieldbus_sensor_definitions_id = fbsensor.fieldbus_sensor_definitions_id
        fbsensor_definitions_object_boolean = bool(FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first())
        if fbsensor_definitions_object_boolean:
            fbsensor_definitions_object = FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first()
            fbsensor_definitions_name = fbsensor_definitions_object.definition_name

        list_address_of_selected_fbsensors.append([current_fbsensor_address, fbsensor_definitions_name, fbsensor_id])

    result = {}

    for fbsensor_address, definition_name, fbsensor_id in list_address_of_selected_fbsensors:
        result.setdefault(definition_name, list()).append([int(fbsensor_address), int(fbsensor_id)])

    return result


def getAssignedExpansionBoardsList(controller_id):
    expansion_board_objects = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).all()
    expansion_board_list = []

    for expansion_board_object in expansion_board_objects:
        expansion_board_list.append(expansion_board_object.expansion_board_number)

    return expansion_board_list


def getFBSensorAddressDefininitionDictionary(controller_id):
    """ return list of  tuples [(address, definition_name)] for a given controller """
    fbsensor_objects = FieldbusSensors.query.filter_by(controller_id=controller_id).order_by(FieldbusSensors.id).all()
    address_definition_tuple_list = []
    sensor_definition_name = "None"

    for sensor_object in fbsensor_objects:
        current_sensor_address = int(sensor_object.modbus_address)
        sensor_definition_id = sensor_object.fieldbus_sensor_definitions_id
        if sensor_definition_id:
            fieldbus_sensor_definition_object = FieldbusSensorDefinitions.query.filter_by(id=sensor_definition_id).first()
            sensor_definition_name = fieldbus_sensor_definition_object.definition_name

        address_definition_tuple_list.append([current_sensor_address, sensor_definition_name])


    # use default dict library
    result_dict = {}
    for sensor_address, sensor_definition_name in address_definition_tuple_list:
        result_dict.setdefault(sensor_definition_name, list()).append(int(sensor_address))

    return result_dict


def removeControllerIDFromExpansionBoards(sales_order_id, controller_id):
    all_expansion_board_objects = db.session.query(ExpansionBoardInputOutput).\
        filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).\
            filter(ExpansionBoardInputOutput.controller_id==controller_id).all()

    for expansion_board_object in all_expansion_board_objects:
        expansion_board_object.controller_id = None
        db.session.add(expansion_board_object)
    
    output_count = OutputZones.query.filter_by(sales_order_id=sales_order_id).filter(OutputZones.controller_id==controller_id).count()
    if output_count == 1:
        output_object = OutputZones.query.filter_by(sales_order_id=sales_order_id).filter(OutputZones.controller_id==controller_id).first()
        output_object.ExpansionBoaard_inout_id = None
        db.session.add(output_object)
    if output_count > 1:
        OutputZones.query.filter_by(sales_order_id=sales_order_id).filter(OutputZones.controller_id==controller_id).delete()
        ouput_zones = OutputZones(sales_order_id=sales_order_id, controller_id = controller_id)
        db.session.add(ouput_zones)




    print(output_count)

        
    db.session.commit()

""" Remove controller_id from Fieldbus Sensors in the FieldbusSensors table, given the controller_id """
def removeControllerIDFromFBSensors(sales_order_id, controller_id):

    all_fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==controller_id).all()

    for fbsensor_object in all_fbsensor_objects :
        fbsensor_object.controller_id = None
        db.session.add(fbsensor_object)

    db.session.commit()



""" Remove controller_id from fans in Jobs table, given the controller_id """
def removeControllerIDFromFans(sales_order_id, controller_id):

    all_fan_objects = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).all()

    for fan_object in all_fan_objects:
        fan_object.controller_id = None
        db.session.add(fan_object)

    db.session.commit()


def edit_fieldbus_sensors(sales_order_id, controller_id, modbus_ranges_index):
    """ creates all the appropriate new sensor assignments for a given controller """

    # create modbus address list using modbus string
    modbus_address_list = decodeModbusRange(modbus_ranges_index)


    # for each fbsensor_id, determine fbsensor_definition_id
    # create default dict {fbsensor_definition_id: {[fbsensor_id, fbsensor_id, fbsensor_id]}}
    definition_id_fbsensor_id_list = []
    for fbsensor_id in modbus_address_list:
        # get fbsensor definition id for this fan
        fbsensor_object = FieldbusSensors.query.filter_by(id=fbsensor_id).first()
        fbsensor_definitions_id = fbsensor_object.fieldbus_sensor_definitions_id
        definition_id_fbsensor_id_list.append([fbsensor_definitions_id, fbsensor_id])

    default_dict = {}
    for definition_id, fbsensor_id in definition_id_fbsensor_id_list:
        default_dict.setdefault(definition_id, list()).append([int(fbsensor_id)])

    # for each definition
    for fbsensor_definition_id in default_dict:

        # for each fbsensor_id with this definition
        for fbsensor_id in default_dict[fbsensor_definition_id]:

            # save controller_id to current fbsensor
            current_fbsensor_object = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
                filter(FieldbusSensors.id==fbsensor_id).first()
            current_fbsensor_object.controller_id = controller_id

            db.session.add(current_fbsensor_object)
    
    db.session.commit()
    

def edit_fan_in_jobs_table(sales_order_id, controller_id, modbus_ranges_index):
    """ creates all the appropriate new fan assignments for a given controller"""

    # create modbus address list using modbus string
    controller_index_list = decodeModbusRange(modbus_ranges_index)

    # for each fan_id, determine fan_definition_id
    # create default dict {fan_definition_id: {[fan_id, fan_id, fan_id]}}
    definition_id_fan_id_list = []
    for fan_id in controller_index_list:
        # get fan definition id for this fan
        fan_object = Job.query.filter_by(Index=fan_id).first()
        fan_definitions_id = fan_object.fan_definitions_id
        definition_id_fan_id_list.append([fan_definitions_id, fan_id])

    default_dict = {}
    for definition_id, fan_id in definition_id_fan_id_list:
        default_dict.setdefault(definition_id, list()).append([int(fan_id)])


    for fan_definition_id in default_dict:

        # get the values from fan definitions table 
        fan_definition_object = FanDefinitions.query.filter_by(id=fan_definition_id).first()

        # get fan definition id using name
        id_fan_definitions = fan_definition_object.id

        comms_fan_definition = fan_definition_object.comms
        no_sensors_fan_definition = fan_definition_object.sensors_count

        sensor_1_type_fan_definition = fan_definition_object.sensor_1_type
        sensor_1_unit_fan_definition = fan_definition_object.sensor_1_unit
        sensor_1_min_fan_definition = fan_definition_object.sensor_1_min
        sensor_1_max_fan_definition = fan_definition_object.sensor_1_max

        sensor_2_type_fan_definition = fan_definition_object.sensor_2_type
        sensor_2_unit_fan_definition = fan_definition_object.sensor_2_unit
        sensor_2_min_fan_definition = fan_definition_object.sensor_2_min
        sensor_2_max_fan_definition = fan_definition_object.sensor_2_max

        command_value_fan_definition = fan_definition_object.command_value
        control_fan_definition = fan_definition_object.control
        minimum_speed_fan_definition = fan_definition_object.minimum_speed
        maximum_speed_fan_definition = fan_definition_object.maximum_speed
        p_val_fan_definition = fan_definition_object.p_val
        i_val_fan_definition = fan_definition_object.i_val
        control_min_fan_definition = fan_definition_object.control_min
        control_max_fan_definition = fan_definition_object.control_max
        smoke_detector_fan_definition = fan_definition_object.smoke_detector
        emergency_fan_definition = fan_definition_object.emergency
        emer_set_speed_fan_definition = fan_definition_object.emer_set_speed
        emer_external_signal_fan_definition = fan_definition_object.emer_external_signal
        emer_start_stop_signal_fan_definition = fan_definition_object.emer_start_stop_signal

        timeout_fan_definition = fan_definition_object.timeout
        timeout_speed_fan_definition = fan_definition_object.timeout_speed
        programmed_fan_definition = 'False'
        # input_by_fan_definition = 
        input_on_fan_definition = getDateAndTime()
        fan_type_fan_definition = fan_definition_object.fan_type
        zone_fan_definition = fan_definition_object.zone
        fault_relay_fan_definition = fan_definition_object.fault_relay

        for fan_id in default_dict[fan_definition_id]:

            current_fan_object = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.Index==fan_id).first()
            current_fan_object.controller_id = controller_id

            current_fan_object.FanType = fan_type_fan_definition
            current_fan_object.Comms = comms_fan_definition
            current_fan_object.No_Sensors = no_sensors_fan_definition
            current_fan_object.Sensor_1_type = sensor_1_type_fan_definition
            current_fan_object.Sensor_1_unit = sensor_1_unit_fan_definition
            current_fan_object.Sensor_1_min = sensor_1_min_fan_definition
            current_fan_object.Sensor_1_max = sensor_1_max_fan_definition
            current_fan_object.Sensor_2_type = sensor_2_type_fan_definition
            current_fan_object.Sensor_2_unit = sensor_2_unit_fan_definition
            current_fan_object.Sensor_2_min = sensor_2_min_fan_definition
            current_fan_object.Sensor_2_max = sensor_2_max_fan_definition
            current_fan_object.Command_Value = command_value_fan_definition
            current_fan_object.Control = control_fan_definition
            current_fan_object.Minimum_Speed = minimum_speed_fan_definition
            current_fan_object.Maximum_Speed = maximum_speed_fan_definition
            current_fan_object.P_val = p_val_fan_definition
            current_fan_object.I_val = i_val_fan_definition
            current_fan_object.Control_Min = control_min_fan_definition
            current_fan_object.Control_Max = control_max_fan_definition
            current_fan_object.SmokeDetector = smoke_detector_fan_definition
            current_fan_object.emergancy = emergency_fan_definition
            current_fan_object.emer_set_speed = emer_set_speed_fan_definition
            current_fan_object.emer_external_signal =  emer_external_signal_fan_definition
            current_fan_object.emer_start_stop_signal = emer_start_stop_signal_fan_definition
            current_fan_object.Timeout = timeout_fan_definition
            current_fan_object.TimeoutSpeed = timeout_speed_fan_definition
            current_fan_object.Programmed = programmed_fan_definition

            current_fan_object.InputOn = input_on_fan_definition
            current_fan_object.Zone = zone_fan_definition
            current_fan_object.FaultRelay = fault_relay_fan_definition


            db.session.add(current_fan_object)

    db.session.commit()

        
def edit_expansion_boards(sales_order_id, controller_id, expansionboard_index_ranges):
    """ create all the appropriate new expansion board assignments for a given controller """

    # create modbus address list using modbus string
    expansion_board_id_list = decodeModbusRange(expansionboard_index_ranges)
    print(expansion_board_id_list, 'expansion list')

    existing_expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()

    new_total_expansion_board_count = len(expansion_board_id_list) + existing_expansion_board_count

    next_expansion_board_number = existing_expansion_board_count + 1

    for index, expansion_board_id in enumerate(expansion_board_id_list):
        print(index,expansion_board_id)
        expansion_board_object = db.session.query(ExpansionBoardInputOutput).\
            filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).\
                filter(ExpansionBoardInputOutput.id==expansion_board_id).first()
        
        expansion_board_object.controller_id = controller_id
        expansion_board_object.expansion_board_number = next_expansion_board_number + index

        db.session.add(expansion_board_object)
    
    expansion_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    output_count = OutputZones.query.filter_by(controller_id=controller_id).count() 
    print(output_count, expansion_count, 'all')
    for index, expansion_board_id in enumerate(expansion_board_id_list):
        if index==0 and existing_expansion_board_count == 0:
            print('firts')
            output_objects = OutputZones.query.filter_by(controller_id=controller_id).first()
            output_objects.ExpansionBoaard_inout_id = expansion_board_id
            db.session.add(output_objects)
        else:
            print('second')
            ouput_zones = OutputZones(sales_order_id=sales_order_id, controller_id = controller_id,ExpansionBoaard_inout_id = expansion_board_id)
            db.session.add(ouput_zones)

    


    db.session.commit()

    # add expansion_board_count to controller input output table
    # controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
    # controller_input_output_object.expansion_board_quantity = new_total_expansion_board_count

    # db.session.add(controller_input_output_object)
    # db.session.commit()

