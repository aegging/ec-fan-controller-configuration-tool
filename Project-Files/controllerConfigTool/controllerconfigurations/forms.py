'''CG Changes This file is modified by CG developers on 24/04/2023'''
'''Changes the data type of the interger to Decimal for tables row  and added validation for non charactor and 2 digit only'''

from flask_wtf import FlaskForm
from wtforms import SubmitField
from wtforms import SubmitField, IntegerField, SelectField, BooleanField, DecimalField, FloatField, StringField, TextAreaField
from wtforms.validators import DataRequired, ValidationError, Optional



class controllerConfigurationsForm(FlaskForm):
    jetvent_min_fan_speed = IntegerField('Min Speed')
    jetvent_max_fan_speed = IntegerField('Max Speed')

    smoke_detector_system_keep_off_delay_minutes = IntegerField('Period of time (minutes) that the motors are shut down after detecting smoke.\
                                                                After this time, the smoke detectors are reset.')
    smoke_detection_system_trigger_delay_seconds = IntegerField('Delay before smoke is triggered (seconds)')
    smoke_detector_system_restrict_response_to_affected_zone = BooleanField('Smoke response restricted to affected zone.')

    fire_mode_exhaust_fan_speed = IntegerField('Exhaust Fan Speed')
    fire_mode_jetvent_fan_speed = IntegerField('Jetvent Fan Speed')
    fire_mode_supply_fan_speed = IntegerField('Supply Fan Speed')
    fire_mode_active_GFA = SelectField('Active GFA', choices=['NO', 'NC'])
    fire_mode_jetvent_fan_run = SelectField('Jetvent Fan Run', choices=['NO', 'NC'])
    fire_mode_jetvent_fan_stop = SelectField('Jetvent Fan Stop', choices=['NO', 'NC'])

    manual_override_exhaust_fan_speed = IntegerField('Exhaust Fan Speed')
    manual_override_jetvent_fan_speed = IntegerField('Jetvent Fan Speed')
    manual_override_supply_fan_speed = IntegerField('Supply Fan Speed')
    manual_override_fail_mode_exhaust_fan = SelectField('Exhaust Fan Speed', choices=['AUTO','ON','OFF'])
    manual_override_fail_mode_jetvent_fan = SelectField('Jetvent Fan Speed', choices=['AUTO','ON','OFF'])
    manual_override_fail_mode_supply_fan = SelectField('Supply Fan Speed', choices=['AUTO','ON','OFF'])

    purge_system_activation_time_hour = IntegerField('Activation Time Hour')
    purge_system_run_duration_time_minutes = IntegerField('Runtime Duration (minutes)')
    purge_system_run_speed_demand_percentage = IntegerField('Purge Fan Speed (%)')

    

    zero_percent_demand_co_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    zero_percent_demand_no2_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    zero_percent_demand_co2_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    zero_percent_demand_temp_celsius = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    zero_percent_demand_external_percent = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    hundred_percent_demand_co_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    hundred_percent_demand_no2_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    hundred_percent_demand_co2_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    hundred_percent_demand_temp_celsius = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    hundred_percent_demand_external_percent = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])

    alarm_threshold_co_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    alarm_threshold_no2_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    alarm_threshold_co2_ppm = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    alarm_threshold_temp_celsius = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
    alarm_threshold_external_percent = DecimalField(render_kw={'type':'number',  'step':'0.01'}, validators=[DataRequired()])
   

    sensor_demand_level = SelectField('Sensor Demand Level', choices=[('FALSE', 'NO'),('TRUE','YES')])
    fan_demand_level = SelectField('Fan Demand Level', choices=[('FALSE', 'NO'), ('TRUE','YES')])
    fan_demand_type = SelectField('Fan Demand Type', choices=[('FALSE', 'NO'), ('TRUE','YES')])


    # variables not in model below
    purge_enable_checkbox = BooleanField('Enable')
    submit_button = SubmitField('Save')
    save_to_all_controllers_button = SubmitField('Save to All Controllers')



