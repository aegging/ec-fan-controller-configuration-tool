import re
from flask import Blueprint
from controllerConfigTool.controllerconfigurations.forms import controllerConfigurationsForm
from controllerConfigTool.controllerconfigurations.utils import getDefaultControllerConfigurationValues, getListControllersWithControllerConfigurations
from controllerConfigTool.models import Controller
from controllerConfigTool.getInfoFromDatabase import findTagPrefixGivenSalesOrderID, getControllerID, getSalesOrderValues
from flask import render_template, url_for, flash, redirect, request
from controllerConfigTool import db

controllerconfigurations_blueprint = Blueprint('controllerconfigurations', __name__)



@controllerconfigurations_blueprint.route('/controller_configurations/<sales_order_id>/<int:current_controller>', methods=['GET','POST'])
def controller_configurations(sales_order_id,current_controller):

    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    # find finished controllers
    list_index_of_finished_controllers = getListControllersWithControllerConfigurations(sales_order_id, tag_prefix)
    
    # get all values from sales order table for this sales order
    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)

    # find current controller in database and add controller configurations data to it
    controller_id = getControllerID(current_controller, sales_order_id)

    # Is there existing controller_configurations data in database for this controller?
    controller_object = Controller.query.filter_by(id=controller_id).first()
    existing_data = controller_object.controller_configurations_data_boolean

    if existing_data:
        default_controller_configurations_dictionary = dict(controller_object.__dict__)
        default_controller_configurations_dictionary.pop('_sa_instance_state', None)
    else:
        default_controller_configurations_dictionary = getDefaultControllerConfigurationValues()

    print(default_controller_configurations_dictionary)

    form = controllerConfigurationsForm(data=default_controller_configurations_dictionary)

    if request.method == 'POST':
        for i in range(1, sales_order_controller_quantity + 1):
            controller_name = 'controller' + str(i)
            if controller_name in request.form:
                return redirect(url_for('controllerconfigurations.controller_configurations', sales_order_id=sales_order_id, current_controller=i))

        if "navigate_sales_order_status_button" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
        elif "submit_button" in request.form:
            if form.validate_on_submit():
            
                default_controller_configurations_dictionary = form.data

                # form.populate_obj(default_controller_configurations_dictionary)
            
                # if purge_enable_checkbox is False, set all purge values to 0
                if default_controller_configurations_dictionary['purge_enable_checkbox'] == 0:
                    default_controller_configurations_dictionary['purge_system_activation_time_hour'] = 0
                    default_controller_configurations_dictionary['purge_system_run_duration_time_minutes'] = 0
                    default_controller_configurations_dictionary['purge_system_run_speed_demand_percentage'] = 0
                    
                # remove unneeded data from dictionary
                default_controller_configurations_dictionary.pop('submit_button')
                default_controller_configurations_dictionary.pop('save_to_all_controllers_button')
                default_controller_configurations_dictionary.pop('csrf_token')

                # set controller_configurations_data field to True to depict the controller has been configured
                default_controller_configurations_dictionary['controller_configurations_data_boolean'] = True
                
                # update existing Row in Controller Table with new data submitted by user
                db.session.query(Controller).filter(Controller.id==controller_id).update(default_controller_configurations_dictionary)
                db.session.commit()


                flash(f'Controller {current_controller} configuration saved', 'success')
                return redirect(url_for('controllerconfigurations.controller_configurations', sales_order_id=sales_order_id, current_controller=current_controller))

        elif "save_to_all_controllers_button" in request.form:
            if form.validate_on_submit():
            
                default_controller_configurations_dictionary = form.data

                # form.populate_obj(default_controller_configurations_dictionary)
            
                # if purge_enable_checkbox is False, set all purge values to 0
                if default_controller_configurations_dictionary['purge_enable_checkbox'] == 0:
                    default_controller_configurations_dictionary['purge_system_activation_time_hour'] = 0
                    default_controller_configurations_dictionary['purge_system_run_duration_time_minutes'] = 0
                    default_controller_configurations_dictionary['purge_system_run_speed_demand_percentage'] = 0
                    
                # remove unneeded data from dictionary
                default_controller_configurations_dictionary.pop('submit_button')
                default_controller_configurations_dictionary.pop('save_to_all_controllers_button')
                default_controller_configurations_dictionary.pop('csrf_token')

                # set controller_configurations_data field to True to depict the controller has been configured
                default_controller_configurations_dictionary['controller_configurations_data_boolean'] = True
                
                # update existing Row in Controller Table with new data submitted by user
                db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).update(default_controller_configurations_dictionary)
                db.session.commit()


                flash(f'Controller {current_controller} configuration saved', 'success')
                return redirect(url_for('controllerconfigurations.controller_configurations', sales_order_id=sales_order_id, current_controller=current_controller))

    return render_template('controller_configurations.html', form=form, current_controller=current_controller, 
    list_index_of_finished_controllers=list_index_of_finished_controllers, sales_order_controller_quantity=sales_order_controller_quantity)
