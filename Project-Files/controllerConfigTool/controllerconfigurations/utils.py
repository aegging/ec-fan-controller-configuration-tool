
from controllerConfigTool.models import Controller
from controllerConfigTool import db
import configparser

DEFAULT_CONTROLLER_CONFIGURATIONS_PATH = DEFAULT_CONTROLLER_CONFIGURATIONS_PATH = 'C:\Program Files\Fantech\ZOOFANS-EC-Fan-Controller-Configuration-Tool\ec-fan-controller-configuration-tool\Project-Files\controllerConfigTool\default_controller_configurations.ini'

class DefaultControllerConfigurationsFile:
    def __init__(self):
        self.config_file = str(DEFAULT_CONTROLLER_CONFIGURATIONS_PATH)
        self.config = configparser.ConfigParser()
        self.config.read(self.config_file)

    def build_dictionary(self):
        result = {}
        
        result['jetvent_min_fan_speed'] = self.config['JETVENT FAN'].getint('MIN_SPEED')
        result['jetvent_max_fan_speed'] = self.config['JETVENT FAN'].getint('MAX_SPEED')

        result['smoke_detection_system_trigger_delay_seconds'] = self.config['SMOKE DETECTION'].getint('SMOKE_DETECTOR_SYSTEM_TRIGGER_DELAY_SECONDS')
        result['smoke_detector_system_keep_off_delay_minutes'] = self.config['SMOKE DETECTION'].getint('SMOKE_DETECTOR_SYSTEM_KEEP_OFF_DELAY_MINUTES')
        result['smoke_detector_system_restrict_response_to_affected_zone'] = self.config['SMOKE DETECTION'].getboolean('SMOKE_DETECTOR_SYSTEM_RESTRICT_RESPONSE_TO_AFFECTED_ZONE')
        
        result['fire_mode_exhaust_fan_speed'] = self.config['FIRE MODE'].getint('FIRE_MODE_EXHAUST_FAN_SPEED')
        result['fire_mode_jetvent_fan_speed'] = self.config['FIRE MODE'].getint('FIRE_MODE_JETVENT_FAN_SPEED')
        result['fire_mode_supply_fan_speed'] = self.config['FIRE MODE'].getint('FIRE_MODE_SUPPLY_FAN_SPEED')
        result['fire_mode_active_GFA'] = self.config['FIRE MODE']['FIRE_MODE_ACTIVE_GFA']
        result['fire_mode_jetvent_fan_run'] = self.config['FIRE MODE']['FIRE_MODE_JETFAN_RUN']
        result['fire_mode_jetvent_fan_stop'] = self.config['FIRE MODE']['FIRE_MODE_JETFAN_STOP']

        result['manual_override_exhaust_fan_speed'] = self.config['MANUAL OVERRIDE'].getint('MANUAL_OVERRIDE_EXHAUST_FAN_SPEED')
        result['manual_override_jetvent_fan_speed'] = self.config['MANUAL OVERRIDE'].getint('MANUAL_OVERRIDE_JETVENT_FAN_SPEED')
        result['manual_override_supply_fan_speed'] = self.config['MANUAL OVERRIDE'].getint('MANUAL_OVERRIDE_SUPPLY_FAN_SPEED')
        result['manual_override_fail_mode_exhaust_fan'] = self.config['MANUAL OVERRIDE']['MANUAL_OVERRIDE_FAIL_MODE_EXHAUST_FAN']
        result['manual_override_fail_mode_jetvent_fan'] = self.config['MANUAL OVERRIDE']['MANUAL_OVERRIDE_FAIL_MODE_JETVENT_FAN']
        result['manual_override_fail_mode_supply_fan'] = self.config['MANUAL OVERRIDE']['MANUAL_OVERRIDE_FAIL_MODE_SUPPLY_FAN']

        result['purge_system_activation_time_hour'] = self.config['PURGE SYSTEM'].getint('PURGE_SYSTEM_ACTIVATION_TIME_HOUR')
        result['purge_system_run_duration_time_minutes'] = self.config['PURGE SYSTEM'].getint('PURGE_SYSTEM_RUN_DURATION_TIME_MINUTES')
        result['purge_system_run_speed_demand_percentage'] = self.config['PURGE SYSTEM'].getint('PURGE_SYSTEM_RUN_SPEED_DEMAND_PERCENTAGE')


        result['zero_percent_demand_co_ppm'] = self.config['UNIT CONFIGURATION'].getint('_0_PERCENT_DEMAND_CO_PPM')
        result['zero_percent_demand_no2_ppm'] = self.config['UNIT CONFIGURATION'].getint('_0_PERCENT_DEMAND_NO2_PPM')
        result['zero_percent_demand_co2_ppm'] = self.config['UNIT CONFIGURATION'].getint('_0_PERCENT_DEMAND_CO2_PPM')
        result['zero_percent_demand_temp_celsius'] = self.config['UNIT CONFIGURATION'].getint('_0_PERCENT_DEMAND_TEMP_CELSIUS')
        result['zero_percent_demand_external_percent'] = self.config['UNIT CONFIGURATION'].getint('_0_PERCENT_DEMAND_EXTERNAL_PERCENT')


        result['hundred_percent_demand_co_ppm'] = self.config['UNIT CONFIGURATION'].getint('_100_PERCENT_DEMAND_CO_PPM')
        result['hundred_percent_demand_no2_ppm'] = self.config['UNIT CONFIGURATION'].getint('_100_PERCENT_DEMAND_NO2_PPM')
        result['hundred_percent_demand_co2_ppm'] = self.config['UNIT CONFIGURATION'].getint('_100_PERCENT_DEMAND_CO2_PPM')
        result['hundred_percent_demand_temp_celsius'] = self.config['UNIT CONFIGURATION'].getint('_100_PERCENT_DEMAND_TEMP_CELSIUS')
        result['hundred_percent_demand_external_percent'] = self.config['UNIT CONFIGURATION'].getint('_100_PERCENT_DEMAND_EXTERNAL_PERCENT')
       
        result['alarm_threshold_co_ppm'] = self.config['UNIT CONFIGURATION'].getint('ALARM_THRESHOLD_CO_PPM')
        result['alarm_threshold_no2_ppm'] = self.config['UNIT CONFIGURATION'].getint('ALARM_THRESHOLD_NO2_PPM')
        result['alarm_threshold_co2_ppm'] = self.config['UNIT CONFIGURATION'].getint('ALARM_THRESHOLD_CO2_PPM')
        result['alarm_threshold_temp_celsius'] = self.config['UNIT CONFIGURATION'].getint('ALARM_THRESHOLD_TEMP_CELSIUS')
        result['alarm_threshold_external_percent'] = self.config['UNIT CONFIGURATION'].getint('ALARM_THRESHOLD_EXTERNAL_PERCENT')
       
        result['sensor_demand_level'] = self.config['UNIT CONFIGURATION']['SENSOR_DEMAND_LEVEL']
        result['fan_demand_level'] = self.config['UNIT CONFIGURATION']['FAN_DEMAND_LEVEL']
        result['fan_demand_type'] = self.config['UNIT CONFIGURATION']['FAN_DEMAND_TYPE']

        return result

def getDefaultControllerConfigurationValues():
    c = DefaultControllerConfigurationsFile()
    result = c.build_dictionary()
    return result



def getListControllersWithControllerConfigurations(sales_order_id, tag_prefix):
    """ return a list of controllers with existing controller configurations data """

    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)


    controller_id_fans_found_list = []

    # for each controller id, search Controller table for existing controller configurations data 
    for this_controller_id in controller_id_list:
        found_controller_id_boolean = bool(db.session.query(Controller).filter(Controller.id==this_controller_id).\
            filter(Controller.controller_configurations_data_boolean==True).first())
        
        # found a fan with this controller id, add to list
        if found_controller_id_boolean:
            controller_id_fans_found_list.append(this_controller_id)


    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)

    return controller_number_list
