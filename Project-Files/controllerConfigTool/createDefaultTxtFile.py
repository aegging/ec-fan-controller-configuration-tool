# CG Changes for merging 3.2.7 version data with 3.4 (To Find out Changes search with keyword 3.2.7 or 3.4) Date 28/03//2023

import os

from sqlalchemy.sql.sqltypes import DATE
from controllerConfigTool.encodeJobNumber import encodeJobNumber
import logging
from controllerConfigTool.dataCalculations import convertFanTypesAndDataStructure
from controllerConfigTool.models import AnalogSensorDefinitions, AnalogSensors, ControllerInputOutputs, ExpansionBoardInputOutput, FanDefinitions, FieldbusSensorDefinitions, FieldbusSensors, SalesOrders, Controller, Job, OutputZones, Zone
from controllerConfigTool.getInfoFromDatabase import getAllCarelKeyValues, getControllerValues, getControllerNumber, getCarelKeyValues_fieldbus_sensor_sense_type_index, getSelectedVersion
from pytz import timezone
from datetime import datetime
from controllerConfigTool import db

class createConfigFileClass:

    def __init__(self, sales_order_id, controller_id, controller_serial_numer):       
        # address_fantype_dictionary => {'JVEC-HP': {1, 2, 3, 4, 5, 6, 7, 8}, 'JVEC-ULP': {9, 10, 11}}
        # initialize logging
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)

        self.selected_version = getSelectedVersion()

        self.carel_key_values_dict = getAllCarelKeyValues()
        self.fan_analog_sensor_type_dict = self.carel_key_values_dict['analog_sensor_type']
        self.fieldbus_sensor_type_dict = self.carel_key_values_dict['fieldbus_sensor_sense_type_index']

        self.controller_type, controller_serial_number, address_fantype_dictionary, grouped_list_fan_types, version_number_id = getControllerValues(controller_id)
        current_controller = getControllerNumber(controller_id)
        self.controller_object = Controller.query.filter_by(id=controller_id).first()
        self.controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
        self.sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
        self.controller_id = controller_id
        self.output_y1_y4_zone = OutputZones.query.filter_by(controller_id=controller_id).first()
        self.total_zone_controller = Zone.query.filter_by(controller_id=controller_id).count()

    #    removed any logic checking if sensors existed to enable alarm relay. Alarm relay will always be active (9-7-2023 - AE)
        
        # job_number (example-> '20-010828-01')
        self.logger.info('Address/Fan Type Dictionary: %s', address_fantype_dictionary)

        # convert string fantypes to integers, convert from dict to list of list
        self.address_fantype_list, self.current_modbus_address, self.modbus_array, self.modbus_ranges = convertFanTypesAndDataStructure(grouped_list_fan_types)

        self.current_controller = current_controller

        self.current_modbus_address = int(self.address_fantype_list[0][0])

        fan_object = db.session.query(Job).filter(Job.Address==self.current_modbus_address).\
            filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).\
                first()

        fan_definitions_id = fan_object.fan_definitions_id
        self.fan_definition_object = FanDefinitions.query.filter_by(id=fan_definitions_id).first()
        fan_sensor1_unit = self.fan_definition_object.sensor_1_unit
        fan_sensor2_unit = self.fan_definition_object.sensor_2_unit

        self.expansion_board_objects = db.session.query(ExpansionBoardInputOutput).\
            filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.expansion_board_number).all()

        self.expansion_board_count = db.session.query(ExpansionBoardInputOutput).\
            filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.expansion_board_number).count()

        # join job and fan definition tables
        self.job_fan_definition_joined_objects = db.session.query(Job.Address, Job.fan_definitions_id,\
            FanDefinitions.sensors_count,\
            FanDefinitions.sensor_1_unit, FanDefinitions.sensor_1_min, FanDefinitions.sensor_1_max,\
                FanDefinitions.sensor_2_unit, FanDefinitions.sensor_2_min, FanDefinitions.sensor_2_max,\
                    FanDefinitions.fan_type, Job.Zone, FanDefinitions.sub_fan_type,\
                ).join(FanDefinitions, FanDefinitions.id == Job.fan_definitions_id).\
            filter(Job.controller_id==controller_id).order_by(Job.Index).all()
    


        for test_data in self.job_fan_definition_joined_objects:
            print(test_data.Address, " : ", test_data.fan_definitions_id)

        # jetvent_fan_type(key, values) -> (0 = NA), (1 = ZPM), (2 = ZAM), (3 = EB5), (4 = IME), (5 = A10), (6 = HNT), (7 = A51)

        # jetvent_sensor1_type(key,values) -> (None = 0), (CO sensor = 1), (NO2 sensor = 2), (deg C sensor = 3), (0-10V sensor = 4)

        # subfan1_fantype(key, values) -> (DUL1 = 3), (None = 0)

        self.logger.info('Sales Order Number: %s | Current Controller: %s | Modbus Ranges: %s',  self.sales_order_object.sales_order_number, self.current_controller, self.modbus_ranges)

        # get system path from Sales Order
        sales_orders_object = SalesOrders.query.filter_by(id=sales_order_id).first()

        # open text file for output
        # THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
        DATE_TIME = self.getDateTime()

        THIS_FOLDER = sales_orders_object.system_path
        CONTROLS_FOLDER = os.path.join(THIS_FOLDER, 'controls')
        JOB_NUMBER_FOLDER = os.path.join(CONTROLS_FOLDER, str(self.sales_order_object.sales_order_number + "__" + DATE_TIME))
        CONTROLLER_CONFIGURATIONS_FOLDER = os.path.join(JOB_NUMBER_FOLDER, 'controller-configurations')


        new_directory = (str(self.controller_type)).upper() + '_Controller' + str(current_controller) + "__MB_" + str(self.modbus_ranges)
        self.THIS_FOLDER = os.path.join(CONTROLLER_CONFIGURATIONS_FOLDER, new_directory)

        # make directory if it does not exists
        if not os.path.exists(self.THIS_FOLDER):
            os.makedirs(self.THIS_FOLDER)

        self.serial_file = os.path.join(self.THIS_FOLDER, 'controller-serial-number.txt')
        self.z = open(self.serial_file, 'w')
        self.z.write(str(controller_serial_numer))
        self.z.close()

        self.my_file = os.path.join(self.THIS_FOLDER, 'default.txt')
        self.f = open(self.my_file, 'w')

        self.logger.info('File path: %s', self.my_file)




    def getSensorConfigurationValues(self, sensor_type):
        if sensor_type == 'CO':
            zero_percent_demand = self.controller_object.zero_percent_demand_co_ppm
            hundred_percent_demand = self.controller_object.hundred_percent_demand_co_ppm
            alarm_threshold = self.controller_object.alarm_threshold_co_ppm
        elif sensor_type == 'NO2':
            zero_percent_demand = self.controller_object.zero_percent_demand_no2_ppm
            hundred_percent_demand = self.controller_object.hundred_percent_demand_no2_ppm
            alarm_threshold = self.controller_object.alarm_threshold_no2_ppm
        elif sensor_type == 'Temp':
            zero_percent_demand = self.controller_object.zero_percent_demand_temp_celsius
            hundred_percent_demand = self.controller_object.hundred_percent_demand_temp_celsius
            alarm_threshold = self.controller_object.alarm_threshold_temp_celsius
        elif sensor_type == 'RH':
            zero_percent_demand = 80
            hundred_percent_demand = 100
            alarm_threshold = 100
        else:
            zero_percent_demand = 0
            hundred_percent_demand = 100
            alarm_threshold = 100

        return zero_percent_demand, hundred_percent_demand, alarm_threshold



    def getDateTime(self):
        """Get time and date."""
        now = datetime.now(timezone('US/Central'))
        return now.strftime("%m-%d-%Y")
                

    def return_class_variables(self):
        return self.THIS_FOLDER, self.modbus_ranges

    # build text file
    def build_file(self):

        self.create_header_lines()
        self.call_encode_job()
        self.handle_controller_type()
        self.write_controller_type()
        self.handle_jetvent_fan_count()
        self.handle_purge()
        self.handle_miscellaneous()
        self.handle_global_variables()
        self.fire_stop() #This sets U5 to N/C, when self.gfa_fire True
        self.relay_1_function()
        self.manual_override()
        self.handle_expansion_boards()
        self.handle_fbsensors()

        # ------------------
        # note -> commented out timezone capabilities because Australian locations are not currently added
        # (key:value) -> 6:Los Angeles; 7:Phoenix; 9:Denver; 11:Chicago; 15:New York
        self.set_timezone(self.sales_order_object.time_zone)
        # ------------------

        # close text file
        self.f.close()  


    def handle_controller_type(self):
        if self.controller_type.upper() == 'MINI':
            self.mini_selected = True
        elif self.controller_type.upper() == 'MAX':
            self.mini_selected = False
        else:
            print('controller type not selected')
            

    def create_header_lines(self):
        self.f.write('#Ver.1.0 cPCO Family Exported Configuration file, ZOOFans\n')
        self.f.write('#Variable	Description	DataType	DefaultValue\n')


    def call_encode_job(self):
        encoded_message = []
        encoded_message = encodeJobNumber(str(self.sales_order_object.sales_order_number), str(self.modbus_array[0]), str(self.modbus_array[len(self.modbus_array)-1]))

        for i in range(0, len(encoded_message)):
            self.f.write("JobNumber[" + str(i+1) + "]" + self.format_datatype("UINT") + str(encoded_message[i]) + '\n' )
        for i in range(len(encoded_message), 20):
            self.f.write("JobNumber[" + str(i+1) + "]" + self.format_datatype("UINT") + str(32) + '\n' )


    def handle_jetvent_fan_count(self):
        """Call text creation for each jetvent fan."""
        # calculate how many jetvent fans, i.e. modbus address 20 - modbus address 10 + 1 = 11 total fans
        print('modbus_array:' + str(self.modbus_array))
        self.jetvent_fan_count = len(self.modbus_array)
        # self.last_used_address = self.address_fantype_list[-1][0]

        self.jetfans_fan_type_dict = self.carel_key_values_dict['jetfans_fan_type']

        current_fan_index = 1
        for fan_object in self.job_fan_definition_joined_objects:
            self.handle_jetvent_per_fan(current_fan_index, fan_object)
            current_fan_index += 1


        for i in range(current_fan_index, 51):
            self.write_to_file("JetFans", i, "Device.Enable", "BOOL", "FALSE")
            

        # create single text line to handle jetvent fan count
        self.write_to_file("JvFanNo", "INT", self.jetvent_fan_count)


    def format_datatype(self, datatype_string):
        return("\t\t" + str(datatype_string) + "\t")


    def write_to_file(self, *args):
        """Writes a line of text to the default.txt file.
        Line is written differently depending on how many arguments are given when method is called 
        (3 or 5 arguments are both valid)

        """
        if len(args) == 5:
            self.f.write(str(args[0]) + "[" + str(args[1]) + "]." + str(args[2]) + self.format_datatype(str(args[3])) + str(args[4]) + "\n" )
        elif len(args) == 3:
            self.f.write(str(args[0]) + self.format_datatype(args[1]) + str(args[2]) + "\n" )
        else:
            self.logger.error('invalid amount of arguments used with write_to_file method')
            

    # create text per jetvent fan 
    def handle_jetvent_per_fan(self, i, fan_object):
        """Call method to write to text file for each JetVent Fan.
        Text to write to file changes depending on if current JetVent Fan should be enabled
        Args:
            i (int): JetVent Fan number

        """
        self.current_fan_sensor1_type = 0
        self.current_fan_sensor2_type = 0
        
        sensor_count = fan_object.sensors_count
        if sensor_count == 1:
    #        self.any_sensors_exist = True  #Do not need (9-7-2023 - AE)
            self.current_fan_sensor1_type = self.fan_analog_sensor_type_dict[fan_object.sensor_1_unit]
        elif sensor_count == 2:
    #        self.any_sensors_exist = True  #Do not need (9-7-2023 - AE)
            self.current_fan_sensor1_type = self.fan_analog_sensor_type_dict[fan_object.sensor_1_unit]
            self.current_fan_sensor2_type = self.fan_analog_sensor_type_dict[fan_object.sensor_2_unit]

        # enable these jetvent fans
        print(f'i: {i} || fan count: {self.jetvent_fan_count}')
        print(f'address fantype list: {self.address_fantype_list[i-1][0]}')
        self.write_to_file("JetFans", i, "Device.Address", "UDINT", fan_object.Address )
        if self.selected_version == "3.2.7":
            self.write_to_file("JetFans", i, "Device.DisplayNumber", "INT", i )
        self.write_to_file("JetFans", i, "Device.Enable", "BOOL", "TRUE" )
        self.write_to_file("JetFans", i, "Device.MaxSpeed", "INT", self.controller_object.jetvent_max_fan_speed )
        self.write_to_file("JetFans", i, "Device.MinSpeed", "INT", self.controller_object.jetvent_min_fan_speed )
        self.write_to_file("JetFans", i, "Device.DemandMax", "INT", "100")
        self.write_to_file("JetFans", i, "Device.DemandMin", "INT", "0")
        self.write_to_file("JetFans", i, "Device.ZoneEncoded", "UINT", fan_object.Zone)
        self.write_to_file("JetFans", i, "Device.SmokeEnable", "BOOL", "FALSE" )
        self.write_to_file("JetFans", i, "Device.SmokeRelInvert", "BOOL", "FALSE" )
        self.write_to_file("JetFans", i, "Device.SmokeType", "BOOL", "TRUE" )
        self.write_to_file("JetFans", i, "Device.SensorTau", "INT", "5" )
        self.write_to_file("JetFans", i, "Device.SensorZone", "INT", fan_object.Zone )
        self.write_to_file("JetFans", i, "Device.Al_Tout", "INT", "60" )
        self.write_to_file("JetFans", i, "Device.Sensors[1].SenseType", "INT", self.current_fan_sensor1_type)
        self.write_to_file("JetFans", i, "Device.Sensors[1].MinSense", "REAL", fan_object.sensor_1_min )
        self.write_to_file("JetFans", i, "Device.Sensors[1].MaxSense", "REAL", fan_object.sensor_1_max )
        self.write_to_file("JetFans", i, "Device.Sensors[1].AlarmThreshold", "REAL", fan_object.sensor_1_max )
        self.write_to_file("JetFans", i, "Device.Sensors[1].ZoneControl", "BOOL", "TRUE" )
        self.write_to_file("JetFans", i, "Device.Sensors[2].SenseType", "INT", self.current_fan_sensor2_type )
        self.write_to_file("JetFans", i, "Device.Sensors[2].MinSense", "REAL", fan_object.sensor_2_min )
        self.write_to_file("JetFans", i, "Device.Sensors[2].MaxSense", "REAL", fan_object.sensor_2_max )
        self.write_to_file("JetFans", i, "Device.Sensors[2].AlarmThreshold", "REAL", fan_object.sensor_2_max )
        self.write_to_file("JetFans", i, "Device.Sensors[2].ZoneControl", "BOOL", "TRUE" )
        self.write_to_file("JetFans", i, "Device.Demands.COMax", "REAL", self.controller_object.hundred_percent_demand_co_ppm )
        self.write_to_file("JetFans", i, "Device.Demands.COMin", "REAL", self.controller_object.zero_percent_demand_co_ppm )
        self.write_to_file("JetFans", i, "Device.Demands.NOMax", "REAL", self.controller_object.hundred_percent_demand_no2_ppm )
        self.write_to_file("JetFans", i, "Device.Demands.NOMin", "REAL", self.controller_object.zero_percent_demand_no2_ppm )
        self.write_to_file("JetFans", i, "Device.Demands.TempMax", "REAL", self.controller_object.hundred_percent_demand_temp_celsius )
        self.write_to_file("JetFans", i, "Device.Demands.TempMin", "REAL", self.controller_object.zero_percent_demand_temp_celsius )
        self.write_to_file("JetFans", i, "ShowSensor2", "BOOL", "TRUE" )
        self.write_to_file("JetFans", i, "FanType", "INT", self.jetfans_fan_type_dict[fan_object.fan_type] )
        self.write_to_file("JetFans", i, "SubFans[1].DemandType", "INT", 0 )

        if self.jetfans_fan_type_dict[fan_object.fan_type] == 7:
            self.write_to_file("JetFans", i, "SubFans[1].FanType", "INT", fan_object.sub_fan_type)
        else:
            self.write_to_file("JetFans", i, "SubFans[1].FanType", "INT", 0 )

        self.write_to_file("JetFans", i, "SubFans[1].MaxDemand", "INT", 100 )
        self.write_to_file("JetFans", i, "SubFans[1].MaxSpeed", "INT", 100 )
        self.write_to_file("JetFans", i, "SubFans[1].MinDemand", "INT", 0 )
        self.write_to_file("JetFans", i, "SubFans[1].MinSpeed", "INT", 12 )
        self.write_to_file("JetFans", i, "SubFans[2].DemandType", "INT", 0 )
        if self.jetfans_fan_type_dict[fan_object.fan_type] == 7:
#            self.write_to_file("JetFans", i, "SubFans[2].FanType", "INT", fan_object.sub_fan_type) #Duplicates Fan1 type on Fan2
             self.write_to_file("JetFans", i, "SubFans[2].FanType", "INT", 0) #changed 3/15/2024 - AE - Will change type to "none" so there is no more motor block error on fans
        else:
            self.write_to_file("JetFans", i, "SubFans[2].FanType", "INT", 0 )
        self.write_to_file("JetFans", i, "SubFans[2].MaxSpeed", "INT", 100 )
        self.write_to_file("JetFans", i, "SubFans[2].MinDemand", "INT", 0 )
        self.write_to_file("JetFans", i, "SubFans[2].MinSpeed", "INT", 0 )



    def decipher_NO_NC(self, NO_NC_value):
        if NO_NC_value == 'NC':
            return "TRUE"
        elif NO_NC_value == 'NO':
            return "FALSE"

    def decipher_boolean(self, boolean_value):
        if boolean_value == True or boolean_value == 'TRUE' or boolean_value == 'True' or boolean_value == 'true' or\
             boolean_value == 'Yes' or boolean_value == 'YES' or boolean_value == 'yes':
            return "TRUE"
        else:
            return "FALSE"

    # if gfa_fire = true; change to N/C (Fire_DIn_Inv = True)
    def fire_stop(self):
        self.write_to_file("Fire_DIn_Inv", "BOOL", self.decipher_NO_NC(self.controller_object.fire_mode_active_GFA))
            
        self.write_to_file("FireRun_DIn_Inv", "BOOL", self.decipher_NO_NC(self.controller_object.fire_mode_jetvent_fan_run))

        self.write_to_file("FireStop_DIn_Inv", "BOOL", self.decipher_NO_NC(self.controller_object.fire_mode_jetvent_fan_stop))


    def manual_override(self):
        self.write_to_file("ExhFan_FrcSpeed", "INT", self.controller_object.manual_override_exhaust_fan_speed)
        self.write_to_file("JVFan_FrcSpeed", "INT", self.controller_object.manual_override_jetvent_fan_speed)
        self.write_to_file("SupFan_FrcSpeed", "INT", self.controller_object.manual_override_supply_fan_speed)
        self.write_to_file("JVFan_FailSafe", "INT", "1")
        self.write_to_file("SupFan_FailSafe", "INT", "1")
        self.write_to_file("ExhFan_FailSafe", "INT", "1")


    def handle_purge(self):
        if self.controller_object.purge_enable_checkbox:
            self.write_to_file("PurgeRun", "INT", self.controller_object.purge_system_run_duration_time_minutes)
            self.write_to_file("PurgeSpeed", "REAL", self.controller_object.purge_system_run_speed_demand_percentage)
            self.write_to_file("PurgeTime", "UNIT", self.controller_object.purge_system_activation_time_hour)
        else:
            self.write_to_file("PurgeRun", "INT", 0)
            self.write_to_file("PurgeSpeed", "REAL", 0)
            self.write_to_file("PurgeTime", "UNIT", 10)


    def handle_miscellaneous(self):
        self.write_to_file("ScheduleNo", "INT", 0)
        self.write_to_file("ExhFan_FireSpeed_2", "INT", self.controller_object.fire_mode_exhaust_fan_speed)
        self.write_to_file("SupFan_FireSpeed_2", "INT", self.controller_object.fire_mode_supply_fan_speed)
        self.write_to_file("JVFan_FireSpeed_2", "INT", self.controller_object.fire_mode_jetvent_fan_speed)
        self.write_to_file("JVFan_FireSpeed_3", "INT", self.controller_object.fire_mode_jetvent_fan_speed)
        self.write_to_file("SmokePurgeDel", "INT", self.controller_object.smoke_detection_system_trigger_delay_seconds)
        self.write_to_file("ExhFan_SmokeSpeed", "INT", 100)
        self.write_to_file("SupFan_SmokeSpeed", "INT", 0)
        self.write_to_file("JVFan_SmokeSpeed", "INT", 0)
        self.write_to_file("SmokeSpeed", "INT", 30)
        self.write_to_file("SmokePurgeTime", "INT", self.controller_object.smoke_detector_system_keep_off_delay_minutes)
        self.write_to_file("SmokeZoned", "BOOL", self.decipher_boolean(self.controller_object.smoke_detector_system_restrict_response_to_affected_zone))
        self.write_to_file("DT_Smoke", "INT", 5)
        self.write_to_file("FieldBus_baud", "UDINT", 19200)
        self.write_to_file("FanGrpNo", "INT", self.total_zone_controller)
        self.write_to_file("GeneralMng.En_Buzz", "BOOL", "FALSE")
        self.write_to_file("GlobalFanDemandLvl", "BOOL", self.decipher_boolean(self.controller_object.fan_demand_level))
        self.write_to_file("GlobalFanDemandType", "BOOL", self.decipher_boolean(self.controller_object.fan_demand_type))
        self.write_to_file("GlobalSensorDemandLvl", "BOOL", self.decipher_boolean(self.controller_object.sensor_demand_level))

        
    def write_controller_type(self):
        if not self.mini_selected: 
            self.write_to_file("MiniMechAll", "BOOL", "FALSE")
        else:
            self.write_to_file("MiniMechAll", "BOOL", "TRUE")


    def handle_global_variables(self):
        self.write_to_file("BMSType", "INT", "0")
        self.write_to_file("GlobalDemandValues.COMin", "REAL", self.controller_object.zero_percent_demand_co_ppm)
        self.write_to_file("GlobalDemandValues.COMax", "REAL", self.controller_object.hundred_percent_demand_co_ppm)
        self.write_to_file("GlobalDemandValues.CO2Min", "REAL", self.controller_object.zero_percent_demand_co2_ppm)
        self.write_to_file("GlobalDemandValues.CO2Max", "REAL", self.controller_object.hundred_percent_demand_co2_ppm)
        self.write_to_file("GlobalDemandValues.NOMin", "REAL", self.controller_object.zero_percent_demand_no2_ppm)
        self.write_to_file("GlobalDemandValues.NOMax", "REAL", self.controller_object.hundred_percent_demand_no2_ppm)
        self.write_to_file("GlobalDemandValues.TempMin", "REAL", self.controller_object.zero_percent_demand_temp_celsius)
        self.write_to_file("GlobalDemandValues.TempMax", "REAL", self.controller_object.hundred_percent_demand_temp_celsius)
        self.write_to_file("GlobalDemandValues.DemandMin", "REAL", self.controller_object.zero_percent_demand_external_percent)
        self.write_to_file("GlobalDemandValues.DemandMax", "REAL", self.controller_object.hundred_percent_demand_external_percent)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.COMin", "REAL", self.controller_object.zero_percent_demand_co_ppm)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.COMax", "REAL", self.controller_object.hundred_percent_demand_co_ppm)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.CO2Min", "REAL", self.controller_object.zero_percent_demand_co2_ppm)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.CO2Max", "REAL", self.controller_object.hundred_percent_demand_co2_ppm)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.DemandMin", "REAL", self.controller_object.zero_percent_demand_external_percent)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.DemandMax", "REAL", self.controller_object.hundred_percent_demand_external_percent)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.NOMin", "REAL", self.controller_object.zero_percent_demand_no2_ppm)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.NOMax", "REAL", self.controller_object.hundred_percent_demand_no2_ppm)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.TempMin", "REAL", self.controller_object.zero_percent_demand_temp_celsius)
        self.write_to_file("ExpansionBoard[0].DeviceDemand.TempMax", "REAL", self.controller_object.hundred_percent_demand_temp_celsius)

    #removed logic to see if sensors exist and changed so relay is always set to be on and trip from sensor alarms
    def relay_1_function(self):

        self.write_to_file("ExpansionBoard[0].FaultRelays[1].RelayFunction", "INT", 3)
        self.write_to_file("ExpansionBoard[0].FaultRelays[1].PolarityNC", "BOOL", "FALSE")

    #Added logic to zone variable and FanType variable type for all outputs (9-7-2023 - AE+BS)
    def handle_expansion_boards(self):
        # Y1 - Y4 Analog Outputs on the Controller
        if self.controller_input_output_object.y1_analog_fan_type != 3:
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "MinSpeed", "INT", self.controller_input_output_object.y1_min_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "MaxSpeed", "INT", self.controller_input_output_object.y1_max_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "DemandMin", "INT", self.controller_input_output_object.y1_min_demand_range_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "DemandMax", "INT", self.controller_input_output_object.y1_max_demand_range_percent)

            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "FanType", "INT", self.controller_input_output_object.y1_analog_fan_type)
            if "3.4" in self.selected_version:
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y1_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "FanType", "USINT", self.controller_input_output_object.y1_analog_fan_type)
            self.write_to_file("ExpansionBoard[0].EnableRelays[1].RelayFunction", "INT", 1)
        else:
            self.write_to_file("ExpansionBoard[0].EnableRelays[1].RelayFunction", "INT", 0)
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "FanType", "INT", 3)

            if "3.4" in self.selected_version:
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y1_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 1, "FanType", "USINT", 3)

        if self.controller_input_output_object.y2_analog_fan_type != 3:
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "MinSpeed", "INT", self.controller_input_output_object.y2_min_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "MaxSpeed", "INT", self.controller_input_output_object.y2_max_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "DemandMin", "INT", self.controller_input_output_object.y2_min_demand_range_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "DemandMax", "INT", self.controller_input_output_object.y2_max_demand_range_percent)
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "FanType", "INT", self.controller_input_output_object.y2_analog_fan_type)
            if "3.4" in self.selected_version:
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y2_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "FanType", "USINT", self.controller_input_output_object.y2_analog_fan_type)
            self.write_to_file("ExpansionBoard[0].EnableRelays[2].RelayFunction", "INT", 2)
        else:
            self.write_to_file("ExpansionBoard[0].EnableRelays[2].RelayFunction", "INT", 0)
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "FanType", "INT", 3)
            if "3.4" in self.selected_version:    
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y2_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 2, "FanType", "USINT", 3)

        if self.controller_input_output_object.y3_analog_fan_type != 3:
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "MinSpeed", "INT", self.controller_input_output_object.y3_min_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "MaxSpeed", "INT", self.controller_input_output_object.y3_max_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "DemandMin", "INT", self.controller_input_output_object.y3_min_demand_range_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "DemandMax", "INT", self.controller_input_output_object.y3_max_demand_range_percent)
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "FanType", "INT", self.controller_input_output_object.y3_analog_fan_type)
            if "3.4" in self.selected_version:  
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y3_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "FanType", "USINT", self.controller_input_output_object.y3_analog_fan_type)
            self.write_to_file("ExpansionBoard[0].EnableRelays[3].RelayFunction", "INT", 3)
        else:
            self.write_to_file("ExpansionBoard[0].EnableRelays[3].RelayFunction", "INT", 0)
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "FanType", "INT", 3)
            if "3.4" in self.selected_version:
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y3_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 3, "FanType", "USINT", 3)


        if self.controller_input_output_object.y4_analog_fan_type != 3:
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "MinSpeed", "INT", self.controller_input_output_object.y4_min_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "MaxSpeed", "INT", self.controller_input_output_object.y4_max_fan_speed_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "DemandMin", "INT", self.controller_input_output_object.y4_min_demand_range_percent)
            self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "DemandMax", "INT", self.controller_input_output_object.y4_max_demand_range_percent)
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "FanType", "INT", self.controller_input_output_object.y4_analog_fan_type)
            if "3.4" in self.selected_version:
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y4_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "FanType", "USINT", self.controller_input_output_object.y4_analog_fan_type)

        else:
            if self.selected_version == "3.2.7":
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "Zones[6]", "BOOL", "FALSE")
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "FanType", "INT", 3)
            if "3.4" in self.selected_version:
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "ZoneEncoded", "UINT", self.output_y1_y4_zone.y4_zone)
                self.write_to_file("ExpansionBoard[0].AnalogueOut", 4, "FanType", "USINT", 3)


        # Analog Sensors U1-U4 on the Controller
        # ---> Get data from database
        # ------>> Get analog sensor ids from the ControllerInputOutput Table
        u1_analog_sensor_object = AnalogSensors.query.filter_by(id=self.controller_input_output_object.u1_analog_sensor_id).first()
        if u1_analog_sensor_object:
            u1_analog_sensor_definitions_id = u1_analog_sensor_object.analog_sensor_definitions_id
            u1_analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=u1_analog_sensor_definitions_id).first()
            u1_sensor_type = u1_analog_sensor_definition_object.sensor_type
            u1_scaling_min = u1_analog_sensor_definition_object.scaling_min
            u1_scaling_max = u1_analog_sensor_definition_object.scaling_max
            u1_zone = u1_analog_sensor_object.zone
            u1_sensor_name = u1_analog_sensor_definition_object.definition_name
            u1_zero_percent_demand, u1_hundred_percent_demand, u1_alarm_threshold= self.getSensorConfigurationValues(u1_sensor_name)

            self.write_to_file("ExpansionBoard[0].Sensors", 1, "AlarmThreshold", "REAL", u1_alarm_threshold)
            self.write_to_file("ExpansionBoard[0].Sensors", 1, "MaxSense", "REAL", u1_scaling_max)
            self.write_to_file("ExpansionBoard[0].Sensors", 1, "MinSense", "REAL", u1_scaling_min)
            self.write_to_file("ExpansionBoard[0].Sensors", 1, "SenseType", "INT", u1_sensor_type)
            self.write_to_file("ExpansionBoard[0].Sensors", 1, "Zone", "INT", u1_zone)
            self.write_to_file("ExpansionBoard[0].Sensors", 1, "ZoneContol", "BOOL", "TRUE")
        else:
            self.write_to_file("ExpansionBoard[0].Sensors", 1, "SenseType", "INT", 0)
        
        u2_analog_sensor_object = AnalogSensors.query.filter_by(id=self.controller_input_output_object.u2_analog_sensor_id).first()
        if u2_analog_sensor_object:
            u2_analog_sensor_definitions_id = u2_analog_sensor_object.analog_sensor_definitions_id
            u2_analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=u2_analog_sensor_definitions_id).first()
            u2_sensor_type = u2_analog_sensor_definition_object.sensor_type
            u2_scaling_min = u2_analog_sensor_definition_object.scaling_min
            u2_scaling_max = u2_analog_sensor_definition_object.scaling_max
            u2_zone = u2_analog_sensor_object.zone
            u2_sensor_name = u2_analog_sensor_definition_object.definition_name
            u2_zero_percent_demand, u2_hundred_percent_demand, u2_alarm_threshold= self.getSensorConfigurationValues(u2_sensor_name)

            self.write_to_file("ExpansionBoard[0].Sensors", 2, "AlarmThreshold", "REAL", u2_alarm_threshold)
            self.write_to_file("ExpansionBoard[0].Sensors", 2, "MaxSense", "REAL", u2_scaling_max)
            self.write_to_file("ExpansionBoard[0].Sensors", 2, "MinSense", "REAL", u2_scaling_min)
            self.write_to_file("ExpansionBoard[0].Sensors", 2, "SenseType", "INT", u2_sensor_type)
            self.write_to_file("ExpansionBoard[0].Sensors", 2, "Zone", "INT", u2_zone)
            self.write_to_file("ExpansionBoard[0].Sensors", 2, "ZoneContol", "BOOL", "TRUE")
        else:
            self.write_to_file("ExpansionBoard[0].Sensors", 2, "SenseType", "INT", 0)

        u3_analog_sensor_object = AnalogSensors.query.filter_by(id=self.controller_input_output_object.u3_analog_sensor_id).first()
        if u3_analog_sensor_object:
            u3_analog_sensor_definitions_id = u3_analog_sensor_object.analog_sensor_definitions_id
            u3_analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=u3_analog_sensor_definitions_id).first()
            u3_sensor_type = u3_analog_sensor_definition_object.sensor_type
            u3_scaling_min = u3_analog_sensor_definition_object.scaling_min
            u3_scaling_max = u3_analog_sensor_definition_object.scaling_max
            u3_zone = u3_analog_sensor_object.zone
            u3_sensor_name = u3_analog_sensor_definition_object.definition_name
            u3_zero_percent_demand, u3_hundred_percent_demand, u3_alarm_threshold= self.getSensorConfigurationValues(u3_sensor_name)

            self.write_to_file("ExpansionBoard[0].Sensors", 3, "AlarmThreshold", "REAL", u3_alarm_threshold)
            self.write_to_file("ExpansionBoard[0].Sensors", 3, "MaxSense", "REAL", u3_scaling_max)
            self.write_to_file("ExpansionBoard[0].Sensors", 3, "MinSense", "REAL", u3_scaling_min)
            self.write_to_file("ExpansionBoard[0].Sensors", 3, "SenseType", "INT", u3_sensor_type)
            self.write_to_file("ExpansionBoard[0].Sensors", 3, "Zone", "INT", u3_zone)
            self.write_to_file("ExpansionBoard[0].Sensors", 3, "ZoneContol", "BOOL", "TRUE")
        else:
            self.write_to_file("ExpansionBoard[0].Sensors", 3, "SenseType", "INT", 0)

        u4_analog_sensor_object = AnalogSensors.query.filter_by(id=self.controller_input_output_object.u4_analog_sensor_id).first()
        if u4_analog_sensor_object:
            u4_analog_sensor_definitions_id = u4_analog_sensor_object.analog_sensor_definitions_id
            u4_analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=u4_analog_sensor_definitions_id).first()
            u4_sensor_type = u4_analog_sensor_definition_object.sensor_type
            u4_scaling_min = u4_analog_sensor_definition_object.scaling_min
            u4_scaling_max = u4_analog_sensor_definition_object.scaling_max
            u4_zone = u4_analog_sensor_object.zone
            u4_sensor_name = u4_analog_sensor_definition_object.definition_name
            u4_zero_percent_demand, u4_hundred_percent_demand, u4_alarm_threshold= self.getSensorConfigurationValues(u4_sensor_name)

            self.write_to_file("ExpansionBoard[0].Sensors", 4, "AlarmThreshold", "REAL", u4_alarm_threshold)
            self.write_to_file("ExpansionBoard[0].Sensors", 4, "MaxSense", "REAL", u4_scaling_max)
            self.write_to_file("ExpansionBoard[0].Sensors", 4, "MinSense", "REAL", u4_scaling_min)
            self.write_to_file("ExpansionBoard[0].Sensors", 4, "SenseType", "INT", u4_sensor_type)
            self.write_to_file("ExpansionBoard[0].Sensors", 4, "Zone", "INT", u4_zone)
            self.write_to_file("ExpansionBoard[0].Sensors", 4, "ZoneContol", "BOOL", "TRUE")
        else:
            self.write_to_file("ExpansionBoard[0].Sensors", 4, "SenseType", "INT", 0)
            
    #Added logic to zone variable and FanType variable type for all inputs (9-7-2023 - AE+BS)
        # Expansion Boards 1-5
        self.write_to_file("ExpBoardNo", "INT", self.expansion_board_count)
        expansion_board_index = 1
        for expansion_board_object in self.expansion_board_objects:
            # handle sensors
            exp_board_u1_analog_sensor_id = expansion_board_object.u1_analog_sensor_id
            exp_board_u2_analog_sensor_id = expansion_board_object.u2_analog_sensor_id
            exp_board_u3_analog_sensor_id = expansion_board_object.u3_analog_sensor_id
            exp_board_u4_analog_sensor_id = expansion_board_object.u4_analog_sensor_id
            zone_outputs_object = OutputZones.query.filter_by(ExpansionBoaard_inout_id=expansion_board_object.id).first()

            if exp_board_u1_analog_sensor_id:
                analog_sensor_object = AnalogSensors.query.filter_by(id=exp_board_u1_analog_sensor_id).first()
                exp_board_u1_definition_id = analog_sensor_object.analog_sensor_definitions_id
                analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=exp_board_u1_definition_id).first()
                exp_board_u1_sensor_type = analog_sensor_definition_object.sensor_type
                exp_board_u1_scaling_min = analog_sensor_definition_object.scaling_min
                exp_board_u1_scaling_max = analog_sensor_definition_object.scaling_max
                exp_board_u1_zone = analog_sensor_object.zone
                exp_u1_sensor_name = analog_sensor_definition_object.definition_name
                exp_u1_zero_percent_demand, exp_u1_hundred_percent_demand, exp_u1_alarm_threshold= self.getSensorConfigurationValues(exp_u1_sensor_name)

                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 1, "SenseType", "INT", exp_board_u1_sensor_type)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 1, "MinSense", "REAL", exp_board_u1_scaling_min)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 1, "MaxSense", "REAL", exp_board_u1_scaling_max)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 1, "AlarmThreshold", "REAL", exp_u1_alarm_threshold)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 1, "Zone", "INT", exp_board_u1_zone)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 1, "ZoneControl", "BOOL", "TRUE")

            if exp_board_u2_analog_sensor_id:
                analog_sensor_object = AnalogSensors.query.filter_by(id=exp_board_u2_analog_sensor_id).first()
                exp_board_u2_definition_id = analog_sensor_object.analog_sensor_definitions_id
                analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=exp_board_u2_definition_id).first()
                exp_board_u2_sensor_type = analog_sensor_definition_object.sensor_type
                exp_board_u2_scaling_min = analog_sensor_definition_object.scaling_min
                exp_board_u2_scaling_max = analog_sensor_definition_object.scaling_max
                exp_board_u2_zone = analog_sensor_object.zone
                exp_u2_sensor_name = analog_sensor_definition_object.definition_name
                exp_u2_zero_percent_demand, exp_u2_hundred_percent_demand, exp_u2_alarm_threshold= self.getSensorConfigurationValues(exp_u2_sensor_name)
            
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 2, "SenseType", "INT", exp_board_u2_sensor_type)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 2, "MinSense", "REAL", exp_board_u2_scaling_min)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 2, "MaxSense", "REAL", exp_board_u2_scaling_max)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 2, "AlarmThreshold", "REAL", exp_u2_alarm_threshold)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 2, "Zone", "INT", exp_board_u2_zone)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 2, "ZoneControl", "BOOL", "TRUE")

            if exp_board_u3_analog_sensor_id:
                analog_sensor_object = AnalogSensors.query.filter_by(id=exp_board_u3_analog_sensor_id).first()
                exp_board_u3_definition_id = analog_sensor_object.analog_sensor_definitions_id
                analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=exp_board_u3_definition_id).first()
                exp_board_u3_sensor_type = analog_sensor_definition_object.sensor_type
                exp_board_u3_scaling_min = analog_sensor_definition_object.scaling_min
                exp_board_u3_scaling_max = analog_sensor_definition_object.scaling_max
                exp_board_u3_zone = analog_sensor_object.zone
                exp_u3_sensor_name = analog_sensor_definition_object.definition_name
                exp_u3_zero_percent_demand, exp_u3_hundred_percent_demand, exp_u3_alarm_threshold= self.getSensorConfigurationValues(exp_u3_sensor_name)

                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 3, "SenseType", "INT", exp_board_u3_sensor_type)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 3, "MinSense", "REAL", exp_board_u3_scaling_min)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 3, "MaxSense", "REAL", exp_board_u3_scaling_max)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 3, "AlarmThreshold", "REAL", exp_u3_alarm_threshold)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 3, "Zone", "INT", exp_board_u3_zone)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 3, "ZoneControl", "BOOL", "TRUE")
            
            if exp_board_u4_analog_sensor_id:
                analog_sensor_object = AnalogSensors.query.filter_by(id=exp_board_u4_analog_sensor_id).first()
                exp_board_u4_definition_id = analog_sensor_object.analog_sensor_definitions_id
                analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=exp_board_u4_definition_id).first()
                exp_board_u4_sensor_type = analog_sensor_definition_object.sensor_type
                exp_board_u4_scaling_min = analog_sensor_definition_object.scaling_min
                exp_board_u4_scaling_max = analog_sensor_definition_object.scaling_max
                exp_board_u4_zone = analog_sensor_object.zone
                exp_u4_sensor_name = analog_sensor_definition_object.definition_name
                exp_u4_zero_percent_demand, exp_u4_hundred_percent_demand, exp_u4_alarm_threshold= self.getSensorConfigurationValues(exp_u4_sensor_name)

                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 4, "SenseType", "INT", exp_board_u4_sensor_type)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 4, "MinSense", "REAL", exp_board_u4_scaling_min)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 4, "MaxSense", "REAL", exp_board_u4_scaling_max)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 4, "AlarmThreshold", "REAL", exp_u4_alarm_threshold)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 4, "Zone", "INT", exp_board_u4_zone)
                self.write_to_file("ExpansionBoard[" + str(expansion_board_index) + "].Sensors", 4, "ZoneControl", "BOOL", "TRUE")



            #  handle analog fans
            if self.selected_version == "3.2.7":
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "Zones[6]", "BOOL", "FALSE")
            if "3.4" in self.selected_version:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "ZoneEncoded", "UINT", zone_outputs_object.u8_zone)
            
            if self.selected_version == "3.2.7":    
                if expansion_board_object.u8_analog_fan_type == 0 or expansion_board_object.u8_analog_fan_type:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "MinSpeed", "INT", expansion_board_object.u8_min_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "MaxSpeed", "INT", expansion_board_object.u8_max_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "DemandMin", "INT", expansion_board_object.u8_min_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "DemandMax", "INT", expansion_board_object.u8_max_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "FanType", "INT", expansion_board_object.u8_analog_fan_type)
                else:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "FanType", "INT", 3)
            if "3.4" in self.selected_version:
                if expansion_board_object.u8_analog_fan_type == 0 or expansion_board_object.u8_analog_fan_type:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "MinSpeed", "INT", expansion_board_object.u8_min_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "MaxSpeed", "INT", expansion_board_object.u8_max_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "DemandMin", "INT", expansion_board_object.u8_min_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "DemandMax", "INT", expansion_board_object.u8_max_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "FanType", "USINT", expansion_board_object.u8_analog_fan_type)
                else:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 1, "FanType", "USINT", 3)
            if expansion_board_object.u8_analog_fan_type == 3:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].DigIns[5].DigFunction", "INT", 0)
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut[1].RunIndicationEnabled", "BOOL", "FALSE")
            else:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].DigIns[5].DigFunction", "INT", 11)
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut[1].RunIndicationEnabled", "BOOL", "TRUE")
            
            if self.selected_version == "3.2.7":
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "Zones[6]", "BOOL", "FALSE")
            if "3.4" in self.selected_version:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "ZoneEncoded", "UINT", zone_outputs_object.u9_zone)

            if self.selected_version == "3.2.7":    
                if expansion_board_object.u9_analog_fan_type == 0 or expansion_board_object.u9_analog_fan_type:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "MinSpeed", "INT", expansion_board_object.u9_min_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "MaxSpeed", "INT", expansion_board_object.u9_max_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "DemandMin", "INT", expansion_board_object.u9_min_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "DemandMax", "INT", expansion_board_object.u9_max_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "FanType", "INT", expansion_board_object.u9_analog_fan_type)
                else:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "FanType", "INT", 3)
            if "3.4" in self.selected_version:
                if expansion_board_object.u9_analog_fan_type == 0 or expansion_board_object.u9_analog_fan_type:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "MinSpeed", "INT", expansion_board_object.u9_min_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "MaxSpeed", "INT", expansion_board_object.u9_max_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "DemandMin", "INT", expansion_board_object.u9_min_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "DemandMax", "INT", expansion_board_object.u9_max_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "FanType", "USINT", expansion_board_object.u9_analog_fan_type)
                else:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 2, "FanType", "USINT", 3)

            if expansion_board_object.u9_analog_fan_type == 3:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].DigIns[6].DigFunction", "INT", 0)
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut[2].RunIndicationEnabled", "BOOL", "FALSE")
            else:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].DigIns[6].DigFunction", "INT", 11)
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut[2].RunIndicationEnabled", "BOOL", "TRUE")
            
            if self.selected_version == "3.2.7":
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "Zones[1]", "BOOL", "TRUE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "Zones[2]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "Zones[3]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "Zones[4]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "Zones[5]", "BOOL", "FALSE")
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "Zones[6]", "BOOL", "FALSE")
            if "3.4" in self.selected_version:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "ZoneEncoded", "UINT", zone_outputs_object.u10_zone)

            if self.selected_version == "3.2.7":    
                if expansion_board_object.u10_analog_fan_type == 0 or expansion_board_object.u10_analog_fan_type:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "MinSpeed", "INT", expansion_board_object.u10_min_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "MaxSpeed", "INT", expansion_board_object.u10_max_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "DemandMin", "INT", expansion_board_object.u10_min_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "DemandMax", "INT", expansion_board_object.u10_max_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "FanType", "INT", expansion_board_object.u10_analog_fan_type)
                else:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "FanType", "INT", 3)
            if "3.4" in self.selected_version:
                if expansion_board_object.u10_analog_fan_type == 0 or expansion_board_object.u10_analog_fan_type:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "MinSpeed", "INT", expansion_board_object.u10_min_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "MaxSpeed", "INT", expansion_board_object.u10_max_fan_speed_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "DemandMin", "INT", expansion_board_object.u10_min_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "DemandMax", "INT", expansion_board_object.u10_max_demand_range_percent)
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "FanType", "USINT", expansion_board_object.u10_analog_fan_type)
                else:
                    self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut", 3, "FanType", "USINT", 3)

            if expansion_board_object.u10_analog_fan_type == 3:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].DigIns[7].DigFunction", "INT", 0)
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut[3].RunIndicationEnabled", "BOOL", "FALSE")
            else:
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].DigIns[7].DigFunction", "INT", 11)
                self.write_to_file(f"ExpansionBoard[{expansion_board_index}].AnalogueOut[3].RunIndicationEnabled", "BOOL", "TRUE")

            expansion_board_index += 1


    def write_int(self, fan_number, modbus_address):
        self.f.write("JetFans[" + str(fan_number) + "].Device.Address" + self.format_datatype("UDINT") + str(modbus_address) + "\n")

    
    def set_timezone(self, timezone):
        # 11 -> Chicago
        self.write_to_file("GeneralMng.Zone_1", "UINT", timezone)
        self.write_to_file("GeneralMng.SetTimezone", "BOOL", "TRUE")


    def handle_fbsensors(self):
        """Call text creation for each fieldbus Sensor."""
        self.fbsensor_count = FieldbusSensors.query.filter_by(controller_id=self.controller_id).count()
        self.fieldbus_sensor_sense_type_index_dict = self.carel_key_values_dict['fieldbus_sensor_sense_type_index']

        if self.fbsensor_count > 0:
            fieldbus_sensor_objects = FieldbusSensors.query.filter_by(controller_id=self.controller_id).all()
            fieldbus_index = 1

            for fieldbus_sensor_object in fieldbus_sensor_objects:
                fieldbus_sensor_id = fieldbus_sensor_object.id

                self.handle_fbsensor_per_fan(fieldbus_sensor_id, fieldbus_index)

                fieldbus_index += 1


        # calculate how many fb sensors, i.e. modbus address 20 - modbus address 10 + 1 = 11 total fb sensors
        self.logger.info('fbsensor count: %s', self.fbsensor_count)

        # create single text line to handle jetvent fan count
        self.write_to_file("FBSenseNo", "INT", self.fbsensor_count)

    
    # create text per fb sensor 
    def handle_fbsensor_per_fan(self, fieldbus_sensor_id, fieldbus_index):
        """Call method to write to text file for each fb sensor.
        Text to write to file changes depending on if current fb sensor should be enabled
        Args:
            i (int): FB Sensor number

        """

        fieldbus_sensor_object = FieldbusSensors.query.filter_by(id=fieldbus_sensor_id).first()
        fb_modbus_address = fieldbus_sensor_object.modbus_address
        fieldbus_sensor_definitions_id = fieldbus_sensor_object.fieldbus_sensor_definitions_id
        fieldbus_sensor_zone = fieldbus_sensor_object.zone

        fieldbus_sensor_definition_object = FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first()

        # enable these jetvent fans
        self.write_to_file("FBSensors", fieldbus_index, "Address", "UDINT", fb_modbus_address)
        self.write_to_file("FBSensors", fieldbus_index, "AlarmDelay", "INT", 60)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.CO2Max", "REAL", self.controller_object.hundred_percent_demand_co2_ppm)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.CO2Min", "REAL", self.controller_object.zero_percent_demand_co2_ppm)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.COMax", "REAL", self.controller_object.hundred_percent_demand_co_ppm)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.COMin", "REAL", self.controller_object.zero_percent_demand_co_ppm)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.DemandMax", "REAL", self.controller_object.hundred_percent_demand_external_percent)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.DemandMin", "REAL", self.controller_object.zero_percent_demand_external_percent)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.NOMax", "REAL", self.controller_object.hundred_percent_demand_no2_ppm)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.NOMin", "REAL", self.controller_object.zero_percent_demand_no2_ppm)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.RHMax", "REAL", 100.0)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.RHMin", "REAL", 80.0)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.TempMax", "REAL", self.controller_object.hundred_percent_demand_temp_celsius)
        self.write_to_file("FBSensors", fieldbus_index, "Demands.TempMin", "REAL", self.controller_object.zero_percent_demand_temp_celsius)
        self.write_to_file("FBSensors", fieldbus_index, "FilterTime", "INT", 10)
        self.write_to_file("FBSensors", fieldbus_index, "GlobalDemand", "BOOL", "FALSE")

        zero_percent_demand, hundred_percent_demand, sensor_1_alarm_threshold= self.getSensorConfigurationValues(fieldbus_sensor_definition_object.sensor_1_type)

        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[1].DirectControl", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_1_control))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[1].Enabled", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_1_enable))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[1].Present", "BOOL", "TRUE" )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[1].SenseTypeIndex", "INT", self.fieldbus_sensor_sense_type_index_dict[fieldbus_sensor_definition_object.sensor_1_type] )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[1].Threshold", "REAL", sensor_1_alarm_threshold)

        zero_percent_demand, hundred_percent_demand, sensor_2_alarm_threshold= self.getSensorConfigurationValues(fieldbus_sensor_definition_object.sensor_2_type)
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[2].DirectControl", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_2_control))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[2].Enabled", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_2_enable ))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[2].Present", "BOOL", "TRUE" )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[2].SenseTypeIndex", "INT", self.fieldbus_sensor_sense_type_index_dict[fieldbus_sensor_definition_object.sensor_2_type] )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[2].Threshold", "REAL", sensor_2_alarm_threshold )

        zero_percent_demand, hundred_percent_demand, sensor_3_alarm_threshold= self.getSensorConfigurationValues(fieldbus_sensor_definition_object.sensor_3_type)
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[3].DirectControl", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_3_control))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[3].Enabled", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_3_enable ))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[3].Present", "BOOL", "TRUE" )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[3].SenseTypeIndex", "INT", self.fieldbus_sensor_sense_type_index_dict[fieldbus_sensor_definition_object.sensor_3_type] )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[3].Threshold", "REAL", sensor_3_alarm_threshold)

        zero_percent_demand, hundred_percent_demand, sensor_4_alarm_threshold= self.getSensorConfigurationValues(fieldbus_sensor_definition_object.sensor_4_type)
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[4].DirectControl", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_4_control))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[4].Enabled", "BOOL", self.decipher_boolean(fieldbus_sensor_definition_object.sensor_4_enable ))
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[4].Present", "BOOL", "TRUE" )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[4].SenseTypeIndex", "INT", self.fieldbus_sensor_sense_type_index_dict[fieldbus_sensor_definition_object.sensor_4_type] )
        self.write_to_file("FBSensors", fieldbus_index, "SenseUnits[4].Threshold", "REAL", sensor_4_alarm_threshold )

        self.write_to_file("FBSensors", fieldbus_index, "SensorTypeIndex", "INT", 4)
        self.write_to_file("FBSensors", fieldbus_index, "SensorZone", "INT", fieldbus_sensor_zone)



        
    
def createDefaultTxtFile(sales_order_id, controller_id, controller_serial_numer):

    c = createConfigFileClass(sales_order_id, controller_id, controller_serial_numer)

    c.build_file()
    configuration_directory, modbus_ranges_string = c.return_class_variables()

    return configuration_directory, modbus_ranges_string