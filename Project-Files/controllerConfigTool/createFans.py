from controllerConfigTool.models import ExpansionBoardInputOutput
from controllerConfigTool import db



def createExpansionBoards(sales_order_id, add_expansionboard_count):

    for i in range(0, int(add_expansionboard_count)):
        expansion_board_object = ExpansionBoardInputOutput(
            sales_order_id = sales_order_id
        )
        db.session.add(expansion_board_object)
        
    db.session.commit()
