from controllerConfigTool.models import AnalogSensors
from controllerConfigTool import db




def createAnalogSensors(sales_order_id, sensor_count, previous_count):
    zone=1
    starting_value = previous_count + 1

    ending_value = starting_value + int(sensor_count)

    for current_address in range(starting_value, ending_value):
        analogsensor = AnalogSensors(
                        sales_order_id = sales_order_id,
                        modbus_address = current_address,
                        zone=zone

                    )
        db.session.add(analogsensor)
    
    db.session.commit()
