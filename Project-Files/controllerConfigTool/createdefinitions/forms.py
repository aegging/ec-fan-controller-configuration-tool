# This file is changed by CG Developers on 01/05/2023. Created new form for other2
from controllerConfigTool.createdefinitions.utils import getCarelKeyValues_AnalogSensors
from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField
from wtforms.validators import DataRequired
from wtforms import StringField,  SubmitField, IntegerField, BooleanField, SelectField
from wtforms.validators import DataRequired
from wtforms.validators import DataRequired, Optional
from sqlalchemy import text
from controllerConfigTool.models import FanValues, Sensors
from controllerConfigTool import db



def init_CustomAnalogSensorDefinitionForm(sensor_type_default_value, min_value, max_value, alarm_threshold_value):
    sensor_type_choices= getCarelKeyValues_AnalogSensors()

    class CustomAnalogSensorDefinitionForm(FlaskForm):
        definition_name = StringField('Definition Name', validators=[DataRequired()])
        standard_boolean = BooleanField('Standard Definition', validators=[Optional()])
        sensor_type = SelectField('Sensor Type', default=sensor_type_default_value, choices=sensor_type_choices)
        scaling_min = IntegerField('Min Scaling', default=min_value, validators=[Optional()])
        scaling_max = IntegerField('Max Scaling', default=max_value, validators=[Optional()])
        alarm_threshold = IntegerField('Alarm Threshold', default=alarm_threshold_value, validators=[Optional()])
        submit_button = SubmitField('Create Definition')
    return CustomAnalogSensorDefinitionForm


def init_CustomFieldbusSensorDefinitionForm(sensor_type_count, form_data_dictionary, fieldbus_sensor_type_id):
    def get_sensor_type_list():
        # find data from sensors table where SignalType = 'Modbus'
        sensor_objects = Sensors.query.filter_by(signal_type='Modbus').all()
        sensor_type_tuple_list = []
        for sensor_object in sensor_objects:
            sensor_type_tuple_list.append([sensor_object.id, sensor_object.name])
        return sensor_type_tuple_list

    class CustomFieldbusSensorDefinitionForm(FlaskForm):
        definition_name = StringField('Definition Name', validators=[DataRequired()])
        standard_boolean = BooleanField('Standard Definition', validators=[Optional()])
        sensor_type = SelectField('Fieldbus Sensor', default=fieldbus_sensor_type_id, choices=get_sensor_type_list())
        submit_button = SubmitField('Create Definition')
        pass

    for i in range(1, sensor_type_count + 1):
        setattr(CustomFieldbusSensorDefinitionForm, f'sensor_{i}_type', StringField(default=form_data_dictionary[f'sensor_{i}_type'], render_kw={'readonly': True}) )
        setattr(CustomFieldbusSensorDefinitionForm, f'sensor_{i}_enable', SelectField(choices=['Yes', 'No']) )
        setattr(CustomFieldbusSensorDefinitionForm, f'sensor_{i}_control', SelectField(choices=['Yes', 'No']) )

    return CustomFieldbusSensorDefinitionForm
  

def initializeCreateFanDefinitionForm():
    stmt = text("Select * From fans Where current='True'")
    fan_values_objects = db.session.query(FanValues).from_statement(stmt).all()
    standard_fans = [""]
    for fan_values in fan_values_objects:
        standard_fans.append(fan_values.name)


    class CreateFanDefinitionForm(FlaskForm):
        standard_type = SelectField('Select Motor Type', choices=standard_fans, default=standard_fans[0])
        standard = BooleanField('Standard Definition')
        definition_name = StringField('Custom Fan Definition Name')
        create_custom_fan_definition_button = SubmitField('Create Custom Fan Definition')
        cancel_fan_definition = SubmitField('Cancel')

    return CreateFanDefinitionForm


def initializeCustomFanDefinitionForm(fan_type, standard, definition_name):
    fan_type_value = fan_type
    standard_value = standard
    definition_name_value = definition_name
    print(f'fan_type: {fan_type_value}')
    fans_object = FanValues.query.filter_by(name=fan_type_value).first()
    sensor_count_value = fans_object.no_sensors

    sensor_count_choices = []

    for i in range(0,sensor_count_value+1):
        sensor_count_choices.append(i)

    default_single_speed_max_speed = fans_object.max_speed

    default_digital_modbus_max_speed = fans_object.max_speed
    default_digital_modbus_min_speed = 0
    default_digital_modbus_min_speed_zone = [1,2,3,4,5,6]

    default_analogue_signal_max_speed = fans_object.max_speed
    default_analogue_signal_min_speed = 0

    control_signal_choices = []
    _0_10v_value = fans_object._0_10v
    _4_20ma_value = fans_object._4_20ma

    default_local_sensor_max_speed = fans_object.max_speed
    default_local_sensor_min_speed = 0

    # if fan definition in database is set to 1 for this fan type; make available to select
    if _0_10v_value == 1:
        control_signal_choices.append('0-10V')
    if _4_20ma_value == 1:
        control_signal_choices.append('4-20mA')

    # sensors_objects = Sensors.query.filter_by(current="True").all()
    sensor_choices = []
    
    # for sensor in sensors_objects:
    #     sensor_choices.append(sensor.name)
    
    sensor_choices.append('LPT-END')
    sensor_choices.append('LPT-TCO')
    sensor_choices.append('OTHER')
        
    sensors_object = Sensors.query.filter_by(name="LPT-END").first()

    lpt_end_unit_of_measure_value = sensors_object.unit_of_measure_1
    lpt_end_min_value = int(sensors_object.min_value_1)
    lpt_end_max_value = int(sensors_object.max_value_1)
    lpt_end_current_draw_value = sensors_object.current_draw
    lpt_end_signal_type_value = sensors_object.signal_type

    print(lpt_end_unit_of_measure_value)

    sensors_object2 = Sensors.query.filter_by(name="LPT-TCO").first()

    lpt_tco_unit_of_measure_value = sensors_object2.unit_of_measure_1
    lpt_tco_min_value = int(sensors_object2.min_value_1)
    lpt_tco_max_value = int(sensors_object2.max_value_1)
    lpt_tco_current_draw_value = sensors_object2.current_draw
    lpt_tco_signal_type_value = sensors_object2.signal_type

    print(lpt_tco_unit_of_measure_value)



    class CustomFanDefinitionForm(FlaskForm):

        standard_string = StringField('Standard Definition', default=standard_value, render_kw={'readonly': True})
        definition_name = StringField('Custom Fan Definition Name', default=definition_name_value, render_kw={'readonly': True})
        fan_type = StringField('Fan Type', default=fan_type_value, render_kw={'readonly': True})
        save_definition_button = SubmitField('Save Definition')
        cancel_fan_definition = SubmitField('Cancel')


        single_speed = IntegerField('Operation Speed', default=default_single_speed_max_speed)

        digital_modbus_matching_controller = SelectField('Matching Controller', choices=["Aviator Control","BMS Control"], default="Aviator Control") 
        digital_modbus_max_fan_rpm = IntegerField('Max Fan RPM', default=default_digital_modbus_max_speed)
        digital_modbus_min_fan_rpm = IntegerField('Min Fan RPM', default=default_digital_modbus_min_speed)
        # digital_modbus_max_speed = IntegerField('Max Fan Speed (%)', default=default_digital_modbus_max_speed)
        # digital_modbus_min_speed = IntegerField('Min Fan Speed (%)', default=default_digital_modbus_min_speed)
        # digital_modbus_demand_range_min = IntegerField('Demand Range Min', default=default_digital_modbus_max_speed)
        # digital_modbus_demand_range_max = IntegerField('Demand Range Max', default=default_digital_modbus_min_speed)
        digital_modbus_zone = SelectField('Zone', choices=default_digital_modbus_min_speed_zone, default=default_digital_modbus_min_speed_zone[0])

        select_sub_fan = SelectField('SubFan Type', choices=[[0,'None'],[1,'WG1'],[2,'ZAB'], [3,'DUL1'],[4,'DUL2'],[5,'DUL3'],[6,'SPA']], default=0)

        analogue_signal_control_signal = SelectField('Control Signal', choices=control_signal_choices)
        analogue_signal_max_speed = IntegerField('Max Speed', default=default_analogue_signal_max_speed)
        analogue_signal_min_speed = IntegerField('Min Speed', default=default_analogue_signal_min_speed)

        local_sensor_set_point = IntegerField('Set Point', render_kw={'readonly': True})
        local_sensor_max_speed = IntegerField('Max Speed', default=default_local_sensor_max_speed)
        local_sensor_min_speed = IntegerField('Min Speed', default=default_local_sensor_min_speed)
        local_sensor_max_control_signal = IntegerField('Max Control Signal', default=0)
        local_sensor_min_control_signal = IntegerField('Min Control Signal', default=0)
        local_sensor_sensor_logic = SelectField('Sensor Logic', choices=["Sensor 1", "Max of Sensor 1 and 2"], default="Sensor 1")

        setup_type_selection = SelectField('Select Setup Type', choices=["Single Speed","Digital Modbus","Analogue Signal","Local Sensor"],default="Digital Modbus")

        sensors_count = SelectField('Number of Sensors:', choices=sensor_count_choices, default=sensor_count_value)
        sensor1_type = SelectField('Sensor 1 Type', choices=sensor_choices, default=sensor_choices[1]) 
        sensor2_type = SelectField('Sensor 2 Type', choices=sensor_choices, default=sensor_choices[1]) 
        smoke_detector = BooleanField('Smoke Detector')

        lpt_end_unit_of_measure = StringField('Unit of Measure', default=lpt_end_unit_of_measure_value, render_kw={'readonly': True})
        lpt_end_min_physical_value = IntegerField('Min Physical Value', default=lpt_end_min_value, render_kw={'readonly': True})
        lpt_end_max_physical_value = IntegerField('Max Physical Value', default=lpt_end_max_value, render_kw={'readonly': True})
        lpt_end_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_end_current_draw_value, render_kw={'readonly': True})
        lpt_end_signal_type = StringField('Signal Type', default=lpt_end_signal_type_value, render_kw={'readonly': True})

        lpt_tco_unit_of_measure = StringField('Unit of Measure', default=lpt_tco_unit_of_measure_value, render_kw={'readonly': True})
        lpt_tco_min_physical_value = IntegerField('Min Physical Value', default=lpt_tco_min_value, render_kw={'readonly': True})
        lpt_tco_max_physical_value = IntegerField('Max Physical Value', default=lpt_tco_max_value, render_kw={'readonly': True})
        lpt_tco_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_tco_current_draw_value, render_kw={'readonly': True})
        lpt_tco_signal_type = StringField('Signal Type', default=lpt_tco_signal_type_value, render_kw={'readonly': True})

        other_unit_of_measure = StringField('Unit of Measure', default=lpt_tco_unit_of_measure_value)
        other_min_physical_value = IntegerField('Min Physical Value', default=lpt_tco_min_value)
        other_max_physical_value = IntegerField('Max Physical Value', default=lpt_tco_max_value)
        other_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_tco_current_draw_value)
        other_signal_type = SelectField('Signal Type', choices=['0-10V','4-20mA','KTY','PT1000'], default=lpt_tco_signal_type_value)

        other2_unit_of_measure = StringField('Unit of Measure', default=lpt_tco_unit_of_measure_value)
        other2_min_physical_value = IntegerField('Min Physical Value', default=lpt_tco_min_value)
        other2_max_physical_value = IntegerField('Max Physical Value', default=lpt_tco_max_value)
        other2_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_tco_current_draw_value)
        other2_signal_type = SelectField('Signal Type', choices=['0-10V','4-20mA','KTY','PT1000'], default=lpt_tco_signal_type_value)



        digital_in_2 = BooleanField('Digital in 2 (D2/E1)', default=False)
        logic = SelectField('Logic', choices=['Active High', 'Active Low/Open'], default="Active High")
        function = SelectField('Function', choices=['Enable','Set speed override'], default="Enable")
        function_speed_override = IntegerField('Override Speed (rpm)', default=0)

        external_analogue_signal = BooleanField('External Analogue Signal (6mA max)')
        external_analogue_signal_selection = SelectField(choices=['0-10V Signal Following Request','0-10V Signal Following Actual Speed','Operating Indication (Above 50rpm 10V Digital)'], default="0-10V Signal Following Request")

        relay_polarity = SelectField(choices=['Normal Open (NO)','Normal Close (NC)'], default="Normal Close (NC)")

        comms_timeout_duration = IntegerField('Timeout Duration (seconds)', default=60)
        comms_timeout_speed = IntegerField('Timeout Speed (rpm)', default=0)


        # def validate_single_speed(self, single_speed):
        #         if single_speed.data > default_single_speed_max_speed:
        #             return ValidationError(f'Operational Speed must be lower than {default_single_speed_max_speed}rpm for fantype {fan_type}')
        #         elif single_speed.data < 0:
        #             return ValidationError(f'Operational Speed must be greater than zero')

        # def validate_digital_modbus_max_speed(self, digital_modbus_max_speed):
        #     self.current_digital_modbus_max_speed = digital_modbus_max_speed.data
        #     if digital_modbus_max_speed.data > default_digital_modbus_max_speed:
        #         return ValidationError(f'Max Speed must be lower than {default_digital_modbus_max_speed}rpm for fantype {fan_type}')
        #     elif digital_modbus_max_speed.data < 0:
        #         return ValidationError(f'Max Speed must be a non-negative value')

        # def valiate_digital_modbus_min_speed(self, digital_modbus_min_speed):
        #     if digital_modbus_min_speed.data > self.current_digital_modbus_max_speed:
        #         return ValidationError(f'Min Speed cannot be lower than Max Speed {self.current_digital_modbus_max_speed}')
        #     elif digital_modbus_min_speed.data > default_digital_modbus_max_speed:
        #         return ValidationError(f'Min Speed must be lower than {default_digital_modbus_max_speed}rpm for fantype {fan_type}')
        #     elif digital_modbus_min_speed.data < 0:
        #         return ValidationError(f'Min Speed must be a non-negative value')

    return CustomFanDefinitionForm



