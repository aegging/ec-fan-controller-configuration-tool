# This file changed by CG developer on 01/05/2023. creatfandefination and standard fan defination
from flask import Blueprint
from flask import render_template, url_for, flash, redirect, request
from controllerConfigTool import db
from controllerConfigTool.createdefinitions.forms import init_CustomAnalogSensorDefinitionForm, init_CustomFieldbusSensorDefinitionForm, initializeCreateFanDefinitionForm, initializeCustomFanDefinitionForm
from controllerConfigTool.createdefinitions.utils import createCustomFanDefinition
from controllerConfigTool.getInfoFromDatabase import findTagPrefixGivenSalesOrderID
from controllerConfigTool.models import AnalogSensorDefinitions, FanValues, FieldbusSensorDefinitions, Sensors
from controllerConfigTool.controllerconfigurations.utils import getDefaultControllerConfigurationValues
from sqlalchemy import null


createdefinitions_blueprint = Blueprint('createdefinitions', __name__)


@createdefinitions_blueprint.route('/redirect_create_analog_sensor_definition/<int:sales_order_id>/<int:sensor_type>/', methods=['GET','POST'])
def redirect_create_analog_sensor_definition(sales_order_id, sensor_type):

    default_values_dictionary = getDefaultControllerConfigurationValues()
    print(default_values_dictionary)
    # calculate for max_value and alarm_threshold
    if sensor_type == 1:
        # CO Sensor Type
        max_value = 200
        alarm_threshold = default_values_dictionary['alarm_threshold_co_ppm']
    elif sensor_type == 2:
        # NO2 Sensor Type
        max_value = 10
        alarm_threshold = default_values_dictionary['alarm_threshold_no2_ppm']
    else:
        # Give default values of zero for other sensors
        max_value = 0
        alarm_threshold = 0

    return redirect(url_for('createdefinitions.create_analog_sensor_definition',sales_order_id=sales_order_id, sensor_type=sensor_type, min_value=0, max_value=max_value, alarm_threshold=alarm_threshold))

@createdefinitions_blueprint.route('/create_analog_sensor_definition/<int:sales_order_id>/<int:sensor_type>/<int:min_value>/<int:max_value>/<int:alarm_threshold>', methods=['GET','POST'])
def create_analog_sensor_definition(sales_order_id, sensor_type, min_value, max_value, alarm_threshold):

    CustomAnalogSensorDefinitionForm = init_CustomAnalogSensorDefinitionForm(sensor_type, min_value, max_value, alarm_threshold)
    custom_analog_sensor_definition_form = CustomAnalogSensorDefinitionForm()

    if request.method == 'POST':
        if "navigate_sales_order_status_button" in request.form:
            return redirect(url_for('itemconfigurations.analog_sensor_definition_assignment', sales_order_id=sales_order_id))

        elif custom_analog_sensor_definition_form.validate_on_submit():
            if custom_analog_sensor_definition_form.submit_button.data:
                standard_data = custom_analog_sensor_definition_form.standard_boolean.data
                print('----standard----', standard_data)

                # create a new analog sensor definition
                new_fb_definition = AnalogSensorDefinitions(        
                    definition_name = custom_analog_sensor_definition_form.definition_name.data,
                    sensor_type = custom_analog_sensor_definition_form.sensor_type.data,
                    scaling_min = custom_analog_sensor_definition_form.scaling_min.data,
                    scaling_max = custom_analog_sensor_definition_form.scaling_max.data,
                    alarm_threshold = custom_analog_sensor_definition_form.alarm_threshold.data
                )
                # if it is standard definition, there is no sales order id
                if standard_data:
                    new_fb_definition.standard = True
                else:
                    new_fb_definition.standard = False
                    new_fb_definition.sales_order_id = sales_order_id

                db.session.add(new_fb_definition)
                db.session.commit()

                flash(f'Analog Sensor Definition {custom_analog_sensor_definition_form.definition_name.data} has been created', 'success')

                return redirect(url_for('itemconfigurations.analog_sensor_definition_assignment', sales_order_id=sales_order_id))

    return render_template('create_analog_sensor_definition.html', custom_analog_sensor_definition_form=custom_analog_sensor_definition_form, sales_order_id=sales_order_id)



@createdefinitions_blueprint.route('/create_fieldbus_sensor_definition/<int:sales_order_id>/<int:fieldbus_sensor_type_id>', methods=['GET','POST'])
def create_fieldbus_sensor_definition(sales_order_id, fieldbus_sensor_type_id):

    # use sensor type id to find data from sensors table
    sensor_table_object = Sensors.query.filter_by(id=fieldbus_sensor_type_id).first()
    sensor_name = sensor_table_object.name
    sensor_unit_of_measure_list = []
    if sensor_table_object.unit_of_measure_1:
        sensor_unit_of_measure_list.append(sensor_table_object.unit_of_measure_1)
    if sensor_table_object.unit_of_measure_2:
        sensor_unit_of_measure_list.append(sensor_table_object.unit_of_measure_2)
    if sensor_table_object.unit_of_measure_3:
        sensor_unit_of_measure_list.append(sensor_table_object.unit_of_measure_3)
    if sensor_table_object.unit_of_measure_4:
        sensor_unit_of_measure_list.append(sensor_table_object.unit_of_measure_4)

    print(f'sensor_unit_of_measure_list: {sensor_unit_of_measure_list}')
    converted_sensor_unit_of_measure_list = []

    for sensor_unit_of_measure in sensor_unit_of_measure_list:
        # for each unit of measure, save unit value
        # allow user to select enable or control
        if 'ppm ' in sensor_unit_of_measure:
            converted_sensor_unit_of_measure_list.append(sensor_unit_of_measure.replace('ppm ',''))
        elif sensor_unit_of_measure == '%RH':
            converted_sensor_unit_of_measure_list.append('RH')
        elif sensor_unit_of_measure == 'degC':
            converted_sensor_unit_of_measure_list.append('Temp')


    form_data_dictionary = {}

    for idx, converted_sensor_unit in enumerate(converted_sensor_unit_of_measure_list):
        print(f'idx:{idx}')
        form_data_dictionary[f'sensor_{idx+1}_type'] = converted_sensor_unit

    # find sensor type count
    sensor_type_count = len(converted_sensor_unit_of_measure_list)

    jinja_variables_list = []
    # create jinja variables
    for i in range(1, sensor_type_count+1):
        jinja_variables_list.append([f'sensor_{i}_type', f'sensor_{i}_enable', f'sensor_{i}_control'])


    CustomFieldbusSensorDefinitionForm = init_CustomFieldbusSensorDefinitionForm(sensor_type_count, form_data_dictionary, fieldbus_sensor_type_id)
    custom_fieldbus_sensor_definition_form = CustomFieldbusSensorDefinitionForm()

    if request.method == 'POST':
        if "navigate_sales_order_status_button" in request.form:
            return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))
        elif custom_fieldbus_sensor_definition_form.validate_on_submit():
            form_data_dictionary = custom_fieldbus_sensor_definition_form.data

            print(f'--------form_data_dictionary: {form_data_dictionary}')


            if sensor_type_count > 1:
                sensor_2_type_value = form_data_dictionary['sensor_2_type']
                sensor_2_enable_value = form_data_dictionary['sensor_2_enable']
                sensor_2_control_value = form_data_dictionary['sensor_2_control']
            else:
                sensor_2_type_value = null()
                sensor_2_enable_value = null()
                sensor_2_control_value = null()

            if sensor_type_count > 2:
                sensor_3_type_value = form_data_dictionary['sensor_3_type']
                sensor_3_enable_value = form_data_dictionary['sensor_3_enable']
                sensor_3_control_value = form_data_dictionary['sensor_3_control']
            else:
                sensor_3_type_value = null()
                sensor_3_enable_value = null()
                sensor_3_control_value = null()
            
            if sensor_type_count > 3:
                sensor_4_type_value = form_data_dictionary['sensor_4_type']
                sensor_4_enable_value = form_data_dictionary['sensor_4_enable']
                sensor_4_control_value = form_data_dictionary['sensor_4_control']
            else:
                sensor_4_type_value = null()
                sensor_4_enable_value = null()
                sensor_4_control_value = null()

            new_fbsensor_definition = FieldbusSensorDefinitions(
                sales_order_id = sales_order_id,
                sensor_id = form_data_dictionary['sensor_type'],
                standard = form_data_dictionary['standard_boolean'],
                definition_name = form_data_dictionary['definition_name'],

                sensor_1_type = form_data_dictionary['sensor_1_type'],
                sensor_1_enable = form_data_dictionary['sensor_1_enable'],
                sensor_1_control = form_data_dictionary['sensor_1_control'],

                sensor_2_type = sensor_2_type_value,
                sensor_2_enable = sensor_2_enable_value,
                sensor_2_control = sensor_2_control_value,


                sensor_3_type = sensor_3_type_value,
                sensor_3_enable = sensor_3_enable_value,
                sensor_3_control = sensor_3_control_value,

                sensor_4_type = sensor_4_type_value,
                sensor_4_enable = sensor_4_enable_value,
                sensor_4_control = sensor_4_control_value
            )
            db.session.add(new_fbsensor_definition)
            db.session.commit()

            definition_name = form_data_dictionary['definition_name']
            flash(f'Fieldbus Sensor Definition, {definition_name}, has been created','success')

            return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))
        # create definition
        # elif custom_fieldbus_sensor_definition_form.validate_on_submit():
        #     if custom_fieldbus_sensor_definition_form.submit_button.data:
        #         standard_data = custom_fieldbus_sensor_definition_form.standard_boolean.data,

        #         new_fb_definition_object = FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first()

        #         # create a new fieldbus sensor definition  
        #         new_fb_definition_object.definition_name = custom_fieldbus_sensor_definition_form.definition_name.data,
        #         new_fb_definition_object.sensor_type = custom_fieldbus_sensor_definition_form.sensor_type.data,

        #         # if it is standard definition, there is no sales order id
        #         if standard_data:
        #             new_fb_definition_object.standard = True
        #         else:
        #             new_fb_definition_object.standard = False
        #             new_fb_definition_object.sales_order_id = sales_order_id

        #         db.session.add(new_fb_definition_object)
        #         db.session.commit()

        #         flash(f'Fieldbus Sensor Definition {custom_fieldbus_sensor_definition_form.definition_name.data} has been created', 'success')

        #         return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))

        # else:
        #     for fb_sensor_definition_data in fb_sensor_definitions_list:
        #         fieldbus_sensor_demand_configurations__id = fb_sensor_definition_data[0]
        #         clear_button_name = "clear_button_" + str(fieldbus_sensor_demand_configurations__id)

        #         if clear_button_name in request.form:
        #             # clear button has been pressed, remove id for this
        #             fb_demand_configurations_object = FBSensorDemandConfigurations.query.filter_by(id=fieldbus_sensor_demand_configurations__id).first()
        #             db.session.delete(fb_demand_configurations_object)
        #             db.session.commit()
        #             flash(f'Removed {fb_sensor_definition_data[1]} sensor', 'danger')
        #             return redirect(url_for('createdefinitions.create_fieldbus_sensor_definition', sales_order_id=sales_order_id, fieldbus_sensor_definitions_id=fieldbus_sensor_definitions_id))

    return render_template('create_fieldbus_sensor_definition.html', jinja_variables_list=jinja_variables_list, sales_order_id=sales_order_id, 
    custom_fieldbus_sensor_definition_form=custom_fieldbus_sensor_definition_form)




@createdefinitions_blueprint.route('/initializestandardfandefinition/', methods=['GET','POST'])
def initializestandardfandefinition():

    CreateFanDefinitionForm = initializeCreateFanDefinitionForm()
    create_fan_definition_form = CreateFanDefinitionForm()

    if "create_custom_fan_definition_button" in request.form:
        motor_type = create_fan_definition_form.standard_type.data
        definition_name = create_fan_definition_form.definition_name.data

        return redirect(url_for('createdefinitions.createstandardfandefinition', motor_type=motor_type, definition_name=definition_name))



    return render_template('initializestandardfandefinition.html', title='Initialize Fan Definition', create_fan_definition_form=create_fan_definition_form)


@createdefinitions_blueprint.route('/initializefandefinition/<sales_order_id>', methods=['GET','POST'])
def initializefandefinition(sales_order_id):

    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    CreateFanDefinitionForm = initializeCreateFanDefinitionForm()
    create_fan_definition_form = CreateFanDefinitionForm()

    if "create_custom_fan_definition_button" in request.form:
        motor_type = create_fan_definition_form.standard_type.data
        standard_boolean = create_fan_definition_form.standard.data
        definition_name = create_fan_definition_form.definition_name.data

        return redirect(url_for('createdefinitions.createfandefinition', sales_order_id=sales_order_id, tag_prefix=tag_prefix,
            motor_type=motor_type, standard_boolean=standard_boolean, definition_name=definition_name))

    elif "cancel_fan_definition" in request.form:
        return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))

    return render_template('initializefandefinition.html', title='Initialize Fan Definition', create_fan_definition_form=create_fan_definition_form)


@createdefinitions_blueprint.route('/createfandefinition/<sales_order_id>/<tag_prefix>/<motor_type>/<standard_boolean>/<definition_name>', methods=['GET','POST'])
def createfandefinition(sales_order_id,tag_prefix,motor_type,standard_boolean,definition_name):

    # convert standard_boolean from string to boolean
    if standard_boolean.lower() == "true":
        standard_boolean = True
    else:
        standard_boolean = False


    CustomFanDefFormObject = initializeCustomFanDefinitionForm(motor_type, standard_boolean, definition_name)
    
    custom_fan_definition_form = CustomFanDefFormObject()
    
    fans_object = FanValues.query.filter_by(name=motor_type).first()
    raw_value_no_digit_in = fans_object.no_dig_in
    raw_value_max_number_of_sensors = fans_object.no_sensors

    
    if request.method == 'POST':
        # grab all data from forms
        # if custom_fan_definition_form.validate_on_submit():
        if "save_definition_button" in request.form:
            # name_fan_definition_form 
            custom_fan_definition_name = custom_fan_definition_form.definition_name.data
            standard_definition_boolean = standard_boolean
            fan_type = custom_fan_definition_form.fan_type.data

            if fan_type == 'A5190' or fan_type == 'a5190':
                sub_fan_type = custom_fan_definition_form.select_sub_fan.data
                print(sub_fan_type,'sub fan -----')
            else:
                sub_fan_type = None


            # setup_types_dropdown_form
            setup_type_dropdown_selection = custom_fan_definition_form.setup_type_selection.data

            # single_speed_form
            single_speed = custom_fan_definition_form.single_speed.data

            # digital_modbus_form
            digital_modbus_matching_controller = custom_fan_definition_form.digital_modbus_matching_controller.data
            digital_modbus_max_fan_rpm = custom_fan_definition_form.digital_modbus_max_fan_rpm.data
            digital_modbus_min_fan_rpm = custom_fan_definition_form.digital_modbus_min_fan_rpm.data
            digital_modbus_zone = custom_fan_definition_form.digital_modbus_zone.data

            # analogue_signal_form
            analogue_signal_control_signal = custom_fan_definition_form.analogue_signal_control_signal.data
            analogue_signal_max_speed = custom_fan_definition_form.analogue_signal_max_speed.data
            analogue_signal_min_speed = custom_fan_definition_form.analogue_signal_min_speed.data

            # local_sensor_form
            local_sensor_set_point = custom_fan_definition_form.local_sensor_set_point.data
            local_sensor_max_speed = custom_fan_definition_form.local_sensor_max_speed.data
            local_sensor_min_speed = custom_fan_definition_form.local_sensor_min_speed.data
            local_sensor_max_control_signal = custom_fan_definition_form.local_sensor_max_control_signal.data
            local_sensor_min_control_signal = custom_fan_definition_form.local_sensor_min_control_signal.data
            local_sensor_sensor_logic = custom_fan_definition_form.local_sensor_sensor_logic.data

            # sensor_setup_form
            sensors_count = custom_fan_definition_form.sensors_count.data
            sensor1_type = custom_fan_definition_form.sensor1_type.data
            sensor2_type = custom_fan_definition_form.sensor2_type.data
            smoke_detector = custom_fan_definition_form.smoke_detector.data
            print(f'sensor1_type in route: {sensor1_type}')

            # Sensor 1 Form
            sensor1_lpt_end_unit_of_measure = custom_fan_definition_form.lpt_end_unit_of_measure.data
            sensor1_lpt_end_min_physical_value = custom_fan_definition_form.lpt_end_min_physical_value.data
            sensor1_lpt_end_max_physical_value = custom_fan_definition_form.lpt_end_max_physical_value.data
            sensor1_lpt_end_current_draw_of_sensor = custom_fan_definition_form.lpt_end_current_draw_of_sensor.data
            sensor1_lpt_end_signal_type = custom_fan_definition_form.lpt_end_signal_type.data

            sensor1_lpt_tco_unit_of_measure = custom_fan_definition_form.lpt_tco_unit_of_measure.data
            sensor1_lpt_tco_min_physical_value = custom_fan_definition_form.lpt_tco_min_physical_value.data
            sensor1_lpt_tco_max_physical_value = custom_fan_definition_form.lpt_tco_max_physical_value.data
            sensor1_lpt_tco_current_draw_of_sensor = custom_fan_definition_form.lpt_tco_current_draw_of_sensor.data
            sensor1_lpt_tco_signal_type = custom_fan_definition_form.lpt_tco_signal_type.data

            sensor1_other_unit_of_measure = custom_fan_definition_form.other_unit_of_measure.data
            sensor1_other_min_physical_value = custom_fan_definition_form.other_min_physical_value.data
            sensor1_other_max_physical_value = custom_fan_definition_form.other_max_physical_value.data
            sensor1_other_current_draw_of_sensor = custom_fan_definition_form.other_current_draw_of_sensor.data
            sensor1_other_signal_type = custom_fan_definition_form.other_signal_type.data

            # Sensor 2 Form
            sensor2_lpt_end_unit_of_measure = custom_fan_definition_form.lpt_end_unit_of_measure.data
            sensor2_lpt_end_min_physical_value = custom_fan_definition_form.lpt_end_min_physical_value.data
            sensor2_lpt_end_max_physical_value = custom_fan_definition_form.lpt_end_max_physical_value.data
            sensor2_lpt_end_current_draw_of_sensor = custom_fan_definition_form.lpt_end_current_draw_of_sensor.data
            sensor2_lpt_end_signal_type = custom_fan_definition_form.lpt_end_signal_type.data

            sensor2_lpt_tco_unit_of_measure = custom_fan_definition_form.lpt_tco_unit_of_measure.data
            sensor2_lpt_tco_min_physical_value = custom_fan_definition_form.lpt_tco_min_physical_value.data
            sensor2_lpt_tco_max_physical_value = custom_fan_definition_form.lpt_tco_max_physical_value.data
            sensor2_lpt_tco_current_draw_of_sensor = custom_fan_definition_form.lpt_tco_current_draw_of_sensor.data
            sensor2_lpt_tco_signal_type = custom_fan_definition_form.lpt_tco_signal_type.data

            sensor2_other_unit_of_measure = custom_fan_definition_form.other2_unit_of_measure.data
            sensor2_other_min_physical_value = custom_fan_definition_form.other2_min_physical_value.data
            sensor2_other_max_physical_value = custom_fan_definition_form.other2_max_physical_value.data
            sensor2_other_current_draw_of_sensor = custom_fan_definition_form.other2_current_draw_of_sensor.data
            sensor2_other_signal_type = custom_fan_definition_form.other2_signal_type.data

            # special functions
            digital_in_2 = custom_fan_definition_form.digital_in_2.data
            logic = custom_fan_definition_form.logic.data
            function = custom_fan_definition_form.function.data
            function_speed_override = custom_fan_definition_form.function_speed_override.data

            external_analogue_signal = custom_fan_definition_form.external_analogue_signal.data
            external_analogue_signal_selection = custom_fan_definition_form.external_analogue_signal_selection.data

            relay_polarity = custom_fan_definition_form.relay_polarity.data

            comms_timeout_duration = custom_fan_definition_form.comms_timeout_duration.data
            comms_timeout_speed = custom_fan_definition_form.comms_timeout_speed.data
        

            # ----------------
            # send data to function and determine what to save to custom fan definition table
            print('PRINT DATA---------')

            print(f'custom_fan_definition_name: {custom_fan_definition_name}, standard_definition_boolean {standard_definition_boolean}, setup_type_dropdown_selection: {setup_type_dropdown_selection}, single_speed: {single_speed},\n' +
                f'digital_modbus_matching_controller: {digital_modbus_matching_controller}, digital_modbus_max_speed: {digital_modbus_max_fan_rpm}, digital_modbus_min_speed: {digital_modbus_min_fan_rpm}, digital_modbus_zone: {digital_modbus_zone},\n' +
                f'analogue_signal_control_signal: {analogue_signal_control_signal}, analogue_signal_max_speed: {analogue_signal_max_speed}, analogue_signal_min_speed: {analogue_signal_min_speed}, local_sensor_set_point: {local_sensor_set_point},\n' +
                f'local_sensor_max_speed: {local_sensor_max_speed}, local_sensor_min_speed, local_sensor_max_control_signal, local_sensor_min_control_signal, \n' +
                f'local_sensor_sensor_logic, sensors_count: {sensors_count}, sensor1_type: {sensor1_type}, sensor2_type, smoke_detector,\n' +
                f'sensor1_lpt_end_unit_of_measure, sensor1_lpt_end_min_physical_value, sensor1_lpt_end_max_physical_value,\n' +
                f'sensor1_lpt_end_current_draw_of_sensor, sensor1_lpt_end_signal_type, sensor1_lpt_tco_unit_of_measure,\n' +
                f'sensor1_lpt_tco_min_physical_value, sensor1_lpt_tco_max_physical_value, sensor1_lpt_tco_current_draw_of_sensor,\n' +
                f'sensor1_lpt_tco_signal_type, sensor1_other_unit_of_measure, sensor1_other_min_physical_value, sensor1_other_max_physical_value,\n' +
                f'sensor1_other_current_draw_of_sensor, sensor1_other_signal_type, sensor2_lpt_end_unit_of_measure,\n' +
                f'sensor2_lpt_end_min_physical_value, sensor2_lpt_end_max_physical_value, sensor2_lpt_end_current_draw_of_sensor,\n' +
                f'sensor2_lpt_end_signal_type, sensor2_lpt_tco_unit_of_measure, sensor2_lpt_tco_min_physical_value,\n' +
                f'sensor2_lpt_tco_max_physical_value, sensor2_lpt_tco_current_draw_of_sensor, sensor2_lpt_tco_signal_type,\n' +
                f'sensor2_other_unit_of_measure, sensor2_other_min_physical_value, sensor2_other_max_physical_value,\n' +
                f'sensor2_other_current_draw_of_sensor, sensor2_other_signal_type, digital_in_2, logic, function, function_speed_override,\n' +
                f'external_analogue_signal, external_analogue_signal_selection, relay_polarity, comms_timeout_duration, comms_timeout_speed\n')

            createCustomFanDefinition( sales_order_id, fan_type,
                custom_fan_definition_name, standard_definition_boolean, setup_type_dropdown_selection, single_speed,
                digital_modbus_matching_controller, digital_modbus_max_fan_rpm, digital_modbus_min_fan_rpm, digital_modbus_zone,
                analogue_signal_control_signal, analogue_signal_max_speed, analogue_signal_min_speed, local_sensor_set_point,
                local_sensor_max_speed, local_sensor_min_speed, local_sensor_max_control_signal, local_sensor_min_control_signal, 
                local_sensor_sensor_logic, sensors_count, sensor1_type, sensor2_type, smoke_detector,
                sensor1_lpt_end_unit_of_measure, sensor1_lpt_end_min_physical_value, sensor1_lpt_end_max_physical_value,
                sensor1_lpt_end_current_draw_of_sensor, sensor1_lpt_end_signal_type, sensor1_lpt_tco_unit_of_measure,
                sensor1_lpt_tco_min_physical_value, sensor1_lpt_tco_max_physical_value, sensor1_lpt_tco_current_draw_of_sensor,
                sensor1_lpt_tco_signal_type, sensor1_other_unit_of_measure, sensor1_other_min_physical_value, sensor1_other_max_physical_value,
                sensor1_other_current_draw_of_sensor, sensor1_other_signal_type, sensor2_lpt_end_unit_of_measure,
                sensor2_lpt_end_min_physical_value, sensor2_lpt_end_max_physical_value, sensor2_lpt_end_current_draw_of_sensor,
                sensor2_lpt_end_signal_type, sensor2_lpt_tco_unit_of_measure, sensor2_lpt_tco_min_physical_value,
                sensor2_lpt_tco_max_physical_value, sensor2_lpt_tco_current_draw_of_sensor, sensor2_lpt_tco_signal_type,
                sensor2_other_unit_of_measure, sensor2_other_min_physical_value, sensor2_other_max_physical_value,
                sensor2_other_current_draw_of_sensor, sensor2_other_signal_type, digital_in_2, logic, function, function_speed_override,
                external_analogue_signal, external_analogue_signal_selection, relay_polarity, comms_timeout_duration, comms_timeout_speed, sub_fan_type
            )

            return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))


        

        elif "cancel_fan_definition" in request.form:
            return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))
    
    return render_template('createfandefinition.html',title='Create Fan Definition', custom_fan_definition_form=custom_fan_definition_form, 
    raw_value_no_digit_in=raw_value_no_digit_in, raw_value_max_number_of_sensors=raw_value_max_number_of_sensors)



@createdefinitions_blueprint.route('/createstandardfandefinition/<motor_type>/<definition_name>', methods=['GET','POST'])
def createstandardfandefinition(motor_type,definition_name):

    CustomFanDefFormObject = initializeCustomFanDefinitionForm(motor_type, True, definition_name)
    custom_fan_definition_form = CustomFanDefFormObject()

    sales_order_id = None

    fans_object = FanValues.query.filter_by(name=motor_type).first()
    raw_value_no_digit_in = fans_object.no_dig_in
    raw_value_max_number_of_sensors = fans_object.no_sensors

    if request.method == 'POST':
        # grab all data from forms
        # if custom_fan_definition_form.validate_on_submit():
        if "save_definition_button" in request.form:
            # name_fan_definition_form 
            custom_fan_definition_name = custom_fan_definition_form.definition_name.data
            standard_definition_boolean = True
            fan_type = custom_fan_definition_form.fan_type.data

            if fan_type == 'A5190' or fan_type == 'a5190':
                sub_fan_type = custom_fan_definition_form.select_sub_fan.data
                print(sub_fan_type,'sub fan -----')
            else:
                sub_fan_type = None


            # setup_types_dropdown_form
            setup_type_dropdown_selection = custom_fan_definition_form.setup_type_selection.data

            # single_speed_form
            single_speed = custom_fan_definition_form.single_speed.data

            # digital_modbus_form
            digital_modbus_matching_controller = custom_fan_definition_form.digital_modbus_matching_controller.data
            digital_modbus_max_speed = custom_fan_definition_form.digital_modbus_max_speed.data
            digital_modbus_min_speed = custom_fan_definition_form.digital_modbus_min_speed.data
            digital_modbus_zone = custom_fan_definition_form.digital_modbus_zone.data

            # analogue_signal_form
            analogue_signal_control_signal = custom_fan_definition_form.analogue_signal_control_signal.data
            analogue_signal_max_speed = custom_fan_definition_form.analogue_signal_max_speed.data
            analogue_signal_min_speed = custom_fan_definition_form.analogue_signal_min_speed.data

            # local_sensor_form
            local_sensor_set_point = custom_fan_definition_form.local_sensor_set_point.data
            local_sensor_max_speed = custom_fan_definition_form.local_sensor_max_speed.data
            local_sensor_min_speed = custom_fan_definition_form.local_sensor_min_speed.data
            local_sensor_max_control_signal = custom_fan_definition_form.local_sensor_max_control_signal.data
            local_sensor_min_control_signal = custom_fan_definition_form.local_sensor_min_control_signal.data
            local_sensor_sensor_logic = custom_fan_definition_form.local_sensor_sensor_logic.data

            # sensor_setup_form
            sensors_count = custom_fan_definition_form.sensors_count.data
            sensor1_type = custom_fan_definition_form.sensor1_type.data
            sensor2_type = custom_fan_definition_form.sensor2_type.data
            smoke_detector = custom_fan_definition_form.smoke_detector.data
            print(f'sensor1_type in route: {sensor1_type}')

            # Sensor 1 Form
            sensor1_lpt_end_unit_of_measure = custom_fan_definition_form.lpt_end_unit_of_measure.data
            sensor1_lpt_end_min_physical_value = custom_fan_definition_form.lpt_end_min_physical_value.data
            sensor1_lpt_end_max_physical_value = custom_fan_definition_form.lpt_end_max_physical_value.data
            sensor1_lpt_end_current_draw_of_sensor = custom_fan_definition_form.lpt_end_current_draw_of_sensor.data
            sensor1_lpt_end_signal_type = custom_fan_definition_form.lpt_end_signal_type.data

            sensor1_lpt_tco_unit_of_measure = custom_fan_definition_form.lpt_tco_unit_of_measure.data
            sensor1_lpt_tco_min_physical_value = custom_fan_definition_form.lpt_tco_min_physical_value.data
            sensor1_lpt_tco_max_physical_value = custom_fan_definition_form.lpt_tco_max_physical_value.data
            sensor1_lpt_tco_current_draw_of_sensor = custom_fan_definition_form.lpt_tco_current_draw_of_sensor.data
            sensor1_lpt_tco_signal_type = custom_fan_definition_form.lpt_tco_signal_type.data

            sensor1_other_unit_of_measure = custom_fan_definition_form.other_unit_of_measure.data
            sensor1_other_min_physical_value = custom_fan_definition_form.other_min_physical_value.data
            sensor1_other_max_physical_value = custom_fan_definition_form.other_max_physical_value.data
            sensor1_other_current_draw_of_sensor = custom_fan_definition_form.other_current_draw_of_sensor.data
            sensor1_other_signal_type = custom_fan_definition_form.other_signal_type.data

            # Sensor 2 Form
            sensor2_lpt_end_unit_of_measure = custom_fan_definition_form.lpt_end_unit_of_measure.data
            sensor2_lpt_end_min_physical_value = custom_fan_definition_form.lpt_end_min_physical_value.data
            sensor2_lpt_end_max_physical_value = custom_fan_definition_form.lpt_end_max_physical_value.data
            sensor2_lpt_end_current_draw_of_sensor = custom_fan_definition_form.lpt_end_current_draw_of_sensor.data
            sensor2_lpt_end_signal_type = custom_fan_definition_form.lpt_end_signal_type.data

            sensor2_lpt_tco_unit_of_measure = custom_fan_definition_form.lpt_tco_unit_of_measure.data
            sensor2_lpt_tco_min_physical_value = custom_fan_definition_form.lpt_tco_min_physical_value.data
            sensor2_lpt_tco_max_physical_value = custom_fan_definition_form.lpt_tco_max_physical_value.data
            sensor2_lpt_tco_current_draw_of_sensor = custom_fan_definition_form.lpt_tco_current_draw_of_sensor.data
            sensor2_lpt_tco_signal_type = custom_fan_definition_form.lpt_tco_signal_type.data

            sensor2_other_unit_of_measure = custom_fan_definition_form.other2_unit_of_measure.data
            sensor2_other_min_physical_value = custom_fan_definition_form.other2_min_physical_value.data
            sensor2_other_max_physical_value = custom_fan_definition_form.other2_max_physical_value.data
            sensor2_other_current_draw_of_sensor = custom_fan_definition_form.other2_current_draw_of_sensor.data
            sensor2_other_signal_type = custom_fan_definition_form.other2_signal_type.data

            # special functions
            digital_in_2 = custom_fan_definition_form.digital_in_2.data
            logic = custom_fan_definition_form.logic.data
            function = custom_fan_definition_form.function.data
            function_speed_override = custom_fan_definition_form.function_speed_override.data

            external_analogue_signal = custom_fan_definition_form.external_analogue_signal.data
            external_analogue_signal_selection = custom_fan_definition_form.external_analogue_signal_selection.data

            relay_polarity = custom_fan_definition_form.relay_polarity.data

            comms_timeout_duration = custom_fan_definition_form.comms_timeout_duration.data
            comms_timeout_speed = custom_fan_definition_form.comms_timeout_speed.data
        

            createCustomFanDefinition( sales_order_id, fan_type,
                custom_fan_definition_name, standard_definition_boolean, setup_type_dropdown_selection, single_speed,
                digital_modbus_matching_controller, digital_modbus_max_speed, digital_modbus_min_speed, digital_modbus_zone,
                analogue_signal_control_signal, analogue_signal_max_speed, analogue_signal_min_speed, local_sensor_set_point,
                local_sensor_max_speed, local_sensor_min_speed, local_sensor_max_control_signal, local_sensor_min_control_signal, 
                local_sensor_sensor_logic, sensors_count, sensor1_type, sensor2_type, smoke_detector,
                sensor1_lpt_end_unit_of_measure, sensor1_lpt_end_min_physical_value, sensor1_lpt_end_max_physical_value,
                sensor1_lpt_end_current_draw_of_sensor, sensor1_lpt_end_signal_type, sensor1_lpt_tco_unit_of_measure,
                sensor1_lpt_tco_min_physical_value, sensor1_lpt_tco_max_physical_value, sensor1_lpt_tco_current_draw_of_sensor,
                sensor1_lpt_tco_signal_type, sensor1_other_unit_of_measure, sensor1_other_min_physical_value, sensor1_other_max_physical_value,
                sensor1_other_current_draw_of_sensor, sensor1_other_signal_type, sensor2_lpt_end_unit_of_measure,
                sensor2_lpt_end_min_physical_value, sensor2_lpt_end_max_physical_value, sensor2_lpt_end_current_draw_of_sensor,
                sensor2_lpt_end_signal_type, sensor2_lpt_tco_unit_of_measure, sensor2_lpt_tco_min_physical_value,
                sensor2_lpt_tco_max_physical_value, sensor2_lpt_tco_current_draw_of_sensor, sensor2_lpt_tco_signal_type,
                sensor2_other_unit_of_measure, sensor2_other_min_physical_value, sensor2_other_max_physical_value,
                sensor2_other_current_draw_of_sensor, sensor2_other_signal_type, digital_in_2, logic, function, function_speed_override,
                external_analogue_signal, external_analogue_signal_selection, relay_polarity, comms_timeout_duration, comms_timeout_speed, sub_fan_type
            )
            flash('Custom Definition Saved', 'success')

            return redirect(url_for('admin.admin'))

        elif "cancel_fan_definition" in request.form:
            return redirect(url_for('admin.admin'))


    return render_template('createfandefinition.html',title='Create Fan Definition', custom_fan_definition_form=custom_fan_definition_form, 
    raw_value_no_digit_in=raw_value_no_digit_in, raw_value_max_number_of_sensors=raw_value_max_number_of_sensors)
