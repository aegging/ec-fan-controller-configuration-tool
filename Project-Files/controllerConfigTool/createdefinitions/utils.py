
from controllerConfigTool.models import CarelKeys, CarelValues
from controllerConfigTool.models import FanDefinitions
from controllerConfigTool import db



# Create custom fan definition
def createCustomFanDefinition(
    sales_order_id, fan_type, custom_fan_definition_name, standard_definition_boolean, setup_type_dropdown_selection, single_speed,
    digital_modbus_matching_controller, digital_modbus_max_fan_rpm, digital_modbus_min_fan_rpm, digital_modbus_zone,
    analogue_signal_control_signal, analogue_signal_max_speed, analogue_signal_min_speed, local_sensor_set_point,
    local_sensor_max_speed, local_sensor_min_speed, local_sensor_max_control_signal, local_sensor_min_control_signal, 
    local_sensor_sensor_logic, sensors_count, sensor1_type, sensor2_type, smoke_detector,
    sensor1_lpt_end_unit_of_measure, sensor1_lpt_end_min_physical_value, sensor1_lpt_end_max_physical_value,
    sensor1_lpt_end_current_draw_of_sensor, sensor1_lpt_end_signal_type, sensor1_lpt_tco_unit_of_measure,
    sensor1_lpt_tco_min_physical_value, sensor1_lpt_tco_max_physical_value, sensor1_lpt_tco_current_draw_of_sensor,
    sensor1_lpt_tco_signal_type, sensor1_other_unit_of_measure, sensor1_other_min_physical_value, sensor1_other_max_physical_value,
    sensor1_other_current_draw_of_sensor, sensor1_other_signal_type, sensor2_lpt_end_unit_of_measure,
    sensor2_lpt_end_min_physical_value, sensor2_lpt_end_max_physical_value, sensor2_lpt_end_current_draw_of_sensor,
    sensor2_lpt_end_signal_type, sensor2_lpt_tco_unit_of_measure, sensor2_lpt_tco_min_physical_value,
    sensor2_lpt_tco_max_physical_value, sensor2_lpt_tco_current_draw_of_sensor, sensor2_lpt_tco_signal_type,
    sensor2_other_unit_of_measure, sensor2_other_min_physical_value, sensor2_other_max_physical_value,
    sensor2_other_current_draw_of_sensor, sensor2_other_signal_type, digital_in_2, logic, function, function_speed_override,
    external_analogue_signal, external_analogue_signal_selection, relay_polarity, comms_timeout_duration, comms_timeout_speed, sub_fan_type
):
    # initialize all variables to none or default value
    data_comms = 'Modbus'
    data_control = 'Remote'

    data_setup_type_dropdown_selection = setup_type_dropdown_selection
    data_single_speed = single_speed
    data_digital_modbus_matching_controller = digital_modbus_matching_controller
    data_digital_modbus_max_speed = digital_modbus_max_fan_rpm
    data_digital_modbus_min_speed = digital_modbus_min_fan_rpm
    data_digital_modbus_zone = digital_modbus_zone
    data_analogue_signal_control_signal = analogue_signal_control_signal
    data_analogue_signal_max_speed = analogue_signal_max_speed
    data_analogue_signal_min_speed = analogue_signal_min_speed
    data_local_sensor_set_point = local_sensor_set_point
    data_local_sensor_max_speed = local_sensor_max_speed
    data_local_sensor_min_speed = local_sensor_min_speed
    data_local_sensor_max_control_signal = local_sensor_max_control_signal
    data_local_sensor_min_control_signal = local_sensor_min_control_signal
    data_local_sensor_sensor_logic = local_sensor_sensor_logic
    data_sensor1_type = sensor1_type
    data_sensor2_type = sensor2_type
    data_sensor1_lpt_end_unit_of_measure = sensor1_lpt_end_unit_of_measure
    data_sensor1_lpt_end_min_physical_value = sensor1_lpt_end_min_physical_value
    data_sensor1_lpt_end_max_physical_value = sensor1_lpt_end_max_physical_value
    data_sensor1_lpt_end_current_draw_of_sensor = sensor1_lpt_end_current_draw_of_sensor
    data_sensor1_lpt_end_signal_type = sensor1_lpt_end_signal_type
    data_sensor1_lpt_tco_unit_of_measure = sensor1_lpt_tco_unit_of_measure
    data_sensor1_lpt_tco_min_physical_value = sensor1_lpt_tco_min_physical_value
    data_sensor1_lpt_tco_max_physical_value = sensor1_lpt_tco_max_physical_value
    data_sensor1_lpt_tco_current_draw_of_sensor = sensor1_lpt_tco_current_draw_of_sensor
    data_sensor1_lpt_tco_signal_type = sensor1_lpt_tco_signal_type
    data_sensor1_other_unit_of_measure = sensor1_other_unit_of_measure
    data_sensor1_other_min_physical_value = sensor1_other_min_physical_value
    data_sensor1_other_max_physical_value = sensor1_other_max_physical_value
    data_sensor1_other_current_draw_of_sensor = sensor1_other_current_draw_of_sensor
    data_sensor1_other_signal_type = sensor1_other_signal_type
    data_sensor2_lpt_end_unit_of_measure = sensor2_lpt_end_unit_of_measure
    data_sensor2_lpt_end_min_physical_value = sensor2_lpt_end_min_physical_value
    data_sensor2_lpt_end_max_physical_value = sensor2_lpt_end_max_physical_value
    data_sensor2_lpt_end_current_draw_of_sensor = sensor2_lpt_end_current_draw_of_sensor
    data_sensor2_lpt_end_signal_type = sensor2_lpt_end_signal_type
    data_sensor2_lpt_tco_unit_of_measure = sensor2_lpt_tco_unit_of_measure
    data_sensor2_lpt_tco_min_physical_value = sensor2_lpt_tco_min_physical_value
    data_sensor2_lpt_tco_max_physical_value = sensor2_lpt_tco_max_physical_value
    data_sensor2_lpt_tco_current_draw_of_sensor = sensor2_lpt_tco_current_draw_of_sensor
    data_sensor2_lpt_tco_signal_type = sensor2_lpt_tco_signal_type
    data_sensor2_other_unit_of_measure = sensor2_other_unit_of_measure
    data_sensor2_other_min_physical_value = sensor2_other_min_physical_value
    data_sensor2_other_max_physical_value = sensor2_other_max_physical_value
    data_sensor2_other_current_draw_of_sensor = sensor2_other_current_draw_of_sensor
    data_sensor2_other_signal_type = sensor2_other_signal_type
    data_digital_in_2 = digital_in_2
    data_logic = logic
    data_function = function
    data_function_speed_override = function_speed_override
    data_external_analogue_signal = external_analogue_signal
    data_external_analogue_signal_selection = external_analogue_signal_selection
    data_relay_polarity = relay_polarity
    data_comms_timeout_duration = comms_timeout_duration
    data_comms_timeout_speed = comms_timeout_speed
    data_sub_fan_type = sub_fan_type
        

    data_emergency = ""
    # special functions emergency
    if digital_in_2 == True:

        if function == 'Enable':
            if logic == 'Active High':
                data_emergency = 'enable|NO| 2'
            elif logic == 'Active Low/Open':
                data_emergency = 'enable|NC| 2'
            

        elif function == 'Set speed override':
            if logic == 'Active High':
                data_emergency = 'override|NO| 2'
            elif logic == 'Active Low/Open':
                data_emergency = 'override|NC| 2'

    else:
        data_emergency = 'none|NO| 0'

    # special functions emer set speed
    if external_analogue_signal == False:    
        data_external_analogue_signal = 'False'
    else:
        if external_analogue_signal_selection == '0-10V Signal Following Request':
            data_external_analogue_signal = 'Demand Monitoring'
        elif external_analogue_signal_selection == '0-10V Signal Following Actual Speed':
            data_external_analogue_signal = 'Speed Monitoring'
        elif external_analogue_signal_selection == 'Operating Indication (Above 50rpm 10V Digital)':
            data_external_analogue_signal = 'Run Signal'

    data_external_analogue_signal 


    # if 1 sensor
    if sensors_count == 1:
        data_sensor1_type = sensor1_type

        data_sensor1_lpt_end_unit_of_measure = sensor1_lpt_end_unit_of_measure
        data_sensor1_lpt_end_min_physical_value = sensor1_lpt_end_min_physical_value
        data_sensor1_lpt_end_max_physical_value = sensor1_lpt_end_max_physical_value
        data_sensor1_lpt_end_current_draw_of_sensor = sensor1_lpt_end_current_draw_of_sensor
        data_sensor1_lpt_end_signal_type = sensor1_lpt_end_signal_type
        data_sensor1_lpt_tco_unit_of_measure = sensor1_lpt_tco_unit_of_measure
        data_sensor1_lpt_tco_min_physical_value = sensor1_lpt_tco_min_physical_value
        data_sensor1_lpt_tco_max_physical_value = sensor1_lpt_tco_max_physical_value
        data_sensor1_lpt_tco_current_draw_of_sensor = sensor1_lpt_tco_current_draw_of_sensor
        data_sensor1_lpt_tco_signal_type = sensor1_lpt_tco_signal_type
        data_sensor1_other_unit_of_measure = sensor1_other_unit_of_measure
        data_sensor1_other_min_physical_value = sensor1_other_min_physical_value
        data_sensor1_other_max_physical_value = sensor1_other_max_physical_value
        data_sensor1_other_current_draw_of_sensor = sensor1_other_current_draw_of_sensor
        data_sensor1_other_signal_type = sensor1_other_signal_type

    # if 2 sensors
    elif sensors_count == 2:
        data_sensor1_type = sensor1_type
        data_sensor2_type = sensor2_type

        data_sensor1_lpt_end_unit_of_measure = sensor1_lpt_end_unit_of_measure
        data_sensor1_lpt_end_min_physical_value = sensor1_lpt_end_min_physical_value
        data_sensor1_lpt_end_max_physical_value = sensor1_lpt_end_max_physical_value
        data_sensor1_lpt_end_current_draw_of_sensor = sensor1_lpt_end_current_draw_of_sensor
        data_sensor1_lpt_end_signal_type = sensor1_lpt_end_signal_type
        data_sensor1_lpt_tco_unit_of_measure = sensor1_lpt_tco_unit_of_measure
        data_sensor1_lpt_tco_min_physical_value = sensor1_lpt_tco_min_physical_value
        data_sensor1_lpt_tco_max_physical_value = sensor1_lpt_tco_max_physical_value
        data_sensor1_lpt_tco_current_draw_of_sensor = sensor1_lpt_tco_current_draw_of_sensor
        data_sensor1_lpt_tco_signal_type = sensor1_lpt_tco_signal_type
        data_sensor1_other_unit_of_measure = sensor1_other_unit_of_measure
        data_sensor1_other_min_physical_value = sensor1_other_min_physical_value
        data_sensor1_other_max_physical_value = sensor1_other_max_physical_value
        data_sensor1_other_current_draw_of_sensor = sensor1_other_current_draw_of_sensor
        data_sensor1_other_signal_type = sensor1_other_signal_type

        data_sensor2_lpt_end_unit_of_measure = sensor2_lpt_end_unit_of_measure
        data_sensor2_lpt_end_min_physical_value = sensor2_lpt_end_min_physical_value
        data_sensor2_lpt_end_max_physical_value = sensor2_lpt_end_max_physical_value
        data_sensor2_lpt_end_current_draw_of_sensor = sensor2_lpt_end_current_draw_of_sensor
        data_sensor2_lpt_end_signal_type = sensor2_lpt_end_signal_type
        data_sensor2_lpt_tco_unit_of_measure = sensor2_lpt_tco_unit_of_measure
        data_sensor2_lpt_tco_min_physical_value = sensor2_lpt_tco_min_physical_value
        data_sensor2_lpt_tco_max_physical_value = sensor2_lpt_tco_max_physical_value
        data_sensor2_lpt_tco_current_draw_of_sensor = sensor2_lpt_tco_current_draw_of_sensor
        data_sensor2_lpt_tco_signal_type = sensor2_lpt_tco_signal_type
        data_sensor2_other_unit_of_measure = sensor2_other_unit_of_measure
        data_sensor2_other_min_physical_value = sensor2_other_min_physical_value
        data_sensor2_other_max_physical_value = sensor2_other_max_physical_value
        data_sensor2_other_current_draw_of_sensor = sensor2_other_current_draw_of_sensor
        data_sensor2_other_signal_type = sensor2_other_signal_type


  
    # which sensors have been selected?
    print(f'sensor1 type: {sensor1_type}')

    if sensor1_type == 'LPT-END':
        data_sensor1_unit_of_measure = data_sensor1_lpt_end_unit_of_measure
        data_sensor1_min_physical_value = data_sensor1_lpt_end_min_physical_value
        data_sensor1_max_physical_value = data_sensor1_lpt_end_max_physical_value
        data_sensor1_current_draw_of_sensor = data_sensor1_lpt_end_current_draw_of_sensor
        data_sensor1_signal_type = data_sensor1_lpt_end_signal_type

    elif sensor1_type == 'LPT-TCO':
        data_sensor1_unit_of_measure = data_sensor1_lpt_tco_unit_of_measure
        data_sensor1_min_physical_value = data_sensor1_lpt_tco_min_physical_value
        data_sensor1_max_physical_value = data_sensor1_lpt_tco_max_physical_value
        data_sensor1_current_draw_of_sensor = data_sensor1_lpt_tco_current_draw_of_sensor
        data_sensor1_signal_type = data_sensor1_lpt_tco_signal_type

    elif sensor1_type == 'OTHER':
        data_sensor1_unit_of_measure = data_sensor1_other_unit_of_measure
        data_sensor1_min_physical_value = data_sensor1_other_min_physical_value
        data_sensor1_max_physical_value = data_sensor1_other_max_physical_value
        data_sensor1_current_draw_of_sensor = data_sensor1_other_current_draw_of_sensor
        data_sensor1_signal_type = data_sensor1_other_signal_type
    else:
        data_sensor1_unit_of_measure = data_sensor1_lpt_end_unit_of_measure
        data_sensor1_min_physical_value = data_sensor1_lpt_end_min_physical_value
        data_sensor1_max_physical_value = data_sensor1_lpt_end_max_physical_value
        data_sensor1_current_draw_of_sensor = data_sensor1_lpt_end_current_draw_of_sensor
        data_sensor1_signal_type = data_sensor1_lpt_end_signal_type


    if sensor2_type == 'LPT-END':
        data_sensor2_unit_of_measure = data_sensor2_lpt_end_unit_of_measure
        data_sensor2_min_physical_value = data_sensor2_lpt_end_min_physical_value
        data_sensor2_max_physical_value = data_sensor2_lpt_end_max_physical_value
        data_sensor2_current_draw_of_sensor = data_sensor2_lpt_end_current_draw_of_sensor
        data_sensor2_signal_type = data_sensor2_lpt_end_signal_type

    elif sensor2_type == 'LPT-TCO':
        data_sensor2_unit_of_measure = data_sensor2_lpt_tco_unit_of_measure
        data_sensor2_min_physical_value = data_sensor2_lpt_tco_min_physical_value
        data_sensor2_max_physical_value = data_sensor2_lpt_tco_max_physical_value
        data_sensor2_current_draw_of_sensor = data_sensor2_lpt_tco_current_draw_of_sensor
        data_sensor2_signal_type = data_sensor2_lpt_tco_signal_type

    elif sensor2_type == 'OTHER':
        data_sensor2_unit_of_measure = data_sensor2_other_unit_of_measure
        data_sensor2_min_physical_value = data_sensor2_other_min_physical_value
        data_sensor2_max_physical_value = data_sensor2_other_max_physical_value
        data_sensor2_current_draw_of_sensor = data_sensor2_other_current_draw_of_sensor
        data_sensor2_signal_type = data_sensor2_other_signal_type
    else:
        data_sensor2_unit_of_measure = data_sensor2_lpt_end_unit_of_measure
        data_sensor2_min_physical_value = data_sensor2_lpt_end_min_physical_value
        data_sensor2_max_physical_value = data_sensor2_lpt_end_max_physical_value
        data_sensor2_current_draw_of_sensor = data_sensor2_lpt_end_current_draw_of_sensor
        data_sensor2_signal_type = data_sensor2_lpt_end_signal_type



    # HANDLE FAN DEF NAME AND DETERMINE IF STANDARD
    
    
    data_zone = 1
    data_emer_start_stop_signal = ""
    


    # ---- Setup Types Tab
    if setup_type_dropdown_selection == 'Single Speed':
        data_single_speed = single_speed
        data_control = 'Fixed Speed'
        data_max_speed = digital_modbus_max_fan_rpm
        data_min_speed = digital_modbus_min_fan_rpm

    elif setup_type_dropdown_selection == 'Digital Modbus':
        data_control = "Remote"
        data_digital_modbus_matching_controller = digital_modbus_matching_controller
        data_max_speed = digital_modbus_max_fan_rpm
        data_min_speed = digital_modbus_min_fan_rpm
        data_zone = digital_modbus_zone

    elif setup_type_dropdown_selection == 'Analogue Signal':
        data_control = "Remote"
        data_comms = '0-10V'

        data_analogue_signal_control_signal = analogue_signal_control_signal
        data_max_speed = analogue_signal_max_speed
        data_min_speed = analogue_signal_min_speed

        # special circumstance
        # if analogue signal is selected and 0-10V, use the following
        if data_analogue_signal_control_signal == "0-10V":
            data_sensor1_unit_of_measure = 'V'
            data_sensor1_min_physical_value = 0
            data_sensor1_max_physical_value = 10
            data_sensor1_current_draw_of_sensor = 25
            data_sensor1_signal_type = "0-10V"

            data_sensor2_unit_of_measure = 'ppm CO'
            data_sensor2_min_physical_value = 0
            data_sensor2_max_physical_value = 200
            data_sensor2_current_draw_of_sensor = 25
            data_sensor2_signal_type = "4-20mA"

        # else if control signal 4-20mA is selected
        elif data_analogue_signal_control_signal == "4-20mA":
            data_sensor1_unit_of_measure = 'mA'
            data_sensor1_min_physical_value = 4
            data_sensor1_max_physical_value = 20
            data_sensor1_current_draw_of_sensor = 25
            data_sensor1_signal_type = "4-20mA"


    elif setup_type_dropdown_selection == 'Local Sensor':
        data_local_sensor_set_point  = local_sensor_set_point
        data_local_sensor_max_speed = local_sensor_max_speed
        data_local_sensor_min_speed = local_sensor_min_speed
        data_max_speed = local_sensor_max_speed
        data_min_speed = local_sensor_min_speed
        data_local_sensor_sensor_logic = local_sensor_sensor_logic

        if data_local_sensor_sensor_logic == 'Sensor 1':
            data_control = 'Sensor Loop 1'
        elif data_local_sensor_sensor_logic == 'Max of Sensor 1 and 2':
            data_control = 'Sensor Loop 1 & 2 Max'

    else:
        print(f'ERROR THIS SHOULD NOT HAPPEN LINE 279')
        # data_min_speed = 0
        # data_max_speed = 1770



   

    # ---- Special Functions Tab

    # if digital in 2 checkbox is checked
    if digital_in_2:
        data_logic = logic
        data_function = function

        # if function is set speed override, save override speed data
        if data_function == 'Set speed override':
            data_function_speed_override = function_speed_override
    
    # if External Analgoue Signal is checked
    if external_analogue_signal:
        data_external_analogue_signal_selection = external_analogue_signal_selection

    data_relay_polarity = relay_polarity

    if data_relay_polarity == 'Normal Close (NC)':
        data_emer_start_stop_signal = 'fault|NC'
    elif data_relay_polarity == 'Normal Open (NO)':
        data_emer_start_stop_signal = 'fault|NO'

    # if setup type is not digital modbus, use 0 for timeout duration and speed
    if setup_type_dropdown_selection != 'Digital Modbus':
        data_comms_timeout_duration = 0
        data_comms_timeout_speed = 0

    if data_relay_polarity == 'Normal Open (NO)':
        data_fault_relay = 'NO'
    else:
        data_fault_relay = 'NC'

    # --- END <-- SPECIAL FUNCTIONS TAB



    # ---- SETUP TYPE SCREEN -->
    print(f'data_setup_type_dropdown_selection: {data_setup_type_dropdown_selection}')
    if data_setup_type_dropdown_selection == 'Analogue Signal':
        data_control_min = data_sensor1_min_physical_value
        data_control_max = data_sensor1_max_physical_value * 0.98
        pval_data_setup_types_min_speed = data_analogue_signal_min_speed
        pval_data_setup_types_max_speed = data_analogue_signal_max_speed
        data_setpoint = data_single_speed
        
        
    elif data_setup_type_dropdown_selection == 'Local Sensor':
        data_control_min = data_local_sensor_min_control_signal
        data_control_max = data_local_sensor_max_control_signal
        pval_data_setup_types_min_speed = data_local_sensor_min_speed
        pval_data_setup_types_max_speed = data_local_sensor_max_speed
        data_setpoint = (data_local_sensor_min_control_signal / data_sensor1_max_physical_value) * 100

    elif data_setup_type_dropdown_selection == 'Digital Modbus':
        data_control_min = data_sensor1_min_physical_value
        data_control_max = data_sensor1_max_physical_value
        pval_data_setup_types_min_speed = data_digital_modbus_min_speed
        pval_data_setup_types_max_speed = data_digital_modbus_max_speed
        data_setpoint = data_single_speed

    elif data_setup_type_dropdown_selection == 'Single Speed':
        data_control_min = data_sensor1_min_physical_value
        data_control_max = data_sensor1_max_physical_value
        pval_data_setup_types_min_speed = 0
        pval_data_setup_types_max_speed = data_single_speed
        data_setpoint = data_single_speed

    else:
        print(f'ERROR THIS SHOULD NOT HAPPEN! LINE 353')

    # ---- END <-- SET UP TYPE SCREEN

    print(f'values-> data_sensor1_max_physical_value:{data_sensor1_max_physical_value}, data_sensor1_min_physical_value:{data_sensor1_min_physical_value}, data_control_max:{data_control_max}, data_control_min:{data_control_min}')
    print(f'pval_data_setup_types_max_speed:{pval_data_setup_types_max_speed}, pval_data_setup_types_min_speed{pval_data_setup_types_min_speed}')

    # send data to fan definitions table
    new_fan_definition = FanDefinitions(
        standard = standard_definition_boolean,
        name = custom_fan_definition_name,
        fan_type = fan_type,
        comms =  data_comms,

        sensors_count = sensors_count,

        sensor_1_type = data_sensor1_signal_type,
        sensor_1_unit = data_sensor1_unit_of_measure,
        sensor_1_min = data_sensor1_min_physical_value,
        sensor_1_max = data_sensor1_max_physical_value,

        sensor_2_type = data_sensor2_signal_type,
        sensor_2_unit = data_sensor2_unit_of_measure,
        sensor_2_min = data_sensor2_min_physical_value,
        sensor_2_max = data_sensor2_max_physical_value,

        # command value is setpoint
        command_value = data_setpoint,
        control = data_control,
        minimum_speed = data_min_speed,
        maximum_speed = data_max_speed,
        # p value = (sensor1 max physical value - sensor1 min physical value) / ( adjust values ) * ( max speed - min speed from sensor setup types tab  )
        
        p_val = -(data_sensor1_max_physical_value - data_sensor1_min_physical_value) / (data_control_max - data_control_min) * (pval_data_setup_types_max_speed - pval_data_setup_types_min_speed),
        i_val = 0,
        control_min = data_control_min,
        control_max = data_control_max,
        smoke_detector = smoke_detector,
        emergency = data_emergency,
        emer_set_speed = data_function_speed_override,
        emer_external_signal = data_external_analogue_signal,
        emer_start_stop_signal = data_emer_start_stop_signal,
        timeout = data_comms_timeout_duration,
        timeout_speed = data_comms_timeout_speed,
        zone = data_zone,
        fault_relay = data_fault_relay,
        sub_fan_type = data_sub_fan_type

    )

    if sales_order_id:
        new_fan_definition.sales_order_id = sales_order_id
        
    db.session.add(new_fan_definition)
    db.session.commit()



def getCarelKeyValues_AnalogSensors():
    carel_key_object = CarelKeys.query.filter_by(carel_fieldname='analog_sensor_type').first()
    carel_key_id = carel_key_object.id
    carel_value_objects = CarelValues.query.filter_by(carel_key_id=carel_key_id).all()

    sensor_type_choices= []
    for carel_value_object in carel_value_objects:
        sensor_type_choices.append([carel_value_object.carel_value, carel_value_object.fieldname_value])

    return sensor_type_choices 
