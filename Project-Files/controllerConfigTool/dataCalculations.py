from operator import itemgetter
import logging
from controllerConfigTool.models import FanDefinitions, FanTypeRelationships


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def splitComma(inputstring):
    arrayOfRanges = inputstring.split(",")
    return arrayOfRanges


def splitDash(inputArray):
    address_pairs_list = []
    for range in inputArray:
        split_list = range.split("-")
        print('----split---', split_list)
        starting_value = int(split_list[0])
        try:
            if split_list[1]:
                ending_value = int(split_list[1])
        except:
            ending_value = starting_value
            
        address_pairs_list.append([starting_value, ending_value])
    return address_pairs_list


def createAddressArray(address_pairs_list):
    final_result = []
    for starting_value, ending_value in address_pairs_list:
        for address in range(starting_value, ending_value+1):
            final_result.append(address)

    return final_result


def decodeModbusRange(modbus_address_range: str) -> list:
    """
    Send method arg 1; string of modbus address range 
        (example:) 1-30,31-60,90
    Returns Array of modbus addresses
        (example:)
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 
        47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 90]

    """
    arrayOfRanges = splitComma(modbus_address_range)
    address_pairs_list = splitDash(arrayOfRanges)
    final_result = createAddressArray(address_pairs_list)
    return final_result


def getCountOfModbusRange(modbus_ranges_indexes: str) -> int:
    print(modbus_ranges_indexes)
    """
    Parameters: Send string of ranges 
        example: 1-10, 90
    Returns: count of total number of fans
        example: The above example would return a count of 11
    """
    modbus_ranges_indexes_list = decodeModbusRange(modbus_ranges_indexes)
    return len(modbus_ranges_indexes_list)


def convertFanTypesAndDataStructure(address_fantype_dictionary):
    """ convert dictionary to list of list with integer fan types (example):
        {'JVEC-HP': {1, 2, 3, 4, 5, 6, 7, 8}, 'JVEC-ULP': {9, 10, 11}, 'JVEC-SP': {35, 36, 37, 38, 39}} =>
        [[1, 1], [2, 1], [3, 1], [4, 1], [5, 1], [6, 1], [7, 1], [8, 1], [9, 2], [10, 2], [11, 2], [35, 1], [36, 1], [37, 1], [38, 1], [39, 1]]
    """
    result_array = []
    data = []
    # logger.info(f'address fantype dict: {address_fantype_dictionary}')
    modbus_address_list = []
    
    for fan_type in address_fantype_dictionary:
        print(address_fantype_dictionary, 'fan_dict')
        for address in address_fantype_dictionary[fan_type]:
            result_array.append([int(address), fan_type])

    # build dictionary with fantype relationship
    # {name: integer, name: integer, name: integer}
    fan_type_relationships_dictionary = {}

    fan_type_relationships_objects = FanTypeRelationships.query.all()
    for fan_type_relationships_object in fan_type_relationships_objects:
        fan_type_relationships_dictionary[fan_type_relationships_object.fan_type_name] = fan_type_relationships_object.fan_type_integer


    for address, fan_definition_name in result_array:
        # find fan_type given fan definition name
        print(result_array, 'result array')
        fan_definition_object = FanDefinitions.query.filter_by(name=fan_definition_name).first()
        fan_type = fan_definition_object.fan_type

        fan_type_int = fan_type_relationships_dictionary[fan_type]

        data.append([int(address), int(fan_type_int)])
        modbus_address_list.append(int(address))

    # logger.info(f'unsorted built array: {data}')

    # sort first value of tuple for each in the list, in case any modbus addresses are not in order
    sorted_result = sorted(data,key=itemgetter(0))
    # logger.info(f'sorted array: {sorted_result}')

    modbus_ranges = createModbusRangesString(modbus_address_list)

    return sorted_result, int(sorted_result[0][0]), modbus_address_list, modbus_ranges


def createModbusRangesString(modbus_address_list):
    # print(f'SHOW MODBUS ADDRESS LIST =================: {modbus_address_list}')

    if not modbus_address_list:
        return ""

    res = []

    # Initialise the values for start and end position variables along with the start
    current_position = modbus_address_list[0]
    start_position = 0
    ending_position = 0

    for i in range(1,len(modbus_address_list)):
        if modbus_address_list[i] == current_position+1:
            # Keep incrementing ending_position and start if the numbers are consecutive
            ending_position = i
            current_position=current_position+1
        else:
            # In case there is a break in the sequence, calculate the difference between start and end
            if start_position == ending_position:
                # If both pointing at same place add the element
                res.append(modbus_address_list[start_position])
            elif ending_position - start_position == 1:
                # In case there are just 2 numbers then add both of them
                res.append(modbus_address_list[start_position])
                res.append(modbus_address_list[ending_position])
            else:
                # In case there are more than 2 numbers, then add the start and end with dash in between
                res.append(str(modbus_address_list[start_position])+"-"+str(modbus_address_list[ending_position]))
            current_position = modbus_address_list[i]
            start_position = i
            ending_position = i
    # Finally just check in the same manner for the last elements
    if start_position == ending_position:
        res.append(modbus_address_list[start_position])
    elif ending_position - start_position == 1:
        res.append(modbus_address_list[start_position])
        res.append(modbus_address_list[ending_position])
    else:
        res.append(str(modbus_address_list[start_position])+"-"+str(modbus_address_list[ending_position]))

    modbus_ranges_string = None
    for item in res:
        if modbus_ranges_string:
            modbus_ranges_string = modbus_ranges_string + "," + str(item)
        else:
            modbus_ranges_string = str(item)
                        
    return modbus_ranges_string