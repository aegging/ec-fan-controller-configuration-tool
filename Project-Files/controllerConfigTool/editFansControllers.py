

from controllerConfigTool.models import AnalogSensors, Controller, Job
from controllerConfigTool import db
from controllerConfigTool.dataCalculations import decodeModbusRange



def edit_analog_sensors(sales_order_id, controller_id, modbus_ranges_index):
    """ creates all the appropriate new sensor assignments for a given controller """

    # create modbus address list using modbus string
    modbus_address_list = decodeModbusRange(modbus_ranges_index)

    # for each analog_sensor_id, determine analog_sensor_definition_id
    # create default dict {analog_sensor_definition_id: {[analog_sensor_id, analog_sensor_id, analog_sensor_id]}}
    definition_id_analog_id_list = []
    for analog_sensor_id in modbus_address_list:
        # get fbsensor definition id for this fan
        analog_sensor_object = AnalogSensors.query.filter_by(id=analog_sensor_id).first()
        analog_sensor_definitions_id = analog_sensor_object.analog_sensor_definitions_id
        definition_id_analog_id_list.append([analog_sensor_definitions_id, analog_sensor_id])

    default_dict = {}
    for definition_id, analog_sensor_id in definition_id_analog_id_list:
        default_dict.setdefault(definition_id, list()).append([int(analog_sensor_id)])

    # for each definition
    for analog_sensor_definition_id in default_dict:

        # for each analog_sensor_id with this definition
        for analog_sensor_id in default_dict[analog_sensor_definition_id]:

            # save controller_id to current fbsensor
            current_analog_sensor_object = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
                filter(AnalogSensors.id==analog_sensor_id).first()
            current_analog_sensor_object.controller_id = controller_id

            db.session.add(current_analog_sensor_object)
    
    db.session.commit()




""" Remove controller_id from Analog Sensors in the AnalogSensors table, given the controller_id """
def removeControllerIDFromAnalogSensors(sales_order_id, controller_id):

    all_analog_sensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).all()

    for analog_sensor_object in all_analog_sensor_objects :
        analog_sensor_object.controller_id = None
        db.session.add(analog_sensor_object)

    db.session.commit()


def deleteController(controller_id):
    controller_object = Controller.query.filter_by(id=controller_id).first()
    controller_object.fan_quantity = None
    controller_object.controller_type = None
    controller_object.controller_serial_number = None
    controller_object.input_by = None
    controller_object.input_on = None
    
    db.session.add(controller_object)
    db.session.commit()





def setControllerProgrammedFalse(controller_id):
    controller_object = Controller.query.filter_by(id=controller_id).first()
    controller_object.programmed = False

    db.session.add(controller_object)
    db.session.commit()


def setControllerProgrammmingErrorFalse(controller_id):
    controller_object = Controller.query.filter_by(id=controller_id).first()
    controller_object.programming_error = False

    db.session.add(controller_object)
    db.session.commit()

