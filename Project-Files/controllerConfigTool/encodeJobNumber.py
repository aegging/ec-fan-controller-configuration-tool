

def encodeJobNumber(job_number, modbus_starting_address, modbus_ending_address):
    # print("job number: " + str(job_number))
    # job_number, modbus_starting_address, modbus_ending_address = str(job_number, modbus_starting_address, modbus_ending_address)
    encoded_message = []
    encoded_message.extend(loop_and_encode(job_number))
    encoded_message.append(encode_character(';'))
    encoded_message.extend(loop_and_encode(modbus_starting_address))
    encoded_message.append(encode_character('-'))
    encoded_message.extend(loop_and_encode(modbus_ending_address))

    return(encoded_message)


def loop_and_encode(list):
    encoded_list = []
    for i in range(0, len(list)):
        encoded_list.append(encode_character(list[i]))
    return encoded_list
    

def encode_character(value):
    value = int(ord(value))
    return value

# if __name__ == "__main__":
#     print(encodeJobNumber('20-011043', '1', '29' ))



