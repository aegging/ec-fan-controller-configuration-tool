from controllerConfigTool.getInfoFromDatabase import getCarelKeyValues_AnalogFans
from controllerConfigTool.models import ControllerInputOutputs, ExpansionBoardInputOutput
from flask_wtf import FlaskForm
from wtforms import IntegerField
from wtforms.widgets import HiddenInput
from wtforms import SubmitField, SelectField, IntegerField
from wtforms.widgets import HiddenInput
from controllerConfigTool import db



def buildAnalogSensorDroppableForm(sensor_count, controller_id_value, expansion_board_number):

    analog_output_choices = getCarelKeyValues_AnalogFans()

    # default ControllerInputOutputs values if no data exist
    u8_analog_fan_type_default_value = u9_analog_fan_type_default_value = u10_analog_fan_type_default_value = None

    u8_min_fan_speed_percent_default_value = u9_min_fan_speed_percent_default_value = u10_min_fan_speed_percent_default_value = 0
    u8_min_demand_range_percent_default_value = u9_min_demand_range_percent_default_value = u10_min_demand_range_percent_default_value = 0

    u8_max_fan_speed_percent_default_value = u9_max_fan_speed_percent_default_value = u10_max_fan_speed_percent_default_value = 100
    u8_max_demand_range_percent_default_value = u9_max_demand_range_percent_default_value = u10_max_demand_range_percent_default_value = 100


    # find default values if there is existing data for this controller id
    existing_controller_input_output_data = bool(db.session.query(ExpansionBoardInputOutput).\
                                                filter(ExpansionBoardInputOutput.controller_id==controller_id_value).\
                                                filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).first())

    if existing_controller_input_output_data:
        expansion_board_input_output_object = db.session.query(ExpansionBoardInputOutput).\
                                                filter(ExpansionBoardInputOutput.controller_id==controller_id_value).\
                                                filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).first()


        if expansion_board_input_output_object.u8_analog_fan_type!= None:
            u8_analog_fan_type_default_value = expansion_board_input_output_object.u8_analog_fan_type
        if expansion_board_input_output_object.u9_analog_fan_type!= None:
            u9_analog_fan_type_default_value = expansion_board_input_output_object.u9_analog_fan_type
        if expansion_board_input_output_object.u10_analog_fan_type!= None:
            u10_analog_fan_type_default_value = expansion_board_input_output_object.u10_analog_fan_type

        if expansion_board_input_output_object.u8_min_fan_speed_percent!= None:
            u8_min_fan_speed_percent_default_value = expansion_board_input_output_object.u8_min_fan_speed_percent
        if expansion_board_input_output_object.u9_min_fan_speed_percent!= None:
            u9_min_fan_speed_percent_default_value = expansion_board_input_output_object.u9_min_fan_speed_percent
        if expansion_board_input_output_object.u10_min_fan_speed_percent!= None:
            u10_min_fan_speed_percent_default_value = expansion_board_input_output_object.u10_min_fan_speed_percent
        
        if expansion_board_input_output_object.u8_min_demand_range_percent!= None:
            u8_min_demand_range_percent_default_value = expansion_board_input_output_object.u8_min_demand_range_percent
        if expansion_board_input_output_object.u9_min_demand_range_percent!= None:
            u9_min_demand_range_percent_default_value = expansion_board_input_output_object.u9_min_demand_range_percent
        if expansion_board_input_output_object.u10_min_demand_range_percent!= None:
            u10_min_demand_range_percent_default_value = expansion_board_input_output_object.u10_min_demand_range_percent
        
        if expansion_board_input_output_object.u8_max_fan_speed_percent!= None:
            u8_max_fan_speed_percent_default_value = expansion_board_input_output_object.u8_max_fan_speed_percent
        if expansion_board_input_output_object.u9_max_fan_speed_percent!= None:
            u9_max_fan_speed_percent_default_value = expansion_board_input_output_object.u9_max_fan_speed_percent
        if expansion_board_input_output_object.u10_max_fan_speed_percent!= None:
            u10_max_fan_speed_percent_default_value = expansion_board_input_output_object.u10_max_fan_speed_percent
        
        if expansion_board_input_output_object.u8_max_demand_range_percent!= None:
            u8_max_demand_range_percent_default_value = expansion_board_input_output_object.u8_max_demand_range_percent
        if expansion_board_input_output_object.u9_max_demand_range_percent!= None:
            u9_max_demand_range_percent_default_value = expansion_board_input_output_object.u9_max_demand_range_percent
        if expansion_board_input_output_object.u10_max_demand_range_percent!= None:
            u10_max_demand_range_percent_default_value = expansion_board_input_output_object.u10_max_demand_range_percent
        



    class ControllerInputOutputAndExpansionBoardForm(FlaskForm):
        submit_button = SubmitField('Save')
        controller_id = IntegerField(default=controller_id_value, widget=HiddenInput(), render_kw={'readonly': True})

        u8_analog_fan_type = SelectField('Fan Type', default=u8_analog_fan_type_default_value, choices=analog_output_choices)
        u8_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=u8_min_fan_speed_percent_default_value)
        u8_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=u8_max_fan_speed_percent_default_value)
        u8_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=u8_min_demand_range_percent_default_value)
        u8_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=u8_max_demand_range_percent_default_value)

        u9_analog_fan_type = SelectField('Fan Type', default=u9_analog_fan_type_default_value, choices=analog_output_choices)
        u9_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=u9_min_fan_speed_percent_default_value)
        u9_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=u9_max_fan_speed_percent_default_value)
        u9_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=u9_min_demand_range_percent_default_value)
        u9_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=u9_max_demand_range_percent_default_value)

        u10_analog_fan_type = SelectField('Fan Type', default=u10_analog_fan_type_default_value, choices=analog_output_choices)
        u10_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=u10_min_fan_speed_percent_default_value)
        u10_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=u10_max_fan_speed_percent_default_value)
        u10_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=u10_min_demand_range_percent_default_value)
        u10_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=u10_max_demand_range_percent_default_value)

        available_sensor_count = IntegerField(default=sensor_count, widget=HiddenInput() ,render_kw={'readonly': True})




    return ControllerInputOutputAndExpansionBoardForm()



