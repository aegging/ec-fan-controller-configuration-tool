from flask import Blueprint

from flask import render_template, url_for, redirect, request
from flask.helpers import flash
from sqlalchemy.sql.expression import exists
from controllerConfigTool.models import AnalogSensorDefinitions, AnalogSensors,\
     ControllerInputOutputs, ExpansionBoardInputOutput, OutputZones

from controllerConfigTool import db
from controllerConfigTool.getInfoFromDatabase import getControllerID, findTagPrefixGivenSalesOrderID,\
    getSalesOrderValues
from controllerConfigTool.expansionboardconfigurations.utils import getListControllersWithExpansionBoardAssignment,\
    getListAvailableAnalogSensors, getListExpansionBoardsWithAssignments
from controllerConfigTool.expansionboardconfigurations.forms import buildAnalogSensorDroppableForm
from sqlalchemy import null
from copy import deepcopy
from sqlalchemy import text


expansionboardconfigurations_blueprint = Blueprint('expansionboardconfigurations', __name__)


@expansionboardconfigurations_blueprint.route('/expansion_board_assignment/<int:sales_order_id>/<int:current_controller>/<int:expansion_board_number>', methods=['GET', 'POST'])
def expansion_board_assignment(sales_order_id, current_controller, expansion_board_number):

    save_confirmation= ""

    # Get controller_id
    controller_id = getControllerID(current_controller, sales_order_id)

    # how many expansion boards for current controller
    expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()

    # get tag prefix
    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    # find finished controllers
    list_index_of_finished_controllers = getListControllersWithExpansionBoardAssignment(sales_order_id, tag_prefix)
    print(f'list_index_of_finished_controllers:  {list_index_of_finished_controllers}')

    list_index_of_finished_expansion_boards = getListExpansionBoardsWithAssignments(controller_id)
    print("----------------controller_id------------------",controller_id)
    print(f'list_index_of_finished_expansion_boards: {list_index_of_finished_expansion_boards}')

    # get all values from sales order table for this sales order
    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)

    # get available sensors
    dictionary_available_sensors, list_available_senors_ids = getListAvailableAnalogSensors(sales_order_id, controller_id)

    for sensor_index in dictionary_available_sensors:
        for sensor_address, sensor_definition_name in dictionary_available_sensors[sensor_index]:
            print(f'index: {sensor_index} || address: {sensor_address} || def name: {sensor_definition_name}')

    # return list of True/False depending if U1-U4 have been assigned a sensor id
    list_assigned_sensors = {}

    u1_u4_availability_boolean_list = [True, True, True, True]
    expansion_board_input_output_object = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id).\
        filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).first()
    

    print(f'list_available_senors_ids: {list_available_senors_ids}')
    input_output_and_expansion_board_form = buildAnalogSensorDroppableForm(len(list_available_senors_ids), controller_id, expansion_board_number)

    expansion_board_id = None

    # Handle Controller Input Output Table Fields
    controller_input_output_variable_list = [
    'u8_analog_fan_type','u9_analog_fan_type', 'u10_analog_fan_type', 
    'u8_min_fan_speed_percent', 'u9_min_fan_speed_percent', 'u10_min_fan_speed_percent', 
    'u8_max_fan_speed_percent', 'u9_max_fan_speed_percent', 'u10_max_fan_speed_percent', 
    'u8_min_demand_range_percent', 'u9_min_demand_range_percent', 'u10_min_demand_range_percent', 
    'u8_max_demand_range_percent', 'u9_max_demand_range_percent', 'u10_max_demand_range_percent']


    if expansion_board_input_output_object:
        expansion_board_id = expansion_board_input_output_object.id
        print(f'expansion_board_id: {expansion_board_id}')

        u1_analog_sensor_id = expansion_board_input_output_object.u1_analog_sensor_id
        u2_analog_sensor_id = expansion_board_input_output_object.u2_analog_sensor_id
        u3_analog_sensor_id = expansion_board_input_output_object.u3_analog_sensor_id
        u4_analog_sensor_id = expansion_board_input_output_object.u4_analog_sensor_id

        if u1_analog_sensor_id:
            u1_u4_availability_boolean_list[0] = u1_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u1_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u1_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

        if u2_analog_sensor_id:
            u1_u4_availability_boolean_list[1] = u2_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u2_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u2_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

        if u3_analog_sensor_id:
            u1_u4_availability_boolean_list[2] = u3_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u3_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u3_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])

        if u4_analog_sensor_id:
            u1_u4_availability_boolean_list[3] = u4_analog_sensor_id

            analog_sensor_object = AnalogSensors.query.filter_by(id=u4_analog_sensor_id).first()
            current_sensor_address = analog_sensor_object.modbus_address
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name
            list_assigned_sensors.setdefault(u4_analog_sensor_id, list()).append([int(current_sensor_address), current_sensor_definition_name])



    print(f'list_assigned_sensors: {list_assigned_sensors}')


    if request.method == "POST":
        # if a Controller button on the left side of page is clicked, navigate to that controller
        for i in range(1, sales_order_controller_quantity + 1):
            controller_name = 'controller' + str(i)
            if controller_name in request.form:
                # before redirect, if fans have been selected, consider current controller configuration as complete. Thus save finished modbus range data into the controller table
                # if controller doesn't exist, create it
                # expansion_board_exist = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).first())
                # if expansion_board_exist:
                return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=i,
                expansion_board_number=1))
                # else:
                    # create expansion board
                    # controller_object = ExpansionBoardInputOutput(
                    #     controller_id = controller_id,
                    #     sales_order_id = sales_order_id,
                    #     expansion_board_number = expansion_board_number
                    # )
                    # db.session.add(controller_object)
                    # db.session.commit()
                    # return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=i, expansion_board_number=1))

        for i in range(1, expansion_board_count + 1):
            expansion_board = 'expansionboard' + str(i)
            if expansion_board in request.form:
                return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=current_controller,
                expansion_board_number=i))
        

        if "clear_button_assignments" in request.form:
            # clear all existing assignments
            if expansion_board_id:

                expansion_board_object = ExpansionBoardInputOutput.query.filter_by(id=expansion_board_id).first()

                if expansion_board_object:
                    expansion_board_object.u1_analog_sensor_id = null()
                    expansion_board_object.u2_analog_sensor_id = null()
                    expansion_board_object.u3_analog_sensor_id = null()
                    expansion_board_object.u4_analog_sensor_id = null()
                    expansion_board_object.expansion_outputs_saved = null()

                    db.session.add(expansion_board_object)

                analog_sensor_objects = AnalogSensors.query.filter_by(expansion_board_id=expansion_board_id).all()
            
                if analog_sensor_objects:
                    for analog_sensor_object in analog_sensor_objects:
                        analog_sensor_object.expansion_board_id = null()
                        analog_sensor_object.controller_id = null()

                        db.session.add(analog_sensor_object)

                db.session.commit()

                return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=current_controller,
                    expansion_board_number=expansion_board_number))
                    
            else:
                flash('Nothing to Clear','danger')
                return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=current_controller,
                    expansion_board_number=expansion_board_number))

        elif "navigate_sales_order_status" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif input_output_and_expansion_board_form.validate_on_submit(): 
            if input_output_and_expansion_board_form.submit_button.data:
                save_confirmation=True
                print(f'inside from submit')
                input_output_and_expansion_board_form_dictionary = input_output_and_expansion_board_form.data
                input_output_and_expansion_board_form_dictionary.pop('submit_button')
                input_output_and_expansion_board_form_dictionary.pop('csrf_token')
                input_output_and_expansion_board_form_dictionary.pop('available_sensor_count')
            
                # make a dictionary for ControllerInputOutput and ExpansionBoardInputOutput
                # seperate the data such that it can be pushed to 2 different tables
                input_output_and_expansion_board_dictionary = deepcopy(input_output_and_expansion_board_form_dictionary)

                # give empty variables sqlalchemy valid null value
                for variable_name in controller_input_output_variable_list:
                    if input_output_and_expansion_board_dictionary[variable_name] == 'None':
                        input_output_and_expansion_board_dictionary[variable_name] = null()


                # place sales order id inside dictionary
                input_output_and_expansion_board_dictionary['sales_order_id'] = sales_order_id

                stmt = text("Select * From expansion_board_input_output Where id=:expansion_board_id and ((u1_analog_sensor_id IS NOT NULL) or (u2_analog_sensor_id IS NOT NULL) or (u3_analog_sensor_id IS NOT NULL)\
                    or (u4_analog_sensor_id IS NOT NULL) )")
                expansion_board_not_null = bool(db.session.query(ExpansionBoardInputOutput).from_statement(stmt).params(expansion_board_id=expansion_board_id).first())
                print(expansion_board_not_null, 'board varification----')
                if expansion_board_not_null:
                    input_output_and_expansion_board_dictionary['expansion_outputs_saved'] = True

                

                db.session.commit()

                # Save Controller Input Output Data
                # is there an existing Expansion Board Input Output data object?
                existing_expansion_board_input_output_object = bool(db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id).\
                                                                filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).first())
                if existing_expansion_board_input_output_object:
                    # existing one found, use this object
                    # controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
                    db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id).\
                                    filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).update(input_output_and_expansion_board_dictionary)
                    db.session.commit()
                else:
                    # report Error
                    flash(f'No Expansion Boards Available for Controller {current_controller}', 'danger')


            return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=current_controller,
                    expansion_board_number=expansion_board_number))



    return render_template('expansion-board-droppable.html', 
            input_output_and_expansion_board_form=input_output_and_expansion_board_form, 
            dictionary_available_sensors=dictionary_available_sensors, u1_u4_availability_boolean_list=u1_u4_availability_boolean_list,
            list_assigned_sensors=list_assigned_sensors, list_available_senors_ids=list_available_senors_ids,
            sales_order_id=sales_order_id, current_controller=current_controller,
            sales_order_controller_quantity=sales_order_controller_quantity,
            list_index_of_finished_controllers=list_index_of_finished_controllers, tag_prefix=tag_prefix,
            expansion_board_count=expansion_board_count, expansion_board_number=expansion_board_number,
            list_index_of_finished_expansion_boards=list_index_of_finished_expansion_boards, controller_input_output_variable_list=controller_input_output_variable_list, save_confirmation=save_confirmation)



@expansionboardconfigurations_blueprint.route("/redirectexpansionboardassignment/<int:sales_order_id>/<int:current_controller>/<int:expansion_board_number>/<controllerPort>/<int:sensor_id>", methods=['GET', 'POST'])
def redirectexpansionboardassignment(sales_order_id, current_controller, expansion_board_number, controllerPort, sensor_id):

    # Get data to show fans selected for currently selected controller
    controller_id = getControllerID(current_controller, sales_order_id)


    existing_expansion_boards_boolean = bool(db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id).\
        filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).first())

    if existing_expansion_boards_boolean:
        expansion_board_object = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id).\
        filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_number).first()
        expansion_board_id = expansion_board_object.id

        print(f'controller id: {controller_id}')


        if controllerPort == 'u1_analog_sensor_id':
            expansion_board_object.u1_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.expansion_board_id = expansion_board_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()

        elif controllerPort == 'u2_analog_sensor_id':
            expansion_board_object.u2_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.expansion_board_id = expansion_board_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()

        elif controllerPort == 'u3_analog_sensor_id':
            expansion_board_object.u3_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.expansion_board_id = expansion_board_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()

        elif controllerPort == 'u4_analog_sensor_id':
            expansion_board_object.u4_analog_sensor_id = sensor_id
            analog_sensors_object = AnalogSensors.query.filter_by(id=sensor_id).first()
            analog_sensors_object.expansion_board_id = expansion_board_id
            analog_sensors_object.sales_order_id = sales_order_id
            db.session.add(analog_sensors_object)
            db.session.commit()
        
        db.session.add(expansion_board_object)
        db.session.commit()

    else:
        flash('No expansion Boards Available', 'danger')
        return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=current_controller,
                expansion_board_number=1))


    return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=current_controller,
     expansion_board_number=expansion_board_number))



