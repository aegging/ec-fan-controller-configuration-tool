from controllerConfigTool.models import Controller, AnalogSensorDefinitions, AnalogSensors, ExpansionBoardInputOutput
from controllerConfigTool import db
from sqlalchemy import text


def getListControllersWithExpansionBoardAssignment(sales_order_id, tag_prefix):
    """ return a list of controllers with any items assignment to them """

    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)

    controller_id_fans_found_list = []

    # for each controller id, search ExpansionBoard table for a match, if there is a match add to list as finished
    for this_controller_id in controller_id_list:
        found_analog_sensor_controller_id_boolean = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=this_controller_id).first())

        if found_analog_sensor_controller_id_boolean:

            stmt = text("Select * From expansion_board_input_output Where controller_id=:controller_id and expansion_outputs_saved IS NOT NULL and ((u8_analog_fan_type IS NOT NULL) or (u9_analog_fan_type IS NOT NULL)\
                or (u10_analog_fan_type IS NOT NULL) or (u1_analog_sensor_id IS NOT NULL) or (u2_analog_sensor_id IS NOT NULL) or (u3_analog_sensor_id IS NOT NULL)\
                    or (u4_analog_sensor_id IS NOT NULL))")
            all_expansion_count = ExpansionBoardInputOutput.query.filter_by(controller_id=this_controller_id).count()
            all_saved_count = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==this_controller_id).filter(ExpansionBoardInputOutput.expansion_outputs_saved!=None).count()
            
            stmt = text("Select * From expansion_board_input_output Where controller_id=:controller_id and expansion_outputs_saved IS NOT NULL ")
            finished_expansion_board_boolean = bool(db.session.query(ExpansionBoardInputOutput).from_statement(stmt).params(controller_id=this_controller_id).first())
            all_saved_count1 = db.session.query(ExpansionBoardInputOutput).from_statement(stmt).params(controller_id=this_controller_id).all()
            if all_expansion_count == all_saved_count and finished_expansion_board_boolean:
                controller_id_fans_found_list.append(this_controller_id)


    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)

    return controller_number_list


def getListExpansionBoardsWithAssignments(controller_id):
    expansion_board_number_list = []
    stmt = text("Select * From expansion_board_input_output Where controller_id=:controller_id and expansion_outputs_saved IS NOT NULL and ((u8_analog_fan_type IS NOT NULL) or (u9_analog_fan_type IS NOT NULL)\
                or (u10_analog_fan_type IS NOT NULL) or (u1_analog_sensor_id IS NOT NULL) or (u2_analog_sensor_id IS NOT NULL) or (u3_analog_sensor_id IS NOT NULL)\
                    or (u4_analog_sensor_id IS NOT NULL) )")
    finished_expansion_board_boolean = bool(db.session.query(ExpansionBoardInputOutput).from_statement(stmt).params(controller_id=controller_id).first())
    if finished_expansion_board_boolean:
        expansion_board_objects = db.session.query(ExpansionBoardInputOutput).from_statement(stmt).params(controller_id=controller_id).all()
        for expansion_board_object in expansion_board_objects:
            expansion_board_number_list.append(expansion_board_object.expansion_board_number)

    return expansion_board_number_list

def getListAvailableAnalogSensors(sales_order_id, controller_id):
    # find all analog sensors available (null controller id and expansion board id)
    available_sensors_boolean = bool(db.session.query(AnalogSensors).filter(AnalogSensors.controller_id==None).\
                                        filter(AnalogSensors.expansion_board_id==None).\
                                            filter(AnalogSensors.fan_id==None).\
                                                filter(AnalogSensors.sales_order_id==sales_order_id).\
                                                    filter(AnalogSensors.analog_sensor_definitions_id!=None).\
                                                        first())

    analog_sensor_id_dictionary = {}
    analog_sensor_id_list = []
    

    # if any exist
    if available_sensors_boolean:
        available_analog_sensors_objects = db.session.query(AnalogSensors).filter(AnalogSensors.controller_id==None).\
                                        filter(AnalogSensors.expansion_board_id==None).\
                                            filter(AnalogSensors.fan_id==None).\
                                                filter(AnalogSensors.sales_order_id==sales_order_id).\
                                                    filter(AnalogSensors.analog_sensor_definitions_id!=None).\
                                                        all()

        # for each available sensors, add to list
        for analog_sensor_object in available_analog_sensors_objects:
            # find the address and definition name
            current_sensor_address = analog_sensor_object.modbus_address
            
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_object.analog_sensor_definitions_id).first()
            current_sensor_definition_name = analog_sensor_definition_object.definition_name

            current_sensor_definition_standard_boolean = analog_sensor_definition_object.standard

            if current_sensor_definition_standard_boolean:
                analog_sensor_id_dictionary.setdefault(analog_sensor_object.id, list()).append([int(current_sensor_address), current_sensor_definition_name])
                analog_sensor_id_list.append(analog_sensor_object.id)
            else:
                analog_sensor_id_dictionary.setdefault(analog_sensor_object.id, list()).append([int(current_sensor_address), f'**{current_sensor_definition_name}'])
                analog_sensor_id_list.append(analog_sensor_object.id)




    return analog_sensor_id_dictionary, analog_sensor_id_list