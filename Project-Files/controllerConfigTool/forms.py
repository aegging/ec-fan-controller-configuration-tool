from flask_wtf import FlaskForm
from sqlalchemy.sql.sqltypes import Integer, String
from wtforms import StringField,  SubmitField, BooleanField, IntegerField, SelectField, TextField,\
    FieldList, FormField, Form
from wtforms.fields.core import FloatField
# from wtforms_components import read_only
from wtforms.widgets import HiddenInput
from wtforms.validators import DataRequired, ValidationError, Optional,InputRequired 
from controllerConfigTool.models import SalesOrders, Settings, FanValues, Job, Sensors, FanDefinitions
from wtforms_sqlalchemy.fields import QuerySelectField
from controllerConfigTool.dataCalculations import getCountOfModbusRange
from controllerConfigTool import db, app
from sqlalchemy import text
# from controllerConfigTool.transverseDirectories import findDirectories



class SalesOrderForm(FlaskForm):
    def checkFanRange(form, field):
        modbus_address = field.data
        if modbus_address:
            if modbus_address < 1 or modbus_address > 247:
                raise ValidationError(f'Invalid Modbus Address Value: {modbus_address}. Values must be within range 1-247.')

    def defaultTimeZone():
        settings_object = Settings.query.filter_by(currently_selected = True).first()
        selected_time_zone = settings_object.time_zone
        return selected_time_zone

    sales_order_number = StringField('Sales Order Number', validators=[DataRequired()])
    controller_quantity = IntegerField('Controller Quantity', validators=[DataRequired()])
    job_type = SelectField('Job Type', choices=['Warehouse'], validators=[DataRequired()])
    tag_name = StringField('Controller Tag Name', validators=[DataRequired()])
    fan_range_1_starting_address = IntegerField('Starting Modbus Address', validators=[Optional(), checkFanRange])
    fan_range_1_ending_address = IntegerField('Ending Modbus Address', validators=[Optional(), checkFanRange])
    fan_range_2_starting_address = IntegerField('Starting Modbus Address', validators=[Optional(), checkFanRange])
    fan_range_2_ending_address = IntegerField('Ending Modbus Address', validators=[Optional(), checkFanRange])
    entered_by = StringField('Entered by:', validators=[DataRequired()])
    time_zone = SelectField('Time Zone: ', choices=[('11','Chicago'),('9','Denver'),('15','New York'),('6','Los Angeles'),('7','Phoenix'),('90','SYD/MELB')], validate_choice=False, default=defaultTimeZone)

    modbus_address_count = StringField('Fan Count:', render_kw={'readonly': True})
    file_path_boolean = BooleanField('Enable Archive Directory', default=True)
    submit = SubmitField('Configure Order')


    def validate_sales_order_number(self, sales_order_number):
        sales_order = SalesOrders.query.filter_by(sales_order_number=sales_order_number.data).first()
        if sales_order:
            raise ValidationError('The Sales Order entered as already been used. Please choose a different one.')
    
    def validate_tag_name(self, tag_name):
        if len(tag_name.data) > 7:
            raise ValidationError('The maximum length for the Tag Name field is 7 characters. Please choose a shorter tag name.')


def initChooseFilePathForm(root_directory):

    # folders = findDirectories(root_directory)
    folders = []
    folders.insert(0, ["",""])


    class ChooseFilePathForm(FlaskForm):

        file_path = SelectField('Choose Archive Directory', default=folders, choices=[(path, name) for path, name in folders])
        current_path = TextField('Current Path Selected', default=root_directory, render_kw={'readonly': True})
        submit_button = SubmitField('Save')

    return ChooseFilePathForm


class ClearFilePathForm(FlaskForm):
    clear_path = SubmitField('Clear')



def initializeControllerForm(default_version_id, sales_order_id):
    fan_choices = [""]
    stmt = text("Select * From fan_definitions Where (standard=True) or sales_order_id=:sales_order_id")
    fan_values_objects_boolean = bool(db.session.query(FanDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all())
    if fan_values_objects_boolean:
        fan_values_objects = db.session.query(FanDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all()

        for fan_values in fan_values_objects:
            fan_choices.append(fan_values.name)
    # else:
    #     fan_choices.append("No Definitions Available")
 

    class ControllerForm(FlaskForm):

        fan_quantity = IntegerField('Fan Quantity', render_kw={'readonly': True}, validators=[DataRequired()])
        modbus_ranges_indexes = StringField('Modbus Range Indexes', widget=HiddenInput(), render_kw={'readonly': True})
        modbus_ranges = StringField('Modbus Ranges', render_kw={'readonly': True}, validators=[DataRequired()])
        gfa_fire_NC = BooleanField('GFA Fire N/C', default=True)
        fan_type = SelectField('Select Fan Definition:', choices=fan_choices, validate_choice=False, default=fan_choices[0])
        save_configuration = SubmitField('Add Fans')
        clear_configuration = SubmitField('Clear Configuration')
        version_number = QuerySelectField('Software Version',
                                        query_factory=lambda: Settings.query.order_by(Settings.id).all(),
                                        default=Settings.query.filter_by(id=default_version_id).one()
                                        )

        def validate_modbus_ranges_indexes(self, modbus_ranges_indexes):
            if modbus_ranges_indexes.data:
                modbus_range_count = getCountOfModbusRange(modbus_ranges_indexes.data)
                if modbus_range_count < 0 or modbus_range_count > 50:
                    raise ValidationError(f'Invalid number of fans selected ({modbus_range_count}). Max number of fans allowed per controller is 50.')  

        def validate_fan_type(self, fan_type):
            if fan_type.data == "Blank" or fan_type.data == "":
                raise ValidationError(f'Fan type cannot be blank')

    return ControllerForm


class SearchSalesOrderForm(FlaskForm):
    sales_order_number = StringField('Sales Order Number', validators=[DataRequired()])
    program_controllers = SubmitField('Program Controllers')
    edit_sales_order = SubmitField('Edit Sales Order')
    find_fans = SubmitField('Find All Fans')
    find_controllers = SubmitField('Find All Controllers in Systems Table')



class ExpansionBoardForm(Form):
    def analog_sensor_choices1():
        analog_sensor_choices1 = [(0, 'None'), (0,'4-20mA'), (6, '0-10V'), (13, 'TEMP-NTC')]
        return analog_sensor_choices1

    def analog_sensor_choices2():
        analog_sensor_choices2 = [(0, 'None'), (1, 'CO'), (2, 'NO2'), (3, 'CO2'), (4, 'Temp'), (5, 'Dem'), (6, 'RH')]
        return analog_sensor_choices2
    
    def create_output_fan_choices():
        analog_output_choices = [(3, 'None'), (0, 'Supply'), (1, 'Mixer'), (2, 'Exhaust')]
        return analog_output_choices

    expansionboard_u1_dropdown1 = SelectField('U1', choices=analog_sensor_choices1())
    expansionboard_u2_dropdown1 = SelectField('U2', choices=analog_sensor_choices1())
    expansionboard_u3_dropdown1 = SelectField('U3', choices=analog_sensor_choices1())
    expansionboard_u4_dropdown1 = SelectField('U4', choices=analog_sensor_choices1())

    expansionboard_u1_dropdown2 = SelectField(choices=analog_sensor_choices2())
    expansionboard_u2_dropdown2 = SelectField(choices=analog_sensor_choices2())
    expansionboard_u3_dropdown2 = SelectField(choices=analog_sensor_choices2())
    expansionboard_u4_dropdown2 = SelectField(choices=analog_sensor_choices2())

    expansionboard_u8_dropdown = SelectField('U8', choices=create_output_fan_choices())
    expansionboard_u9_dropdown = SelectField('U9', choices=create_output_fan_choices())
    expansionboard_u10_dropdown = SelectField('U10', choices=create_output_fan_choices())


class ExpansionBoardsFieldList(Form):
    name = StringField('Name', widget=HiddenInput(), render_kw={'readonly': True})
    expansion_board = FieldList(FormField(ExpansionBoardForm), min_entries=5, max_entries=5)


class ProgramControllersForm(FlaskForm):
    def create_analog_sensor_choices1():
        analog_sensor_choices1 = [(0, 'None'), (0,'4-20mA'), (6, '0-10V'), (13, 'TEMP-NTC')]
        return analog_sensor_choices1
    
    def create_analog_sensor_choices2():
        analog_sensor_choices2 = [(0, 'None'), (1, 'CO'), (2, 'NO2'), (3, 'CO2'), (4, 'Temp'), (5, 'Dem'), (6, 'RH')]
        return analog_sensor_choices2

    def create_output_fan_choices():
        analog_output_choices = [(3, 'None'), (0, 'Supply'), (1, 'Mixer'), (2, 'Exhaust')]
        return analog_output_choices

    ip = StringField('IP', validators=[DataRequired()])
    controller_type = SelectField('Type', validators=[DataRequired()])
    controller_serial_number = StringField('Serial Number', validators=[DataRequired()])
    webpages_boolean = BooleanField('Webpages',  default=True)
    upload_ap1_file_boolean = BooleanField('AP1 File (Software)',  default=True)

    fb_sensor_starting_address = IntegerField('Starting Address', validators=[Optional()])
    fb_sensor_ending_address = IntegerField('Ending Address', validators=[Optional()])
    fb_sensor_co_alarm = FloatField('CO Alarm Threshold', validators=[Optional()])
    fb_sensor_no2_alarm = FloatField('NO2 Alarm Threshold', validators=[Optional()])

    mixerfan_min_fanspeed = IntegerField('Min Fanspeed', default=25, validators=[DataRequired()])
    mixerfan_max_fanspeed = IntegerField('Max Fanspeed', default=100, validators=[DataRequired()])

    
    controller_u1_dropdown1 = SelectField('U1', choices=create_analog_sensor_choices1())
    controller_u2_dropdown1 = SelectField('U2', choices=create_analog_sensor_choices1())
    controller_u3_dropdown1 = SelectField('U3', choices=create_analog_sensor_choices1())
    controller_u4_dropdown1 = SelectField('U4', choices=create_analog_sensor_choices1())

    controller_u1_dropdown2 = SelectField(choices=create_analog_sensor_choices2())
    controller_u2_dropdown2 = SelectField(choices=create_analog_sensor_choices2())
    controller_u3_dropdown2 = SelectField(choices=create_analog_sensor_choices2())
    controller_u4_dropdown2 = SelectField(choices=create_analog_sensor_choices2())

    controller_y1_dropdown = SelectField('Y1', choices=create_output_fan_choices())
    controller_y2_dropdown = SelectField('Y2', choices=create_output_fan_choices())
    controller_y3_dropdown = SelectField('Y3', choices=create_output_fan_choices())
    controller_y4_dropdown = SelectField('Y4', choices=create_output_fan_choices())

    expansion_board_quantity_dropdown = SelectField('Quantity', choices=[0,1,2,3,4,5])
    fieldbus_sensors_boolean_dropdown = SelectField('Quantity', choices=[0, '> 0'])
    controller_fan_demand_dropdown = SelectField('Fan Demand', choices=['N', 'Y'])

    fan_demand_min_fan_speed = IntegerField('Min', default = 0)
    fan_demand_max_fan_speed = IntegerField('Max', default = 100)
    fan_demand_min_demand_range = IntegerField('Min', default = 0)
    fan_demand_max_demand_range = IntegerField('Max', default = 100)


    program_controller_button = SubmitField('Program Controller')

    def validate_mixerfan_min_fanspeed(self, mixerfan_min_fanspeed):
        if mixerfan_min_fanspeed.data:
            __mixerfan_min_speed = mixerfan_min_fanspeed.data
            if __mixerfan_min_speed < 0 or __mixerfan_min_speed > 100:
                raise ValidationError('Invalid value for Mixer Fan Min Speed. Choose a value in range 0-100')


    def validate_mixerfan_max_fanspeed(self, mixerfan_max_fanspeed):
        if mixerfan_max_fanspeed.data:
            __mixerfan_max_speed = mixerfan_max_fanspeed.data
            if __mixerfan_max_speed < 0 or __mixerfan_max_speed > 100:
                raise ValidationError('Invalid value for Mixer Fan Min Speed. Choose a value in range 0-100')


    def validate_controller_type(self, controller_type):
        if controller_type.data == 'No Controller Type Enabled':
            raise ValidationError(f'Selected Software Version Does Not Have A Controller Type Enabled')




class ProgramControllersAndExpansionBoardFieldListForm(FlaskForm):
    expansion_board_forms = FormField(ExpansionBoardsFieldList)
    program_controllers_form = FormField(ProgramControllersForm)



def initializeSettingsForm(version_id):
    are_there_settings = bool(Settings.query.filter_by(id=version_id).first())
    if are_there_settings:
        settings_object = Settings.query.filter_by(id=version_id).first()
        version_number_value = settings_object.version_number
        cfactory_path_value = settings_object.cfactory_path
        mini_http_folder_directory_value = settings_object.mini_http_folder_directory
        max_http_folder_directory_value = settings_object.max_http_folder_directory
        mini_ap1_directory_value = settings_object.mini_ap1_directory
        max_ap1_directory_value = settings_object.max_ap1_directory
        time_zone_value = settings_object.time_zone
        enable_version_value = settings_object.enable_version
        valid_with_max_controller_value = settings_object.valid_with_max_controller
        valid_with_mini_controller_value = settings_object.valid_with_mini_controller
    else:
        version_number_value = None
        cfactory_path_value = None
        mini_http_folder_directory_value = None
        max_http_folder_directory_value = None
        mini_ap1_directory_value = None
        max_ap1_directory_value = None
        time_zone_value = None
        enable_version_value = True
        valid_with_max_controller_value = False
        valid_with_mini_controller_value = False


    class SettingsForm(FlaskForm):
        if are_there_settings:
            version_number=StringField('Enter Version Number', default=version_number_value, render_kw={'readonly': True})
            cfactory_path=StringField('Enter Path for C.factory.exe', default=cfactory_path_value)
            mini_http_folder_directory=StringField('Enter Path for Mini HTTP Folder', default=mini_http_folder_directory_value)
            max_http_folder_directory=StringField('Enter Path for Max HTTP Folder', default=max_http_folder_directory_value)
            mini_ap1_directory=StringField('Enter Path for Mini Ap1 file', default=mini_ap1_directory_value)
            max_ap1_directory=StringField('Enter Path for Max Ap1 file', default=max_ap1_directory_value)
            enable_version = BooleanField('Enable Version To Allow Access To Users', default=enable_version_value)
            valid_with_max_controller = BooleanField('Enable for Max Controllers', default=valid_with_max_controller_value)
            valid_with_mini_controller = BooleanField('Enable for Mini Controllers', default=valid_with_mini_controller_value)
            time_zone = SelectField('Time Zone: ', choices=[('11','Chicago'),('9','Denver'),('15','New York'),('6','Los Angeles'),('7','Phoenix'),('90','SYD/MELB')], validate_choice=False, default=time_zone_value)
        else:
            version_number=StringField('Enter Version Number')
            cfactory_path=StringField('Enter Path For C.factory.exe')
            mini_http_folder_directory=StringField('Enter Path For Mini HTTP Folder')
            max_http_folder_directory=StringField('Enter Path For Max HTTP Folder')
            mini_ap1_directory=StringField('Enter Path For Mini Ap1 File')
            max_ap1_directory=StringField('Enter Path For Max Ap1 File')
            enable_version = BooleanField('Enable Version To Allow Access To Users', default=False)
            valid_with_max_controller = BooleanField('Enable for Max Controllers', default=False)
            valid_with_mini_controller = BooleanField('Enable for Mini Controllers', default=False)
            time_zone = SelectField('Time Zone: ', choices=[('11','Chicago'),('9','Denver'),('15','New York'),('6','Los Angeles'),('7','Phoenix'),('90','SYD/MELB')], validate_choice=False)
     
        submit = SubmitField('Save Settings')

    return SettingsForm


class SelectVersionForm(FlaskForm):
    def settings_query():
        return Settings.query.filter_by(enable_version = True).all()

    version_number = QuerySelectField(query_factory=settings_query, allow_blank=True, get_label="version_number",validators=[DataRequired()])
    save_settings = SubmitField('Save Settings')
    edit_settings = SubmitField('Edit Settings')


class AdminForm(FlaskForm):
    add_version_button = SubmitField('Add New')
    edit_version_button = SubmitField('Edit Existing')
    create_standard_fan_definition_button = SubmitField('Add New')



def initializeCustomFanDefinitionForm(fan_type, standard, definition_name):
    fan_type_value = fan_type
    standard_value = standard
    definition_name_value = definition_name
    print(f'fan_type: {fan_type_value}')
    fans_object = FanValues.query.filter_by(name=fan_type_value).first()
    sensor_count_value = fans_object.no_sensors

    sensor_count_choices = []

    for i in range(0,sensor_count_value+1):
        sensor_count_choices.append(i)

    default_single_speed_max_speed = fans_object.max_speed

    default_digital_modbus_max_speed = fans_object.max_speed
    default_digital_modbus_min_speed = 0
    default_digital_modbus_min_speed_zone = [1,2,3,4,5,6]

    default_analogue_signal_max_speed = fans_object.max_speed
    default_analogue_signal_min_speed = 0

    control_signal_choices = []
    _0_10v_value = fans_object._0_10v
    _4_20ma_value = fans_object._4_20ma

    default_local_sensor_max_speed = fans_object.max_speed
    default_local_sensor_min_speed = 0

    # if fan definition in database is set to 1 for this fan type; make available to select
    if _0_10v_value == 1:
        control_signal_choices.append('0-10V')
    if _4_20ma_value == 1:
        control_signal_choices.append('4-20mA')

    # sensors_objects = Sensors.query.filter_by(current="True").all()
    sensor_choices = []
    
    # for sensor in sensors_objects:
    #     sensor_choices.append(sensor.name)
    
    sensor_choices.append('LPT-END')
    sensor_choices.append('LPT-TCO')
    sensor_choices.append('OTHER')
        
    sensors_object = Sensors.query.filter_by(name="LPT-END").first()

    lpt_end_unit_of_measure_value = sensors_object.unit_of_measure
    lpt_end_min_value = int(sensors_object.min_value)
    lpt_end_max_value = int(sensors_object.max_value)
    lpt_end_current_draw_value = sensors_object.current_draw
    lpt_end_signal_type_value = sensors_object.signal_type

    print(lpt_end_unit_of_measure_value)

    sensors_object2 = Sensors.query.filter_by(name="LPT-TCO").first()

    lpt_tco_unit_of_measure_value = sensors_object2.unit_of_measure
    lpt_tco_min_value = int(sensors_object2.min_value)
    lpt_tco_max_value = int(sensors_object2.max_value)
    lpt_tco_current_draw_value = sensors_object2.current_draw
    lpt_tco_signal_type_value = sensors_object2.signal_type

    print(lpt_tco_unit_of_measure_value)



    class CustomFanDefinitionForm(FlaskForm):

        standard_string = StringField('Standard Definition', default=standard_value, render_kw={'readonly': True})
        definition_name = StringField('Custom Fan Definition Name', default=definition_name_value, render_kw={'readonly': True})
        fan_type = StringField('Fan Type', default=fan_type_value, render_kw={'readonly': True})
        save_definition_button = SubmitField('Save Definition')
        cancel_fan_definition = SubmitField('Cancel')


        single_speed = IntegerField('Operation Speed', default=default_single_speed_max_speed)

        digital_modbus_matching_controller = SelectField('Matching Controller', choices=["Aviator Control","BMS Control"], default="Aviator Control") 
        digital_modbus_max_speed = IntegerField('Max Speed', default=default_digital_modbus_max_speed)
        digital_modbus_min_speed = IntegerField('Min Speed', default=default_digital_modbus_min_speed)
        digital_modbus_zone = SelectField('Zone', choices=default_digital_modbus_min_speed_zone, default=default_digital_modbus_min_speed_zone[0])

        analogue_signal_control_signal = SelectField('Control Signal', choices=control_signal_choices)
        analogue_signal_max_speed = IntegerField('Max Speed', default=default_analogue_signal_max_speed)
        analogue_signal_min_speed = IntegerField('Min Speed', default=default_analogue_signal_min_speed)

        local_sensor_set_point = IntegerField('Set Point', render_kw={'readonly': True})
        local_sensor_max_speed = IntegerField('Max Speed', default=default_local_sensor_max_speed)
        local_sensor_min_speed = IntegerField('Min Speed', default=default_local_sensor_min_speed)
        local_sensor_max_control_signal = IntegerField('Max Control Signal', default=0)
        local_sensor_min_control_signal = IntegerField('Min Control Signal', default=0)
        local_sensor_sensor_logic = SelectField('Sensor Logic', choices=["Sensor 1", "Max of Sensor 1 and 2"], default="Sensor 1")

        setup_type_selection = SelectField('Select Setup Type', choices=["Single Speed","Digital Modbus","Analogue Signal","Local Sensor"],default="Digital Modbus")

        sensors_count = SelectField('Number of Sensors:', choices=sensor_count_choices, default=sensor_count_value)
        sensor1_type = SelectField('Sensor 1 Type', choices=sensor_choices, default=sensor_choices[1]) 
        sensor2_type = SelectField('Sensor 2 Type', choices=sensor_choices, default=sensor_choices[1]) 
        smoke_detector = BooleanField('Smoke Detector')

        lpt_end_unit_of_measure = StringField('Unit of Measure', default=lpt_end_unit_of_measure_value, render_kw={'readonly': True})
        lpt_end_min_physical_value = IntegerField('Min Physical Value', default=lpt_end_min_value, render_kw={'readonly': True})
        lpt_end_max_physical_value = IntegerField('Max Physical Value', default=lpt_end_max_value, render_kw={'readonly': True})
        lpt_end_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_end_current_draw_value, render_kw={'readonly': True})
        lpt_end_signal_type = StringField('Signal Type', default=lpt_end_signal_type_value, render_kw={'readonly': True})

        lpt_tco_unit_of_measure = StringField('Unit of Measure', default=lpt_tco_unit_of_measure_value, render_kw={'readonly': True})
        lpt_tco_min_physical_value = IntegerField('Min Physical Value', default=lpt_tco_min_value, render_kw={'readonly': True})
        lpt_tco_max_physical_value = IntegerField('Max Physical Value', default=lpt_tco_max_value, render_kw={'readonly': True})
        lpt_tco_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_tco_current_draw_value, render_kw={'readonly': True})
        lpt_tco_signal_type = StringField('Signal Type', default=lpt_tco_signal_type_value, render_kw={'readonly': True})

        other_unit_of_measure = StringField('Unit of Measure', default=lpt_tco_unit_of_measure_value)
        other_min_physical_value = IntegerField('Min Physical Value', default=lpt_tco_min_value)
        other_max_physical_value = IntegerField('Max Physical Value', default=lpt_tco_max_value)
        other_current_draw_of_sensor = StringField('Current Draw of Sensor (mA)', default=lpt_tco_current_draw_value)
        other_signal_type = SelectField('Signal Type', choices=['0-10V','4-20mA','KTY','PT1000'], default=lpt_tco_signal_type_value)

        digital_in_2 = BooleanField('Digital in 2 (D2/E1)', default=False)
        logic = SelectField('Logic', choices=['Active High', 'Active Low/Open'], default="Active High")
        function = SelectField('Function', choices=['Enable','Set speed override'], default="Enable")
        function_speed_override = IntegerField('Override Speed (rpm)', default=0)

        external_analogue_signal = BooleanField('External Analogue Signal (6mA max)')
        external_analogue_signal_selection = SelectField(choices=['0-10V Signal Following Request','0-10V Signal Following Actual Speed','Operating Indication (Above 50rpm 10V Digital)'], default="0-10V Signal Following Request")

        relay_polarity = SelectField(choices=['Normal Open (NO)','Normal Close (NC)'], default="Normal Close (NC)")

        comms_timeout_duration = IntegerField('Timeout Duration (seconds)', default=60)
        comms_timeout_speed = IntegerField('Timeout Speed (rpm)', default=0)


        # def validate_single_speed(self, single_speed):
        #         if single_speed.data > default_single_speed_max_speed:
        #             return ValidationError(f'Operational Speed must be lower than {default_single_speed_max_speed}rpm for fantype {fan_type}')
        #         elif single_speed.data < 0:
        #             return ValidationError(f'Operational Speed must be greater than zero')

        # def validate_digital_modbus_max_speed(self, digital_modbus_max_speed):
        #     self.current_digital_modbus_max_speed = digital_modbus_max_speed.data
        #     if digital_modbus_max_speed.data > default_digital_modbus_max_speed:
        #         return ValidationError(f'Max Speed must be lower than {default_digital_modbus_max_speed}rpm for fantype {fan_type}')
        #     elif digital_modbus_max_speed.data < 0:
        #         return ValidationError(f'Max Speed must be a non-negative value')

        # def valiate_digital_modbus_min_speed(self, digital_modbus_min_speed):
        #     if digital_modbus_min_speed.data > self.current_digital_modbus_max_speed:
        #         return ValidationError(f'Min Speed cannot be lower than Max Speed {self.current_digital_modbus_max_speed}')
        #     elif digital_modbus_min_speed.data > default_digital_modbus_max_speed:
        #         return ValidationError(f'Min Speed must be lower than {default_digital_modbus_max_speed}rpm for fantype {fan_type}')
        #     elif digital_modbus_min_speed.data < 0:
        #         return ValidationError(f'Min Speed must be a non-negative value')

    return CustomFanDefinitionForm





def initializeSelectFanType():
    fan_choices = [""]
    stmt = text("Select * From fans Where current='True'")
    fan_values_objects = db.session.query(FanValues).from_statement(stmt).all()
    for fan_values in fan_values_objects:
        fan_choices.append(fan_values.name)
    
    print(f'select fan type: {fan_choices}')
    
    class SelectFanType(FlaskForm):
        select_fan_type = SelectField('Select Motor Type',choices=fan_choices, default=fan_choices[0])
        # get_data = SubmitField('Create Fan Definition')

    return SelectFanType



def initializeCreateFanDefinitionForm():
    stmt = text("Select * From fans Where current='True'")
    fan_values_objects = db.session.query(FanValues).from_statement(stmt).all()
    standard_fans = [""]
    for fan_values in fan_values_objects:
        standard_fans.append(fan_values.name)


    class CreateFanDefinitionForm(FlaskForm):
        standard_type = SelectField('Select Motor Type', choices=standard_fans, default=standard_fans[0])
        standard = BooleanField('Standard Definition')
        definition_name = StringField('Custom Fan Definition Name')
        create_custom_fan_definition_button = SubmitField('Create Custom Fan Definition')
        cancel_fan_definition = SubmitField('Cancel')

    return CreateFanDefinitionForm


class backButton(FlaskForm):
    back_button = SubmitField('Go Back to Controllers View')





# def initializeCreateStandardFanDefinition():


#     class CreateStandardFanDefinition(FlaskForm):
#         standard = BooleanField('Standard Definition')
#         fan_type = StringField('Fan Type')
#         comms = StringField('Comms')
#         sensors_count = IntegerField('Sensors Count')
#         sensor_1_type = StringField('Sensor 1 Type')
#         sensor_1_unit = StringField('Sensor 1 Unit')
#         sensor_1_min = IntegerField('Sensor 1 Min')
#         sensor_1_max = IntegerField('Sensor 1 Max')
#         sensor_2_type = StringField('Sensor 2 Type')
#         sensor_2_unit = StringField('Sensor 2 Unit')
#         sensor_2_min = IntegerField('Sensor 2 Min')
#         sensor_2_max = IntegerField('Sensor 2 Max')
#         command_value = IntegerField('Command Value')
#         control = StringField('Control')
#         minimum_speed = IntegerField('Min Speed')
#         maximum_speed = IntegerField('Max Speed')
#         p_val = IntegerField('P Value')
#         i_val = IntegerField('I Value')
#         control_min = IntegerField('Control Min')
#         control_max = IntegerField('Control Max')
#         smoke_detector = BooleanField('Smoke Detector')
#         emergency = StringField('Emergency')
#         emer_set_speed = IntegerField('Emergency Set Speed')
#         emer_external_signal = StringField('Emergency External Signal')
#         emer_start_stop_signal = StringField('Emergency Start Stop Signal')
#         timeout = IntegerField('Timeout')
#         timeout_speed = IntegerField('Timeout Speed')
#         zone = IntegerField('Zone')
#         fault_relay = StringField('Fault Relay')


#     return CreateStandardFanDefinition