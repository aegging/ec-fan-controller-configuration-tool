from operator import or_
from wtforms.fields.core import Field
from controllerConfigTool.models import AnalogSensorDefinitions, ControllerInputOutputs, ExpansionBoardInputOutput, FanValues, FieldbusSensorDefinitions, SalesOrders, Controller,\
     Job, Settings, Systems, FanDefinitions, Controller, FieldbusSensors, AnalogSensors, CarelKeys, CarelValues
from controllerConfigTool import app, db
from sqlalchemy import text, null
from controllerConfigTool.dataCalculations import createModbusRangesString
from pytz import timezone
from datetime import datetime





""" find all available fieldbus sensors for this sales order which is NOT assigned to a controller """
def getListOfAvailableFBSensors(sales_order_id):
    available_fbsensors = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==None).order_by(FieldbusSensors.id).all()
    list_address_of_available_fbsensors = []

    for fbsensor in available_fbsensors:
        list_address_of_available_fbsensors.append([fbsensor.modbus_address, fbsensor.id])

    return list_address_of_available_fbsensors


def findAnalogSensorsWithAnalogSensorDefinition(sales_order_id):
    """ find all analog sensors that have been assigned a analog sensor definition """
    all_analog_sensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
        filter(AnalogSensors.controller_id==None).order_by(AnalogSensors.id).all()

    list_address_of_selected_analog_sensors = []
    analog_sensor_definitions_name = "None"

    for analog_sensor in all_analog_sensor_objects:
        current_analog_sensor_address = int(analog_sensor.modbus_address)
        analog_sensor_id = analog_sensor.id

        # get analog defintion name
        analog_sensor_definitions_id = analog_sensor.analog_sensor_definitions_id
        analog_definitions_object_boolean = bool(AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first())
        if analog_definitions_object_boolean:
            analog_sensor_definitions_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first()
            analog_sensor_definitions_name = analog_sensor_definitions_object.definition_name

        list_address_of_selected_analog_sensors.append([current_analog_sensor_address, analog_sensor_definitions_name, analog_sensor_id])

    result = {}

    for analog_sensor_address, definition_name, analog_sensor_id in list_address_of_selected_analog_sensors:
        result.setdefault(definition_name, list()).append([int(analog_sensor_address), int(analog_sensor_id)])

    return result


def getListAnalogSensorConfiguredControllers(sales_order_id, tag_prefix):
    """ return a list of controllers with analog sensors assigned to them in Analog Sensor Table"""
    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)

    controller_id_fans_found_list = []

    # for each controller id, search FieldbusSensors table for a match, if there is a match add to list as finished
    for this_controller_id in controller_id_list:
        found_controller_id_boolean = bool(AnalogSensors.query.filter_by(controller_id=this_controller_id).first())
        
        # found a fan with this controller id, add to list
        if found_controller_id_boolean:
            controller_id_fans_found_list.append(this_controller_id)


    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)

    return controller_number_list


def getListFanConfiguredControllers(sales_order_id, tag_prefix):
    """ return a list of controllers with fans assigned to them in Jobs Table """

    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)


    controller_id_fans_found_list = []

    # for each controller id, search Jobs table for a match, if there is a match add to list as finished
    for this_controller_id in controller_id_list:
        found_controller_id_boolean = bool(Job.query.filter_by(controller_id=this_controller_id).first())
        
        # found a fan with this controller id, add to list
        if found_controller_id_boolean:
            controller_id_fans_found_list.append(this_controller_id)


    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)

    return controller_number_list


def getSalesOrderValues(sales_order_id):
    """ Return all sales order values """
    sales_order_id = int(sales_order_id)
    current_sales_order_object = SalesOrders.query.filter_by(id = sales_order_id).first()
    current_sales_order_number = current_sales_order_object.sales_order_number
    sales_order_controller_quantity = current_sales_order_object.controller_quantity
    entered_by = current_sales_order_object.input_by
    time_zone = current_sales_order_object.time_zone

    return(current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone)


def convertFanType(fan_type):
    try:
        carel_key_object = CarelKeys.query.filter_by(carel_fieldname='jetfans_fantype').first()
        carel_key_id = carel_key_object.id

        carel_values_object = db.session.query(CarelValues).filter(CarelValues.carel_key_id==carel_key_id).\
                    filter(CarelValues.fieldname_value==fan_type).first()
        
        carel_value = carel_values_object.carel_value
        return carel_value
    except:
        return ""


def getControllerValues(controller_id):
    """ Return all controller values    """
    controller_object = Controller.query.filter_by(id=controller_id).order_by(Controller.id).first()
    sales_order_id = controller_object.sales_order_id
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    controller_type = sales_order_object.controller_type
    controller_serial_number = controller_object.controller_serial_number
    version_number_id = sales_order_object.version_number_id

    # creates list of address fan type pairs [(address, fan_type),(address, fan_type)]
    list_address_of_selected_fans = getModbusRangesPerControllerID(controller_id)

    grouped_list_fan_definitions = groupListAddressFans_fandefinition(list_address_of_selected_fans)

    grouped_list_fan_types = groupListAddressFans_fantype(list_address_of_selected_fans)


    return controller_type, controller_serial_number, grouped_list_fan_definitions, grouped_list_fan_types, version_number_id


def groupListAddressFans_fandefinition(list_address_of_selected_fans):
    result = {}

    for address, fan_definitions_name, fan_type in list_address_of_selected_fans:
        result.setdefault(fan_definitions_name, list()).append(int(address))

    return result


def groupListAddressFans_fantype(list_address_of_selected_fans):
    result = {}

    for address, fan_definitions_name, fan_type in list_address_of_selected_fans:
        result.setdefault(fan_definitions_name, list()).append(int(address))

    return result


def getAnalogSensorAddressDefinitionDictonary(controller_id):
    """ return list of tuples [(address, definition_name)] for a given controller """
    analog_sensor_objects = AnalogSensors.query.filter_by(controller_id=controller_id).order_by(AnalogSensors.id).all()
    address_definition_tuple_list = []
    sensor_definition_name = "None"

    for sensor_object in analog_sensor_objects:
        current_sensor_address = int(sensor_object.modbus_address)
        sensor_definition_id = sensor_object.analog_sensor_definitions_id
        if sensor_definition_id:
            analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=sensor_definition_id).first()
            sensor_definition_name = analog_sensor_definition_object.definition_name

        address_definition_tuple_list.append([current_sensor_address, sensor_definition_name])


    # use default dict library
    result_dict = {}
    for sensor_address, sensor_definition_name in address_definition_tuple_list:
        result_dict.setdefault(sensor_definition_name, list()).append(int(sensor_address))

    return result_dict






def getModbusRangesPerControllerID(controller_id):
    """ Return list of modbus ranges for a controller and fan type """
    all_fans_object = Job.query.filter_by(controller_id=controller_id).order_by(Job.Index).all()
    list_address_of_selected_fans = []
    fan_definitions_name = "None"
    for fan in all_fans_object:
        current_fan_address = int(fan.Address)
        fan_type = fan.FanType

        # get fan defintion name
        fan_definition_id = fan.fan_definitions_id
        fan_definitions_object_boolean = bool(FanDefinitions.query.filter_by(id=fan_definition_id).first())
        if fan_definitions_object_boolean:
            fan_definitions_object = FanDefinitions.query.filter_by(id=fan_definition_id).first()
            fan_definitions_name = fan_definitions_object.name

        list_address_of_selected_fans.append([current_fan_address, fan_definitions_name, fan_type])
        

    return list_address_of_selected_fans

def getControllerID(current_controller, sales_order_id):
    """ Return ID of controller given current controller button number and the sales order id """

    all_controllers_object = Controller.query.filter_by(sales_order_id=sales_order_id).order_by(Controller.id).all()
    count = 1
    current_controller = int(current_controller)
    for controller in all_controllers_object:
        if current_controller == count:
            return controller.id 
        else :
            count += 1
    return 'ERROR'


def getControllerNumber(controller_id):
    """ Get controller button number given controller id """
    controller_object = Controller.query.filter_by(id=controller_id).first()
    sales_order_id = controller_object.sales_order_id

    all_controllers_object = Controller.query.filter_by(sales_order_id=sales_order_id).all()
    controller_number = 1

    for controller in all_controllers_object:
        if controller.id == controller_id:
            return controller_number
        else:
            controller_number += 1




def getTotalControllers(sales_order_id):
    """ Find the total controllers for a given Sales Order ID"""
    total_controllers = Controller.query.filter_by(sales_order_id=sales_order_id).count()
    return total_controllers



# return count of finished controllers
def getTotalProgrammedControllers(sales_order_id):
    # find total programmed controllers
    total_controllers_programmed = db.session.query(Controller).\
                    filter(Controller.sales_order_id==sales_order_id).\
                    filter(Controller.programmed==True).\
                    filter(Controller.programming_error==False).count()
    return total_controllers_programmed


def getTagPrefix(tagname):
    """ return the tagname of controller without the integer value """
    prefix = "".join(filter(lambda x: not x.isdigit(), tagname)) 
    return prefix


def getProgramSettings(controller_id):
    controller_object = Controller.query.filter_by(id=controller_id).first()
    software_version_id = controller_object.version_number_id

    program_settings_object = Settings.query.filter_by(id=software_version_id).first()
    cfactory_path = program_settings_object.cfactory_path
    mini_http_folder_directory = program_settings_object.mini_http_folder_directory
    max_http_folder_directory = program_settings_object.max_http_folder_directory
    mini_ap1_directory = program_settings_object.mini_ap1_directory
    max_ap1_directory = program_settings_object.max_ap1_directory
    version_number = program_settings_object.version_number

    return cfactory_path, mini_http_folder_directory, max_http_folder_directory, mini_ap1_directory, max_ap1_directory, version_number



def getVersionGivenID(version_number_id):
    settings_object = Settings.query.filter_by(id=version_number_id).first()
    select_version = settings_object.version_number
    return select_version


def getIDGivenVersion(version_number):
    settings_object = Settings.query.filter_by(version_number=version_number).first()
    settings_id = settings_object.id
    return settings_id


# if there are no settings, return true
def areSettingsEmpty():
    found_settings_boolean = bool(Settings.query.first())
    return not found_settings_boolean


def findFirstNonConfiguredController(sales_order_id, tag_prefix):
    # find the next controller to configure
    # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)

    

    # for each controller id, search FieldbusSensors, Fans, and Expansion Board tables for a match, if there is a match add to list as finished
    for this_controller_id in controller_id_list:
        found_fbsensor_controller_id_boolean = bool(FieldbusSensors.query.filter_by(controller_id=this_controller_id).first())
        found_fan_controller_id_boolean = bool(Job.query.filter_by(controller_id=this_controller_id).first())
        found_expansion_board_id_boolean = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=this_controller_id).first())
        
        # found a fan with this controller id, add to list
        if not found_fbsensor_controller_id_boolean and not found_fan_controller_id_boolean and not found_expansion_board_id_boolean:
            controller_object = Controller.query.filter_by(id=this_controller_id).first()
            controller_tag_name = controller_object.tag_name
            controller_number = int(controller_tag_name.strip(tag_prefix))
            return controller_number
    
    # none available, return controller 1
    controller_number = 1
    
    return controller_number




def getDefaultFanType():
    fan_type_name_object = FanValues.query.filter_by(current='True').first()
    fan_type_name = str(fan_type_name_object.name)
    print(f'---------------------fan type name:  {fan_type_name}')
    return str(fan_type_name)


def getArchiveDirectory(sales_order_id):
    sales_order_object_boolean = bool(SalesOrders.query.filter_by(id=sales_order_id).first())
    if sales_order_object_boolean:
        sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
        archived_directory = sales_order_object.system_path
    else:
        archived_directory = ""

    return archived_directory



""" find unaddressed items in sales order
    find remaining by comparing count in sales order table w/ how how many have actually been created (look for this sales order id in appropriate table)
    controllers_count, mixer_fans_count, sensors_count, expansion_boards_count
"""
def findUnaddressedItems(sales_order_id, mixer_fan = None, fieldbus_sensors = None):
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    mixer_fans_count, fieldbus_sensors_count = mixer_fan, fieldbus_sensors
    # print(mixer_fans_count, fieldbus_sensors_count, '-------------from database')

    # initial_controller_count = sales_order_object.controller_quantity
    initial_mixer_fans_count = sales_order_object.mixer_fan_quantity
    initial_fieldbus_sensors_count = sales_order_object.fieldbus_sensor_quantity
    initial_analog_sensors_count = sales_order_object.analog_sensor_quantity
    # initial_expansion_board_count = sales_order_object.expansion_board_count
    # initial_exhaust_supply_fan_count = sales_order_object.exhaust_supply_fan_quantity

    # if initial_controller_count is None:
    #     initial_controller_count = 0

    if initial_mixer_fans_count is None:
        initial_mixer_fans_count = 0

    if initial_fieldbus_sensors_count is None:
        initial_fieldbus_sensors_count = 0

    if initial_analog_sensors_count is None:
        initial_analog_sensors_count = 0

    # if initial_expansion_board_count is None:
    #     initial_expansion_board_count = 0

    # if initial_exhaust_supply_fan_count is None:
    #     initial_exhaust_supply_fan_count = 0
    


    #find how many have modbus address and sales order id
    # controller_count = db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).filter(Controller.input_on!=None).count() 
    if mixer_fan is None:
        mixer_fans_count = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.Address!=None).count()

    if fieldbus_sensors is None:
        fieldbus_sensors_count = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
        filter(FieldbusSensors.modbus_address!=None).count()

    analog_sensors_count = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
        filter(AnalogSensors.modbus_address!=None).count()

    # expansion_board_count = db.session.query(ExpansionBoards).filter(ExpansionBoards.sales_order_id==sales_order_id).filter(ExpansionBoards.controller_id!=None).count()

    # exhaust_supply_fan_count = db.session.query(SupplyExhaustFans).filter(SupplyExhaustFans.sales_order_id==sales_order_id).\
    #     filter(SupplyExhaustFans.modbus_address!=None).count()


    # controller_count_difference = initial_controller_count - controller_count
    mixer_fan_count_difference = initial_mixer_fans_count - mixer_fans_count
    fieldbus_sensors_count_difference = initial_fieldbus_sensors_count - fieldbus_sensors_count
    analog_sensors_count_difference = initial_analog_sensors_count - analog_sensors_count
    # expansion_board_count_difference = initial_expansion_board_count - expansion_board_count
    # exhaust_supply_fan_count_difference = initial_exhaust_supply_fan_count - exhaust_supply_fan_count


    count_results = {}
    # count_results['Controllers'] = controller_count_difference
    count_results['Fans'] = mixer_fan_count_difference
    count_results['Fieldbus Sensors'] = fieldbus_sensors_count_difference
    count_results['Analog Sensors'] = analog_sensors_count_difference
    # count_results['Expansion Boards'] = expansion_board_count_difference
    # count_results['Supply Exhaust Fans'] = exhaust_supply_fan_count_difference

    return count_results



""" Return tag_prefix given the sales_order_id"""
def findTagPrefixGivenSalesOrderID(sales_order_id):
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    tag_prefix = sales_order_object.tag_name
    return tag_prefix


def getAllCarelKeyValues():
    result_dict = {}
    carel_keys_list = []
    carel_key_objects = CarelKeys.query.all()
    for carel_key_object in carel_key_objects:
        carel_keys_list.append([carel_key_object.id, carel_key_object.carel_fieldname])

    for carel_key_id, carel_key_fieldname in carel_keys_list:
        carel_value_objects = CarelValues.query.filter_by(carel_key_id=carel_key_id).all()

        temp_dict = {}

        for carel_value_object in carel_value_objects:
            fieldname_value = carel_value_object.fieldname_value
            carel_value = carel_value_object.carel_value
            temp_dict[fieldname_value] = carel_value

        result_dict[carel_key_fieldname] = temp_dict

    return result_dict


def getCarelKeyValues_fieldbus_sensor_sense_type_index():
    result_dict = {}
    carel_key_object = CarelKeys.query.filter_by(carel_fieldname='fieldbus_sensor_sense_type_index').first()
    carel_key_id = carel_key_object.id
    carel_value_objects = CarelValues.query.filter_by(carel_key_id=carel_key_id).all()

    for carel_value_object in carel_value_objects:
        fieldname_value = carel_value_object.fieldname_value
        carel_value = carel_value_object.carel_value

        result_dict[fieldname_value] = carel_value

    return result_dict


def getCarelKeyValues_AnalogFans():
    carel_key_object = CarelKeys.query.filter_by(carel_fieldname='analog_fan_type').first()
    carel_key_id = carel_key_object.id
    carel_value_objects = CarelValues.query.filter_by(carel_key_id=carel_key_id).all()

    sensor_fan_choices= []
    for carel_value_object in carel_value_objects:
        sensor_fan_choices.append([carel_value_object.carel_value, carel_value_object.fieldname_value])

    return sensor_fan_choices 



def getDateAndTime():
    """Get time and date."""
    now = datetime.now(timezone('US/Central'))
    return now.strftime("%m/%d/%Y %H:%M:%S")
        


def getSelectedVersion():
    settings_object = Settings.query.filter_by(currently_selected=True).first()
    if settings_object:
        select_version = settings_object.version_number
    else:
        select_version = ""
    return select_version


def getSettingsID():
    settings_object = Settings.query.filter_by(currently_selected=True).first()
    settings_id = settings_object.id
    return settings_id
