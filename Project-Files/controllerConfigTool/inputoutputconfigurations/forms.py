from controllerConfigTool.getInfoFromDatabase import getCarelKeyValues_AnalogFans
from controllerConfigTool.models import Controller, ControllerInputOutputs, ExpansionBoardInputOutput
from controllerConfigTool import db
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, IntegerField
from wtforms.widgets import HiddenInput



###################################
######## Input/Output Forms #######
###################################
def build_controller_input_output_expansion_board_form(controller_id_value, sales_order_controller_quantity, tag_prefix):
    sales_order_id = int(Controller.query.with_entities(Controller.sales_order_id).filter_by(id=controller_id_value).scalar())
    print(f'sales order id: {sales_order_id}')

    analog_output_choices = getCarelKeyValues_AnalogFans()

    # default ControllerInputOutputs values if no data exist
    y1_analog_fan_type_default_value = y2_analog_fan_type_default_value = None
    y3_analog_fan_type_default_value = y4_analog_fan_type_default_value =  None

    y1_min_fan_speed_percent_default_value = y2_min_fan_speed_percent_default_value = y3_min_fan_speed_percent_default_value = 0
    y4_min_fan_speed_percent_default_value = y1_min_demand_range_percent_default_value = y2_min_demand_range_percent_default_value = 0
    y3_min_demand_range_percent_default_value = y4_min_demand_range_percent_default_value = 0

    y1_max_fan_speed_percent_default_value = y2_max_fan_speed_percent_default_value = y3_max_fan_speed_percent_default_value = 100
    y4_max_fan_speed_percent_default_value = y1_max_demand_range_percent_default_value = y2_max_demand_range_percent_default_value = 100
    y3_max_demand_range_percent_default_value = y4_max_demand_range_percent_default_value = 100


    # find default values if there is existing data for this controller id
    existing_controller_input_output_data = bool(ControllerInputOutputs.query.filter_by(controller_id=controller_id_value).first())
    if existing_controller_input_output_data:
        controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id_value).first()

        if controller_input_output_object.y1_analog_fan_type:
            y1_analog_fan_type_default_value = controller_input_output_object.y1_analog_fan_type
        if controller_input_output_object.y2_analog_fan_type:
            y2_analog_fan_type_default_value = controller_input_output_object.y2_analog_fan_type
        if controller_input_output_object.y3_analog_fan_type:
            y3_analog_fan_type_default_value = controller_input_output_object.y3_analog_fan_type
        if controller_input_output_object.y4_analog_fan_type:
            y4_analog_fan_type_default_value = controller_input_output_object.y4_analog_fan_type
        if controller_input_output_object.y1_min_fan_speed_percent:
            y1_min_fan_speed_percent_default_value = controller_input_output_object.y1_min_fan_speed_percent
        if controller_input_output_object.y2_min_fan_speed_percent:
            y2_min_fan_speed_percent_default_value = controller_input_output_object.y2_min_fan_speed_percent
        if controller_input_output_object.y3_min_fan_speed_percent:
            y3_min_fan_speed_percent_default_value = controller_input_output_object.y3_min_fan_speed_percent
        if controller_input_output_object.y4_min_fan_speed_percent:
            y4_min_fan_speed_percent_default_value = controller_input_output_object.y4_min_fan_speed_percent
        if controller_input_output_object.y1_min_demand_range_percent:
            y1_min_demand_range_percent_default_value = controller_input_output_object.y1_min_demand_range_percent
        if controller_input_output_object.y2_min_demand_range_percent:
            y2_min_demand_range_percent_default_value = controller_input_output_object.y2_min_demand_range_percent
        if controller_input_output_object.y3_min_demand_range_percent:
            y3_min_demand_range_percent_default_value = controller_input_output_object.y3_min_demand_range_percent
        if controller_input_output_object.y4_min_demand_range_percent:
            y4_min_demand_range_percent_default_value = controller_input_output_object.y4_min_demand_range_percent
        if controller_input_output_object.y1_max_fan_speed_percent:
            y1_max_fan_speed_percent_default_value = controller_input_output_object.y1_max_fan_speed_percent
        if controller_input_output_object.y2_max_fan_speed_percent:
            y2_max_fan_speed_percent_default_value = controller_input_output_object.y2_max_fan_speed_percent
        if controller_input_output_object.y3_max_fan_speed_percent:
            y3_max_fan_speed_percent_default_value = controller_input_output_object.y3_max_fan_speed_percent
        if controller_input_output_object.y4_max_fan_speed_percent:
            y4_max_fan_speed_percent_default_value = controller_input_output_object.y4_max_fan_speed_percent
        if controller_input_output_object.y1_max_demand_range_percent:
            y1_max_demand_range_percent_default_value = controller_input_output_object.y1_max_demand_range_percent
        if controller_input_output_object.y2_max_demand_range_percent:
            y2_max_demand_range_percent_default_value = controller_input_output_object.y2_max_demand_range_percent
        if controller_input_output_object.y3_max_demand_range_percent:
            y3_max_demand_range_percent_default_value = controller_input_output_object.y3_max_demand_range_percent
        if controller_input_output_object.y4_max_demand_range_percent:
            y4_max_demand_range_percent_default_value  = controller_input_output_object.y4_max_demand_range_percent




    class ControllerInputOutputAndExpansionBoardForm(FlaskForm):
        submit_button = SubmitField('Save')
        controller_id = IntegerField(default=controller_id_value, widget=HiddenInput(), render_kw={'readonly': True})

        y1_analog_fan_type = SelectField('Fan Type', default=y1_analog_fan_type_default_value, choices=analog_output_choices)
        y1_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y1_min_fan_speed_percent_default_value)
        y1_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y1_max_fan_speed_percent_default_value)
        y1_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y1_min_demand_range_percent_default_value)
        y1_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y1_max_demand_range_percent_default_value)

        y2_analog_fan_type = SelectField('Fan Type', default=y2_analog_fan_type_default_value, choices=analog_output_choices)
        y2_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y2_min_fan_speed_percent_default_value)
        y2_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y2_max_fan_speed_percent_default_value)
        y2_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y2_min_demand_range_percent_default_value)
        y2_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y2_max_demand_range_percent_default_value)

        y3_analog_fan_type = SelectField('Fan Type', default=y3_analog_fan_type_default_value, choices=analog_output_choices)
        y3_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y3_min_fan_speed_percent_default_value)
        y3_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y3_max_fan_speed_percent_default_value)
        y3_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y3_min_demand_range_percent_default_value)
        y3_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y3_max_demand_range_percent_default_value)

        y4_analog_fan_type = SelectField('Fan Type', default=y4_analog_fan_type_default_value, choices=analog_output_choices)
        y4_min_fan_speed_percent = IntegerField('Min Fan Speed(%)', default=y4_min_fan_speed_percent_default_value)
        y4_max_fan_speed_percent = IntegerField('Max Fan Speed(%)', default=y4_max_fan_speed_percent_default_value)
        y4_min_demand_range_percent = IntegerField('Min Fan Demand(%)', default=y4_min_demand_range_percent_default_value)
        y4_max_demand_range_percent = IntegerField('Max Fan Demand(%)', default=y4_max_demand_range_percent_default_value)

        navigate_sales_order_status_button = SubmitField('Return Sales Order Status')
        pass

    for i in range(1, 6):
        existing_expansion_board_input_output_data = bool(db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id_value).\
                                                            filter(ExpansionBoardInputOutput.expansion_board_number==i).first())

        # provide existing data as default values
        if existing_expansion_board_input_output_data:
            # get data object
            expansion_board_data_object = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id_value).\
                                                            filter(ExpansionBoardInputOutput.expansion_board_number==i).first()

            setattr(ControllerInputOutputAndExpansionBoardForm, f'expansion_board_number_{i}', IntegerField(default=i, widget=HiddenInput(),render_kw={'readonly': True}))
            setattr(ControllerInputOutputAndExpansionBoardForm, f'u8_analog_fan_type_{i}', SelectField(default=expansion_board_data_object.u8_analog_fan_type, label='U8', choices=analog_output_choices))
            setattr(ControllerInputOutputAndExpansionBoardForm, f'u9_analog_fan_type_{i}', SelectField(default=expansion_board_data_object.u9_analog_fan_type, label='U9', choices=analog_output_choices))
            setattr(ControllerInputOutputAndExpansionBoardForm, f'u10_analog_fan_type_{i}', SelectField(default=expansion_board_data_object.u10_analog_fan_type, label='U10', choices=analog_output_choices))

        # no data exist, thus default values not necessary
        else:
            setattr(ControllerInputOutputAndExpansionBoardForm, f'expansion_board_number_{i}', IntegerField(default=i, widget=HiddenInput(),render_kw={'readonly': True}))
            setattr(ControllerInputOutputAndExpansionBoardForm, f'u8_analog_fan_type_{i}', SelectField(label='U8', choices=analog_output_choices))
            setattr(ControllerInputOutputAndExpansionBoardForm, f'u9_analog_fan_type_{i}', SelectField(label='U9', choices=analog_output_choices))
            setattr(ControllerInputOutputAndExpansionBoardForm, f'u10_analog_fan_type_{i}', SelectField(label='U10', choices=analog_output_choices))

    for i in range(1, sales_order_controller_quantity + 1):
        setattr(ControllerInputOutputAndExpansionBoardForm, f'controller_button_{i}', SubmitField(label=f'{tag_prefix}{i}'))


    return ControllerInputOutputAndExpansionBoardForm()
    


############################################
######## End of Input/Output Forms #########
############################################

