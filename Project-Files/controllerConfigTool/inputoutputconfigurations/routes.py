from flask import Blueprint
from controllerConfigTool.getInfoFromDatabase import findTagPrefixGivenSalesOrderID
from controllerConfigTool.inputoutputconfigurations.forms import build_controller_input_output_expansion_board_form
from controllerConfigTool.inputoutputconfigurations.utils import getListControllerWithInputOutputAssignment
from controllerConfigTool.getInfoFromDatabase import getSalesOrderValues, getControllerID
from controllerConfigTool import db
from flask import render_template, url_for, redirect, request
from copy import deepcopy
from sqlalchemy import null

from controllerConfigTool.models import AnalogSensors, ControllerInputOutputs, ExpansionBoardInputOutput

inputoutputconfigurations_blueprint = Blueprint('inputoutputconfigurations', __name__)




@inputoutputconfigurations_blueprint.route('/input_output_assignment/<sales_order_id>/<int:current_controller>', methods=['GET','POST'])
def input_output_assignment(sales_order_id, current_controller):

    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    # find finished controllers
    list_index_of_finished_controllers = getListControllerWithInputOutputAssignment(sales_order_id, tag_prefix)
    
    # get all values from sales order table for this sales order
    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)

    # find current controller in database and add controller configurations data to it
    controller_id = getControllerID(current_controller, sales_order_id)

    # send controller_id to form so that we can use as a hidden field
    controller_input_output_and_expansion_board_form = build_controller_input_output_expansion_board_form(controller_id, sales_order_controller_quantity, tag_prefix)

    # create variable list
    expansion_board_variable_list = []
    controller_input_output_variable_list = []

    controller_input_output_variable_list = [
    'y1_analog_fan_type','y2_analog_fan_type', 'y3_analog_fan_type', 'y4_analog_fan_type',
    'y1_min_fan_speed_percent', 'y2_min_fan_speed_percent', 'y3_min_fan_speed_percent', 'y4_min_fan_speed_percent',
    'y1_max_fan_speed_percent', 'y2_max_fan_speed_percent', 'y3_max_fan_speed_percent', 'y4_max_fan_speed_percent',
    'y1_min_demand_range_percent', 'y2_min_demand_range_percent', 'y3_min_demand_range_percent', 'y4_min_demand_range_percent',
    'y1_max_demand_range_percent', 'y2_max_demand_range_percent', 'y3_max_demand_range_percent', 'y4_max_demand_range_percent']

    for i in range(1, 6):
        expansion_board_variable_list.append([f'expansion_board_number_{i}', 
                     f'u8_analog_fan_type_{i}', f'u9_analog_fan_type_{i}', f'u10_analog_fan_type_{i}'])


    controller_buttons_list = []
    for i in range(1, sales_order_controller_quantity + 1):
        controller_buttons_list.append([i, f'controller_button_{i}'])


    if request.method == 'POST':
        # Controller Button Navigation
        for controller_index, controller_button_name in controller_buttons_list:
            if controller_button_name in request.form:
                return redirect(url_for('inputoutputconfigurations.input_output_assignment', sales_order_id=sales_order_id, current_controller=controller_index))

        # Form Submission
        if controller_input_output_and_expansion_board_form.navigate_sales_order_status_button.data:
                return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))


        if controller_input_output_and_expansion_board_form.validate_on_submit():
            if controller_input_output_and_expansion_board_form.submit_button.data:
                controller_input_output_and_expansion_board_form_dictionary = controller_input_output_and_expansion_board_form.data


                controller_input_output_and_expansion_board_form_dictionary.pop('submit_button')
                controller_input_output_and_expansion_board_form_dictionary.pop('csrf_token')
                controller_input_output_and_expansion_board_form_dictionary.pop('navigate_sales_order_status_button')
            
                # pop off controller buttons
                for controller_index, controller_button_name in controller_buttons_list:
                    controller_input_output_and_expansion_board_form_dictionary.pop(controller_button_name)

                # make a dictionary for ControllerInputOutput and ExpansionBoardInputOutput
                # seperate the data such that it can be pushed to 2 different tables
                controller_input_output_dictionary = deepcopy(controller_input_output_and_expansion_board_form_dictionary)
                expansion_board_form_dictionary = deepcopy(controller_input_output_and_expansion_board_form_dictionary)

                for variable_row in expansion_board_variable_list:
                    for variable_name in variable_row:
                        controller_input_output_dictionary.pop(variable_name)
                
                for variable_name in controller_input_output_variable_list:
                    expansion_board_form_dictionary.pop(variable_name)

                # give empty variables sqlalchemy valid null value
                for variable_row in expansion_board_variable_list:
                    for variable_name in variable_row:
                        if expansion_board_form_dictionary[variable_name] == 'None':
                            expansion_board_form_dictionary[variable_name] = null()
                
                # give empty variables sqlalchemy valid null value
                for variable_name in controller_input_output_variable_list:
                    if controller_input_output_dictionary[variable_name] == 'None':
                        controller_input_output_dictionary[variable_name] = null()


                # place sales order id inside dictionaries
                controller_input_output_dictionary['sales_order_id'] = sales_order_id
                expansion_board_form_dictionary['sales_order_id'] = sales_order_id

                # Save Controller Input Output Data
                # is there an existing Controller Input Output data object?
                existing_controller_input_output_object = bool(ControllerInputOutputs.query.filter_by(controller_id=controller_id).first())
                if existing_controller_input_output_object:
                    # existing one found, use this object
                    # controller_input_output_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
                    db.session.query(ControllerInputOutputs).filter(ControllerInputOutputs.controller_id==controller_id).update(controller_input_output_dictionary)
                    db.session.commit()
                else:
                    # create one
                    controller_input_output_object = ControllerInputOutputs(**controller_input_output_dictionary)
                    db.session.add(controller_input_output_object)
                    db.session.commit()

                # Save Expansion Board Data
                # is there existing data?
                # existing_expansion_board_input_output_object = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).first())
                # if existing_expansion_board_input_output_object:
                    
                # there is an existing data entry in database, update this entry with new data
                for variable_row in expansion_board_variable_list:
                    new_data_submission_dictionary = {}
                    new_data_submission_dictionary['controller_id'] = expansion_board_form_dictionary['controller_id']
                    new_data_submission_dictionary['sales_order_id'] = expansion_board_form_dictionary['sales_order_id']
                    # save each variable of current row into a new dictionary
                    for variable_name in variable_row:
                        temp_variable_name, expansion_board_index = variable_name.rsplit('_', 1)
                        new_data_submission_dictionary[temp_variable_name] = expansion_board_form_dictionary[variable_name]

                    # determine if expansion_board_index
                    existing_expansion_board_index_object = bool(db.session.query(ExpansionBoardInputOutput).\
                        filter(ExpansionBoardInputOutput.controller_id==controller_id).\
                            filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_index).first())

                    if existing_expansion_board_index_object:
                        db.session.query(ExpansionBoardInputOutput).\
                        filter(ExpansionBoardInputOutput.controller_id==controller_id).\
                            filter(ExpansionBoardInputOutput.expansion_board_number==expansion_board_index).update(new_data_submission_dictionary)
                        db.session.commit()
                    else:
                        expansion_board_input_output_object = ExpansionBoardInputOutput(**new_data_submission_dictionary)
                        db.session.add(expansion_board_input_output_object)
                        db.session.commit()




                # set all expansion_board_ids to null for analog sensors with th
                existing_expansion_board_input_output_object = bool(ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).first())
                temp_expansion_board_ids_list = []
                
                if existing_expansion_board_input_output_object:

                    expansion_board_input_output_objects = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).all()

                    for expansion_board_input_output_object in expansion_board_input_output_objects:
                        temp_expansion_board_ids_list.append(expansion_board_input_output_object.id)

                    for current_expansion_board_id in temp_expansion_board_ids_list:
                        analog_sensor_objects = AnalogSensors.query.filter_by(expansion_board_id=current_expansion_board_id).all()
                        for analog_sensor_object in analog_sensor_objects:
                            if analog_sensor_object:
                                analog_sensor_object.expansion_board_id = null()
                                db.session.add(analog_sensor_object)

                        db.session.commit()


                return redirect(url_for('inputoutputconfigurations.input_output_assignment', sales_order_id=sales_order_id, current_controller=current_controller))
 

    return render_template('input_output_assignment.html', controller_input_output_and_expansion_board_form=controller_input_output_and_expansion_board_form,
     sales_order_controller_quantity=sales_order_controller_quantity, list_index_of_finished_controllers=list_index_of_finished_controllers,
     current_controller=current_controller, expansion_board_variable_list=expansion_board_variable_list, controller_buttons_list=controller_buttons_list)

