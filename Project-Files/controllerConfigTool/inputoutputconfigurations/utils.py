from controllerConfigTool.models import Controller, ControllerInputOutputs, ExpansionBoardInputOutput, SalesOrders
from controllerConfigTool import db



def getListControllerWithInputOutputAssignment(sales_order_id, tag_prefix):
     # find all controller objects for given sales order
    controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()

    # create list of controller ids
    controller_id_list = []
    for controller_object in controller_objects:
        controller_id_list.append(controller_object.id)


    controller_id_fans_found_list = []

    # for each controller id, search Controller table for existing controller configurations data 
    for this_controller_id in controller_id_list:
        found_controller_id_boolean = bool(db.session.query(ControllerInputOutputs).filter(ControllerInputOutputs.controller_id==this_controller_id).first())
        
        # found a fan with this controller id, add to list
        if found_controller_id_boolean:
            controller_id_fans_found_list.append(this_controller_id)


    # use controller ids to find the controller number
    # create list of controller numbers and return
    controller_number_list = []
    for this_controller_id in controller_id_fans_found_list:
        # find controller object
        controller_object = Controller.query.filter_by(id=this_controller_id).first()
        controller_tag_name = controller_object.tag_name
        controller_number = int(controller_tag_name.strip(tag_prefix))
        controller_number_list.append(controller_number)

    return controller_number_list

