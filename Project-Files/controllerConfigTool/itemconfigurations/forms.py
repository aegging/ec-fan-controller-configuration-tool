from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField, SelectField, IntegerField
from wtforms.validators import DataRequired, ValidationError, Optional
from wtforms.widgets import HiddenInput


class FieldbusSensorSelectDefinition(FlaskForm):
    modbus_ranges_indexes = StringField('Modbus Range Indexes', widget=HiddenInput(), render_kw={'readonly': True}, validators=[DataRequired()])
    modbus_ranges = StringField('Address Selection:', render_kw={'readonly': True}, validators=[DataRequired()])

    select_definition_dropdown = SelectField('Select Definition', validators=[DataRequired()])
    assign_sensor_definition_button = SubmitField('Assign Definition')
    # create_sensor_definition_button = SubmitField('Create Sensor Definition')
    return_sales_order_status_button = SubmitField('Return')


class FanSelectDefinition(FlaskForm):
    modbus_ranges_indexes = StringField('Modbus Range Indexes', widget=HiddenInput(), render_kw={'readonly': True}, validators=[DataRequired()])
    modbus_ranges = StringField('Address Selection:', render_kw={'readonly': True}, validators=[DataRequired()])

    select_definition_dropdown = SelectField('Select Definition', validators=[DataRequired()])
    assign_fan_definition_button = SubmitField('Assign Definition')


class FanDefinitionButtons(FlaskForm):
    create_fan_definition_button = SubmitField('Create Fan Definition')
    return_sales_order_status_button = SubmitField('Return')
    view_fan_definitions_button = SubmitField('View Existing Fan Definitions')


class addAnalogSensorsForm(FlaskForm):
    add_sensors = IntegerField()
    add_analog_sensors_button = SubmitField('Add Analog Sensors')


class AnalogSensorSelectDefinition(FlaskForm):
    modbus_ranges_indexes = StringField('Modbus Range Indexes', widget=HiddenInput(), render_kw={'readonly': True}, validators=[DataRequired()])
    modbus_ranges = StringField('Address Selection:', render_kw={'readonly': True}, validators=[DataRequired()])

    select_definition_dropdown = SelectField('Select Definition', validators=[DataRequired()])
    assign_sensor_definition_button = SubmitField('Assign Definition')
    

class AnalogSensorDefinitionButtons(FlaskForm):
    create_sensor_definition_button = SubmitField('Create Sensor Definition')
    return_sales_order_status_button = SubmitField('Return')
