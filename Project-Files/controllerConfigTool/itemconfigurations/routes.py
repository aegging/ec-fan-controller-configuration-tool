'''CG Change, This file has been changed by CG DEvelopers'''
from flask import Blueprint
from wtforms.validators import ValidationError
from controllerConfigTool.itemconfigurations.utils import getFBSensorsWithNoDefinition,\
    AssignDefinitionFieldbusSensors, getFansWithNoDefinition, AssignDefinitionFans,\
        getAnalogSensorsWithNoDefinition, AssignDefinitionAnalogSensors
from flask import render_template, url_for, flash, redirect, request
from controllerConfigTool.models import AnalogSensorDefinitions, AnalogSensors, FanDefinitions, FieldbusSensorDefinitions, FieldbusSensors,\
    Job, SalesOrders
from controllerConfigTool.itemconfigurations.forms import AnalogSensorDefinitionButtons, FanDefinitionButtons, FieldbusSensorSelectDefinition, FanSelectDefinition,\
    addAnalogSensorsForm, AnalogSensorSelectDefinition
from controllerConfigTool import app, db
from sqlalchemy import text
from controllerConfigTool.dataCalculations import decodeModbusRange
from controllerConfigTool.createSensors import createAnalogSensors

itemconfigurations_blueprint = Blueprint('itemconfigurations', __name__)


# Fieldbus Sensor 
@itemconfigurations_blueprint.route('/fieldbus_sensor_definition_assignment/<int:sales_order_id>', methods=['GET', 'POST'])
def fieldbus_sensor_definition_assignment(sales_order_id):

    # get data of existing fieldbus sensor definitions
    fieldbus_sensor_definition_choices = [""]
    stmt = text("Select * From fieldbus_sensor_definitions Where (standard=True) or sales_order_id=:sales_order_id")
    fieldbus_sensor_values_objects_boolean = bool(db.session.query(FieldbusSensorDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all())
    if fieldbus_sensor_values_objects_boolean:
        fieldbus_sensor_values_objects = db.session.query(FieldbusSensorDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all()

        for fb_sensor_values in fieldbus_sensor_values_objects:
            fieldbus_sensor_definition_choices.append(fb_sensor_values.definition_name)


    # create form for selecting existing sensor definition (look for all standad definitions and non standard with this sales order id)
    select_fieldbus_sensor_definition_form = FieldbusSensorSelectDefinition() 
    select_fieldbus_sensor_definition_form.select_definition_dropdown.choices = fieldbus_sensor_definition_choices

    
    #  create button to create a new sensor definition and redirect to a new route in order to do so
    sensor_definition_buttons = AnalogSensorDefinitionButtons()

    # create array of available sensors with this sales order e.g.-> [[address,index],[address,index]]
    # index is necessary for jobs with overlapping sensor ranges 
    available_sensors = getFBSensorsWithNoDefinition(sales_order_id)
    print('available sensors')
    print(available_sensors)

    # get data for fieldbus sensors that have been given a definition
    # select all fb sensors where fieldbus_sensor_definition_id != None for this sales order
    fbsensors_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.fieldbus_sensor_definitions_id!=None).\
        filter(FieldbusSensors.sales_order_id==sales_order_id).all()

    #  create list [[address,definition_name],[address,definition_name]]
    available_fbsensors_list = []
    for fbsensors_object in fbsensors_objects:
        # for each fb sensor, add address and 
        current_sensor_address = fbsensors_object.modbus_address
        fieldbus_sensor_definitions_id = fbsensors_object.fieldbus_sensor_definitions_id

        # get definition name
        fieldbus_sensor_definition_object = FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first()
        definition_name = fieldbus_sensor_definition_object.definition_name

        available_fbsensors_list.append([current_sensor_address, definition_name])

    address_sensortype_dictionary = {}

    for address, definition_name in available_fbsensors_list:
        address_sensortype_dictionary.setdefault(definition_name, list()).append(int(address))


    if request.method == 'POST':
        if sensor_definition_buttons.create_sensor_definition_button.data:
            # redirect to create fieldbus sensor definition
            
            # send sensor type 48 to make cGas default selection
            return redirect(url_for('createdefinitions.create_fieldbus_sensor_definition', sales_order_id=sales_order_id, fieldbus_sensor_type_id=48))

        elif sensor_definition_buttons.return_sales_order_status_button.data:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif select_fieldbus_sensor_definition_form.assign_sensor_definition_button.data:
            # Assign button was pressed, save selected definition to selected addresses
            modbus_ranges_indexes = select_fieldbus_sensor_definition_form.modbus_ranges_indexes.data
            select_definition_dropdown = select_fieldbus_sensor_definition_form.select_definition_dropdown.data
            modbus_ranges = select_fieldbus_sensor_definition_form.modbus_ranges.data

            # create modbus address list using modbus string
            sensor_index__list = decodeModbusRange(modbus_ranges_indexes)

            AssignDefinitionFieldbusSensors(sensor_index__list, select_definition_dropdown)

            flash(f'Fieldbus Sensors {modbus_ranges} were assigned definition {select_definition_dropdown}', 'success')

            available_sensors = getFBSensorsWithNoDefinition(sales_order_id)
            if not available_sensors:
                return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
            else:
                return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))

        
        # clear_button_{{ sensor_type }}
        # loop through all possible buttons
        else:
            # get list of all sensor type names
            fieldbus_sensor_definitions_objects = FieldbusSensorDefinitions.query.all()
            fieldbus_definitions_name_list = []
            for fieldbus_sensor_definitions_object in fieldbus_sensor_definitions_objects:
                fieldbus_definitions_name_list.append(fieldbus_sensor_definitions_object.definition_name)

            for i in range(0, len(fieldbus_definitions_name_list)):
                # create button name
                definition_name = fieldbus_definitions_name_list[i]
                clear_button_name = "clear_button_" + str(definition_name)
                
                # if clear button was pressed, set all fieldbus sensor definition ids to null for this definition name
                if clear_button_name in request.form:
                    # find definition id using definition name
                    fieldbus_sensor_definition_object = FieldbusSensorDefinitions.query.filter_by(definition_name=definition_name).first()
                    fieldbus_definition_id = fieldbus_sensor_definition_object.id

                    # find all fieldbus sensors with this sales order id and this definition id
                    fieldbus_sensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.fieldbus_sensor_definitions_id==fieldbus_definition_id).\
                        filter(FieldbusSensors.sales_order_id==sales_order_id).all()

                    # loop through each fb sensor
                    for fieldbus_sensor_object in fieldbus_sensor_objects:
                        fieldbus_sensor_object.fieldbus_sensor_definitions_id = None

                        db.session.add(fieldbus_sensor_object)
                        
                    # commit to database
                    db.session.commit()

                    return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))



    return render_template('fieldbus_sensor_definition_assignment.html', 
    select_fieldbus_sensor_definition_form=select_fieldbus_sensor_definition_form, 
    sensor_definition_buttons=sensor_definition_buttons, available_sensors=available_sensors, 
    address_sensortype_dictionary=address_sensortype_dictionary)



#######

# Fan Definition Assignment
@itemconfigurations_blueprint.route('/fan_definition_assignment/<int:sales_order_id>', methods=['GET', 'POST'])
def fan_definition_assignment(sales_order_id):

    # get data of existing fan definitions
    fan_definition_choices = [""]
    stmt = text("Select * From fan_definitions Where (standard=True) or sales_order_id=:sales_order_id")
    fan_values_objects_boolean = bool(db.session.query(FanDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all())
    if fan_values_objects_boolean:
        fan_values_objects = db.session.query(FanDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all()

        for fan_values in fan_values_objects:
            fan_definition_choices.append(fan_values.name)


    fan_definition_buttons = FanDefinitionButtons()
    # create form for selecting existing fan definition (look for all standard definitions and non standard with this sales order id)
    select_fan_definition_form = FanSelectDefinition() 
    select_fan_definition_form.select_definition_dropdown.choices = fan_definition_choices


    # create array of available fan with this sales order e.g.-> [[address,index],[address,index]]
    # index is necessary for jobs with overlapping fan ranges 
    available_fans = getFansWithNoDefinition(sales_order_id)


    # get data for fans that have been given a definition
    # select all fans where fan_definition_id != None for this sales order
    fans_objects = db.session.query(Job).filter(Job.fan_definitions_id!=None).\
        filter(Job.sales_order_id==sales_order_id).all()

    #  create list [[address,definition_name],[address,definition_name]]
    available_fans_list = []
    for fan_object in fans_objects:
        # for each fan, add address and 
        current_sensor_address = fan_object.Address
        fan_definitions_id = fan_object.fan_definitions_id

        # get definition name
        fan_definition_object = FanDefinitions.query.filter_by(id=fan_definitions_id).first()
        definition_name = fan_definition_object.name

        available_fans_list.append([current_sensor_address, definition_name])

    address_definitionname_dictionary = {}

    for address, definition_name in available_fans_list:
        address_definitionname_dictionary.setdefault(definition_name, list()).append(int(address))


    if request.method == 'POST':
        if fan_definition_buttons.create_fan_definition_button.data:
            # redirect to create fan definition
            return redirect(url_for('createdefinitions.initializefandefinition', sales_order_id=sales_order_id))
        elif fan_definition_buttons.view_fan_definitions_button.data:
            return redirect(url_for('viewdefinitions.viewfandefinition', sales_order_id=sales_order_id))
        elif fan_definition_buttons.return_sales_order_status_button.data:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif select_fan_definition_form.validate_on_submit():
            if select_fan_definition_form.assign_fan_definition_button.data:
                # Assign button was pressed, save selected definition to selected addresses
                modbus_ranges_indexes = select_fan_definition_form.modbus_ranges_indexes.data
                select_definition_dropdown = select_fan_definition_form.select_definition_dropdown.data
                modbus_ranges = select_fan_definition_form.modbus_ranges.data

                # create modbus address list using modbus string
                fan_index__list = decodeModbusRange(modbus_ranges_indexes)

                # determine if there are enough analog sensors for this assignment to be valid

                operation_success = AssignDefinitionFans(fan_index__list, select_definition_dropdown, sales_order_id)

                if operation_success:
                    flash(f'Fans {modbus_ranges} were assigned definition {select_definition_dropdown}', 'success')

                    available_fans = getFansWithNoDefinition(sales_order_id)
                    if not available_fans:
                        return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
                    else:
                        return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))
                else:
                    flash(f'Not enough Analog Sensors available to complete the requested operation', 'danger')
                    return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))

        
        # clear_button_{{ fan_definition_name }}
        # loop through all possible buttons
        else:
            # get list of all fan definition names
            fan_definitions_objects = FanDefinitions.query.all()
            fan_definitions_name_list = []
            for fan_definitions_object in fan_definitions_objects:
                fan_definitions_name_list.append(fan_definitions_object.name)

            for i in range(0, len(fan_definitions_name_list)):
                # create button name
                definition_name = fan_definitions_name_list[i]
                clear_button_name = "clear_button_" + str(definition_name)
                
                # if clear button was pressed, set all fan definition ids to null for this definition name
                if clear_button_name in request.form:
                    # find definition id using definition name
                    fan_definition_object = FanDefinitions.query.filter_by(name=definition_name).first()
                    fan_definition_id = fan_definition_object.id

                    # find all fan with this sales order id and this definition id
                    fan_objects = db.session.query(Job).filter(Job.fan_definitions_id==fan_definition_id).\
                        filter(Job.sales_order_id==sales_order_id).all()

                    fan_id_remove_list = []
                    # loop through each fan
                    for fan_object in fan_objects:
                        fan_object.fan_definitions_id = None
                        fan_id_remove_list.append(fan_object.Index)
                        db.session.add(fan_object)
                        
                    # commit to database
                    db.session.commit()

                    # remove all analog sensors for each fan id
                    for current_fan_id in fan_id_remove_list:
                        analog_sensor_objects = AnalogSensors.query.filter_by(fan_id=current_fan_id).all()
                        for analog_sensor_object in analog_sensor_objects:
                            analog_sensor_object.fan_id = None
                            db.session.add(analog_sensor_object)
                        db.session.commit()

                    return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))



            

    return render_template('fan_definition_assignment.html', 
    select_fan_definition_form=select_fan_definition_form, available_fans=available_fans, address_definitionname_dictionary=address_definitionname_dictionary,
    fan_definition_buttons=fan_definition_buttons)



######


# Analog Sensor
@itemconfigurations_blueprint.route('/analog_sensor_definition_assignment/<int:sales_order_id>', methods=['GET', 'POST'])
def analog_sensor_definition_assignment(sales_order_id):

    analog_sensor_definition_buttons = AnalogSensorDefinitionButtons()
    add_analog_sensors_form = addAnalogSensorsForm()

    # get data of existing analog sensor definitions
    # analog_definitions_objects = AnalogSensorDefinitions.query.all()
    stmt = text("Select * From analog_sensor_definitions Where (standard=True) or sales_order_id=:sales_order_id")
    analog_definitions_objects = AnalogSensorDefinitions.query.filter_by(standard = True).all()
    analog_values_objects_boolean = bool(db.session.query(AnalogSensorDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all())
    if analog_values_objects_boolean:
        analog_definitions_objects = db.session.query(AnalogSensorDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all()
    definition_choices = [""]

    for analog_definitions_object in analog_definitions_objects:
        definition_choices.append(analog_definitions_object.definition_name)

    # create form for selecting existing sensor definition (look for all standad definitions and non standard with this sales order id)
    select_analog_sensor_definition_form = AnalogSensorSelectDefinition() 
    select_analog_sensor_definition_form.select_definition_dropdown.choices = definition_choices

    #  create button to create a new sensor definition and redirect to a new route in order to do so


    # create array of available sensors with this sales order e.g.-> [[address,index],[address,index]]
    # index is necessary for jobs with overlapping sensor ranges 
    available_sensors = getAnalogSensorsWithNoDefinition(sales_order_id)


    # get data for fieldbus sensors that have been given a definition
    # select all fb sensors where fieldbus_sensor_definition_id != None
    analog_sensors_objects = db.session.query(AnalogSensors).filter(AnalogSensors.analog_sensor_definitions_id!=None).\
        filter(AnalogSensors.sales_order_id==sales_order_id).all()

    #  create list [[address,definition_name],[address,definition_name]]
    available_analog_sensors_list = []
    for analog_sensors_object in analog_sensors_objects:
        # for each fb sensor, add address and 
        current_sensor_address = analog_sensors_object.modbus_address
        analog_sensor_definitions_id = analog_sensors_object.analog_sensor_definitions_id

        # get definition name
        analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first()
        definition_name = analog_sensor_definition_object.definition_name

        available_analog_sensors_list.append([current_sensor_address, definition_name])

    address_sensortype_dictionary = {}

    for address, definition_name in available_analog_sensors_list:
        address_sensortype_dictionary.setdefault(definition_name, list()).append(int(address))

    if request.method == 'POST':
        if analog_sensor_definition_buttons.create_sensor_definition_button.data:
            # redirect to create fieldbus sensor definition
            return redirect(url_for('createdefinitions.redirect_create_analog_sensor_definition', sales_order_id=sales_order_id, sensor_type=1))

        elif select_analog_sensor_definition_form.validate_on_submit():
            if select_analog_sensor_definition_form.assign_sensor_definition_button.data:
                # Assign button was pressed, save selected definition to selected addresses
                modbus_ranges_indexes = select_analog_sensor_definition_form.modbus_ranges_indexes.data
                select_definition_dropdown = select_analog_sensor_definition_form.select_definition_dropdown.data
                modbus_ranges = select_analog_sensor_definition_form.modbus_ranges.data

                # create modbus address list using modbus string
                sensor_index__list = decodeModbusRange(modbus_ranges_indexes)

                AssignDefinitionAnalogSensors(sensor_index__list, select_definition_dropdown)

                flash(f'Analog Sensors {modbus_ranges} were assigned definition {select_definition_dropdown}', 'success')

                available_sensors = getAnalogSensorsWithNoDefinition(sales_order_id)
                if not available_sensors:
                    return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
                else:
                    return redirect(url_for('itemconfigurations.analog_sensor_definition_assignment', sales_order_id=sales_order_id))

        elif analog_sensor_definition_buttons.return_sales_order_status_button.data:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
        elif "add_analog_sensors_button" in request.form:
            if add_analog_sensors_form.validate_on_submit():
                sensor_count = add_analog_sensors_form.add_sensors.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                previous_count = sales_order_object.analog_sensor_quantity
                if previous_count is not None:
                    sales_order_object.analog_sensor_quantity = previous_count + sensor_count
                else:
                    sales_order_object.analog_sensor_quantity = sensor_count

                db.session.add(sales_order_object)
                db.session.commit()

                # create row for each analog sensor in analog_sensors table
                createAnalogSensors(sales_order_id, sensor_count, previous_count)

                return redirect(url_for('itemconfigurations.analog_sensor_definition_assignment', sales_order_id=sales_order_id))
        # clear_button_{{ sensor_type }}
        # loop through all possible buttons
        else:
            # get list of all sensor type names
            analog_sensor_definitions_objects = AnalogSensorDefinitions.query.all()
            analog_definitions_name_list = []
            for analog_sensor_definitions_object in analog_sensor_definitions_objects:
                analog_definitions_name_list.append(analog_sensor_definitions_object.definition_name)

            for i in range(0, len(analog_definitions_name_list)):
                # create button name
                definition_name = analog_definitions_name_list[i]
                clear_button_name = "clear_button_" + str(definition_name)
                
                # if clear button was pressed, set all analog sensor definition ids to null for this definition name
                if clear_button_name in request.form:
                    # find definition id using definition name
                    analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(definition_name=definition_name).first()
                    analog_definition_id = analog_sensor_definition_object.id

                    # find all analog sensors with this sales order id and this definition id
                    analog_sensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.analog_sensor_definitions_id==analog_definition_id).\
                        filter(AnalogSensors.sales_order_id==sales_order_id).all()

                    # loop through each fb sensor
                    for analog_sensor_object in analog_sensor_objects:
                        analog_sensor_object.analog_sensor_definitions_id = None

                        db.session.add(analog_sensor_object)
                        
                    # commit to database
                    db.session.commit()

                    return redirect(url_for('itemconfigurations.analog_sensor_definition_assignment', sales_order_id=sales_order_id))

    return render_template('analog_sensor_definition_assignment.html', select_analog_sensor_definition_form=select_analog_sensor_definition_form, 
    available_sensors=available_sensors, address_sensortype_dictionary=address_sensortype_dictionary, add_analog_sensors_form=add_analog_sensors_form,
    analog_sensor_definition_buttons=analog_sensor_definition_buttons)






