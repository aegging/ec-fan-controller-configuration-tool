from controllerConfigTool import db
from controllerConfigTool.models import AnalogSensorDefinitions, AnalogSensors, FanDefinitions,\
     FieldbusSensors, FieldbusSensorDefinitions, Job




""" find all available fieldbus sensors for this sales order which have NO definition assigned """
def getFBSensorsWithNoDefinition(sales_order_id):
    available_fbsensors = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
        filter(FieldbusSensors.fieldbus_sensor_definitions_id==None).order_by(FieldbusSensors.id).all()
    list_address_of_available_fbsensors = []

    for fbsensor in available_fbsensors:
        list_address_of_available_fbsensors.append([fbsensor.modbus_address, fbsensor.id])

    return list_address_of_available_fbsensors


"""
Assign definition to fieldbus sensors
Given list of fieldbus sensor ids, and fieldbus sensor definition name
"""
def AssignDefinitionFieldbusSensors(sensor_index__list, select_definition_dropdown):

    # find fieldbus sensor definition id using the definition name that was selected
    fieldbus_sensor_definition_object = FieldbusSensorDefinitions.query.filter_by(definition_name=select_definition_dropdown).first()

    fieldbus_sensor_definition_id = fieldbus_sensor_definition_object.id

    for index in sensor_index__list:

        # find sensor object using fieldbus sensor id
        fieldbus_sensor_object = FieldbusSensors.query.filter_by(id=index).first()

        # assign definition id to current fieldbus sensor object
        fieldbus_sensor_object.fieldbus_sensor_definitions_id = fieldbus_sensor_definition_id
                    
        # add to session
        db.session.add(fieldbus_sensor_object)
    
    # commit all changes
    db.session.commit()


""" find all available fans for this sales order which have NO definition assigned """
def getFansWithNoDefinition(sales_order_id):
    available_fans = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.fan_definitions_id==None).order_by(Job.Index).all()
    list_address_of_available_fans = []

    for fan in available_fans:
        list_address_of_available_fans.append([fan.Address, fan.Index])

    return list_address_of_available_fans



def populate_current_fan_sensor_type(fan_sensor_unit):
    # replace this by using FanSensorTypeRelationships table once we do database migration
    if fan_sensor_unit == 'ppm CO':
        return 1
    elif fan_sensor_unit == 'ppm NO2':
        return 2
    elif fan_sensor_unit =='degC':
        return 3
    elif fan_sensor_unit == 'V':
        return 4
    elif fan_sensor_unit == 'ppm CO2':
        return 5
    else:
        return 0


"""
Assign definition to Fans
Given list of fan ids, and fan definition name
"""
def AssignDefinitionFans(fan_index__list, select_definition_dropdown, sales_order_id):

    # find fandefinition id using the definition name that was selected
    fan_definition_object = FanDefinitions.query.filter_by(name=select_definition_dropdown).first()

    fan_definition_id = fan_definition_object.id
    fan_type = fan_definition_object.fan_type
    comms = fan_definition_object.comms
    sensor_count = fan_definition_object.sensors_count
    sensor_1_type = fan_definition_object.sensor_1_type
    sensor_1_unit = fan_definition_object.sensor_1_unit
    sensor_1_min = fan_definition_object.sensor_1_min
    sensor_1_max = fan_definition_object.sensor_1_max

    sensor_2_type = fan_definition_object.sensor_2_type
    sensor_2_unit = fan_definition_object.sensor_2_unit
    sensor_2_min = fan_definition_object.sensor_2_min
    sensor_2_max = fan_definition_object.sensor_2_max

    command_value = fan_definition_object.command_value

    control = fan_definition_object.control
    minimum_speed = fan_definition_object.minimum_speed
    maximum_speed = fan_definition_object.maximum_speed
    p_val = fan_definition_object.p_val
    i_val = fan_definition_object.i_val
    control_min = fan_definition_object.control_min
    control_max = fan_definition_object.control_max
    smoke_detector = fan_definition_object.smoke_detector
    emergency = fan_definition_object.emergency
    emer_set_speed = fan_definition_object.emer_set_speed
    emer_external_signal = fan_definition_object.emer_external_signal
    emer_start_stop_signal = fan_definition_object.emer_start_stop_signal
    timeout = fan_definition_object.timeout
    timeout_speed = fan_definition_object.timeout_speed
    zone = fan_definition_object.zone
    fault_relay = fan_definition_object.fault_relay

    for index in fan_index__list:

        # find sensor object using fieldbus sensor id
        fan_object = Job.query.filter_by(Index=index).first()

        # before fan definition assignment, determine if enough analog sensors are available and make assignment
        current_fan_sensor1_type = 0
        current_fan_sensor2_type = 0

        if sensor_count == 1:
            current_fan_sensor1_type = populate_current_fan_sensor_type(fan_definition_object.sensor_1_unit)
        elif sensor_count == 2:
            current_fan_sensor1_type = populate_current_fan_sensor_type(fan_definition_object.sensor_1_unit)
            current_fan_sensor2_type = populate_current_fan_sensor_type(fan_definition_object.sensor_2_unit)


        if current_fan_sensor1_type != 0:
            # find available analog sensor for the given sensor type
            analog_sensor_1_exist = bool(db.session.query(AnalogSensors).join(AnalogSensorDefinitions, AnalogSensorDefinitions.id == AnalogSensors.analog_sensor_definitions_id).\
                    filter(AnalogSensorDefinitions.sensor_type==current_fan_sensor1_type).\
                        filter(AnalogSensors.fan_id==None).\
                            filter(AnalogSensors.controller_id==None).\
                                filter(AnalogSensors.expansion_board_id==None).\
                                    filter(AnalogSensors.sales_order_id==sales_order_id).\
                                    first())

            if analog_sensor_1_exist:
                analog_sensor_1_object = db.session.query(AnalogSensors).join(AnalogSensorDefinitions, AnalogSensorDefinitions.id == AnalogSensors.analog_sensor_definitions_id).\
                    filter(AnalogSensorDefinitions.sensor_type==current_fan_sensor1_type).\
                        filter(AnalogSensors.fan_id==None).\
                            filter(AnalogSensors.controller_id==None).\
                                filter(AnalogSensors.expansion_board_id==None).\
                                    filter(AnalogSensors.sales_order_id==sales_order_id).\
                                    first()

                analog_sensor_1_object.fan_id = index
                db.session.add(analog_sensor_1_object)
                
            else:
                return False


        if current_fan_sensor2_type != 0:
            # find available analog sensor for the given sensor type
            analog_sensor_2_exist = bool(db.session.query(AnalogSensors).join(AnalogSensorDefinitions, AnalogSensorDefinitions.id == AnalogSensors.analog_sensor_definitions_id).\
                    filter(AnalogSensorDefinitions.sensor_type==current_fan_sensor2_type).\
                        filter(AnalogSensors.fan_id==None).\
                            filter(AnalogSensors.controller_id==None).\
                                filter(AnalogSensors.expansion_board_id==None).\
                                    filter(AnalogSensors.sales_order_id==sales_order_id).\
                                    first())

            if analog_sensor_2_exist:
                analog_sensor_2_object = db.session.query(AnalogSensors).join(AnalogSensorDefinitions, AnalogSensorDefinitions.id == AnalogSensors.analog_sensor_definitions_id).\
                    filter(AnalogSensorDefinitions.sensor_type==current_fan_sensor2_type).\
                        filter(AnalogSensors.fan_id==None).\
                            filter(AnalogSensors.controller_id==None).\
                                filter(AnalogSensors.expansion_board_id==None).\
                                    filter(AnalogSensors.sales_order_id==sales_order_id).\
                                    first()

                analog_sensor_2_object.fan_id = index
                db.session.add(analog_sensor_2_object)
                
            else:
                return False


        # assign definition id to current fieldbus sensor object
        fan_object.fan_definitions_id = fan_definition_id

        fan_object.Comms = comms
        fan_object.No_Sensors = sensor_count
        fan_object.Sensor_1_type = sensor_1_type
        fan_object.Sensor_1_unit = sensor_1_unit
        fan_object.Sensor_1_min = sensor_1_min
        fan_object.Sensor_1_max = sensor_1_max

        fan_object.Sensor_2_type = sensor_2_type
        fan_object.Sensor_2_unit = sensor_2_unit
        fan_object.Sensor_2_min = sensor_2_min
        fan_object.Sensor_2_max = sensor_2_max

        fan_object.Command_Value = command_value
        fan_object.Control = control       
        fan_object.Minimum_Speed = minimum_speed
        fan_object.Maximum_Speed = maximum_speed
        fan_object.P_val = p_val
        fan_object.I_val = i_val
        fan_object.Control_Min = control_min
        fan_object.Control_Max = control_max
        fan_object.SmokeDetector = smoke_detector
        fan_object.emergancy = emergency
        fan_object.emer_set_speed = emer_set_speed
        fan_object.emer_external_signal = emer_external_signal
        fan_object.emer_start_stop_signal = emer_start_stop_signal
        fan_object.Timeout = timeout
        fan_object.TimeoutSpeed = timeout_speed
        fan_object.FanType = fan_type
        fan_object.Zone = zone
        fan_object.FaultRelay = fault_relay



        # add to session
        db.session.add(fan_object)
        db.session.commit()

    # commit all changes
    # db.session.commit()
    return True

""" find all available analog sensors for this sales order which have NO definition assigned """
def getAnalogSensorsWithNoDefinition(sales_order_id):
    available_analog_sensors = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
        filter(AnalogSensors.analog_sensor_definitions_id==None).order_by(AnalogSensors.id).all()
    list_address_of_available_analog_sensors = []

    for analog_sensor in available_analog_sensors:
        list_address_of_available_analog_sensors.append([analog_sensor.modbus_address, analog_sensor.id])

    return list_address_of_available_analog_sensors


"""
Assign definition to analog sensors
Given list of analog sensor ids, and analog sensor definition name
"""
def AssignDefinitionAnalogSensors(sensor_index__list, select_definition_dropdown):

    # find fieldbus sensor definition id using the definition name that was selected
    analog_sensor_definition_object = AnalogSensorDefinitions.query.filter_by(definition_name=select_definition_dropdown).first()

    analog_sensor_definition_id = analog_sensor_definition_object.id

    for index in sensor_index__list:

        # find sensor object using fieldbus sensor id
        analog_sensor_object = AnalogSensors.query.filter_by(id=index).first()

        # assign definition id to current fieldbus sensor object
        analog_sensor_object.analog_sensor_definitions_id = analog_sensor_definition_id
                    
        # add to session
        db.session.add(analog_sensor_object)
    
    # commit all changes
    db.session.commit()


