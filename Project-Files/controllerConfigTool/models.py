'''CG Chnages, This file is modified by CG developers on 07/04/2023 '''
'''Added Float data type in the controllers table and migration and update also has been done in the database'''

from enum import unique

from sqlalchemy.sql.expression import null
from controllerConfigTool import db
from sqlalchemy.dialects.mysql import TINYINT


class SalesOrders(db.Model):
    __tablename__ = 'sales_orders'
    
    id = db.Column(db.Integer, primary_key=True)
    controllers = db.relationship('Controller', backref=db.backref('salesorder'), lazy=True)
    fans = db.relationship('Job', backref=db.backref('salesorder'), lazy=True)
    fan_definitions = db.relationship('FanDefinitions', backref=db.backref('salesorder'), lazy=True)

    version_number_id = db.Column(db.Integer, db.ForeignKey('settings.id'), nullable=True)
    controller_type = db.Column(db.String(20), unique=False, nullable=True)
    
    sales_order_number = db.Column(db.String(80), unique=True, nullable=False)
    time_zone = db.Column(db.Integer, unique=False, nullable=True)
    system_path = db.Column(db.String(200), unique=False, nullable=True)
    tag_name = db.Column(db.String(20), unique=False, nullable=False)

    controller_quantity = db.Column(db.Integer, unique=False, nullable=False)
    expansion_board_quantity = db.Column(db.Integer, unique=False, nullable=True)
    mixer_fan_quantity = db.Column(db.Integer, unique=False, nullable=True)
    fieldbus_sensor_quantity = db.Column(db.Integer, unique=False, nullable=True)
    analog_sensor_quantity = db.Column(db.Integer, unique=False, nullable=True)
    fieldbus_drives_quantity = db.Column(db.Integer, unique=False, nullable=True)

    input_by = db.Column(db.String(20), nullable=False)
    input_on = db.Column(db.String(20), nullable=False)

    timestamp = db.Column(db.DateTime, nullable=True)
    
    def __repr__(self):
        return f"Sales Orders('{self.sales_order_number}','{self.controller_quantity}')"


class Controller(db.Model):
    __tablename__ = 'controllers'

    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=False)
    version_number_id = db.Column(db.Integer, db.ForeignKey('settings.id'), nullable=True)
    fans = db.relationship('Job', backref=db.backref('controller'), lazy=True)
    controller_program_status = db.relationship('ControllerProgramStatus', backref=db.backref('controller'), lazy=True)

    tag_name = db.Column(db.String(80), unique=False, nullable=False)
    controller_type = db.Column(db.String(20), unique=False, nullable=True)
    controller_serial_number = db.Column(db.String(80), unique=False, nullable=True)

    input_by = db.Column(db.String(20), nullable=True)
    input_on = db.Column(db.String(20), nullable=True)

    # Controller Configurations Form ---->
    controller_configurations_data_boolean = db.Column(db.Boolean, unique=False, nullable=True)

    jetvent_min_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    jetvent_max_fan_speed = db.Column(db.Integer, unique=False, nullable=True)

    smoke_detector_system_keep_off_delay_minutes = db.Column(db.Integer, unique=False, nullable=True)
    smoke_detection_system_trigger_delay_seconds = db.Column(db.Integer, unique=False, nullable=True)
    smoke_detector_system_restrict_response_to_affected_zone = db.Column(db.Boolean, unique=False, nullable=True)

    fire_mode_exhaust_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    fire_mode_jetvent_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    fire_mode_supply_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    fire_mode_active_GFA = db.Column(db.String(10), unique=False, nullable=True)
    fire_mode_jetvent_fan_run = db.Column(db.String(10), unique=False, nullable=True)
    fire_mode_jetvent_fan_stop = db.Column(db.String(10), unique=False, nullable=True)

    manual_override_exhaust_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    manual_override_jetvent_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    manual_override_supply_fan_speed = db.Column(db.Integer, unique=False, nullable=True)
    manual_override_fail_mode_exhaust_fan = db.Column(db.String(10), unique=False, nullable=True)
    manual_override_fail_mode_jetvent_fan = db.Column(db.String(10), unique=False, nullable=True)
    manual_override_fail_mode_supply_fan = db.Column(db.String(10), unique=False, nullable=True)

    purge_enable_checkbox = db.Column(db.Boolean, unique=False, nullable=True)
    purge_system_activation_time_hour = db.Column(db.Integer, unique=False, nullable=True)
    purge_system_run_duration_time_minutes = db.Column(db.Integer, unique=False, nullable=True)
    purge_system_run_speed_demand_percentage = db.Column(db.Integer, unique=False, nullable=True)

    zero_percent_demand_co_ppm = db.Column(db.Float, unique=False, nullable=True)
    zero_percent_demand_no2_ppm = db.Column(db.Float, unique=False, nullable=True)
    zero_percent_demand_co2_ppm = db.Column(db.Float, unique=False, nullable=True)
    zero_percent_demand_temp_celsius = db.Column(db.Float, unique=False, nullable=True)
    zero_percent_demand_external_percent = db.Column(db.Float, unique=False, nullable=True)

    hundred_percent_demand_co_ppm = db.Column(db.Float, unique=False, nullable=True)
    hundred_percent_demand_no2_ppm = db.Column(db.Float, unique=False, nullable=True)
    hundred_percent_demand_co2_ppm = db.Column(db.Float, unique=False, nullable=True)
    hundred_percent_demand_temp_celsius = db.Column(db.Float, unique=False, nullable=True)
    hundred_percent_demand_external_percent = db.Column(db.Float, unique=False, nullable=True)

    alarm_threshold_co_ppm = db.Column(db.Float, unique=False, nullable=True)
    alarm_threshold_no2_ppm = db.Column(db.Float, unique=False, nullable=True)
    alarm_threshold_co2_ppm = db.Column(db.Float, unique=False, nullable=True)
    alarm_threshold_temp_celsius = db.Column(db.Float, unique=False, nullable=True)
    alarm_threshold_external_percent = db.Column(db.Float, unique=False, nullable=True)

    sensor_demand_level = db.Column(db.String(10), unique=False, nullable=True)
    fan_demand_level = db.Column(db.String(10), unique=False, nullable=True)
    fan_demand_type = db.Column(db.String(10), unique=False, nullable=True)
    # <--- Controller Configurations Form


    def __repr__(self):
        return f"Controller('{self.salesorder.sales_order_number}','{self.tag_name}','{self.fan_quantity}','{self.gfa_fire_NC}','{self.sales_order_id}','{self.modbus_ranges}','{self.controller_type}', {self.controller_serial_number}, {self.programmed}, {self.programming_error})"


class ControllerInputOutputs(db.Model):
    __tablename__ = 'controller_input_outputs'

    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=False)

    analog_outputs_saved = db.Column(db.Boolean, unique=False, nullable=True)

    u1_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)
    u2_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)
    u3_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)
    u4_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)

    y1_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    y1_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y1_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y1_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    y1_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)

    y2_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    y2_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y2_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y2_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    y2_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)

    y3_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    y3_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y3_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y3_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    y3_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)

    y4_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    y4_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y4_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    y4_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    y4_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)

    Y1=db.Column(db.Integer, unique=False, nullable=True, default=False)
    Y2=db.Column(db.Integer, unique=False, nullable=True, default=False)
    Y3=db.Column(db.Integer, unique=False, nullable=True, default=False)
    Y4=db.Column(db.Integer, unique=False, nullable=True, default=False)

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __update__(self, **entries):
        self.__dict__.update(entries)


class ExpansionBoardInputOutput(db.Model):
    __tablename__ = 'expansion_board_input_output'

    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

    expansion_board_number = db.Column(db.Integer, unique=False, nullable=True)

    u1_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)
    u2_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)
    u3_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)
    u4_analog_sensor_id = db.Column(db.Integer, db.ForeignKey('analog_sensors.id'), nullable=True)

    u8_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    u8_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    u8_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    u8_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    u8_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)

    u9_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    u9_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    u9_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    u9_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    u9_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)

    u10_analog_fan_type = db.Column(db.Integer, unique=False, nullable=True)
    u10_min_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    u10_max_fan_speed_percent = db.Column(db.Integer, unique=False, nullable=True)
    u10_min_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    u10_max_demand_range_percent = db.Column(db.Integer, unique=False, nullable=True)
    expansion_outputs_saved = db.Column(db.Boolean, unique=False, nullable=True)


    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __update__(self, **entries):
        self.__dict__.update(entries)



class ControllerProgramStatus(db.Model):
    __tablename__ = 'controller_program_status'

    id = db.Column(db.Integer, primary_key=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=False)

    # upon completion, change program_success True, and program_in_progress & program_error False
    # mark program in progress when program starts
    # if program gives up, mark program_errror True, program_success & program_in_progress False
    program_success = db.Column(db.Boolean, unique=False, nullable=True)
    program_in_progress = db.Column(db.Boolean, unique=False, nullable=True)
    program_error = db.Column(db.Boolean, unique=False, nullable=True)


class Settings(db.Model):
    __tablename__ = 'settings'

    id = db.Column(db.Integer, primary_key=True)
    controllers = db.relationship('Controller', backref=db.backref('settings'), lazy=True)
    version_number = db.Column(db.String(35), nullable=False)
    currently_selected = db.Column(db.Boolean, unique=False, nullable=True, default=False)
    enable_version = db.Column(db.Boolean, unique=False, nullable=True, default=True)
    valid_with_max_controller = db.Column(db.Boolean, unique=False, nullable=True, default=False)
    valid_with_mini_controller = db.Column(db.Boolean, unique=False, nullable=True, default=False)
    cfactory_path=db.Column(db.String(200), unique=False, nullable=True)
    mini_http_folder_directory=db.Column(db.String(200), unique=False, nullable=True)
    max_http_folder_directory=db.Column(db.String(200), unique=False, nullable=True)
    mini_ap1_directory=db.Column(db.String(200), unique=False, nullable=True)
    max_ap1_directory=db.Column(db.String(200), unique=False, nullable=True)
    time_zone = db.Column(db.Integer, unique=False, nullable=True)
    
    input_on = db.Column(db.String(20), nullable=True)
    timestamp = db.Column(db.DateTime, nullable=True)

    def __repr__(self):
        return self.version_number


class Job(db.Model):
    __tablename__ = 'jobs'

    Index = db.Column(db.Integer, primary_key=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    fan_definitions_id = db.Column(db.Integer, db.ForeignKey('fan_definitions.id'), nullable=True)

    Job_number = db.Column('Job number', db.String(45), nullable=False)
    Comms = db.Column(db.String(45), nullable=False)
    Address = db.Column(db.Integer, nullable=False)
    No_Sensors = db.Column('No Sensors', db.String(45), nullable=False)
    Sensor_1_type = db.Column('Sensor 1 type', db.String(45), nullable=False)
    Sensor_1_unit = db.Column('Sensor 1 unit', db.String(45), nullable=False)
    Sensor_1_min = db.Column('Sensor 1 min', db.String(45), nullable=False)
    Sensor_1_max = db.Column('Sensor 1 max', db.String(45), nullable=False)
    Sensor_2_type = db.Column('Sensor 2 type', db.String(45), nullable=False)
    Sensor_2_unit = db.Column('Sensor 2 unit', db.String(45), nullable=False)
    Sensor_2_min = db.Column('Sensor 2 min', db.String(45), nullable=False)
    Sensor_2_max = db.Column('Sensor 2 max', db.String(45), nullable=False)
    Command_Value = db.Column('Command Value', db.String(45), nullable=False)
    Control = db.Column(db.String(45), nullable=False)
    Minimum_Speed = db.Column('Minimum Speed', db.String(45), nullable=False)
    Maximum_Speed = db.Column('Maximum Speed', db.String(45), nullable=False)
    P_val = db.Column(db.String(45), nullable=False)
    I_val = db.Column(db.String(45), nullable=False)
    Control_Min = db.Column('Control Min', db.String(45), nullable=False)
    Control_Max = db.Column('Control Max', db.String(45), nullable=False)
    SmokeDetector = db.Column(TINYINT, nullable=False, default=0)
    emergancy = db.Column(db.String(45), nullable=False)
    emer_set_speed = db.Column('emer set speed', db.String(45), nullable=False)
    emer_external_signal = db.Column('emer external signal', db.String(45), nullable=False)
    emer_start_stop_signal = db.Column('emer start stop signal', db.String(45), nullable=False)
    Timeout = db.Column(db.String(10), nullable=False, default='0')
    TimeoutSpeed = db.Column(db.String(10), nullable=False, default='0')
    Programmed = db.Column(db.String(45), nullable=False)
    InputBy = db.Column(db.String(45), nullable=False)
    InputOn = db.Column(db.String(25), nullable=False, default='')
    FanType = db.Column(db.String(45), nullable=False)
    Zone = db.Column(db.String(2), nullable=True, default='1')
    FaultRelay = db.Column(db.String(5), nullable=False)


class systemprogrammed(db.Model):
    __tablename__ = 'systemprogrammed'

    Project = db.Column(db.String(20), nullable=False)
    ProgramDate = db.Column(db.String(35), nullable=False)
    Computer_ID = db.Column(db.String(15), nullable=False)
    Program_ID =  db.Column(db.Integer, primary_key=True)
    SerialNo = db.Column(db.String(25), nullable=True, default="")
    VersionNo = db.Column(db.String(10), nullable=False)
    ProgOption = db.Column(db.String(10), nullable=False, default="All")


class Systems(db.Model):
    __tablename__ = 'systems'

    job_number = db.Column('JobNumber', db.String(25), nullable=False, primary_key=True)
    entered_on = db.Column('EnteredOn', db.String(25), nullable=False)
    fan_1 = db.Column('Fan1', db.String(25), nullable=True)
    programmed = db.Column('Programmed', db.String(10), nullable=False)


# motor definitions
class FanValues(db.Model):
    __tablename__ = 'fans'

    id = db.Column(db.Integer, primary_key=True, unique=True, nullable=False, autoincrement=True)

    name = db.Column('Name', db.String(45), nullable=False)
    current = db.Column('Current', db.String(10), nullable=False, default='True')
    thrust = db.Column('Thrust', db.String(10), nullable=False, default='NA')
    motor_brand = db.Column('MotorBrand', db.String(15), nullable=False, default='EMB')
    comms = db.Column('Comms', db.String(45), nullable=False, default='0')
    speeds = db.Column('Speeds', db.String(45), nullable=False, default='0')
    max_speed = db.Column('max_speed', db.Integer, nullable=False, default='0')

    no_sensors = db.Column('no_sensors', db.Integer, nullable=False, default='0')
    no_dig_in = db.Column('no_dig_in', db.Integer, nullable=False, default='0')
    no_dig_out = db.Column('no_dig_out', db.Integer, nullable=False, default='0')
    modbus = db.Column('MODbus', db.Integer, nullable=False, default='0')
    _0_10v = db.Column('0-10V', db.Integer, nullable=False, default='0')
    _4_20ma = db.Column('4-20mA', db.Integer, nullable=False, default='0')
    pwm = db.Column('PWM', db.Integer, default='0')
    pt1000 = db.Column('PT1000', TINYINT, nullable=False, default='0')
    kty = db.Column('KTY', TINYINT, nullable=False, default='0')
    _20v = db.Column('20V', db.Float, nullable=False, default='0')
    _10v = db.Column('10V', db.Float, nullable=False, default='0')
    sound_limit = db.Column('sound_limit', db.Integer, nullable=False, default='0')
    motor = db.Column('Motor', db.String(45), nullable=False, default='0')
    smoke_detection = db.Column('SmokeDetection', TINYINT, nullable=False, default='0')


class Sensors(db.Model):
    __tablename__ = 'sensors'

    id = db.Column('SensorNo', db.Integer, primary_key=True, unique=True, nullable=False, autoincrement=True)

    current = db.Column('Current', db.String(45), nullable=False)
    name = db.Column('Name', db.String(45), nullable=False)
    unit_of_measure_1 = db.Column('UnitOfMeasure', db.String(45), nullable=False)
    min_value_1 = db.Column('MinValue', db.String(45), nullable=False)
    max_value_1 = db.Column('MaxValue', db.String(45), nullable=False)
    unit_of_measure_2 = db.Column('UOM2', db.String(45), nullable=False)
    min_value_2 = db.Column('MinVal2', db.String(45), nullable=False)
    max_value_2 = db.Column('MaxVal2', db.String(45), nullable=False)
    unit_of_measure_3 = db.Column('UOM3', db.String(45), nullable=False)
    min_value_3 = db.Column('MinVal3', db.String(45), nullable=False)
    max_value_3 = db.Column('MaxVal3', db.String(45), nullable=False)
    unit_of_measure_4 = db.Column('UOM4', db.String(45), nullable=False)
    min_value_4 = db.Column('MinVal4', db.String(45), nullable=False)
    max_value_4 = db.Column('MaxVal4', db.String(45), nullable=False)
    current_draw = db.Column('CurrentDraw', db.String(45))
    signal_type = db.Column('SignalType', db.String(45), nullable=False)


class FanDefinitions(db.Model):
    __tablename__ = 'fan_definitions'

    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)

    # sequence_number = db.Column(db.Integer)
    standard = db.Column(db.Boolean)

    name = db.Column(db.String(45), nullable=False)
    fan_type = db.Column(db.String(45), nullable=False)
    sub_fan_type = db.Column(db.Integer, nullable=True)
    comms = db.Column(db.String(45), nullable=False)
    sensors_count = db.Column(db.Integer, nullable=False)
    sensor_1_type = db.Column(db.String(45))
    sensor_1_unit = db.Column(db.String(45))
    sensor_1_min = db.Column(db.Integer)
    sensor_1_max = db.Column(db.Integer)
    sensor_2_type = db.Column(db.String(45))
    sensor_2_unit = db.Column(db.String(45))
    sensor_2_min = db.Column(db.Integer)
    sensor_2_max = db.Column(db.Integer)
    command_value = db.Column(db.Integer)
    control = db.Column(db.String(45))
    minimum_speed = db.Column(db.Integer)
    maximum_speed = db.Column(db.Integer)
    p_val = db.Column(db.Integer)
    i_val = db.Column(db.Integer)
    control_min = db.Column(db.Integer)
    control_max = db.Column(db.Float)
    smoke_detector = db.Column(db.Boolean, default=0)
    emergency = db.Column(db.String(45))
    emer_set_speed = db.Column(db.Integer)
    emer_external_signal = db.Column(db.String(45))
    emer_start_stop_signal = db.Column(db.String(45))
    timeout = db.Column(db.Integer, default=60)
    timeout_speed = db.Column(db.Integer, default=0)
    zone = db.Column(db.Integer, default=1)
    fault_relay = db.Column(db.String(5))


class FanTypeRelationships(db.Model):
    __tablename__ = 'fan_type_relationships'

    id = db.Column(db.Integer, primary_key=True)
    
    fan_type_name = db.Column(db.String(60), nullable=False)
    fan_type_integer = db.Column(db.Integer, nullable=False)


# Fieldbus Sensors
class FieldbusSensors(db.Model):
    __tablename__ = 'fieldbus_sensors'
 
    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
    fieldbus_sensor_definitions_id = db.Column(db.Integer, db.ForeignKey('fieldbus_sensor_definitions.id'), nullable=True)
    modbus_address = db.Column(db.Integer, nullable=False) #'Modbus Address', FBSensors[1].Address
    zone = db.Column(db.Integer, nullable=True)


class FieldbusSensorDefinitions(db.Model):
    __tablename__ = 'fieldbus_sensor_definitions'

    id = db.Column(db.Integer, primary_key=True)
    # sales order id will be null if it is a standard definition
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    sensor_id = db.Column(db.Integer, db.ForeignKey('sensors.SensorNo'), nullable=True)

    standard = db.Column(db.Boolean, nullable=True)
    definition_name = db.Column(db.String(120), unique=True, nullable=True)

    sensor_1_type = db.Column(db.String(60), nullable=True)
    sensor_1_enable = db.Column(db.String(10), nullable=True)
    sensor_1_control = db.Column(db.String(10), nullable=True)
    
    sensor_2_type = db.Column(db.String(60), nullable=True)
    sensor_2_enable = db.Column(db.String(10), nullable=True)
    sensor_2_control = db.Column(db.String(10), nullable=True)

    sensor_3_type = db.Column(db.String(60), nullable=True)
    sensor_3_enable = db.Column(db.String(10), nullable=True)
    sensor_3_control = db.Column(db.String(10), nullable=True)

    sensor_4_type = db.Column(db.String(60), nullable=True)
    sensor_4_enable = db.Column(db.String(10), nullable=True)
    sensor_4_control = db.Column(db.String(10), nullable=True)
    



# Analog Sensors
class AnalogSensors(db.Model):
    __tablename__ = 'analog_sensors'
 
    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
    expansion_board_id = db.Column(db.Integer, db.ForeignKey('expansion_board_input_output.id'), nullable=True)
    fan_id = db.Column(db.Integer, db.ForeignKey('jobs.Index'), nullable=True)
    analog_sensor_definitions_id = db.Column(db.Integer, db.ForeignKey('analog_sensor_definitions.id'), nullable=True)


    modbus_address = db.Column(db.Integer, nullable=False)
    zone = db.Column(db.Integer, nullable=True)


class AnalogSensorDefinitions(db.Model):
    __tablename__ = 'analog_sensor_definitions'

    id = db.Column(db.Integer, primary_key=True)
    # sales order id will be null if it is a standard definition
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    standard = db.Column(db.Boolean)

    definition_name = db.Column(db.String(120), unique=True, nullable=False)
    sensor_type = db.Column(db.String(60), nullable=False)
    scaling_min = db.Column(db.Integer, nullable=False)
    scaling_max = db.Column(db.Integer, nullable=False)
    alarm_threshold = db.Column(db.Integer, nullable=False, server_default='0')

class CarelKeys(db.Model):
    __tablename__ = 'admin_carel_keys'

    id = db.Column(db.Integer, primary_key=True)
    carel_values = db.relationship('CarelValues', backref=db.backref('carelkeys'), lazy=True)

    carel_fieldname = db.Column(db.String(200), nullable=False)


class CarelValues(db.Model):
    __tablename__ = 'admin_carel_values'

    id = db.Column(db.Integer, primary_key=True)
    carel_key_id = db.Column(db.Integer, db.ForeignKey('admin_carel_keys.id'), nullable=True)

    fieldname_value = db.Column(db.String(200), nullable=False)
    carel_value = db.Column(db.Integer, nullable=False)


# 
class Zone(db.Model):
    __tablename__ = 'zone'

    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
    zone_number = db.Column(db.Integer, nullable=True, default=1)
#     # input
    # output

class OutputZones(db.Model):
    __tablename__ = 'outputzone'

    id = db.Column(db.Integer, primary_key=True)
    sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
    controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
    controller_inout_id = db.Column(db.Integer, db.ForeignKey('controller_input_outputs.id'), nullable=True)
    ExpansionBoaard_inout_id = db.Column(db.Integer, db.ForeignKey('expansion_board_input_output.id'), nullable=True)
    y1_zone = db.Column(db.Integer, nullable=True)
    y2_zone = db.Column(db.Integer, nullable=True)
    y3_zone = db.Column(db.Integer, nullable=True)
    y4_zone = db.Column(db.Integer, nullable=True)
    u8_zone = db.Column(db.Integer, nullable=True)
    u9_zone = db.Column(db.Integer, nullable=True)
    u10_zone = db.Column(db.Integer, nullable=True)

# #  Supply Exhaust Fans
# class SupplyExhaustFans(db.Model):
#     __tablename__ = 'supply_exhuast_fans'

#     id = db.Column(db.Integer, primary_key=True)
#     sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
#     supply_exhaust_fans_definition_id = db.Column(db.Integer, db.ForeignKey('supply_exhaust_fans_definitions.id'), nullable=True)
#     modbus_address = db.Column(db.Integer, nullable=False) 


# class SupplyExhaustFansDefinitions(db.Model):
#     __tablename__ = 'supply_exhaust_fans_definitions'

#     id = db.Column(db.Integer, primary_key=True)
#     # sales order id will be null if it is a standard definition
#     sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
#     standard = db.Column(db.Boolean)

#     definition_name = db.Column(db.String(120), unique=True, nullable=False)
#     sensor_type = db.Column(db.String(60), nullable=False)

#     sensor_demand_min = db.Column(db.Integer, nullable=False)
#     sensor_demand_max = db.Column(db.Integer, nullable=False)

#     alarm_threshold = db.Column(db.Integer, nullable=False)


# class MixerFans(db.Model):
#     __tablename__ = 'mixer_fans'

#     id = db.Column(db.Integer, primary_key=True)
#     sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)
    
#     fan_type = db.Column(db.Integer, nullable=False) # 'Fan Type', 
#     fan_address = db.Column(db.Integer, nullable=False) # 'Fan Address', 
#     fan_zone = db.Column(db.Integer, nullable=False) # 'Fan Zone', 

#     # subfan 1
#     subfan1_zone = db.Column(db.Integer, nullable=False) # 'SubFan 1 Zone', 
#     subfan1_type = db.Column(db.Integer, nullable=False) # 'SubFan 1 Type', 
#     subfan1_min_speed = db.Column(db.Integer, nullable=False) # 'SubFan 1 Min Speed', 
#     subfan1_max_speed = db.Column(db.Integer, nullable=False) # 'SubFan 1 Max Speed', 

#     # subfan 2
#     subfan2_zone = db.Column(db.Integer, nullable=False) # 'SubFan 2 Zone',
#     subfan2_type = db.Column(db.Integer, nullable=False) # 'SubFan 2 Type', 
#     subfan2_min_speed = db.Column(db.Integer, nullable=False) # 'SubFan 2 Min Speed', 
#     subfan2_max_speed = db.Column(db.Integer, nullable=False) # 'SubFan 2 Max Speed', 

#     # sensor 1
#     sensor1_type = db.Column(db.Integer, nullable=False) # 'Sensor 1 Type', 
#     sensor1_min_scaling = db.Column(db.Integer, nullable=False) # 'Sensor 1 Min Scaling', 
#     sensor1_max_scaling = db.Column(db.Integer, nullable=False) # 'Sensor 1 Max Scaling', 
#     sensor1_alarm_threshold = db.Column(db.Integer, nullable=False) # 'Sensor 1 Alarm Threshold', 

#     # sensor 2
#     sensor2_type = db.Column(db.Integer, nullable=False) # 'Sensor 2 Type', 
#     sensor2_min_scaling = db.Column(db.Integer, nullable=False) # 'Sensor 2 Min Scaling', 
#     sensor2_max_scaling = db.Column(db.Integer, nullable=False) # 'Sensor 2 Max Scaling', 
#     sensor2_alarm_threshold = db.Column(db.Integer, nullable=False) # 'Sensor 2 Alarm Threshold', 


# class ExpansionBoards(db.Model):
#     __tablename__ = 'expansion_boards'

#     id = db.Column(db.Integer, primary_key=True)
#     sales_order_id = db.Column(db.Integer, db.ForeignKey('sales_orders.id'), nullable=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     u1_sensor_type = db.Column(db.Integer, nullable=False) # 'U1 Sensor Type', 
#     u1_sensor_zone = db.Column(db.Integer, nullable=False) # 'U1 Sensor Zone', 
#     u1_sensor_zone_control = db.Column(db.Integer, nullable=False) # 'U1 Sensor Zone Control', 
#     u1_min_scaling = db.Column(db.Integer, nullable=False) # 'U1 Min Scaling', 
#     u1_max_scaling = db.Column(db.Integer, nullable=False) # 'U1 Max Scaling', 
#     u1_alarm_threshold = db.Column(db.Integer, nullable=False) # 'U1 Alarm Threshold', 

#     u2_sensor_type = db.Column(db.Integer, nullable=False) # 'U2 Sensor Type', 
#     u2_sensor_zone = db.Column(db.Integer, nullable=False) # 'U2 Sensor Zone', 
#     u2_sensor_zone_control = db.Column(db.Integer, nullable=False) # 'U2 Sensor Zone Control', 
#     u2_min_scaling = db.Column(db.Integer, nullable=False) # 'U2 Min Scaling', 
#     u2_max_scaling = db.Column(db.Integer, nullable=False) # 'U2 Max Scaling', 
#     u2_alarm_threshold = db.Column(db.Integer, nullable=False) # 'U2 Alarm Threshold', 

#     u3_sensor_type = db.Column(db.Integer, nullable=False) # 'U3 Sensor Type', 
#     u3_sensor_zone = db.Column(db.Integer, nullable=False) # 'U3 Sensor Zone', 
#     u3_sensor_zone_control = db.Column(db.Integer, nullable=False) # 'U3 Sensor Zone Control', 
#     u3_min_scaling = db.Column(db.Integer, nullable=False) # 'U3 Min Scaling', 
#     u3_max_scaling = db.Column(db.Integer, nullable=False) # 'U3 Max Scaling', 
#     u3_alarm_threshold = db.Column(db.Integer, nullable=False) # 'U3 Alarm Threshold', 

#     u4_sensor_type = db.Column(db.Integer, nullable=False) # 'U4 Sensor Type', 
#     u4_sensor_zone = db.Column(db.Integer, nullable=False) # 'U4 Sensor Zone', 
#     u4_sensor_zone_control = db.Column(db.Integer, nullable=False) # 'U4 Sensor Zone Control', 
#     u4_min_scaling = db.Column(db.Integer, nullable=False) # 'U4 Min Scaling', 
#     u4_max_scaling = db.Column(db.Integer, nullable=False) # 'U4 Max Scaling', 
#     u4_alarm_threshold = db.Column(db.Integer, nullable=False) # 'U4 Alarm Threshold', 

#     dig_in_5_digfunction = db.Column(db.Integer, nullable=False) # 'Dig In 5 Function', 
#     dig_in_5_inverted = db.Column(db.Boolean, nullable=False) # 'Dig In 5 Inverted', 
#     dig_in_5_fan_zone = db.Column(db.Integer, nullable=False) # 'Dig In 5 Fan Zone', 

#     dig_in_6_digfunction = db.Column(db.Integer, nullable=False) # 'Dig In 6 Function', 
#     dig_in_6_inverted = db.Column(db.Boolean, nullable=False) # 'Dig In 6 Inverted', 
#     dig_in_6_fan_zone = db.Column(db.Integer, nullable=False) # 'Dig In 6 Fan Zone', 

#     dig_in_7_digfunction = db.Column(db.Integer, nullable=False) # 'Dig In 7 Function', 
#     dig_in_7_inverted = db.Column(db.Boolean, nullable=False) # 'Dig In 7 Inverted', 
#     dig_in_7_fan_zone = db.Column(db.Integer, nullable=False) # 'Dig In 7 Fan Zone', 

#     u8_output = db.Column(db.Integer, nullable=False) # 'U8 Output', 
#     u8_demand_type = db.Column(db.Integer, nullable=False) # U8 Demand Type
#     u8_fan_zone = db.Column(db.Integer, nullable=False) # 'U8 Fan Zone', 
#     u8_fan_min_speed = db.Column(db.Integer, nullable=False) # 'U8 Fan Min Speed', 
#     u8_fan_max_speed = db.Column(db.Integer, nullable=False) # 'U8 Fan Max Speed', 

#     u9_output = db.Column(db.Integer, nullable=False) # 'U9 Output', 
#     u9_demand_type = db.Column(db.Integer, nullable=False) # U9 Demand Type
#     u9_fan_zone = db.Column(db.Integer, nullable=False) # 'U9 Fan Zone', 
#     u9_fan_min_speed = db.Column(db.Integer, nullable=False) # 'U9 Fan Min Speed', 
#     u9_fan_max_speed = db.Column(db.Integer, nullable=False) # 'U9 Fan Max Speed', 

#     u10_output = db.Column(db.Integer, nullable=False) # 'U10 Output', 
#     u10_demand_type = db.Column(db.Integer, nullable=False) # U10 Demand Type
#     u10_fan_zone = db.Column(db.Integer, nullable=False) # 'U10 Fan Zone', 
#     u10_fan_min_speed = db.Column(db.Integer, nullable=False) # 'U10 Fan Min Speed', 
#     u10_fan_max_speed = db.Column(db.Integer, nullable=False) # 'U10 Fan Max Speed', 

#     relay_2_alarm_type = db.Column(db.Integer, nullable=False) # 'Relay 2 Alarm Type', 
#     relay_2_failesafe = db.Column(db.Integer, nullable=False) # 'Relay 2 Failsafe', 

#     relay_3_enable = db.Column(db.Integer, nullable=False) # 'Relay 3 Enable', 
#     relay_4_enable = db.Column(db.Integer, nullable=False) # 'Relay 4 Enable', 
#     relay_5_enable = db.Column(db.Integer, nullable=False) # 'Relay 5 Enable', 

#     relay_1_run_indication_fan_group = db.Column(db.Integer, nullable=False) # 'Relay 1 Run Ind Fan Group', 
#     relay_1_run_type = db.Column(db.Integer, nullable=False) # 'Relay 1 Run Type', 
#     relay_1_fan_zone = db.Column(db.Integer, nullable=False) # 'Relay Fan Zone', 
#     relay_1_comm_type = db.Column(db.Integer, nullable=False) # 'Relay 1 Comm Type', 
#     relay_1_polarity = db.Column(db.Boolean, nullable=False) # 'Relay 1 Polarity', 


# class Fire(db.Model):
#     __tablename__ = 'fire'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     gfa_u5 = db.Column(db.Integer, nullable=False) # 'GFA U5', 
#     fire_mode_supply_fan_speed = db.Column(db.Integer, nullable=False) # 'Fire Mode Supply Fan Speed', 
#     fire_mode_exhaust_fan_speed = db.Column(db.Integer, nullable=False) # 'Fire Mode Exhaust Fan Speed', 
#     fire_mode_mixer_fan_speed = db.Column(db.Integer, nullable=False) # 'Fire Mode Mixer Fan Speed',

#     fire_run_id7 = db.Column(db.Integer, nullable=False) #'Fire Run (ID7)',  
#     fire_stop_id8 = db.Column(db.Integer, nullable=False) # 'Fire Stop (ID8)', 
#     mixer_fan_run_speed = db.Column(db.Integer, nullable=False) # 'Mixer Fan Run Speed', 


# class Override(db.Model):
#     __tablename__ = 'override'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     supply_fan_fail_safe = db.Column(db.Integer, nullable=False) # 'Supply Fan Fail/Safe', 
#     ixer_fan_fail_safe = db.Column(db.Integer, nullable=False) # 'Mixer Fan Fail/Safe', 
#     xhaust_fan_fail_safe = db.Column(db.Integer, nullable=False) # 'Exhaust Fan Fail/Safe', 

#     supply_fan_run_speed = db.Column(db.Integer, nullable=False) # 'Supply Fan Run Speed', 
#     mixer_fan_run_speed = db.Column(db.Integer, nullable=False) # 'Mixer Fan Run Speed', 
#     exhaust_fan_run_speed = db.Column(db.Integer, nullable=False) # 'Exhaust Fan Run Speed', 



# class IndividualControl(db.Model):
#     __tablename__ = 'individual_control'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     sensor_demand_level = db.Column(db.Boolean, nullable=True) # 'Sensor Demand Level', 
#     fan_demand_level = db.Column(db.Boolean, nullable=True) # 'Fan Demand Level', 
#     fan_demand_type = db.Column(db.Boolean, nullable=True) # 'Fan Demand Type', 
#     demand_band = db.Column(db.Integer, nullable=False) # 'Demand Band', 
#     delay = db.Column(db.Integer, nullable=False) # 'Delay', 


# class GlobalDemandConfigurations(db.Model):
#     __tablename__ = 'global_demand_configurations'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     co_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'CO Demand @ 0%', 
#     no2_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'NO2 Demand @ 0%', 
#     temp_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'Temperature Demand @ 0%', 
#     external_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'External Demand @ 0%',
#     co2_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'CO2 Demand @ 0%', 

#     co_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'CO Demand @ 100%', 
#     no2_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'NO2 Demand @ 100%', 
#     temp_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'Temperature Demand @ 100%', 
#     external_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'External Demand @ 100%', 
#     co2_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'CO2 Demand @ 100%', 





# class ControllerSensorDemandConfigurations(db.Model):
#     __tablename__ = 'controller_demand_configurations'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     co_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'CO Demand @ 0%', 
#     no2_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'NO2 Demand @ 0%', 
#     temp_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'Temperature Demand @ 0%', 
#     external_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'External Demand @ 0%',
#     co2_demand_at_0_percent = db.Column(db.Integer, nullable=False) # 'CO2 Demand @ 0%', 

#     co_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'CO Demand @ 100%', 
#     no2_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'NO2 Demand @ 100%', 
#     temp_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'Temperature Demand @ 100%', 
#     external_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'External Demand @ 100%', 
#     co2_demand_at_100_percent = db.Column(db.Integer, nullable=False) # 'CO2 Demand @ 100%', 


# class Purge(db.Model):
#     __tablename__ = 'purge'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     start_time = db.Column(db.String(20), nullable=False) # 'Start Time', 
#     run_duration = db.Column(db.Integer, nullable=False) # 'Run Duration', 
#     fan_speed = db.Column(db.Integer, nullable=False) # 'Fan Speed', 


# class Smoke(db.Model):
#     __tablename__ = 'smoke'

#     id = db.Column(db.Integer, primary_key=True)
#     controller_id = db.Column(db.Integer, db.ForeignKey('controllers.id'), nullable=True)

#     smoke_detector_system_trigger_delay_seconds = db.Column(db.Integer, nullable=False) # Trigger Delay # CAREL VAR-> SmokePurgeDel
#     smoke_detector_system_keep_off_delay_minutes = db.Column(db.Integer, nullable=False) #  Keep Off Delay # CAREL VAR-> SmokePurgeTime
    
#     smoke_detector_system_fan_purge_time = db.Column(db.Integer, nullable=False) # Fan Purge Time # CAREL VAR-> 
#     smoke_detector_system_fan_purge_speed =db.Column(db.Integer, nullable=False) # Fan Purge Speed # CAREL VAR-> smokeSpeed

#     smoke_mode_mixer_fan_speed = db.Column(db.Integer, nullable=False) # Mixer Fan Speed
#     smoke_mode_exhaust_fan_speed = db.Column(db.Integer, nullable=False) # Exhaust Fan Speed
#     smoke_mode_supply_fan_speed = db.Column(db.Integer, nullable=False) # Supply Fan Speed

#     smoke_mode_zoned = db.Column(db.Boolean, nullable=True)









