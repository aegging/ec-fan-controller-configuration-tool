from controllerConfigTool.models import Settings

def create_controller_type_list(version_id):
        controller_type_list = [""]
        settings_object = Settings.query.filter_by(id=version_id).first()
        if settings_object.valid_with_max_controller:
            controller_type_list.append('MAX')
        if settings_object.valid_with_mini_controller:
            controller_type_list.append('MINI')
        if not controller_type_list:
            controller_type_list.append('No Controller Type')
        return controller_type_list