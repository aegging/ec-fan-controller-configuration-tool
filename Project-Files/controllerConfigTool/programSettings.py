
from controllerConfigTool.models import Settings, Controller, systemprogrammed, SalesOrders
from controllerConfigTool.getInfoFromDatabase import getVersionGivenID
from controllerConfigTool import db
from controllerConfigTool.getInfoFromDatabase import getDateAndTime
from controllerConfigTool.editFansControllers import setControllerProgrammmingErrorFalse




def programControllerFinished(controller_id):
    saveSystemProgrammed(controller_id)
    # change Controller button from yellow to Green
    # setControllerProgrammmingErrorFalse(controller_id)


def saveSystemProgrammed(controller_id):
    controller_object = Controller.query.filter_by(id=controller_id).first()
    sales_order_id = controller_object.sales_order_id
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()

    project_name = createProjectName(sales_order_object.sales_order_number, controller_object.tag_name)

    system_programmed_object = systemprogrammed(
        Project = project_name,
        ProgramDate = getDateAndTime(),
        Computer_ID = 'WebTool',
        SerialNo = controller_object.controller_serial_number,
        VersionNo = getVersionGivenID(controller_object.version_number_id),
        ProgOption = 'all'

    )
    db.session.add(system_programmed_object)
    db.session.commit()
    

def createProjectName(sales_order_number, tag_name):
    project_name = str(sales_order_number) + '-' + str(tag_name)
    return project_name


def createDefaultSettings():
    
    default_settings = Settings(
        cfactory_path = r'C:\Program Files (x86)\CAREL\c.suite\4.6.11\Carel c.factory\c.factory.exe',
        mini_http_folder_directory = r'C:\Program Files\Fantech\EasyWinload\bin3.2\Mini\WebPage',
        max_http_folder_directory = r'C:\Program Files\Fantech\EasyWinload\bin3.2\WebPage',
        mini_ap1_directory = r'C:\Program Files\Fantech\EasyWinload\bin3.2\Mini\OS\Autorun.ap1',
        max_ap1_directory = r'C:\Program Files\Fantech\EasyWinload\bin3.2\OS\Autorun.ap1',
        input_on = getDateAndTime(),
        currently_selected = True,
        version_number = '3.2.2',
        time_zone=90
    )
    db.session.add(default_settings)
    db.session.commit()


