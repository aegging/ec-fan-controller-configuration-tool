from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField, SelectField, IntegerField, BooleanField, FloatField
from wtforms.validators import DataRequired, Optional, ValidationError
from controllerConfigTool.models import Job, Settings
from controllerConfigTool import db



class ProgramControllersForm(FlaskForm):
    ip = StringField('IP', validators=[DataRequired()])
    controller_serial_number = StringField('Serial Number', validators=[DataRequired()])
    webpages_boolean = BooleanField('Webpages',  default=True)
    upload_ap1_file_boolean = BooleanField('AP1 File (Software)',  default=True)

    submit = SubmitField('Program Controller')


