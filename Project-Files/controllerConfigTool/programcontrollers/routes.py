from flask import Blueprint
from flask import render_template, url_for, flash, redirect, request
from controllerConfigTool import db
from controllerConfigTool.getInfoFromDatabase import getAllCarelKeyValues, getArchiveDirectory, getControllerID, getControllerNumber,\
     getControllerValues, getSalesOrderValues, getTotalControllers, getTotalProgrammedControllers, getVersionGivenID
from controllerConfigTool.models import Controller, ControllerProgramStatus
from controllerConfigTool.programcontrollers.utils import updateControllerButtons, uploadToController
from controllerConfigTool.programcontrollers.forms import ProgramControllersForm
from controllerConfigTool.createDefaultTxtFile import createDefaultTxtFile
from threading import Thread
from sqlalchemy import null, or_

programcontrollers_blueprint = Blueprint('programcontrollers', __name__)



@programcontrollers_blueprint.route("/programcontroller/<int:sales_order_id>/<int:current_controller>/<tag_prefix>/", methods=['GET', 'POST'])
def programcontroller(sales_order_id, current_controller, tag_prefix):

    # get archived directory to display to user
    archived_directory = getArchiveDirectory(sales_order_id)

    # find total programmed controllers
    # total_controllers_programmed = getTotalProgrammedControllers(sales_order_id)

    # find controller id
    controller_id = getControllerID(current_controller, sales_order_id)

    # get data from database for current controller
    controller_type, controller_serial_number, address_fantype_dictionary, grouped_list_fan_types, version_number_id = getControllerValues(controller_id)
    selected_version = getVersionGivenID(version_number_id)

    program_controllers_form = ProgramControllersForm()
    
    # get sales order values
    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)

    # update GREEN, YELLOW, RED controller buttons
    list_index_of_programmed_controllers, list_index_of_programming_controllers, list_index_of_error_controllers = updateControllerButtons(sales_order_id)

    # get total controllers for sales order so that we know how many controller buttons to create
    total_countroller_buttons = getTotalControllers(sales_order_id)

    test_dict = getAllCarelKeyValues()


    # if one of the controller buttons are pressed
    if request.method == 'POST':
        if "navigate_edit_sales_order" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        for i in range(1, total_countroller_buttons + 1):
            controller_name = 'controller' + str(i)
            if controller_name in request.form:
                return redirect(url_for(
                                'programcontrollers.programcontroller', sales_order_id=sales_order_id, current_controller=i, tag_prefix=tag_prefix))

    # if submit button is pressed and form data is valid
    if program_controllers_form.validate_on_submit():
        
        # get data from database for current controller
        controller_type, controller_serial_number, address_fantype_dictionary, grouped_list_fan_types, version_number_id = getControllerValues(controller_id)

        # data from form
        ip_address = program_controllers_form.ip.data
        controller_serial_number = program_controllers_form.controller_serial_number.data
        webpages_boolean = program_controllers_form.webpages_boolean.data
        upload_ap1_file_boolean = program_controllers_form.upload_ap1_file_boolean.data


        
        # create configuration file
        configuration_directory, modbus_ranges_string = createDefaultTxtFile(sales_order_id, controller_id,\
            controller_serial_number)

        # save controller type, serial #, and set programmed column to true for current controller
        controller_object = Controller.query.filter_by(id=controller_id).first()
        controller_object.controller_type = controller_type
        controller_object.controller_serial_number = controller_serial_number
        controller_object.modbus_ranges = modbus_ranges_string
        #This is used to show controller is in progress (yellow button), When programming is successful, this is changed to False and button turns Green
        controller_object.programming_error = True 
        db.session.add(controller_object)
        db.session.commit()


        # get current controller button number
        controller_number = getControllerNumber(controller_id)


        # Flash message to show controller has been successfully saved into database
        flash(f'Configuration File for Controller {controller_number} has been successfully created and has been stored here-> {configuration_directory}', 'success')

        # Thread upload to controller to allow multiple uploads at once
        background_thread = Thread(target=uploadToController, args=(ip_address, controller_type, configuration_directory, webpages_boolean, upload_ap1_file_boolean, controller_id))
        background_thread.start()

        # set controller programming in progress, clear any existing states
        # is there an existing entry for this controller?
        controller_program_status_object = ControllerProgramStatus.query.filter_by(controller_id=controller_id).first()

        if controller_program_status_object:
            controller_program_status_object.program_success = null()
            controller_program_status_object.program_error = null()
            controller_program_status_object.program_in_progress = True
        else:
            # create new entry
            controller_program_status_object = ControllerProgramStatus(
                controller_id = controller_id,
                program_success = null(),
                program_error = null(),
                program_in_progress = True
            )

        db.session.add(controller_program_status_object)
        db.session.commit()

        # # are there any remaining controllers to be programmed?
        # total_controllers_not_programmed = db.session.query(ControllerProgramStatus).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
        #     filter(Controller.sales_order_id==sales_order_id).filter(or_(ControllerProgramStatus.program_success==False), (ControllerProgramStatus.program_success==None)).count()
        # print(f'total_controllers_not_programmed: {total_controllers_not_programmed}')
        # if total_controllers_not_programmed == 0:
        #     # Flash user that all controllers have finished or began programming
        #     flash(f'All Controllers have been commanded to be programmed. Once all Controller buttons turn green, they will be finished.')

        #     # redirect to first controller for job
        #     return redirect(url_for(
        #                         'programcontrollers.programcontroller', sales_order_id=sales_order_id, current_controller=current_controller, tag_prefix=tag_prefix))

        # # find the next controller to program.  This is a test of the push pull.
        # first_remaining_controller = db.session.query(ControllerProgramStatus).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
        #     filter(Controller.sales_order_id==sales_order_id).filter(or_(ControllerProgramStatus.program_success==False), (ControllerProgramStatus.program_success==None)).first()
        # controller_id = first_remaining_controller.controller_id

        # get current controller number
        current_controller = getControllerNumber(controller_id)

        # find total programmed controllers
        # total_controllers_programmed = getTotalProgrammedControllers(sales_order_id)
  

        return redirect(url_for(
                                'programcontrollers.programcontroller', sales_order_id=sales_order_id, current_controller=current_controller, tag_prefix=tag_prefix))



    return render_template('programcontroller.html', title=f'Program Controller (total_controllers_programmed/{sales_order_controller_quantity} Finished)',
                             program_controllers_form=program_controllers_form, sales_order_id=sales_order_id,
                            #  total_controllers_programmed=total_controllers_programmed, 
                             address_fantype_dictionary=address_fantype_dictionary,
                             current_controller=current_controller, sales_order_controller_quantity=sales_order_controller_quantity, 
                             sales_order_number=current_sales_order_number, tag_prefix=tag_prefix, 
                             list_index_of_programmed_controllers=list_index_of_programmed_controllers,
                             list_index_of_programming_controllers=list_index_of_programming_controllers,
                             list_index_of_error_controllers=list_index_of_error_controllers, 
                            #  gfa_fire_NC=gfa_fire_NC, 
                             selected_version=selected_version,
                             controller_type=controller_type,
                             archived_directory=archived_directory
                             )

    