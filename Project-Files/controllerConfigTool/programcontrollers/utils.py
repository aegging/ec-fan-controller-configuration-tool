
import subprocess
import time
import logging
import shutil
from controllerConfigTool.getInfoFromDatabase import getProgramSettings
from controllerConfigTool.programSettings import programControllerFinished
from controllerConfigTool.models import Controller, SalesOrders, ControllerProgramStatus
import os
from datetime import datetime
from pytz import timezone
from controllerConfigTool import db


class uploadClass:
    def __init__(self, user_entered_ip, controller_type, logger, controller_id):
        
        # get Program Settings from database
        cfactory_path, mini_http_folder_directory, max_http_folder_directory, mini_ap1_directory, max_ap1_directory, version_number = getProgramSettings(controller_id)

        self.logger = logger
        self.ControllerIP = str(user_entered_ip) 
        self.cfactory_path = cfactory_path
        self.mini_http_folder_directory = mini_http_folder_directory
        self.max_http_folder_directory = max_http_folder_directory
        self.mini_ap1_directory = mini_ap1_directory
        self.max_ap1_directory = max_ap1_directory
        self.cfactory_path = '"' + self.cfactory_path + '"'
        self.settings_version = version_number


        if controller_type == 'MAX':
            self.http_directory = max_http_folder_directory
            self.ap1_directory = max_ap1_directory
        elif controller_type == 'MINI':
            self.http_directory = mini_http_folder_directory
            self.ap1_directory = mini_ap1_directory
        else:
            self.logger.warning('The following controller type is not valid: %s. Please Choose MAX or MINI', controller_type)


    def pushConfiguration(self, configuration_directory):
        subprocess.run(self.cfactory_path + r' -o copyfile --from "' + configuration_directory + r'/default.txt' + r'" --to "/" --ftp ' + self.ControllerIP, shell=True)
        time.sleep(10)
        completed = subprocess.run(self.cfactory_path + r' -o copyfile --from "' + configuration_directory + r'/Factorydefault.txt' + r'" --to "/" --ftp ' + self.ControllerIP, shell=True)
        time.sleep(10)
        return_code = completed.returncode
        self.logger.info("Push Configuration File method return code: %s", return_code)
        return return_code


    def stopApplication(self, failed_to_stop=False):
        # if it has failed to stop enough times
        if failed_to_stop:
            self.logger.warning('Gave up trying to connect to Controller with IP Address %s', self.ControllerIP)
            return 0

        completed = subprocess.run(self.cfactory_path + r" -o stopapp --ip " + self.ControllerIP, shell=True, stdout=subprocess.PIPE,)
        return_code = completed.returncode
        self.logger.info("Stop Application method return code: %s", return_code)

        if failed_to_stop == False and return_code != 0:
            self.logger.warning('Failed to connect to Controller with IP Address %s. Will try again in 10 seconds.', self.ControllerIP)

        return return_code


    def uploadWebpages(self):
        completed = subprocess.run(self.cfactory_path + r' -o copyfolder --from "' + self.http_directory + r'" --to "/" --ftp ' + self.ControllerIP, shell=True)           
        return_code = completed.returncode
        self.logger.info("Upload Webpages method return code: %s", return_code)
        time.sleep(10)


    def uploadAp1File(self):
        completed = subprocess.run(self.cfactory_path + r' -o copyfile --from "' + self.ap1_directory + r'" --to "/UPGRADE/" --ftp ' + self.ControllerIP, shell=True)     
        return_code = completed.returncode
        self.logger.info("Upload Ap1 File method return code: %s", return_code)
        return return_code


    def startApplication(self):
        time.sleep(10)
        completed = subprocess.run(self.cfactory_path + r" -o restartapp --ip " + self.ControllerIP, shell=True,
                        stdout=subprocess.PIPE,
                        )
        return_code = completed.returncode
        self.logger.info("Start Application method return code: %s", return_code)
        return return_code
    

    def deleteFile(self, file_name):
        completed = subprocess.run(self.cfactory_path + r" -o deletefile --file /" + file_name + r" --ftp " + self.ControllerIP, shell=True)
        return_code = completed.returncode
        self.logger.info("Delete File method return code: %s", return_code)


    def deleteConfigurationFiles(self):
        files = ['default.txt', 'Factorydefault.txt']
        for file in files:
            try:
                self.deleteFile(file)
                self.logger.info('File successfully deleted: %s', file)
                time.sleep(2)
            except:
                self.logger.warning('File, %s ,could not be deleted (or did not exist) ', file)
                time.sleep(2)


    def copyFile(self, configuration_directory):
        shutil.copy(configuration_directory + "/default.txt", configuration_directory + '/Factorydefault.txt')
        time.sleep(5)

    def copyAP1File(self, software_directory):
        software_directory = software_directory + "/Autorun.ap1"
        # software_directory = os.path.join(software_directory, 'Autorun.ap1')
        print(f'ap1 directory: {self.ap1_directory} || software directory: {software_directory}')
        shutil.copy(self.ap1_directory , software_directory)
        time.sleep(5)

def uploadToController(ip, controller_type, configuration_directory, webpages_boolean, upload_ap1_file_boolean, controller_id):
    # initialize logging
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(__name__)

    logger.info('IP Address: %s || Controller Type: %s', ip, controller_type)
    logger.info('Configuration Directory: %s', configuration_directory)

    c = uploadClass(ip, controller_type, logger, controller_id)

    c.copyFile(configuration_directory)

    # get system path using controller id
    controller_object = Controller.query.filter_by(id=controller_id).first()
    sales_order_id = controller_object.sales_order_id
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    THIS_FOLDER = sales_order_object.system_path
    job_number = sales_order_object.sales_order_number
    DATE_TIME = (datetime.now(timezone('US/Central'))).strftime("%m-%d-%Y")

    CONTROLS_FOLDER = os.path.join(THIS_FOLDER, 'controls')
    JOB_NUMBER_FOLDER = os.path.join(CONTROLS_FOLDER, str(job_number + "__" + DATE_TIME) )
    CONTROLLER_SOFTWARE_FOLDER = os.path.join(JOB_NUMBER_FOLDER, str(c.settings_version) + "-" + (str(controller_type)).upper() + '-controller-software')

    print(f'CONTROLLER SOFTWARE FOLDER: {CONTROLLER_SOFTWARE_FOLDER}')

    if not os.path.exists(CONTROLLER_SOFTWARE_FOLDER):
            os.makedirs(CONTROLLER_SOFTWARE_FOLDER)

    c.copyAP1File(CONTROLLER_SOFTWARE_FOLDER)


    # try to stop controller until success
    did_stop_fail = doCommandUntilSuccess(c, "stop")

    if did_stop_fail:
        logger.warning('Failed to communicate with Controller %s. Check IP address {ip} and try again.', configuration_directory)
        # add in return statement to be used for sending command to database to set controller programmed column back to False
        
        setControllerProgrammedFalse(controller_id)
        return

    c.deleteConfigurationFiles()

    did_push_fail = doCommandUntilSuccessWithArg(c, "pushConfig", configuration_directory)
    # c.pushConfiguration(configuration_directory)

    if did_push_fail:
        logger.warning('Failed to push configuration to Controller %s. Check IP {ip} address and try again.', configuration_directory)
        # add in return statement to be used for sending command to database to set controller programmed column back to False
        setControllerProgrammedFalse(controller_id)
        return

    if webpages_boolean:
        c.uploadWebpages()

    if upload_ap1_file_boolean:
        c.uploadAp1File()
    else:
        # try to start controller until success
        c.startApplication()
    
    logger.info(f'FINISHED UPLOADING TO CONTROLLER ({ip})')
    # save entry to systemprogrammed for this controller
    programControllerFinished(controller_id)

    controller_program_status_object = ControllerProgramStatus.query.filter_by(controller_id=controller_id).first()
    controller_program_status_object.program_success = True
    controller_program_status_object.program_in_progress = False
    controller_program_status_object.program_error = False
    
    db.session.add(controller_program_status_object)
    db.session.commit()


def doCommandUntilSuccessWithArg(c, method_name_string, arg):
    did_it_fail = False
    method_name = getMethodName(c, method_name_string)
    return_code = 1
    attempts_to_succeed = 10

    while(return_code != 0 and attempts_to_succeed > 0):
        return_code = method_name(arg)
        attempts_to_succeed = attempts_to_succeed - 1

        if attempts_to_succeed == 1:
            # if it reaches this point, it has failed too many times, 
            # send True argument to method so that it will trigger a failed message and return 0 to break infinite while loop
            return_code = method_name(True) 
            did_it_fail = True

        time.sleep(10)

    return did_it_fail

# Method needs a failed_to_stop argument added in order to use this function
# Such as the one used in stopApplication()
def doCommandUntilSuccess(c, method_name_string):
    did_it_fail = False
    method_name = getMethodName(c, method_name_string)
    return_code = 1
    attempts_to_succeed = 10
    while(return_code != 0 and attempts_to_succeed > 0):
        return_code = method_name()
        attempts_to_succeed = attempts_to_succeed - 1

        if attempts_to_succeed == 1:
            # if it reaches this point, it has failed too many times, 
            # send True argument to method so that it will trigger a failed message and return 0 to break infinite while loop
            return_code = method_name(True) 
            did_it_fail = True

        time.sleep(10)

    return did_it_fail
     
        
def getMethodName(c, method_name_string):
    name_dict = {'start':c.startApplication, 'stop':c.stopApplication, 'uploadWebPages':c.uploadWebpages, 'uploadAp1File':c.uploadAp1File, 'pushConfig':c.pushConfiguration}
    method_name = name_dict.get(method_name_string)
    return method_name



def updateControllerButtons(sales_order_id):
    """
    programmed_controllers: list of FINISHED/PROGRAMMED controllers; GREEN CONTROLLER BUTTONS
    programming_controllers: list of IN PROCESS/PROGRAMMING controllers; YELLOW CONTROLLER BUTTONS
    error_controllers: list of ERROR controllers; RED CONTROLLER BUTTONS; These controllers were given command to be programmed but ended up failing to finish
    """
    programmed_controllers=getListProgrammedControllers(sales_order_id)    
    programming_controllers=getListProgrammingControllers(sales_order_id)
    error_controllers=getListErrorControllers(sales_order_id)

    return programmed_controllers, programming_controllers, error_controllers



# if 'programmed' is True and 'programming_error' is False; Turn Controller button green
def getListProgrammedControllers(sales_order_id):
    """ find all Programmed controllers """
    items_exist = bool(db.session.query(ControllerProgramStatus.controller_id).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
            filter(Controller.sales_order_id==sales_order_id).filter(ControllerProgramStatus.program_success==True).first())
    all_programmed_controllers = db.session.query(ControllerProgramStatus.controller_id).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
            filter(Controller.sales_order_id==sales_order_id).filter(ControllerProgramStatus.program_success==True).all()


    list_index_of_programmed_controllers = []
    if items_exist:
        for item in all_programmed_controllers:
            controller_id = item.controller_id
            current_controller_index = getControllerNumber(controller_id)
            list_index_of_programmed_controllers.append(current_controller_index)

    return list_index_of_programmed_controllers


# if 'programmed' is True and 'programming_error' is True; Turn Controller button yellow
# Both 'programmed' and 'programming_error' are set to True the moment the 'Program Controller' button is pressed
# Once the controller finishes programming, 'programming_error' is set to False and then we know to turn the Controller button green
def getListProgrammingControllers(sales_order_id):
    """ find all Programming controllers """
    items_exist = bool(db.session.query(ControllerProgramStatus.controller_id).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
            filter(Controller.sales_order_id==sales_order_id).filter(ControllerProgramStatus.program_in_progress==True).first())
    all_programming_controllers = db.session.query(ControllerProgramStatus.controller_id).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
            filter(Controller.sales_order_id==sales_order_id).filter(ControllerProgramStatus.program_in_progress==True).all()

    list_index_of_programming_controllers = []
    if items_exist:
        for item in all_programming_controllers:
            controller_id = item.controller_id
            current_controller_index = getControllerNumber(controller_id)
            list_index_of_programming_controllers.append(current_controller_index)

    return list_index_of_programming_controllers


def getListErrorControllers(sales_order_id):
    """ find all Error controllers """
    items_exist = bool(db.session.query(ControllerProgramStatus.controller_id).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
            filter(Controller.sales_order_id==sales_order_id).filter(ControllerProgramStatus.program_error==True).first())
    all_error_controllers = db.session.query(ControllerProgramStatus.controller_id).join(Controller, Controller.id == ControllerProgramStatus.controller_id).\
            filter(Controller.sales_order_id==sales_order_id).filter(ControllerProgramStatus.program_error==True).all()

    list_index_of_error_controllers = []
    if items_exist:
        for item in all_error_controllers:
            # if item.programming_error==True:
            controller_id = item.controller_id
            current_controller_index = getControllerNumber(controller_id)
            list_index_of_error_controllers.append(current_controller_index)

    return list_index_of_error_controllers


def getControllerNumber(controller_id):
    """ Get controller button number given controller id """
    controller_object = Controller.query.filter_by(id=controller_id).first()
    sales_order_id = controller_object.sales_order_id

    all_controllers_object = Controller.query.filter_by(sales_order_id=sales_order_id).all()
    controller_number = 1

    for controller in all_controllers_object:
        if controller.id == controller_id:
            return controller_number
        else:
            controller_number += 1


def setControllerProgrammedFalse(controller_id):
    controller_program_status_object = ControllerProgramStatus.query.filter_by(controller_id=controller_id).first()
    controller_program_status_object.program_success = False
    controller_program_status_object.program_in_progress = False
    controller_program_status_object.program_error = True
    
    db.session.add(controller_program_status_object)
    db.session.commit()
    

    