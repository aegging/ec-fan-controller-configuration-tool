import urllib.request
import json


def getControllerTypeAndMacAddress(ip):

    url = "http://"+ str(ip) +"/get_sys_info.cgi"
    status_code = 0

    try:
        status_code = urllib.request.urlopen(url).getcode()
        print(f'status code: {status_code}')
    
    except urllib.error.URLError as e:
        print(e.reason) 

    # if status code is 200, then controller exists at that IP
    if status_code == 200:
        file = urllib.request.urlopen(url)

        text_string = ""

        for line in file:
            decoded_line = line.decode("utf-8")
            text_string = text_string + decoded_line

        result_dict = json.loads(text_string)
        controller_type = result_dict["pcotype"]
        mac_address = result_dict["mac"]

        return controller_type, mac_address

    # could not find controller at that IP
    else:
        return 0, 0



if __name__ == '__main__':
    controller_type, mac_address = getControllerTypeAndMacAddress("10.1.10.8")

    print(f'controller type: {controller_type}')
    print(f'mac address: {mac_address}')