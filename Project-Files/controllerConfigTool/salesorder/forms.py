from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField, SelectField
from wtforms.validators import DataRequired
from controllerConfigTool.models import SalesOrders, Settings
from wtforms import StringField,  SubmitField, BooleanField, IntegerField, SelectField
from wtforms.validators import DataRequired, ValidationError, Optional
from controllerConfigTool.getInfoFromDatabase import findFirstNonConfiguredController,\
     getSelectedVersion, getSettingsID, getDateAndTime



class SalesOrderForm(FlaskForm):
    # def defaultTimeZone():
    #     settings_object = Settings.query.filter_by(currently_selected = True).first()
    #     selected_time_zone = settings_object.time_zone
    #     return selected_time_zone  

    def get_settings_choices():
        settings_objects = Settings.query.filter_by(enable_version = True).order_by(Settings.version_number).all()
        settings_choices = [['', '']]
        for settings_object in settings_objects:
            settings_choices.append([settings_object.id, settings_object.version_number])
        # settings_choices = "3.4.0BO"

        return settings_choices


    sales_order_number = StringField('Sales Order Number', validators=[DataRequired()])
    tag_name = StringField('Controller Tag Name', default="ZF", validators=[DataRequired()])
    entered_by = StringField('Entered by:', validators=[DataRequired()])
    time_zone = SelectField('Time Zone: ', choices=[('',''),('11','Central Daylight Time'),('9','Mountain Daylight Time'),('15','Eastern Daylight'),('6','Pacific'),('7','Mountain Standard Time'),('90','SYD/MELB')], validators=[DataRequired()])
    file_path_boolean = BooleanField('Archive Directory', default=True)
    submit = SubmitField('Configure Order')


    # counts
    controller_quantity = IntegerField('Controllers', default=0, validators=[DataRequired()])
    mixer_fan_quantity = IntegerField('Mixer Fans', default=0, validators=[DataRequired()])
    fieldbus_sensor_quantity = StringField('Fieldbus Sensors', render_kw={'type':'number'}, default=0, validators=[DataRequired()])
    analog_sensor_quantity = StringField('Analog Sensors', render_kw={'type':'number'}, default=0, validators=[DataRequired()])
    expansion_board_quantity = StringField('Expansion Boards', render_kw={'type':'number'}, default=0, validators=[DataRequired()])
    fieldbus_drives_quantity = StringField('Fieldbus Drives', render_kw={'type':'number'}, default=0, validators=[DataRequired()])

    software_version = SelectField('Software', choices=get_settings_choices(), validators=[DataRequired()])


    # software_version= getSelectedVersion()

    # def validate_software_version(self, software_version):
    #     if software_version.data == 'None':
    #         raise ValidationError('Software Version cannot be None')

    # CG Changes for validating filed on sales order page start Date 28/03/2023
    def validate_controller_quantity(self, controller_quantity):
        if int(controller_quantity.data) == 0:
            raise ValidationError('Please Enter atleast 1 or more than 1 quantity.')
        
    # def validate_fieldbus_sensor_quantity(self, fieldbus_sensor_quantity):
    #     if int(fieldbus_sensor_quantity.data) == '':
    #         raise ValidationError('Please Enter atleast 1 or more than 1 quantity.')
    
   
        
    def validate_mixer_fan_quantity(self, mixer_fan_quantity):
        if int(mixer_fan_quantity.data) == 0:
            raise ValidationError('Please Enter atleast 1 or more than 1 quantity.')


    

    def validate_tag_name(self, tag_name):
        if len(tag_name.data) > 7:
            raise ValidationError('The maximum length for the Tag Name field is 7 characters. Please choose a shorter tag name.')


    def validate_expansion_board_quantity(self, expansion_board_quantity):
        if int(expansion_board_quantity.data) > self.controller_quantity.data*5:
            raise ValidationError('Invalid number of Expansion Boards. Maximum of 5 per controller are allowed.')
    
    def validate_sales_order_number(self, sales_order_number):
        # search database for existing sales order number
        already_exist = bool(SalesOrders.query.filter_by(sales_order_number=sales_order_number.data).first())
        if already_exist:
            raise ValidationError('Invalid. Please choose a Sales Order Number that is not already in use.')
