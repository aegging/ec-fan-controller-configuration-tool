from flask import render_template, url_for, flash, redirect, request
from flask import Blueprint

from controllerConfigTool import db, app
from controllerConfigTool.models import ControllerProgramStatus, SalesOrders, Controller, ControllerInputOutputs,Zone, OutputZones
from controllerConfigTool.createFans import createExpansionBoards
from controllerConfigTool.createSensors import createAnalogSensors
from controllerConfigTool.getInfoFromDatabase import findFirstNonConfiguredController,\
     getSelectedVersion, getSettingsID, getDateAndTime
from controllerConfigTool.salesorder.forms import SalesOrderForm

from controllerConfigTool.models import Settings
from controllerConfigTool.admin.utils import saveVersionSelection



salesorder_blueprint = Blueprint('salesorder', __name__)

@salesorder_blueprint.route('/', methods=['GET', 'POST'])
@salesorder_blueprint.route("/createsalesorder/", methods=['GET', 'POST'])


# def currentdisplayversion():
#     display_version = Settings.query.filter_by(currently_selected = True).first()
#     # print("------------------------------",display_version,  type(display_version),"------------------------------------")
#     display_version = str(display_version)
#     return display_version


def createsalesorder():

    # find selected software version
    selected_version = getSelectedVersion()
    # find corresponding id
    settings_id = getSettingsID()

    sales_order_form = SalesOrderForm()

    if request.method == 'POST':
        # when submit button is pressed, if form data is valid
        if sales_order_form.validate_on_submit():

            order = SalesOrders(
                sales_order_number=sales_order_form.sales_order_number.data,
                time_zone=sales_order_form.time_zone.data,
                controller_quantity=sales_order_form.controller_quantity.data,
                mixer_fan_quantity=sales_order_form.mixer_fan_quantity.data,
                fieldbus_sensor_quantity=sales_order_form.fieldbus_sensor_quantity.data,
                analog_sensor_quantity=sales_order_form.analog_sensor_quantity.data,
                expansion_board_quantity=sales_order_form.expansion_board_quantity.data,
                fieldbus_drives_quantity=sales_order_form.fieldbus_drives_quantity.data,
                tag_name=sales_order_form.tag_name.data,
                input_by=sales_order_form.entered_by.data, 
                input_on=getDateAndTime()
                )
            db.session.add(order)
            db.session.commit()

            # CG CHANGES 23/03/2023 to update software version in settings table start
            select_version= sales_order_form.software_version.data
            saveVersionSelection(select_version)
            # end


            flash(f'Sales Order data saved into database for order {sales_order_form.sales_order_number.data}!', 'success')

            sales_order_object = SalesOrders.query.filter_by(sales_order_number = sales_order_form.sales_order_number.data).first()
            sales_order_controller_quantity = sales_order_object.controller_quantity
            sales_order_id = sales_order_object.id
            tag_prefix = sales_order_form.tag_name.data

            print("-------",sales_order_id, "-------", tag_prefix,"-----", select_version)

            # Initialize rows in Controller table
            for item in range(0, sales_order_controller_quantity):
                controller = Controller(
                                        sales_order_id=sales_order_id, tag_name=tag_prefix + str(item + 1), version_number_id=select_version
                                        )
                db.session.add(controller)

            db.session.commit()

            controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()
            for controller_object in controller_objects:
                controller_input_output = ControllerInputOutputs(
                                            sales_order_id = controller_object.sales_order_id,
                                            controller_id = controller_object.id 
                                            )
                controller_program_status_object = ControllerProgramStatus(
                    controller_id = controller_object.id
                )

                zone_obj= Zone(sales_order_id=sales_order_id, controller_id=controller_object.id)
                db.session.add(zone_obj)

                ouput_zones = OutputZones(sales_order_id=sales_order_id, controller_id = controller_object.id)
                db.session.add(ouput_zones)


                db.session.add(controller_input_output)
                db.session.add(controller_program_status_object)


            db.session.commit()

            # create expansionboards for the given sales order id
            createExpansionBoards(sales_order_id, sales_order_form.expansion_board_quantity.data)

            # create row for each analog sensor in analog_sensors table
            # send 0 to start w/ address 1 
            createAnalogSensors(sales_order_id, sales_order_form.analog_sensor_quantity.data, 0)

            current_controller = findFirstNonConfiguredController(sales_order_id, tag_prefix)

            # if user wants to choose directory for archiving files
            file_path_boolean = sales_order_form.file_path_boolean.data
            if file_path_boolean:
                return redirect(url_for('choosefilepath.redirectchoosefilepath_', sales_order_id=sales_order_id, 
                                tag_prefix=tag_prefix, current_controller=current_controller, root_directory=app.config['ARCHIVE_ROOT_DIRECTORY']))
            else:
                return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id
                                    )) 
        else:
            flash('Sales Order data failed to save to database', 'danger')

    return render_template('salesorder.html', title='New Sales Order', sales_order_form=sales_order_form, selected_version=selected_version)
