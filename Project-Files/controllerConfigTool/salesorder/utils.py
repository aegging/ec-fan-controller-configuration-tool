from controllerConfigTool.models import Settings

def getSelectedVersion():
    settings_object = Settings.query.filter_by(currently_selected=True).first()
    select_version = settings_object.version_number
    return select_version


def getSettingsID():
    settings_object = Settings.query.filter_by(currently_selected=True).first()
    settings_id = settings_object.id
    return settings_id