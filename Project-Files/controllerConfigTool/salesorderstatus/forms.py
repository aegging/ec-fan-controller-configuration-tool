from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField, IntegerField, SelectField
from wtforms.validators import DataRequired, ValidationError
from controllerConfigTool.models import SalesOrders, Settings
from controllerConfigTool.getInfoFromDatabase import getArchiveDirectory
from controllerConfigTool.admin.utils import saveVersionSelection


class addFansForm(FlaskForm):
    add_fans = IntegerField()
    add_fans_button = SubmitField('Add Fans')


class addFieldbusSensorsForm(FlaskForm):
    add_sensors = IntegerField()
    add_fieldbus_sensors_button = SubmitField('Add Fieldbus Sensors')


class addAnalogSensorsForm(FlaskForm):
    add_sensors = IntegerField()
    add_analog_sensors_button = SubmitField('Add Analog Sensors')


class addExpansionBoardsForm(FlaskForm):
    add_expansion_boards = IntegerField('Add Expansion Boards')
    add_expansion_boards_button = SubmitField('Add Expansion Boards')


class addControllersForm(FlaskForm):
    add_controllers = IntegerField('Add Controllers')
    add_controllers_button = SubmitField('Add Controllers')

# CG cganges Added addaddresscontroller form  31/03/2023 start
class addaddresscontroller(FlaskForm):
    add_address_button = SubmitField('Assign Address') 
    # end


def build_sales_order_configurations_form(sales_order_id):
    archive_directory_default = getArchiveDirectory(sales_order_id)
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()

    software_version_default_id = sales_order_object.version_number_id
    controller_type_default = sales_order_object.controller_type

    show_program_controller_button_sales_order_config = True
    if software_version_default_id == None or controller_type_default == None:
        show_program_controller_button_sales_order_config = False
    
    # CG chnages 24/03/2023  start

    currently_selected_object = Settings.query.filter_by(currently_selected = True).first()
    if currently_selected_object:
        settings_id = currently_selected_object.id
    # if none are selected, default to 3.2.7 version
    else:
        settings_id = 5

    # find all enabled versions
    settings_query_objects = Settings.query.filter_by(enable_version = True).all()
    settings_enabled_version_list = []
    for settings_object in settings_query_objects:
        settings_enabled_version_list.append([settings_object.id, settings_object.version_number])

    # CG chnages 24/03/2023  end



    # # start

    
    # # settings_choices = [[None, None]] -- original
    # for settings_object in settings_objects:
    #     settings_choices.append([settings_object.id, settings_object.version_number])    
    # #  CG Changes removed duplicate default version data   
    # # settings_choices.remove([settings_current_version.id, str(settings_current_version)])

   
    # # end
        
    controller_type_choices = [None, 'MAX', 'MINI']

    class SalesOrderConfigurations(FlaskForm):

        software_version_dropdown = SelectField('Software Version', choices=settings_enabled_version_list, validators=[DataRequired()], default=settings_id)
        archive_directory = StringField('Archive Directory', default=archive_directory_default, render_kw={'readonly': True})
        change_archive_directory_button = SubmitField('Change Archive Directory')
        controller_type_dropdown = SelectField('Controller Type', default=controller_type_default, choices=controller_type_choices, validators=[DataRequired()])
        sales_order_configurations_save_button = SubmitField('Save Changes')


        def validate_software_version_dropdown(self, software_version_dropdown):
            if software_version_dropdown.data == 'None':
                raise ValidationError('Invalid Software Version Selection')

        def validate_controller_type_dropdown(self, controller_type_dropdown):
            if controller_type_dropdown.data == 'None':
                raise ValidationError('Invalid Controller Type Selection')

    return SalesOrderConfigurations(), show_program_controller_button_sales_order_config


def controller_quantity_form_builder(controller_quantity, tag_prefix):
    class ControllerButtonsSalesOrderStatusForm(FlaskForm):
        pass

    for i in range(1, controller_quantity+1):
        setattr(ControllerButtonsSalesOrderStatusForm, f'controller_name_{i}', StringField(default=f'{tag_prefix}{i}', render_kw={'readonly': True}))
        setattr(ControllerButtonsSalesOrderStatusForm, f'assignments_button_{i}', SubmitField(label="Assignments"))
        setattr(ControllerButtonsSalesOrderStatusForm, f'expansion_board_input_output_button_{i}', SubmitField(label="Expansion Boards"))
        setattr(ControllerButtonsSalesOrderStatusForm, f'analog_sensor_assignment_button_{i}', SubmitField(label="Analog Inputs/Outputs"))
        setattr(ControllerButtonsSalesOrderStatusForm, f'configurations_button_{i}', SubmitField(label="Configurations"))
        setattr(ControllerButtonsSalesOrderStatusForm, f'zone_configuration_button_{i}', SubmitField(label="Zone"))

    return ControllerButtonsSalesOrderStatusForm()
