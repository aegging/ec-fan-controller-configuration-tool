#Developed/Changed By : Chandan Vishwakarma & Jayant Sarvade (CG changes)
from flask import render_template, url_for, flash, redirect, request
from flask import Blueprint

from controllerConfigTool import db, app
from controllerConfigTool.models import ControllerProgramStatus, SalesOrders, Controller, ControllerInputOutputs,Settings, Zone, OutputZones
from controllerConfigTool.createFans import createExpansionBoards
from controllerConfigTool.createSensors import createAnalogSensors
from controllerConfigTool.salesorderstatus.forms import addFansForm, addFieldbusSensorsForm,\
    addAnalogSensorsForm, addControllersForm, addExpansionBoardsForm, build_sales_order_configurations_form,\
        controller_quantity_form_builder, addaddresscontroller
from controllerConfigTool.getInfoFromDatabase import findTagPrefixGivenSalesOrderID, getControllerID
from controllerConfigTool.salesorderstatus.utils import determineItemAssignmentGivenController, findItemsWithNoController,\
    findItemsWithNoDefinition, findNotNullControllerInputOutput, findZoneWithNoItems, findZoneWithNoController
from controllerConfigTool.admin.utils import saveVersionSelection
from controllerConfigTool.addressrangecreation.utils import createFans, createFieldbusSensors, findaddressedItems



salesorderstatus_blueprint = Blueprint('salesorderstatus', __name__)
@salesorderstatus_blueprint.route('/salesorderstatusview/<int:sales_order_id>', methods=['GET','POST'])
def salesorderstatusview(sales_order_id):
    
    # find sales order quantities
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    controller_quantity = sales_order_object.controller_quantity

    mixer_fan_quantity = sales_order_object.mixer_fan_quantity
    fieldbus_sensor_quantity = sales_order_object.fieldbus_sensor_quantity
    analog_sensor_quantity = sales_order_object.analog_sensor_quantity
    expansion_board_quantity = sales_order_object.expansion_board_quantity

    add_fans_form = addFansForm()
    add_fieldbus_sensors_form = addFieldbusSensorsForm()
    add_analog_sensors_form = addAnalogSensorsForm()
    add_expansion_boards_form = addExpansionBoardsForm()
    add_controllers_form = addControllersForm()
    add_address_range_form = addaddresscontroller() #CG changes calling addaddresscontroller form 31/03/2023
    sales_order_configurations_form, show_program_controller_button_sales_order_config = build_sales_order_configurations_form(sales_order_id)

    address_string_dictionary = findaddressedItems(sales_order_id)

    # find tag prefix
    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)

    sales_order_status_form = controller_quantity_form_builder(controller_quantity, tag_prefix)

    # create variable names for jinja template
    variable_names_list = []
    for i in range(1, controller_quantity + 1):
        # determine if fans, fbsensors, or analog sensors have been assigned for controller i
        # if they have, we will save the button type into a tuple with the associated variable name
        # i.e. if there is assignment, we want a green button so send 'success', else send 'default'

        # Get data to show fans selected for currently selected controller
        controller_id = getControllerID(i, sales_order_id)

        button_type_dictionary = determineItemAssignmentGivenController(controller_id, sales_order_id)

        # only add buttons if sales order quantity is greater than zero
        nested_buttons_var_list = []
        nested_buttons_var_list.append([f'controller_name_{i}', button_type_dictionary['controller_name']])
        nested_buttons_var_list.append([f'assignments_button_{i}', button_type_dictionary['assignments']])
        nested_buttons_var_list.append([f'analog_sensor_assignment_button_{i}', button_type_dictionary['analog_sensor_assignment']])
        if expansion_board_quantity > 0:
            nested_buttons_var_list.append([f'expansion_board_input_output_button_{i}', button_type_dictionary['expansion_board_input_output']])
        nested_buttons_var_list.append([f'zone_configuration_button_{i}', button_type_dictionary['zone']])
        nested_buttons_var_list.append([f'configurations_button_{i}', button_type_dictionary['configurations']])
       

        variable_names_list.append(nested_buttons_var_list)

    # create variable names for request method. Don't include the controller_name_{i} variable since this is a readonly string field
    # and we don't care about it being selected
    request_method_variable_names_list = []
    for i in range(1, controller_quantity + 1):
        request_method_variable_names_list.append([f'assignments_button_{i}', f'analog_sensor_assignment_button_{i}',
                                    f'expansion_board_input_output_button_{i}', f'configurations_button_{i}', f'zone_configuration_button_{i}'])


    unconfigured_items_dict = findItemsWithNoDefinition(sales_order_id)

    # find remaining items in sales order
    # find remaining by comparing count in sales order table w/ how how many have actually been created (look for this sales order id in appropriate table)
    unassigned_items_dict, show_program_controller_button = findItemsWithNoController(sales_order_id) # replace this with future unassigned items function

    # check to see if controller_input_outputs table is not None
    show_program_controller_button_controller_input_output = findNotNullControllerInputOutput(sales_order_id)

    zone_item_count = findZoneWithNoItems(controller_id, sales_order_id)

    zone_item_count = True if zone_item_count else ""
    test_zone = findZoneWithNoController(sales_order_id)

    # both must be true in order for program controllers button to appear (must have zero unassigned items and a none empty selection
    # for software version and controller type)
    show_program_controller_button = show_program_controller_button and show_program_controller_button_sales_order_config and show_program_controller_button_controller_input_output and test_zone
    # sales_order_data = namedtuple('SalesOrderData', ['controller_name', 'controller_configurations', ])

    # if "configure_fans" in request.form:
    #     return redirect(url_for('insert_fan_address_ranges', sales_order_id=sales_order_id))
    if request.method == 'POST':
        if "add_fans_button" in request.form:
            if add_fans_form.validate_on_submit():
                fans_count = add_fans_form.add_fans.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                previous_count =  int(sales_order_object.mixer_fan_quantity)
                if previous_count is not None:
                    sales_order_object.mixer_fan_quantity = previous_count + fans_count
                else:
                    sales_order_object.mixer_fan_quantity = fans_count

                db.session.add(sales_order_object)
                db.session.commit()
 
                return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id))

        elif "add_fieldbus_sensors_button" in request.form:
            if add_fieldbus_sensors_form.validate_on_submit():
                sensor_count = add_fieldbus_sensors_form.add_sensors.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                previous_count = sales_order_object.fieldbus_sensor_quantity
                if previous_count is not None:
                    sales_order_object.fieldbus_sensor_quantity = previous_count + sensor_count
                else:
                    sales_order_object.fieldbus_sensor_quantity = sensor_count

                db.session.add(sales_order_object)
                db.session.commit()
                return redirect(url_for('addressrangecreation.address_range_creation', sales_order_id=sales_order_id))

        elif "add_analog_sensors_button" in request.form:
            if add_analog_sensors_form.validate_on_submit():
                sensor_count = add_analog_sensors_form.add_sensors.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                previous_count = sales_order_object.analog_sensor_quantity
                if previous_count is not None:
                    sales_order_object.analog_sensor_quantity = previous_count + sensor_count
                else:
                    sales_order_object.analog_sensor_quantity = sensor_count

                db.session.add(sales_order_object)
                db.session.commit()

                # create row for each analog sensor in analog_sensors table
                createAnalogSensors(sales_order_id, sensor_count, previous_count)

                return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif "add_expansion_boards_button" in request.form:
            if add_expansion_boards_form.validate_on_submit():
                expansion_board_count = add_expansion_boards_form.add_expansion_boards.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                previous_count = int(sales_order_object.expansion_board_quantity)
                if previous_count is not None:
                    sales_order_object.expansion_board_quantity = previous_count + expansion_board_count
                else:
                    sales_order_object.expansion_board_quantity = expansion_board_count

                db.session.add(sales_order_object)
                db.session.commit()

                # create expansionboards for the given sales order id
                createExpansionBoards(sales_order_id, expansion_board_count)

                return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif "add_controllers_button" in request.form:
            if add_controllers_form.validate_on_submit():
                controller_count = add_controllers_form.add_controllers.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                previous_count = int(sales_order_object.controller_quantity)
                if previous_count is not None:
                    sales_order_object.controller_quantity = previous_count + controller_count
                else:
                    sales_order_object.controller_quantity = controller_count
                    previous_count = 0

                db.session.add(sales_order_object)
                db.session.commit()


                # Initialize rows in Controller table
                for item in range(previous_count + 1, previous_count + controller_count + 1):
                    controller = Controller(
                                            sales_order_id=sales_order_id, tag_name=tag_prefix + str(item)
                                            )
                    db.session.add(controller)
                    

                db.session.commit()

                controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                for controller_object in controller_objects:
                    controller_input_output = ControllerInputOutputs(
                                                sales_order_id = sales_order_id,
                                                controller_id = controller_object.id,
                                                version_number_id = sales_order_object.version_number_id
                                                )
                    db.session.add(controller_input_output)
                    
                    controller_program_status_object = ControllerProgramStatus(
                        controller_id = controller_object.id
                    )

                    zone_boolean = bool(Zone.query.filter_by(controller_id=controller_object.id).first())
                    if not zone_boolean:
                        zone_obj= Zone(sales_order_id=sales_order_id, controller_id=controller_object.id)
                        db.session.add(zone_obj)

                    OutputZones_boolean = bool(OutputZones.query.filter_by(controller_id=controller_object.id).first())
                    if not OutputZones_boolean:
                        ouput_zones = OutputZones(sales_order_id=sales_order_id, controller_id = controller_object.id)
                        db.session.add(ouput_zones)

                    db.session.add(controller_program_status_object)



                db.session.commit()

                return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif "change_archive_directory_button" in request.form:
            return redirect(url_for('choosefilepath.redirectchoosefilepath', sales_order_id=sales_order_id, 
                        root_directory=app.config['ARCHIVE_ROOT_DIRECTORY']))


            # set new version to True in database
            

        elif "sales_order_configurations_save_button" in request.form:
            if sales_order_configurations_form.validate_on_submit():
                software_version = sales_order_configurations_form.software_version_dropdown.data
                controller_type = sales_order_configurations_form.controller_type_dropdown.data
                sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
                sales_order_object.version_number_id = software_version
                sales_order_object.controller_type = controller_type

                db.session.add(sales_order_object)
                db.session.commit()
                # CG chnages for settings and controller db save 24/03/2023 start 
                saveVersionSelection(software_version)
                sales_order_controller_quantity = sales_order_object.controller_quantity
                sales_order_id = sales_order_object.id
                # Initialize rows in Controller table
                try:  
                    update_software_version_controller  = Controller.query.filter_by(sales_order_id=sales_order_id).update(dict(version_number_id=software_version))
                except:
                    print("not entered into function----------------------")
                # end

                db.session.commit()   
                flash('Sales Order Configurations Section Saved', 'success')
                return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))

        elif "configure_fieldbus_sensors_button" in request.form:
            return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))

        elif "configure_analog_sensors_button" in request.form:
            return redirect(url_for('itemconfigurations.analog_sensor_definition_assignment', sales_order_id=sales_order_id))

        elif "configure_fans_button" in request.form:
            return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))

        elif "program_controllers_button" in request.form:
            return redirect(url_for(
                                'programcontrollers.programcontroller', sales_order_id=sales_order_id, current_controller=1, tag_prefix=tag_prefix))
        # elif "configure_exhaust_supply_fans_button" in request.form:
        #     return redirect(url_for('exhaust_supply_fan_assignment', sales_order_id=sales_order_id))

        # CG changes redirect page to addressrangecreation 31/03/2023 start
        elif "add_address_button" in request.form:
            if add_address_range_form.validate_on_submit():
                return redirect(url_for('addressrangecreation.address_range_display', sales_order_id=sales_order_id))
        # end

        else:
            print('-------sales_order_status_form_validation --------------')
            if sales_order_status_form.validate_on_submit():
                for variable_names in request_method_variable_names_list:
                    for variable_name in variable_names:
                        if variable_name in request.form:
                            print(f'-------BUTTON WAS SELECTED: {variable_name}---------')

                            # split the selected button into two fields, variable name and index
                            split_variablename_index = variable_name.rsplit('_', 1)
                            selected_button_name = split_variablename_index[0]
                            selected_controller_index = split_variablename_index[1]
                            print("========split_variablename_index========",split_variablename_index)

                            print(f'selected variable name: {selected_button_name} || selected index: {selected_controller_index}')


                            # Now that we have this information. We just need to redirect to the appropriate route for each possible button selection
                            # Send the selected_controller_index value as a route parameter, in order to handle any number of controllers
                            if selected_button_name == 'assignments_button':
                                return redirect(url_for('controllerassignments.controller_assignments', sales_order_id=sales_order_id, current_controller=selected_controller_index))

                            if selected_button_name == 'configurations_button':
                                return redirect(url_for('controllerconfigurations.controller_configurations', sales_order_id=sales_order_id, current_controller=selected_controller_index))

                            if selected_button_name == 'expansion_board_input_output_button':
                                return redirect(url_for('expansionboardconfigurations.expansion_board_assignment', sales_order_id=sales_order_id, current_controller=selected_controller_index,
                                expansion_board_number=1))

                            if selected_button_name == 'analog_sensor_assignment_button':
                                return redirect(url_for('analogsensorassignment.analog_sensor_assignment', sales_order_id=sales_order_id, current_controller=selected_controller_index))

                            if selected_button_name == 'zone_configuration_button':
                                return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=selected_controller_index))

    return render_template('salesorderstatus.html', sales_order_status_form=sales_order_status_form, 
                        unassigned_items_dict=unassigned_items_dict, add_fans_form=add_fans_form,
                        unconfigured_items_dict=unconfigured_items_dict, variable_names_list=variable_names_list,
                        add_fieldbus_sensors_form=add_fieldbus_sensors_form, add_analog_sensors_form=add_analog_sensors_form,
                        add_expansion_boards_form=add_expansion_boards_form, add_controllers_form=add_controllers_form,
                        mixer_fan_quantity=mixer_fan_quantity, fieldbus_sensor_quantity=fieldbus_sensor_quantity, 
                        analog_sensor_quantity=analog_sensor_quantity, expansion_board_quantity=expansion_board_quantity,
                        sales_order_configurations_form=sales_order_configurations_form, show_program_controller_button=show_program_controller_button,
                        add_address_range_form=add_address_range_form,address_string_dictionary=address_string_dictionary)

