# CG changes findZoneWithNoItems,findZoneWithNoController july 2023
# Developed/Changed By : Chandan Vishwakarma & Jayant Sarvade ( CG changes )

from controllerConfigTool.models import Controller, ExpansionBoardInputOutput,\
     SalesOrders, Job, FieldbusSensors, ControllerInputOutputs, AnalogSensors, OutputZones
from controllerConfigTool import db
from sqlalchemy import text

"""
create a function for finding unassigned items (items that have not been assigned to a controller)
"""
def findItemsWithNoController(sales_order_id):
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()

    # initial_controller_count = sales_order_object.controller_quantity
    initial_mixer_fans_count = sales_order_object.mixer_fan_quantity
    initial_fieldbus_sensors_count = sales_order_object.fieldbus_sensor_quantity
    initial_analog_sensors_count = sales_order_object.analog_sensor_quantity
    initial_expansion_board_count = sales_order_object.expansion_board_quantity
    # initial_exhaust_supply_fan_count = sales_order_object.exhaust_supply_fan_quantity

    # if initial_controller_count is None:
    #     initial_controller_count = 0

    if initial_mixer_fans_count is None:
        initial_mixer_fans_count = 0

    if initial_fieldbus_sensors_count is None:
        initial_fieldbus_sensors_count = 0

    if initial_analog_sensors_count is None:
        initial_analog_sensors_count = 0

    if initial_expansion_board_count is None:
        initial_expansion_board_count = 0

    # if initial_exhaust_supply_fan_count is None:
    #     initial_exhaust_supply_fan_count = 0
    


    #find how many have modbus address and sales order id
    # controller_count = db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).filter(Controller.input_on!=None).count() 

    mixer_fans_count = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id!=None).count()

    fieldbus_sensors_count = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
        filter(FieldbusSensors.controller_id!=None).count()

    stmt = text("Select * From analog_sensors Where sales_order_id=:sales_order_id and ((controller_id IS NOT NULL) or (expansion_board_id IS NOT NULL))")
    analog_sensors_count = len(db.session.query(AnalogSensors).from_statement(stmt).params(sales_order_id=sales_order_id).all())

    expansion_board_count = ExpansionBoardInputOutput.query.filter_by(sales_order_id=sales_order_id).count()

    # exhaust_supply_fan_count = db.session.query(SupplyExhaustFans).filter(SupplyExhaustFans.sales_order_id==sales_order_id).\
    #     filter(SupplyExhaustFans.modbus_address!=None).count()


    # controller_count_difference = initial_controller_count - controller_count
    mixer_fan_count_difference = initial_mixer_fans_count - mixer_fans_count
    fieldbus_sensors_count_difference = initial_fieldbus_sensors_count - fieldbus_sensors_count
    analog_sensors_count_difference =  db.session.query(AnalogSensors).filter(AnalogSensors.controller_id==None).\
                                        filter(AnalogSensors.expansion_board_id==None).\
                                            filter(AnalogSensors.fan_id==None).\
                                                filter(AnalogSensors.sales_order_id==sales_order_id).\
                                                    filter(AnalogSensors.analog_sensor_definitions_id!=None).\
                                                        count()
    expansion_board_count_difference = initial_expansion_board_count - expansion_board_count

    count_results = {}
    # count_results['Controllers'] = controller_count_difference
    count_results['Fans'] = mixer_fan_count_difference
    count_results['Fieldbus Sensors'] = fieldbus_sensors_count_difference
    count_results['Analog Sensors'] = analog_sensors_count_difference
    count_results['Expansion Boards'] = expansion_board_count_difference
    # count_results['Supply Exhaust Fans'] = exhaust_supply_fan_count_difference

    show_program_controller_button = False
    if mixer_fan_count_difference == 0 and fieldbus_sensors_count_difference == 0 and \
        analog_sensors_count_difference == 0 and expansion_board_count_difference == 0:
        show_program_controller_button = True

    return count_results, show_program_controller_button



"""
return True if all controllers for a given sales order have data in the controller_input_output table
else return False
"""
def findNotNullControllerInputOutput(sales_order_id):
    controller_input_output_objects = ControllerInputOutputs.query.filter_by(sales_order_id=sales_order_id).all()

    for controller_input_output_object in controller_input_output_objects:
        if not controller_input_output_object.analog_outputs_saved:
            return False
    
    return True



"""
find items with no definition assigned
"""
def findItemsWithNoDefinition(sales_order_id):
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()

    # initial_controller_count = sales_order_object.controller_quantity
    initial_mixer_fans_count = sales_order_object.mixer_fan_quantity
    initial_fieldbus_sensors_count = sales_order_object.fieldbus_sensor_quantity
    initial_analog_sensors_count = sales_order_object.analog_sensor_quantity
    # initial_expansion_board_count = sales_order_object.expansion_board_count
    # initial_exhaust_supply_fan_count = sales_order_object.exhaust_supply_fan_quantity

    # if initial_controller_count is None:
    #     initial_controller_count = 0

    if initial_mixer_fans_count is None:
        initial_mixer_fans_count = 0

    if initial_fieldbus_sensors_count is None:
        initial_fieldbus_sensors_count = 0

    if initial_analog_sensors_count is None:
        initial_analog_sensors_count = 0

    # if initial_expansion_board_count is None:
    #     initial_expansion_board_count = 0

    # if initial_exhaust_supply_fan_count is None:
    #     initial_exhaust_supply_fan_count = 0
    


    #find how many have modbus address and sales order id
    # controller_count = db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).filter(Controller.input_on!=None).count() 

    mixer_fans_count = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.fan_definitions_id!=None).count()

    fieldbus_sensors_count = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
        filter(FieldbusSensors.fieldbus_sensor_definitions_id!=None).count()

    analog_sensors_count = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
        filter(AnalogSensors.analog_sensor_definitions_id!=None).count()

    # expansion_board_count = db.session.query(ExpansionBoards).filter(ExpansionBoards.sales_order_id==sales_order_id).filter(ExpansionBoards.controller_id!=None).count()

    # exhaust_supply_fan_count = db.session.query(SupplyExhaustFans).filter(SupplyExhaustFans.sales_order_id==sales_order_id).\
    #     filter(SupplyExhaustFans.modbus_address!=None).count()


    # controller_count_difference = initial_controller_count - controller_count
    mixer_fan_count_difference = initial_mixer_fans_count - mixer_fans_count
    fieldbus_sensors_count_difference = initial_fieldbus_sensors_count - fieldbus_sensors_count
    analog_sensors_count_difference = initial_analog_sensors_count - analog_sensors_count
    # expansion_board_count_difference = initial_expansion_board_count - expansion_board_count
    # exhaust_supply_fan_count_difference = initial_exhaust_supply_fan_count - exhaust_supply_fan_count


    count_results = {}
    # count_results['Controllers'] = controller_count_difference
    count_results['Fans'] = mixer_fan_count_difference
    count_results['Fieldbus Sensors'] = fieldbus_sensors_count_difference
    count_results['Analog Sensors'] = analog_sensors_count_difference
    # count_results['Expansion Boards'] = expansion_board_count_difference
    # count_results['Supply Exhaust Fans'] = exhaust_supply_fan_count_difference

    return count_results


"""
# determine if fans, fbsensors, or analog sensors have been assigned for controller i
# if they have, we will save the button type into a tuple with the associated variable name
# i.e. if there is assignment, we want a green button so send 'success', else send 'default'
"""
def determineItemAssignmentGivenController(controller_id, sales_order_id):
    # find sales order quantities
    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    expansion_board_quantity = sales_order_object.expansion_board_quantity

    fans_count = Job.query.filter_by(controller_id=controller_id).count()
    fieldbus_sensors_count = FieldbusSensors.query.filter_by(controller_id=controller_id).count()
    expansionboard_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    
    controller_object = db.session.query(Controller).filter(Controller.id==controller_id).first()
    controller_configurations_boolean = controller_object.controller_configurations_data_boolean

    

    stmt = text("Select * From expansion_board_input_output Where controller_id=:controller_id and ((u8_analog_fan_type IS NOT NULL) or (u9_analog_fan_type IS NOT NULL)\
                or (u10_analog_fan_type IS NOT NULL) or (u1_analog_sensor_id IS NOT NULL) or (u2_analog_sensor_id IS NOT NULL) or (u3_analog_sensor_id IS NOT NULL)\
                    or (u4_analog_sensor_id IS NOT NULL) )")

    expansion_board_input_output_count = bool(db.session.query(ExpansionBoardInputOutput).from_statement(stmt).params(controller_id=controller_id).first())


    expansion_board_input_output_saved = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.controller_id==controller_id).filter(ExpansionBoardInputOutput.expansion_outputs_saved!=None).count()

    zone_count = findZoneWithNoItems(controller_id,sales_order_id)

    button_type_dictionary = {}


    # analog input/outputs
    found_analog_sensor_controller_id_boolean = bool(AnalogSensors.query.\
        filter_by(controller_id=controller_id).first())

    controller_input_output_boolean = False
    controller_inputoutput_object = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
    if controller_inputoutput_object:
        controller_input_output_boolean = controller_inputoutput_object.analog_outputs_saved

    if found_analog_sensor_controller_id_boolean or controller_input_output_boolean:
        button_type_dictionary['analog_sensor_assignment'] = 'success'
    else:
        button_type_dictionary['analog_sensor_assignment'] = 'default'


    if fieldbus_sensors_count > 0 or fans_count > 0 or expansionboard_count > 0:
        button_type_dictionary['assignments'] = 'success'

    else:
        button_type_dictionary['assignments'] = 'default'

    if controller_configurations_boolean:
        button_type_dictionary['configurations'] = 'success'
    else:
        button_type_dictionary['configurations'] = 'default'


    if expansion_board_quantity > 0:
        if expansion_board_input_output_count and expansion_board_input_output_saved==expansionboard_count:
            button_type_dictionary['expansion_board_input_output'] = 'success'
        else:
            button_type_dictionary['expansion_board_input_output'] = 'default'
    #CG changes
    if zone_count:
        button_type_dictionary['zone'] = 'success'
    else:
        button_type_dictionary['zone'] = 'default'



    # handle defaults
    button_type_dictionary['controller_name'] = 'controller-fields'
    
    return button_type_dictionary


def findZoneWithNoItems(controller_id, sales_order_id):
    result = False
    expansionboard_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    fan_zone_count = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).filter(Job.Zone==None).count()
    analog_input_count = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).filter(AnalogSensors.zone==None).count()
    fieldbus_sensors_zone_count = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==controller_id).filter(FieldbusSensors.zone==None).count()

    
    output_input_count_controller = []
    output_input_all = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).all()
    for output in output_input_all:
        output_input_count = ''
        if expansionboard_count >=1:
            if bool(output.ExpansionBoaard_inout_id!=None):
                y1_zone = bool(output.y1_zone==None)
                y2_zone = bool(output.y2_zone==None)
                y3_zone = bool(output.y3_zone==None)
                y4_zone = bool(output.y4_zone==None)
                u8_zone = bool(output.u8_zone==None)
                u9_zone = bool(output.u9_zone==None)
                u10_zone = bool(output.u10_zone==None)

                
                if y1_zone and y2_zone and y3_zone and y4_zone and u8_zone and u9_zone and u10_zone:
                    output_input_count = False
                if not y1_zone and not y2_zone and not y3_zone and not y4_zone and not u8_zone and not u9_zone and not u10_zone:
                    output_input_count = True
                if not y1_zone and not y2_zone and not y3_zone and not y4_zone and u8_zone and u9_zone and u10_zone:
                    output_input_count = False
        if expansionboard_count == 0:
            y1_zone = bool(output.y1_zone==None)
            y2_zone = bool(output.y2_zone==None)
            y3_zone = bool(output.y3_zone==None)
            y4_zone = bool(output.y4_zone==None)
            if y1_zone and y2_zone and y3_zone and y4_zone:
                output_input_count = False
            if not y1_zone and not y2_zone and not y3_zone and not y4_zone:
                output_input_count = True
        output_input_count_controller.append(output_input_count)
        

    analog_input_expans_count = True
    expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
    for id in expansion_ids:
        all_asensor_objects_expans = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.expansion_board_id==id.id).\
                filter(AnalogSensors.zone==None).order_by(AnalogSensors.id).all()
        if all_asensor_objects_expans:
            analog_input_expans_count = False
        elif analog_input_expans_count and analog_input_expans_count==[]:
            analog_input_expans_count = True
    
    final_expansion = True if False not in output_input_count_controller else False
    if  '' in output_input_count_controller:
        final_expansion = False
     

    if fieldbus_sensors_zone_count == 0 and fan_zone_count == 0 and analog_input_count == 0 and analog_input_expans_count and final_expansion:
        result = True

    return result
    

def findZoneWithNoController(sales_order_id):
    result = False
    mixer_fans_count = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.Zone==None).count()

    fieldbus_sensors_count = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
        filter(FieldbusSensors.zone==None).count()
    
    analog_sensors_count_difference =  db.session.query(AnalogSensors).\
                                        filter(AnalogSensors.expansion_board_id==None).\
                                            filter(AnalogSensors.fan_id==None).\
                                                filter(AnalogSensors.sales_order_id==sales_order_id).\
                                                    filter(AnalogSensors.analog_sensor_definitions_id!=None).\
                                                        filter(AnalogSensors.zone==None).count()
    expansionboard_count = ExpansionBoardInputOutput.query.filter_by(sales_order_id=sales_order_id).count()

    
    
    output_input_count_all = []
    output_input_all = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).all()
    for output in output_input_all:
        output_input_count = ''
        if bool(output.ExpansionBoaard_inout_id!=None):
            y1_zone = bool(output.y1_zone==None)
            y2_zone = bool(output.y2_zone==None)
            y3_zone = bool(output.y3_zone==None)
            y4_zone = bool(output.y4_zone==None)
            u8_zone = bool(output.u8_zone==None)
            u9_zone = bool(output.u9_zone==None)
            u10_zone = bool(output.u10_zone==None)

            if y1_zone and y2_zone and y3_zone and y4_zone and u8_zone and u9_zone and u10_zone:
                output_input_count = False
            
            if not y1_zone and not y2_zone and not y3_zone and not y4_zone and not u8_zone and not u9_zone and not u10_zone:
                output_input_count = True


        if bool(output.ExpansionBoaard_inout_id==None):
            y1_zone = bool(output.y1_zone==None)
            y2_zone = bool(output.y2_zone==None)
            y3_zone = bool(output.y3_zone==None)
            y4_zone = bool(output.y4_zone==None)
            if y1_zone or y2_zone or y3_zone or y4_zone:
                output_input_count = False
            if not y1_zone and not y2_zone and not y3_zone and not y4_zone:
                output_input_count = True
        output_input_count_all.append(output_input_count)
        
        

    final_output = True if False not in output_input_count_all else False
    if '' in output_input_count_all:
        final_output = False
       
    if mixer_fans_count == 0 and fieldbus_sensors_count == 0 and analog_sensors_count_difference == 0 and output_input_count and final_output:
        result = True

    return result
