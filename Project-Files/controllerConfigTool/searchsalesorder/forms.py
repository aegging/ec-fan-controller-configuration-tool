from flask_wtf import FlaskForm
from wtforms import StringField,  SubmitField
from wtforms.validators import DataRequired



class SearchSalesOrderForm(FlaskForm):
    sales_order_number = StringField('Sales Order Number', validators=[DataRequired()])
    program_controllers = SubmitField('Program Controllers')
    edit_sales_order = SubmitField('Edit Sales Order')
    find_fans = SubmitField('Find All Fans')
    find_controllers = SubmitField('Find All Controllers in Systems Table')
