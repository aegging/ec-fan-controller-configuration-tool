from flask import render_template, url_for, flash, redirect, request
from flask import Blueprint

from controllerConfigTool import db
from controllerConfigTool.models import Job, SalesOrders, Controller, Systems
from controllerConfigTool.searchsalesorder.forms import SearchSalesOrderForm
from controllerConfigTool.searchsalesorder.utils import findFirstNonProgrammedController, getControllerDataGivenSalesOrder, getFanDataGivenSalesOrder, getFanDataGivenSalesOrderNumber, getTotalUnprogrammedControllers, updateViewState
from controllerConfigTool.getInfoFromDatabase import findFirstNonConfiguredController, findTagPrefixGivenSalesOrderID, getDefaultFanType, getTagPrefix

searchsalesorder_blueprint = Blueprint('searchsalesorder', __name__)

@searchsalesorder_blueprint.route("/searchsalesordernumber/<view_state>/", methods=['GET', 'POST'])
def searchsalesordernumber(view_state):
    search_sales_order_form = SearchSalesOrderForm()
    title = updateViewState(view_state)

    if request.method == 'POST':
        if search_sales_order_form.validate_on_submit():
            # does that sales order exist?
            sales_order_number = str(search_sales_order_form.sales_order_number.data)
            found_sales_order_boolean = bool(SalesOrders.query.filter_by(sales_order_number = sales_order_number).first())
            print(f'sales_order_number: {sales_order_number}')
            if(found_sales_order_boolean):

                current_sales_order_object = SalesOrders.query.filter_by(sales_order_number = sales_order_number).first()
                sales_order_id = current_sales_order_object.id
                print(f'sales_order_id: {sales_order_id}')
                # are there any unprogrammed controllers?

                first_remaining_controller = Controller.query.filter_by(sales_order_id=sales_order_id).first()

                tagname=first_remaining_controller.tag_name
                tag_prefix = getTagPrefix(tagname)

                if "edit_sales_order" in request.form:
                    return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
                

                elif "program_controllers" in request.form:
                    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)
                    return redirect(url_for(
                                    'programcontrollers.programcontroller', sales_order_id=sales_order_id, current_controller=1, tag_prefix=tag_prefix))

                
                elif "find_fans" in request.form:
                    # Send Sales Order ID to redirect in order to display all fans associated with job
                    fan_count = Job.query.filter_by(sales_order_id=sales_order_id).count()

                    # if fans are found, display them
                    if fan_count > 0:
                        flash(f'Fans for {sales_order_number} Found', 'success')
                        return redirect(url_for('searchsalesorder.displayfans',sales_order_id=sales_order_id))

                    # No Fans found
                    else:
                        flash(f'No Fans Have Been Found For Sales Order {sales_order_number}', 'danger')
                        return redirect(url_for('searchsalesorder.searchsalesordernumber', view_state='find_fans'))
                elif "find_controllers" in request.form:
                    # Find all controller data in the systems table
                    wildcard_sales_order_number = f"%{sales_order_number}%"
                    print(f'=----- wildcard: {wildcard_sales_order_number}')
                    controller_count = Systems.query.filter(Systems.job_number.like(wildcard_sales_order_number)).count()
                    controller_object = Systems.query.filter(Systems.job_number.like(wildcard_sales_order_number)).all()
                    print(controller_object)

                    print(f'--------controller count: {controller_count}')

                    if controller_count > 0:
                        flash(f'Controllers for {sales_order_number} Found', 'success')
                        return redirect(url_for('searchsalesorder.displaycontrollers', sales_order_id=sales_order_id))
                    else:
                        flash(f'No Controllers Have Been Found For Sales Order {sales_order_number}', 'danger')
                        return redirect(url_for('searchsalesorder.searchsalesordernumber', view_state='find_fans'))
                    

            else:
                # could not find a sales order in salesorder table, try to search database for old entries that do not have sales order
                wildcard_sales_order_number = "%{}%".format(sales_order_number)

                if "find_controllers" in request.form:
                    # Find all controller data in the systems table    
                    controller_count = Systems.query.filter(Systems.job_number.like(wildcard_sales_order_number)).count()

                    if controller_count > 0:
                        flash(f'Controllers for {sales_order_number} Found', 'success')
                        return redirect(url_for('searchsalesorder.displaycontrollers', sales_order_number=sales_order_number))
                    else:
                        flash(f'No Controllers Have Been Found For Sales Order {sales_order_number}', 'danger')
                        return redirect(url_for('searchsalesorder.searchsalesordernumber', view_state='find_fans'))
                elif "find_fans" in request.form:

                    fan_count = Job.query.filter(Job.Job_number.like(wildcard_sales_order_number)).count()

                    # if fans are found, display them
                    if fan_count > 0:
                        flash(f'Fans for {sales_order_number} Found', 'success')
                        return redirect(url_for('searchsalesorder.displaywildcardfans',sales_order_number=sales_order_number))

                    # No Fans found
                    else:
                        flash(f'No Fans Have Been Found For Sales Order {sales_order_number}', 'danger')
                        return redirect(url_for('searchsalesorder.searchsalesordernumber', view_state='find_fans'))


    return render_template('searchsalesorder.html', title=title, search_sales_order_form=search_sales_order_form, view_state=view_state)



@searchsalesorder_blueprint.route('/displayfans/<int:sales_order_id>', methods=['GET','POST'])
def displayfans(sales_order_id):

    sales_order_object = SalesOrders.query.filter_by(id=sales_order_id).first()
    sales_order_number = sales_order_object.sales_order_number

    # return the following: job number, address, no of sensors, command value, max speed, fanType
    fan_data = getFanDataGivenSalesOrder(sales_order_id)

    return render_template('displayfans.html', title='Display Fans', fan_data=fan_data, sales_order_number=sales_order_number)


@searchsalesorder_blueprint.route('/displaywildcardfans/<sales_order_number>', methods=['GET','POST'])
def displaywildcardfans(sales_order_number):

    # return the following: job number, address, no of sensors, command value, max speed, fanType
    fan_data = getFanDataGivenSalesOrderNumber(sales_order_number)

    return render_template('displayfans.html', title='Display Fans', fan_data=fan_data, sales_order_number=sales_order_number)
            

@searchsalesorder_blueprint.route('/displaycontrollers/<sales_order_number>', methods=['GET','POST'])
def displaycontrollers(sales_order_number):

    # return the following: job number, address, no of sensors, command value, max speed, fanType
    controller_data = getControllerDataGivenSalesOrder(sales_order_number)

    return render_template('displaycontrollers.html', title='Display Controllers', controller_data=controller_data, sales_order_number=sales_order_number)
            
