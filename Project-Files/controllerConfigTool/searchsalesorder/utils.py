from controllerConfigTool import db
from controllerConfigTool.models import Controller, Job, Systems



def updateViewState(view_state):
    if view_state == "edit_sales_order":
        title = 'Edit Controllers'
    elif view_state == 'program_controller':
        title = 'Program Controllers'
    elif view_state == "find_fans":
        title = 'Search Database'
    else:
        title = 'no button was pressed, this should never happen'
        view_state = 'no view defined'

    return title


def getTotalUnprogrammedControllers(sales_order_id):
    """ Find total unprogrammed controllers for a given Sales Order ID """
    unconfigured_controllers_count = db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).filter(Controller.programmed==False).count()
    return unconfigured_controllers_count


def findFirstNonProgrammedController(sales_order_id, tag_prefix):
    # find the next controller to configure
    remaining_count = db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).filter(Controller.programmed==False).count()
    if remaining_count > 0:
        first_remaining_controller = db.session.query(Controller).filter(Controller.sales_order_id==sales_order_id).filter(Controller.programmed==False).first()
        next_controller_to_do = first_remaining_controller.tag_name
        next_controller_to_do = next_controller_to_do.strip(tag_prefix)
        return next_controller_to_do
    else:
        # if there are no controllers remaining to configure, just go to the first one
        next_controller_to_do = 1
        return next_controller_to_do


def getFanDataGivenSalesOrder(sales_order_id):
    fan_data = []
    all_fans_object = Job.query.filter_by(sales_order_id=sales_order_id).order_by(Job.Index).all()
    # return the following: job number, address, no of sensors, command value, max speed, fanType
    row_index = 0
    for current_fan in all_fans_object:
        job_number = current_fan.Job_number
        address = current_fan.Address
        number_of_sensors = current_fan.No_Sensors
        command_value = current_fan.Command_Value
        max_speed = current_fan.Maximum_Speed
        fan_type = (current_fan.FanType)
        row_index = row_index + 1

        fan_data.append([row_index, job_number, address, number_of_sensors, command_value, max_speed, fan_type])

    return fan_data


def getFanDataGivenSalesOrderNumber(sales_order_number):
    fan_data = []
    wildcard_sales_order_number = "%{}%".format(sales_order_number)
    all_fans_object = Job.query.filter(Job.Job_number.like(wildcard_sales_order_number)).order_by(Job.Index).all()
    # return the following: job number, address, no of sensors, command value, max speed, fanType
    row_index = 0
    for current_fan in all_fans_object:
        job_number = current_fan.Job_number
        address = current_fan.Address
        number_of_sensors = current_fan.No_Sensors
        command_value = current_fan.Command_Value
        max_speed = current_fan.Maximum_Speed
        fan_type = (current_fan.FanType)
        row_index = row_index + 1

        fan_data.append([row_index, job_number, address, number_of_sensors, command_value, max_speed, fan_type])

    return fan_data


def getControllerDataGivenSalesOrder(sales_order_number):
    controller_data = []

    wildcard_sales_order_number = "%{}%".format(sales_order_number)
    all_controllers_object = Systems.query.filter(Systems.job_number.like(wildcard_sales_order_number)).order_by(Systems.entered_on).all()
    row_index = 0

    for current_controller in all_controllers_object:
        row_index = row_index + 1
        job_number = current_controller.job_number
        entered_on = current_controller.entered_on
        fan_1 = current_controller.fan_1
        programmed = current_controller.programmed


        controller_data.append([row_index, job_number, entered_on, fan_1, programmed])
    
    return controller_data
    

    