from controllerConfigTool.getInfoFromDatabase import getListProgrammedControllers, getListProgrammingControllers, getListErrorControllers

def updateControllerButtons(sales_order_id):
    """
    programmed_controllers: list of FINISHED/PROGRAMMED controllers; GREEN CONTROLLER BUTTONS
    programming_controllers: list of IN PROCESS/PROGRAMMING controllers; YELLOW CONTROLLER BUTTONS
    error_controllers: list of ERROR controllers; RED CONTROLLER BUTTONS; These controllers were given command to be programmed but ended up failing to finish
    """
    programmed_controllers=getListProgrammedControllers(sales_order_id)    
    programming_controllers=getListProgrammingControllers(sales_order_id)
    error_controllers=getListErrorControllers(sales_order_id)

    return programmed_controllers, programming_controllers, error_controllers


