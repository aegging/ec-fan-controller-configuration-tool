
from flask import render_template, url_for, redirect
from flask import Blueprint

from controllerConfigTool.models import CarelKeys, CarelValues
from controllerConfigTool.viewdefinitions.forms import backButton
from controllerConfigTool.viewdefinitions.utils import getFanDefinitionsValues
from collections import defaultdict

viewdefinitions_blueprint = Blueprint('viewdefinitions', __name__)



@viewdefinitions_blueprint.route("/viewfandefinition/<sales_order_id>/", methods=['GET','POST'])
def viewfandefinition(sales_order_id):
    result = getFanDefinitionsValues(sales_order_id)    


    form = backButton()

    if form.validate_on_submit():
        return redirect(url_for('itemconfigurations.fan_definition_assignment', sales_order_id=sales_order_id))


    return render_template('viewfandefinition.html', title='View Fan Definition', result=result, form=form)


@viewdefinitions_blueprint.route("/viewfbsensordefinition/<sales_order_id>/", methods=['GET','POST'])
def viewfbsensordefinition(sales_order_id):
    result = getFanDefinitionsValues(sales_order_id)    

    form = backButton()

    if form.validate_on_submit():
        return redirect(url_for('itemconfigurations.fieldbus_sensor_definition_assignment', sales_order_id=sales_order_id))


    return render_template('viewfbsensordefinition.html', title='View Fan Definition', result=result, form=form)


@viewdefinitions_blueprint.route('/carel_key_value_pairs', methods=['GET','POST'])
def carel_key_value_pairs():

    # find all carel keys 
    carel_key_objects = CarelKeys.query.all()

    carel_key_list = []
    carel_key_values_default_dict = defaultdict(list)

    # loop through each carel key
    for carel_key_object in carel_key_objects:
        carel_key = carel_key_object.carel_fieldname
        carel_key_list.append(carel_key)

        # loop through each carel value with this key
        carel_value_objects = CarelValues.query.filter_by(carel_key_id=carel_key_object.id).all()
        for carel_value_object in carel_value_objects:
            # for each value, append to defaultdict list variable
            carel_value_name = carel_value_object.fieldname_value
            carel_value = carel_value_object.carel_value
            carel_key_values_default_dict[carel_key].append([carel_value_name, carel_value])


    return render_template('carel_key_value_pairs.html', carel_key_values_default_dict=carel_key_values_default_dict, carel_key_list=carel_key_list)