from controllerConfigTool import db
from controllerConfigTool.models import FanDefinitions
from sqlalchemy import text


def getFanDefinitionsValues(sales_order_id):
    fan_definition_values = []

    stmt = text("Select * From fan_definitions Where (standard=True) or sales_order_id=:sales_order_id")
    fan_values_objects_boolean = bool(db.session.query(FanDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all())
    if fan_values_objects_boolean:
        fan_values_objects = db.session.query(FanDefinitions).from_statement(stmt).params(sales_order_id=sales_order_id).all()

        for fan_values in fan_values_objects:
            
            name = fan_values.name
            fan_type = fan_values.fan_type
            comms= fan_values.comms
            sensors_count = fan_values.sensors_count
            sensor_1_type = fan_values.sensor_1_type
            sensor_1_unit = fan_values.sensor_1_unit
            sensor_1_min = fan_values.sensor_1_min
            sensor_1_max = fan_values.sensor_1_max
            sensors_count = fan_values.sensors_count
            sensor_2_type = fan_values.sensor_2_type
            sensor_2_unit = fan_values.sensor_2_unit
            sensor_2_min = fan_values.sensor_2_min
            sensor_2_max = fan_values.sensor_2_max
            command_value = fan_values.command_value
            control = fan_values.control
            minimum_speed = fan_values.minimum_speed
            maximum_speed = fan_values.maximum_speed
            p_val = fan_values.p_val
            i_val = fan_values.i_val
            control_min = fan_values.control_min
            control_max = fan_values.control_max
            smoke_detector = fan_values.smoke_detector
            emergency = fan_values.emergency
            emer_set_speed = fan_values.emer_set_speed
            emer_external_signal = fan_values.emer_external_signal
            emer_start_stop_signal = fan_values.emer_start_stop_signal
            timeout = fan_values.timeout
            timeout_speed = fan_values.timeout_speed
            zone = fan_values.zone
            fault_relay = fan_values.fault_relay

            fan_definition_values.append([name, fan_type, comms, sensors_count, sensor_1_type, sensor_1_unit, sensor_1_min, sensor_1_max,
                                    sensor_2_type, sensor_2_unit, sensor_2_min, sensor_2_max, command_value, control, minimum_speed, maximum_speed,
                                    p_val, i_val, control_min, control_max, smoke_detector, emergency, emer_set_speed, emer_external_signal,
                                    emer_start_stop_signal, timeout, timeout_speed, zone, fault_relay
                                    ])

    return fan_definition_values

