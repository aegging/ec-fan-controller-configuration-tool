#Developed/Changed By : Chandan Vishwakarma & Jayant Sarvade (CG changes)

from flask_wtf import FlaskForm
from wtforms import SubmitField, IntegerField, StringField,SelectField
from wtforms.validators import ValidationError, Optional ,DataRequired
from wtforms.widgets import HiddenInput
from controllerConfigTool import db
# from controllerConfigTool.models import SalesOrders, zone
from controllerConfigTool.zoneconfigure.utils import totalzone
from controllerConfigTool.dataCalculations import getCountOfModbusRange


def get_selected_zone_choices(zone_count):
    selected_choice = []
    for item in range(1, zone_count+1):
        selected_choice.append(item)
    return selected_choice



def updated_zone(sales_order_id, controller_id):

    count = totalzone(sales_order_id,controller_id)
    u_count = [[1,"1"],[2,"2"],[3,"3"],[4,"4"],[5,"5"],[6,"6"]]


    class ZoneConfigurationFrom(FlaskForm):
        Reset= SubmitField('Reset')
        Reset_all_controllers= SubmitField('Reset All Controllers')
        update= SubmitField('Update')
        zones = SelectField('Create Zones', choices=u_count, validators=[DataRequired()], default=count)
        # assign_zone= SubmitField('Assign Zone')

    return ZoneConfigurationFrom()

def selected_zone(sales_order_id, controller_id):

    count = totalzone(sales_order_id,controller_id)
    outputs=[['Y1',"Y1"],['Y2',"Y2"],['Y3',"Y3"],['Y4',"Y4"],['Y5',"Y5"],['Y6',"Y6"]]
        
    class ZoneSelectedConfigurationForm(FlaskForm):
        assign_zone= SubmitField('Assign Zone')
        selected_zone = SelectField('Select Zones', choices=get_selected_zone_choices(count), validators=[DataRequired()])
        fan_address_ranges = StringField('Addresses Selection', render_kw={'readonly': True} )
        input_address_ranges = StringField('Addresses Selection', render_kw={'readonly': True})
        output_addres_ranges = StringField('Addresses Selection', render_kw={'readonly': True})
        fbsensor_address_ranges = StringField('Addresses Selection', render_kw={'readonly': True})
        zone_fbsensor_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
        zone_fan_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
        zone_input_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
        # zone_output_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
        zone_output_index_ranges = StringField(widget=HiddenInput(), render_kw={'readonly': True})
        


        def validate_zone_fbsensor_index_ranges(self, zone_fbsensor_index_ranges):
            if zone_fbsensor_index_ranges.data:
                modbus_range_count = getCountOfModbusRange(zone_fbsensor_index_ranges.data)
                if modbus_range_count < 0 or modbus_range_count > 50:
                    raise ValidationError(f'Invalid number of FBSensors selected ({modbus_range_count}). Max number of Sensors allowed per controller is 50.')

        def validate_zone_fan_index_ranges(self, zone_fan_index_ranges):
            if zone_fan_index_ranges.data:
                modbus_range_count = getCountOfModbusRange(zone_fan_index_ranges.data)
                if modbus_range_count < 0 or modbus_range_count > 50:
                    raise ValidationError(f'Invalid number of FBSensors selected ({modbus_range_count}). Max number of Sensors allowed per controller is 50.') 

        
        def validate_zone_input_index_ranges(self, zone_input_index_ranges):
            if zone_input_index_ranges.data:
                modbus_range_count = getCountOfModbusRange(zone_input_index_ranges.data)
                if modbus_range_count < 0 or modbus_range_count > 50:
                    raise ValidationError(f'Invalid number of Fans selected ({modbus_range_count}). Max number of Fans allowed per controller is 50.') 
        
 
    return ZoneSelectedConfigurationForm()

