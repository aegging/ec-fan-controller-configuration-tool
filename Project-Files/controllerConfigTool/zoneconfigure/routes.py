#Developed/Changed By : Chandan Vishwakarma & Jayant Sarvade (CG changes)

from flask import Blueprint
from flask import render_template, url_for, flash, redirect, request
from .forms import selected_zone, updated_zone
from controllerConfigTool import db
from controllerConfigTool.zoneconfigure.utils import findaddressedItems, removeControllerIDFromFans,removeControllerIDFromFBSensors, removeControllerIDFromASensor, findFansWithFanDefinition, findFBSensorsWithFBSensorDefinition,\
findASensorWithAsensorDefinition, createzone, clearzone, totalzone, edit_fieldbus_sensors_zone, edit_fan_job_zone, edit_analog_input_zone, findoutputwithaddress, edit_analog_output_zone
from controllerConfigTool.getInfoFromDatabase import findTagPrefixGivenSalesOrderID, getControllerID, getSalesOrderValues, getTotalControllers, getControllerValues
from controllerConfigTool.controllerassignments.utils import getListControllersWithAssignment
from controllerConfigTool.models import Controller, ControllerInputOutputs, FanDefinitions, FieldbusSensors, Job, ExpansionBoardInputOutput,\
    FieldbusSensorDefinitions, AnalogSensors, AnalogSensorDefinitions, Zone
from.utils import reset_zone
# from controllerconfigurations.utils import getListControllersWithControllerConfigurations

zone_blueprint = Blueprint('zone', __name__)

@zone_blueprint.route('/zoneconfigure/<int:sales_order_id>/<int:current_controller>', methods=['GET','POST'])
def zoneconfigure(sales_order_id,current_controller):

    tag_prefix = findTagPrefixGivenSalesOrderID(sales_order_id)
    controller_id = getControllerID(current_controller, sales_order_id)

    available_fb_sensors = findFBSensorsWithFBSensorDefinition(sales_order_id, controller_id)

    total_countroller_buttons = getTotalControllers(sales_order_id)
    list_index_of_finished_controllers = getListControllersWithAssignment(sales_order_id, tag_prefix)

    
    # # create array of available fans with this sales order e.g.-> [[address,index],[address,index]]
    # # index is necessary for jobs with overlapping fan ranges 
    available_fans = findFansWithFanDefinition(sales_order_id, controller_id)

    available_asensor = findASensorWithAsensorDefinition(sales_order_id, controller_id)

    available_output, all_expansions = findoutputwithaddress(sales_order_id, controller_id)

    controller_type, controller_serial_number, address_fantype_dictionary, grouped_list_fan_types, version_number_id = getControllerValues(controller_id)


    # # find available expansion boards (expansion boards with no controller_id assignment)
    # available_expansion_board_list = findAvailableExpansionBoards(sales_order_id)

    zone_configuration_form= updated_zone(sales_order_id, controller_id)

    zone_selected_configuration_form= selected_zone(sales_order_id, controller_id)

    zone_default_zone = findaddressedItems(sales_order_id,controller_id)

    # zone_choices=get_zones_choices()

    total_zone_number= totalzone(sales_order_id, controller_id)
    if not total_zone_number:
        total_zone_number = 0

    (current_sales_order_number, sales_order_controller_quantity, entered_by, time_zone) = getSalesOrderValues(sales_order_id)
    # total_zone=1

    if request.method == 'POST':
        if 'update' in request.form:
            total_zone = zone_configuration_form.zones.data
            total_zone= int(total_zone)
            for i in range(total_zone+1, 7):
                clearzone(sales_order_id, controller_id, i)
            createzone(sales_order_id, controller_id,total_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))


    #     # if a Controller button on the left side of page is clicked, navigate to that controller
        for i in range(1, total_countroller_buttons + 1):
            controller_name = 'controller' + str(i)
            if controller_name in request.form:
                # before redirect, if fans have been selected, consider current controller configuration as complete. Thus save finished modbus range data into the controller table
                return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=i))
                
        
        # remove all controller assignments from fans created and then redirect back to this page
        if "clear1" in request.form:
            clear_zone = 1
            clearzone(sales_order_id, controller_id, clear_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))
        elif "clear2" in request.form:
            clear_zone = 2
            clearzone(sales_order_id, controller_id, clear_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))
        elif "clear3" in request.form:
            clear_zone = 3
            clearzone(sales_order_id, controller_id, clear_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))
        elif "clear4" in request.form:
            clear_zone = 4
            clearzone(sales_order_id, controller_id, clear_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))
        elif "clear5" in request.form:
            clear_zone = 5
            clearzone(sales_order_id, controller_id, clear_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))
        elif "clear6" in request.form:
            clear_zone = 6
            clearzone(sales_order_id, controller_id, clear_zone)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))

        
        elif "navigate_sales_order_status_button" in request.form:
            return redirect(url_for('salesorderstatus.salesorderstatusview', sales_order_id=sales_order_id))
        

        elif 'Reset' in request.form:
            reset_zone(sales_order_id,controller_id)
            createzone(sales_order_id, controller_id,1)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))
        
        elif 'Reset_all_controllers' in request.form:
            # reset_zone(sales_order_id)
            controller_objects = Controller.query.filter_by(sales_order_id=sales_order_id).all()
            controller_id_list = []
            for controller_object in controller_objects:
                controller_id_list.append(controller_object.id)
            for controller_id in controller_id_list:
                reset_zone(sales_order_id,controller_id)
                createzone(sales_order_id, controller_id,1)
            return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))


        elif 'assign_zone' in request.form:
            if zone_selected_configuration_form.validate_on_submit():
                zone_number = zone_selected_configuration_form.selected_zone.data
                if zone_selected_configuration_form.selected_zone.data and zone_selected_configuration_form.zone_fbsensor_index_ranges.data != "":
                    fb_sensor_add = zone_selected_configuration_form.zone_fbsensor_index_ranges.data
                    edit_fieldbus_sensors_zone(sales_order_id, controller_id, fb_sensor_add, zone_number)
                if zone_selected_configuration_form.selected_zone.data and zone_selected_configuration_form.zone_fan_index_ranges.data != "":
                    fan_add = zone_selected_configuration_form.zone_fan_index_ranges.data
                    edit_fan_job_zone(sales_order_id, controller_id, fan_add, zone_number)
                if zone_selected_configuration_form.selected_zone.data and zone_selected_configuration_form.zone_input_index_ranges.data != "":
                    input_add = zone_selected_configuration_form.zone_input_index_ranges.data
                    edit_analog_input_zone(sales_order_id, controller_id, input_add, zone_number)
                if zone_selected_configuration_form.selected_zone.data and zone_selected_configuration_form.zone_output_index_ranges.data !="":
                    output_add = zone_selected_configuration_form.zone_output_index_ranges.data
                    edit_analog_output_zone(sales_order_id, controller_id, output_add, zone_number, all_expansions)

                

                

                
                return redirect(url_for('zone.zoneconfigure', sales_order_id=sales_order_id, current_controller=current_controller))

    analog_availability=False
    expansion1_availability=False
    expansion2_availability=False
    expansion3_availability=False
    expansion4_availability=False
    expansion5_availability=False
    
    for key in available_asensor['Analog'].keys():
        if key != None:
            analog_availability=True
   
    try:
        if available_asensor['expansion1'].keys():
            expansion1_availability=True
    except:
        expansion1_availability=False
    
    try:
        if available_asensor['expansion2'].keys():
            expansion2_availability=True
    except:
        expansion2_availability=False
    
    try:
        if available_asensor['expansion3'].keys():
            expansion3_availability=True
    except:
        expansion3_availability=False
    
    try:
        if available_asensor['expansion4'].keys():
            expansion4_availability=True
    except:
        expansion4_availability=False

    try:
        if available_asensor['expansion5'].keys():
            expansion5_availability=True
    except:
        expansion5_availability=False

    


    return render_template('zoneconfigure.html',zone_configuration_form=zone_configuration_form,zone_selected_configuration_form=zone_selected_configuration_form,
                           sales_order_id=sales_order_id, zone_default_zone=zone_default_zone,
                           sales_order_controller_quantity=sales_order_controller_quantity,
                           current_controller=current_controller, list_index_of_finished_controllers=list_index_of_finished_controllers, tag_prefix=tag_prefix,
                           address_fantype_dict=address_fantype_dictionary, available_fans=available_fans, available_fb_sensors= available_fb_sensors, 
                           available_asensor=available_asensor,total_zone_number=total_zone_number, available_output=available_output,analog_availability=analog_availability,
                           expansion1_availability=expansion1_availability,expansion2_availability=expansion2_availability,expansion3_availability=expansion3_availability,
                           expansion4_availability=expansion4_availability,expansion5_availability=expansion5_availability )
