#Developed/Changed By : Chandan Vishwakarma & Jayant Sarvade (CG changes)

from controllerConfigTool import db
from controllerConfigTool.dataCalculations import decodeModbusRange
from controllerConfigTool.getInfoFromDatabase import getDateAndTime
from controllerConfigTool.models import Controller, ControllerInputOutputs, FanDefinitions, FieldbusSensors, Job, ExpansionBoardInputOutput,\
    FieldbusSensorDefinitions, AnalogSensors, AnalogSensorDefinitions, Zone, OutputZones
from controllerConfigTool.dataCalculations import createModbusRangesString







def createzone(sales_order_id, controller_id, z_number):
    zone_check = bool(Zone.query.filter_by(sales_order_id=sales_order_id).filter(Zone.controller_id==controller_id).first())
    if zone_check:
        Zone.query.filter_by(sales_order_id=sales_order_id).filter(Zone.controller_id==controller_id).delete()
    for i in range(1, int(z_number)+1):
        zone = Zone(sales_order_id=sales_order_id, controller_id=controller_id,zone_number=i)
        db.session.add(zone)
    db.session.commit()

def clearzone(sales_order_id, controller_id, clear_zone):
    removeControllerIDFromFans(sales_order_id, controller_id, clear_zone)
    removeControllerIDFromFBSensors(sales_order_id, controller_id, clear_zone)
    removeControllerIDFromASensor(sales_order_id, controller_id, clear_zone)
    removeControllerIDFromOutputZone(sales_order_id, controller_id, clear_zone)

def reset_zone(sales_order_id,controller_id=None):
    if controller_id== None:
        all_fan_objects = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).all()
    if controller_id !=  None:
        all_fan_objects = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).all()
    if all_fan_objects:
        for fan_object in all_fan_objects:
        # fan_object.controller_id = None
            fan_object.Zone = 1
            db.session.add(fan_object)

    all_asensor_objects_expans_lst = []
    if controller_id== None:
        all_asensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).all()
        all_asensor_objects_expans_lst.append(all_asensor_objects)

    if controller_id !=  None:
        # all_asensor_objects_expans_lst = []
        all_asensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).all()
        all_asensor_objects_expans_lst.append(all_asensor_objects)
        expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
        if expansion_board_count >= 1:
            expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
            for id in expansion_ids:
                expansion_id = id.id
                all_asensor_objects_exap = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.expansion_board_id==expansion_id).all()
                all_asensor_objects_expans_lst.append(all_asensor_objects_exap)
    
    
    
    if all_asensor_objects_expans_lst:
        for all_asensor in all_asensor_objects_expans_lst:
            for asensor_object in all_asensor:
                # asensor_object.controller_id = None
                asensor_object.zone = 1
                db.session.add(asensor_object)

    if controller_id== None:
        all_fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).all()

    if controller_id !=  None:
        all_fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==controller_id).all()
    if all_fbsensor_objects:
        for fbsensor_object in all_fbsensor_objects:
            # fbsensor_object.controller_id = None
            fbsensor_object.zone = 1
            db.session.add(fbsensor_object)
    
    if controller_id== None:
        all_output_objects = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).all()

    if controller_id !=  None:
        all_output_objects = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).all()
        expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
        
        if all_output_objects and expansion_board_count >= 1:
            for output_object in all_output_objects:
                # fbsensor_object.controller_id = None
                output_object.y1_zone = 1
                output_object.y2_zone = 1
                output_object.y3_zone = 1
                output_object.y4_zone = 1
                output_object.u8_zone = 1
                output_object.u9_zone = 1
                output_object.u10_zone = 1
                db.session.add(output_object)
        elif all_output_objects and expansion_board_count == 0:
            for output_object in all_output_objects: 
                output_object.y1_zone = 1
                output_object.y2_zone = 1
                output_object.y3_zone = 1
                output_object.y4_zone = 1
                db.session.add(output_object)
        
    
    db.session.commit()




def totalzone(sales_order_id, controller_id):
    zone_object = db.session.query(Zone).filter(Zone.sales_order_id==sales_order_id).\
            filter(Zone.controller_id==controller_id).count()
    return zone_object


def findFansWithFanDefinition(sales_order_id, controller_id):
    """ find all fans that have been assigned a fan definition """
    all_fans_object = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).\
            filter(Job.Zone==None).order_by(Job.Index).all()
    list_address_of_selected_fans = []
    fan_definitions_name = "None"
    for fan in all_fans_object:
        current_fan_address = int(fan.Address)
        fan_index = fan.Index

        # get fan defintion name
        fan_definition_id = fan.fan_definitions_id
        fan_definitions_object_boolean = bool(FanDefinitions.query.filter_by(id=fan_definition_id).first())
        if fan_definitions_object_boolean:
            fan_definitions_object = FanDefinitions.query.filter_by(id=fan_definition_id).first()
            fan_definitions_name = fan_definitions_object.name

        list_address_of_selected_fans.append([current_fan_address, fan_definitions_name, fan_index])

    result = {}

    for fan_address, definition_name, fan_index in list_address_of_selected_fans:
        result.setdefault(definition_name, list()).append([int(fan_address), int(fan_index)])

    return result


def  findFBSensorsWithFBSensorDefinition(sales_order_id, controller_id):
    """ find all fbsensors that have been assigned a fbsensor definition """
    all_fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==controller_id).\
            filter(FieldbusSensors.zone==None).order_by(FieldbusSensors.id).all()
    list_address_of_selected_fbsensors = []
    fbsensor_definitions_name = "None"
    for fbsensor in all_fbsensor_objects:
        current_fbsensor_address = int(fbsensor.modbus_address)
        fbsensor_id = fbsensor.id

        # get fieldbus defintion name
        fieldbus_sensor_definitions_id = fbsensor.fieldbus_sensor_definitions_id
        fbsensor_definitions_object_boolean = bool(FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first())
        if fbsensor_definitions_object_boolean:
            fbsensor_definitions_object = FieldbusSensorDefinitions.query.filter_by(id=fieldbus_sensor_definitions_id).first()
            fbsensor_definitions_name = fbsensor_definitions_object.definition_name

        list_address_of_selected_fbsensors.append([current_fbsensor_address, fbsensor_definitions_name, fbsensor_id])

    result = {}

    for fbsensor_address, definition_name, fbsensor_id in list_address_of_selected_fbsensors:
        result.setdefault(definition_name, list()).append([int(fbsensor_address), int(fbsensor_id)])

    return result

def  findASensorWithAsensorDefinition(sales_order_id,controller_id):
    """ find all fbsensors that have been assigned a fbsensor definition """
    # expansion_id_list = []
    # all_asensor_objects_expans_lst = []
    # expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    
    all_asensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).\
            filter(AnalogSensors.zone==None).order_by(AnalogSensors.id).all()
    

    
    # all_asensor_objects_expans_lst.append(all_asensor_objects)
    # if expansion_board_count >= 1:
    #     expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
    #     for id in expansion_ids:
    #         expansion_id = id.id
    #         expansion_id_list.append(expansion_id)
    #         all_asensor_objects_expans = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.expansion_board_id==expansion_id).\
    #             filter(AnalogSensors.zone==None).order_by(AnalogSensors.id).all()
    #         all_asensor_objects_expans_lst.append(all_asensor_objects_expans)

    list_address_of_selected_asensors = []
    asensor_definitions_name = "None"
    # for all_objects in all_asensor_objects_expans_lst:
    for asensor in all_asensor_objects:
        current_asensor_address = int(asensor.modbus_address)
        asensor_id = asensor.id

        # get fieldbus defintion name
        analog_sensor_definitions_id = asensor.analog_sensor_definitions_id
        analog_definitions_object_boolean = bool(AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first())
        if analog_definitions_object_boolean:
            asensor_definitions_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first()
            asensor_definitions_name = asensor_definitions_object.definition_name

        list_address_of_selected_asensors.append([current_asensor_address, asensor_definitions_name, asensor_id])

    temp = {}
    result = {}
    final_result = {}

    for asensor_address, definition_name, asensor_id in list_address_of_selected_asensors:
        temp.setdefault(definition_name, list()).append([int(asensor_address), int(asensor_id)])

    result['Analog'] = temp 
    eresult = findexpansionsenssoraddress(sales_order_id, controller_id)
    eresult['Analog'] = temp
    final_result = result.update(eresult)
    return result


def findexpansionsenssoraddress(sales_order_id,controller_id):
    expansion_id_list = []
    all_asensor_objects_expans_lst = []
    expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    final_result = {}

    # all_asensor_objects_expans_lst.append(all_asensor_objects)
    if expansion_board_count >= 1:
        expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
        for id in expansion_ids:
            expansion_id = id.id
            expansion_id_list.append(expansion_id)
            all_asensor_objects_expans = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.expansion_board_id==expansion_id).\
                filter(AnalogSensors.zone==None).order_by(AnalogSensors.id).all()
            all_asensor_objects_expans_lst.append(all_asensor_objects_expans)

            list_address_of_selected_asensors = []
            asensor_definitions_name = "None"
            for asensor in all_asensor_objects_expans:
                current_asensor_address = int(asensor.modbus_address)
                asensor_id = asensor.id

                # get fieldbus defintion name
                analog_sensor_definitions_id = asensor.analog_sensor_definitions_id
                analog_definitions_object_boolean = bool(AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first())
                if analog_definitions_object_boolean:
                    asensor_definitions_object = AnalogSensorDefinitions.query.filter_by(id=analog_sensor_definitions_id).first()
                    asensor_definitions_name = asensor_definitions_object.definition_name

                list_address_of_selected_asensors.append([current_asensor_address, asensor_definitions_name, asensor_id])
            if list_address_of_selected_asensors:
                result = {}

                for asensor_address, definition_name, asensor_id in list_address_of_selected_asensors:
                    result.setdefault(definition_name, list()).append([int(asensor_address), int(asensor_id)])
                
                final_result['expansion'+str(id.expansion_board_number)] = result



    return final_result

    



def  findoutputwithaddress(sales_order_id,controller_id):
    """ find all fbsensors that have been assigned a fbsensor definition """
    result = {}
    inout_id = ControllerInputOutputs.query.filter_by(controller_id=controller_id).first()
    
    if inout_id:
        list_address_of_selected_output = []
        all_y1_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.controller_inout_id==inout_id.id).filter(OutputZones.y1_zone==None).first())
        if all_y1_output_objects:
            list_address_of_selected_output.append([1, 1])
        all_y2_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.controller_inout_id==inout_id.id).filter(OutputZones.y2_zone==None).first())
        if all_y2_output_objects:
            list_address_of_selected_output.append([2, 2])
        all_y3_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.controller_inout_id==inout_id.id).filter(OutputZones.y3_zone==None).first())
        if all_y3_output_objects:
            list_address_of_selected_output.append([3, 3])
        all_y4_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.controller_inout_id==inout_id.id).filter(OutputZones.y4_zone==None).first())
        if all_y4_output_objects:
            list_address_of_selected_output.append([4, 4])
        for op_address, op_id in list_address_of_selected_output:
            result.setdefault("output", list()).append([int(op_address), int(op_id)])
    
    


    check_expansion = bool(db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).first())
    expansion_id = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).first()
    expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
    p=1
    all_expansion = {}
    expansions = {}
    for i,id in enumerate(expansion_ids):
        i = 1+(i+2)*3
        list_address_of_selected_output_expan = []
        if expansion_id != "" and check_expansion:
            all_u8_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.ExpansionBoaard_inout_id==id.id).filter(OutputZones.u8_zone==None).first())
            if all_u8_output_objects:
                list_address_of_selected_output_expan.append([8, i])
                expansions[i] = [id.id, 8]
        if expansion_id != "" and check_expansion:
            all_u9_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.ExpansionBoaard_inout_id==id.id).filter(OutputZones.u9_zone==None).first())
            if all_u9_output_objects:
                list_address_of_selected_output_expan.append([9, i+1])
                expansions[i+1] = [id.id, 9]
        if check_expansion and expansion_id != "":
            all_u10_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.ExpansionBoaard_inout_id==id.id).filter(OutputZones.u10_zone==None).first())
            if all_u10_output_objects:
                list_address_of_selected_output_expan.append([10, i+2])
                expansions[i+2] = [id.id, 10]
        if list_address_of_selected_output_expan:
            result['expansion'+str(p)] = list_address_of_selected_output_expan
        p +=1

    all_expansion[controller_id] = expansions

    return result, all_expansion



def removeControllerIDFromFans(sales_order_id, controller_id,clear_zone):

    all_fan_objects = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).filter(Job.Zone==clear_zone).all()

    for fan_object in all_fan_objects:
        # fan_object.controller_id = None
        fan_object.Zone = None
        db.session.add(fan_object)

    db.session.commit()


def findaddressedItems(sales_order_id,controller_id):

    #find how many have modbus address and sales order id

    all_mixer_fan_address_string = {}
    all_fieldbus_sensors_address_string = {}
    all_analog_sensors_address_string = {}
    all_analog_output_Address_string = {}
    # all_asensor_objects_expans_lst = []

    t_zone = totalzone(sales_order_id,controller_id)
    expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    

    for zone_num in range(1,t_zone +1):
        all_asensor_objects_expans_lst = []

        mixer_fan_objects = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).filter(Job.controller_id==controller_id).filter(Job.Zone==zone_num).order_by(Job.Index).all()

        fieldbus_sensors_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.controller_id==controller_id).filter(FieldbusSensors.sales_order_id==sales_order_id).\
            filter(FieldbusSensors.zone==zone_num).order_by(FieldbusSensors.id).all()

        analog_sensors_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).\
            filter(AnalogSensors.zone==zone_num).order_by(AnalogSensors.id).all()
        all_asensor_objects_expans_lst.append(analog_sensors_objects)
        
        if expansion_board_count >= 1:
            expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
            for id in expansion_ids:
                expansion_id = id.id
                all_asensor_objects_expans = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.expansion_board_id==expansion_id).\
                    filter(AnalogSensors.zone==zone_num).order_by(AnalogSensors.id).all()
                all_asensor_objects_expans_lst.append(all_asensor_objects_expans)



        # build array of [address, id] for each item
        mixer_fan_address_list = []
        fieldbus_sensors_list = []
        analog_sensors_list = []
        exhaust_supply_fan_list = []


        if mixer_fan_objects:
            for mixer_fan_object in mixer_fan_objects:
                mixer_fan_address_list.append(int(mixer_fan_object.Address))

        if fieldbus_sensors_objects:
            for fieldbus_sensor_object in fieldbus_sensors_objects:
                fieldbus_sensors_list.append(int(fieldbus_sensor_object.modbus_address))

        if all_asensor_objects_expans_lst:
            for analog_objects in all_asensor_objects_expans_lst:
                for analog_sensors_object in analog_objects:
                    analog_sensors_list.append(int(analog_sensors_object.modbus_address))

        # if exhaust_supply_fan_objects:
        #     for exhaust_supply_fan_object in exhaust_supply_fan_objects:
        #         exhaust_supply_fan_list.append(int(exhaust_supply_fan_object.modbus_address))

 
        analog_output_list = findinoutaddreess(controller_id, zone_num)
        analog_output_expan_list = findinoutexpansionaddrress(controller_id, zone_num)
        

        # create modbus address string for each array
        mixer_fan_address_string = createModbusRangesString(mixer_fan_address_list)
        fieldbus_sensors_address_string = createModbusRangesString(fieldbus_sensors_list)
        analog_sensors_address_string = createModbusRangesString(analog_sensors_list)
            # exhaust_supply_address_string = createModbusRangesString(exhaust_supply_fan_list)
        analog_output_address_string = createModbusRangesString(analog_output_list)
        analog_output_expansion_address_list = createModbusRangesString(analog_output_expan_list)

        y_res = ''
        u_res = ''
        for char in analog_output_address_string:
            if "-" or "," not in char:
                y_res = y_res + 'Y'+char
        for char in analog_output_expansion_address_list:
            if "-" or "," not in char:
                u_res = u_res + 'U'+char

        analog_output_all_string = y_res +","+ u_res if y_res else u_res
        analog_output_all_string = analog_output_all_string.replace('U-',"-").replace('U,',',').replace('U1U0','U10').replace('Y-',"-").replace('Y,',',')
        
        all_mixer_fan_address_string[zone_num] = mixer_fan_address_string
        all_fieldbus_sensors_address_string[zone_num] = fieldbus_sensors_address_string
        all_analog_sensors_address_string[zone_num] = analog_sensors_address_string
        all_analog_output_Address_string[zone_num] = analog_output_all_string



    address_string_dictionary = {}
    
    address_string_dictionary['Mixer Fans'] = all_mixer_fan_address_string
    address_string_dictionary['Fieldbus Sensors'] = all_fieldbus_sensors_address_string
    address_string_dictionary['Analog Sensors'] = all_analog_sensors_address_string
    # address_string_dictionary['Exhaust Supply Fans'] = exhaust_supply_address_string
    address_string_dictionary['Output Sensors'] = all_analog_output_Address_string
    return address_string_dictionary


def findinoutaddreess(controller_id, zone_n):
    list_address_of_selected_output = []

    all_y1_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y1_zone==zone_n).first())
    if all_y1_output_objects:
        list_address_of_selected_output.append(1)
    all_y2_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y2_zone==zone_n).first())
    if all_y2_output_objects:
        list_address_of_selected_output.append(2)
    all_y3_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y3_zone==zone_n).first())
    if all_y3_output_objects:
        list_address_of_selected_output.append(3)
    all_y4_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y4_zone==zone_n).first())
    if all_y4_output_objects:
        list_address_of_selected_output.append(4)

    return list_address_of_selected_output

def findinoutexpansionaddrress(controller_id, zone_n):
    list_address_of_selected_output = []

    all_u8_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.u8_zone==zone_n).first())
    if all_u8_output_objects:
        list_address_of_selected_output.append(8)
    all_u9_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.u9_zone==zone_n).first())
    if all_u9_output_objects:
        list_address_of_selected_output.append(9)
    all_u10_output_objects = bool(db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).filter(OutputZones.u10_zone==zone_n).first())
    if all_u10_output_objects:
        list_address_of_selected_output.append(10)
    
    return list_address_of_selected_output


def removeControllerIDFromFBSensors(sales_order_id, controller_id,clear_zone):

    all_fbsensor_objects = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).filter(FieldbusSensors.controller_id==controller_id).filter(FieldbusSensors.zone==clear_zone).all()

    for fbsensor_object in all_fbsensor_objects :
        # fbsensor_object.controller_id = None
        fbsensor_object.zone = None
        db.session.add(fbsensor_object)

    db.session.commit()


""" Remove controller_id, zone from fans in analog sensor table, given the controller_id,zone """
def removeControllerIDFromASensor(sales_order_id, controller_id, clear_zone):
    all_asensor_objects_expans_lst = []
    all_asensor_objects = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).filter(AnalogSensors.zone==clear_zone).all()
    all_asensor_objects_expans_lst.append(all_asensor_objects)
    expansion_board_count = ExpansionBoardInputOutput.query.filter_by(controller_id=controller_id).count()
    if expansion_board_count >= 1:
        expansion_ids = db.session.query(ExpansionBoardInputOutput).filter(ExpansionBoardInputOutput.sales_order_id==sales_order_id).filter(ExpansionBoardInputOutput.controller_id==controller_id).order_by(ExpansionBoardInputOutput.id).all()
        for id in expansion_ids:
            expansion_id = id.id
            all_asensor_objects_exap = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.expansion_board_id==expansion_id).filter(AnalogSensors.zone==clear_zone).all()
            all_asensor_objects_expans_lst.append(all_asensor_objects_exap)
    

    for all_asensor in all_asensor_objects_expans_lst:
        for asensor_object in all_asensor:
            # asensor_object.controller_id = None
            asensor_object.zone = None
            db.session.add(asensor_object)

    db.session.commit()

def removeControllerIDFromOutputZone(sales_order_id, controller_id,clear_zone):

    all_inout_y1_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y1_zone==clear_zone).all()
    for y1_object in all_inout_y1_object:
        y1_object.y1_zone = None
        db.session.add(y1_object)
    all_inout_y2_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y2_zone==clear_zone).all()
    for y2_object in all_inout_y2_object:
        y2_object.y2_zone = None
        db.session.add(y2_object)
    all_inout_y3_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y3_zone==clear_zone).all()
    for y3_object in all_inout_y3_object:
        y3_object.y3_zone = None
        db.session.add(y3_object)
    all_inout_y4_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.y4_zone==clear_zone).all()
    for y4_object in  all_inout_y4_object:
        y4_object.y4_zone = None
        db.session.add(y4_object)
    all_inout_u8_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.u8_zone==clear_zone).all()
    for u8_object in  all_inout_u8_object:
        u8_object.u8_zone = None
        db.session.add(u8_object)
    all_inout_u9_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.u9_zone==clear_zone).all()
    for u9_object in  all_inout_u9_object:
        u9_object.u9_zone = None
        db.session.add(u9_object)
    all_inout_u10_object = db.session.query(OutputZones).filter(OutputZones.sales_order_id==sales_order_id).filter(OutputZones.controller_id==controller_id).filter(OutputZones.u10_zone==clear_zone).all()
    for u10_object in  all_inout_u10_object:
        u10_object.u10_zone = None
        db.session.add(u10_object)
    db.session.commit()



def edit_fieldbus_sensors_zone(sales_order_id, controller_id, modbus_ranges_index, zone_num):
    """ creates all the appropriate new sensor assignments for a given controller """

    # create modbus address list using modbus string
    modbus_address_list = decodeModbusRange(modbus_ranges_index)
   
    # for each fbsensor_id, determine fbsensor_definition_id
    # create default dict {fbsensor_definition_id: {[fbsensor_id, fbsensor_id, fbsensor_id]}}
    definition_id_fbsensor_id_list = []
    for fbsensor_id in modbus_address_list:
        # get fbsensor definition id for this fan
        fbsensor_object = FieldbusSensors.query.filter_by(id=fbsensor_id).first()
        fbsensor_definitions_id = fbsensor_object.fieldbus_sensor_definitions_id
        definition_id_fbsensor_id_list.append([fbsensor_definitions_id, fbsensor_id])

    default_dict = {}
    for definition_id, fbsensor_id in definition_id_fbsensor_id_list:
        default_dict.setdefault(definition_id, list()).append([int(fbsensor_id)])

    # for each definition
    for fbsensor_definition_id in default_dict:

        # for each fbsensor_id with this definition
        for fbsensor_id in default_dict[fbsensor_definition_id]:

            # save controller_id to current fbsensor
            current_fbsensor_object = db.session.query(FieldbusSensors).filter(FieldbusSensors.sales_order_id==sales_order_id).\
                filter(FieldbusSensors.controller_id==controller_id).filter(FieldbusSensors.id==fbsensor_id).first()
            current_fbsensor_object.zone = zone_num

            db.session.add(current_fbsensor_object)
    
    db.session.commit()


def edit_fan_job_zone(sales_order_id, controller_id, modbus_ranges_index, zone_num):
    """ creates all the appropriate new sensor assignments for a given controller """

    # create modbus address list using modbus string
    modbus_address_list = decodeModbusRange(modbus_ranges_index)
   
    # for each fbsensor_id, determine fbsensor_definition_id
    # create default dict {fbsensor_definition_id: {[fbsensor_id, fbsensor_id, fbsensor_id]}}
    definition_id_fan_id_list = []
    for fan_id in modbus_address_list:
        # get fan definition id for this fan
        fan_object = Job.query.filter_by(Index=fan_id).first()
        fan_definitions_id = fan_object.fan_definitions_id
        definition_id_fan_id_list.append([fan_definitions_id, fan_id])

    default_dict = {}
    for definition_id, fan_id in definition_id_fan_id_list:
        default_dict.setdefault(definition_id, list()).append([int(fan_id)])

    # for each definition
    for fan_definitions_id in default_dict:

        # for each fbsensor_id with this definition
        for fan_id in default_dict[fan_definitions_id]:

            # save controller_id to current fbsensor
            current_fan_object = db.session.query(Job).filter(Job.sales_order_id==sales_order_id).\
                filter(Job.controller_id==controller_id).filter(Job.Index==fan_id).first()
            current_fan_object.Zone = zone_num

            db.session.add(current_fan_object)
    
    db.session.commit()




def edit_analog_input_zone(sales_order_id, controller_id, modbus_ranges_index, zone_num):
    """ creates all the appropriate new sensor assignments for a given controller """

    # create modbus address list using modbus string
    modbus_address_list = decodeModbusRange(modbus_ranges_index)

    # for each fbsensor_id, determine fbsensor_definition_id
    # create default dict {fbsensor_definition_id: {[fbsensor_id, fbsensor_id, fbsensor_id]}}
    definition_id_input_id_list = []
    for input_id in modbus_address_list:
        # get fan definition id for this fan
        input_object = AnalogSensors.query.filter_by(id=input_id).first()
        input_definitions_id = input_object.analog_sensor_definitions_id
        definition_id_input_id_list.append([input_definitions_id, input_id])

    default_dict = {}
    for definition_id, input_id in definition_id_input_id_list:
        default_dict.setdefault(definition_id, list()).append([int(input_id)])

    # for each definition
    for input_definitions_id in default_dict:

        # for each fbsensor_id with this definition
        for input_id in default_dict[input_definitions_id]:

            # save controller_id to current fbsensor
            current_input_object = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).filter(AnalogSensors.controller_id==controller_id).filter(AnalogSensors.id==input_id).first()
            
            if current_input_object is None:
                current_input_object = db.session.query(AnalogSensors).filter(AnalogSensors.sales_order_id==sales_order_id).\
                filter(AnalogSensors.id==input_id).first()
            current_input_object.zone = zone_num

            db.session.add(current_input_object)
    
    db.session.commit()


def edit_analog_output_zone(sales_order_id, controller_id, modbus_ranges_index, zone_num, all_expansions):
    """ creates all the appropriate new sensor assignments for a given controller """

    # create modbus address list using modbus string
    modbus_address_list = decodeModbusRange(modbus_ranges_index)
    # current_output_object = OutputZones.query.filter_by(controller_id = controller_id).first()
    # op_id = current_output_object.id
    current_output_object1 = OutputZones.query.filter_by(controller_id = controller_id).all()
    for id in modbus_address_list:
        if id <= 4:
            for oid in current_output_object1:
                current_output_object = db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).\
                filter(OutputZones.id==oid.id).first()
                if id==1:
                    current_output_object.y1_zone = zone_num
                    db.session.add(current_output_object)
                    db.session.commit()
                if id==2:
                    current_output_object.y2_zone = zone_num
                    db.session.add(current_output_object)
                    db.session.commit()
                if id==3:
                    current_output_object.y3_zone = zone_num
                    db.session.add(current_output_object)
                    db.session.commit()
                if id==4:
                    current_output_object.y4_zone = zone_num
                    db.session.add(current_output_object)
                    db.session.commit()
        elif id >= 6:
            expn_id = all_expansions[controller_id][id][0]
            zone_id = all_expansions[controller_id][id][1]
            current_output_object_expn = db.session.query(OutputZones).filter(OutputZones.controller_id==controller_id).\
                filter(OutputZones.ExpansionBoaard_inout_id==expn_id).first()
            if zone_id==8:
                current_output_object_expn.u8_zone = zone_num
                db.session.add(current_output_object_expn)
                db.session.commit()
            if zone_id==9:
                current_output_object_expn.u9_zone = zone_num
                db.session.add(current_output_object_expn)
                db.session.commit()
            if zone_id==10:
                current_output_object_expn.u10_zone = zone_num
                db.session.add(current_output_object_expn)
                db.session.commit()
    
    db.session.commit()
