"""empty message

Revision ID: 364eb7e1f5bf
Revises: 5397b58d56ef
Create Date: 2023-07-11 14:12:13.601869

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '364eb7e1f5bf'
down_revision = '5397b58d56ef'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('fan_definitions', sa.Column('sub_fan_type', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('fan_definitions', 'sub_fan_type')
    # ### end Alembic commands ###
