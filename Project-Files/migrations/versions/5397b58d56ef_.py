"""empty message

Revision ID: 5397b58d56ef
Revises: ff5494e0e82e
Create Date: 2023-05-31 10:22:35.656120

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5397b58d56ef'
down_revision = 'ff5494e0e82e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###
