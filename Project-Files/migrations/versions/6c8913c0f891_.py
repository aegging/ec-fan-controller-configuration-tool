"""empty message

Revision ID: 6c8913c0f891
Revises: 364eb7e1f5bf
Create Date: 2023-07-13 06:41:42.331760

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6c8913c0f891'
down_revision = '364eb7e1f5bf'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('expansion_board_input_output', sa.Column('expansion_outputs_saved', sa.Boolean(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('expansion_board_input_output', 'expansion_outputs_saved')
    # ### end Alembic commands ###
