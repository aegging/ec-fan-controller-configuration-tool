"""empty message

Revision ID: 97fc3a2577af
Revises: 
Create Date: 2020-11-03 17:42:46.671662

"""
from alembic import op
import sqlalchemy as sa
# from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '97fc3a2577af'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('jobs', sa.Column('controller_id', sa.Integer(), nullable=True))
    op.add_column('jobs', sa.Column('sales_order_id', sa.Integer(), nullable=True))
    op.create_foreign_key(None, 'jobs', 'sales_orders', ['sales_order_id'], ['id'])
    op.create_foreign_key(None, 'jobs', 'controller', ['controller_id'], ['id'])
    # ### end Alembic commands ###

