from controllerConfigTool import db
from flask import Flask
from controllerConfigTool.configFile import getConfigFile

db_pass, db_ip = getConfigFile()
db_uri = 'mysql+pymysql://jetvent:' + str(db_pass) + '@'+ str(db_ip) + '/jetvent'
app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
app.config['SQLALCHEMY_DATABASE_URI'] = db_uri

db.create_all()
