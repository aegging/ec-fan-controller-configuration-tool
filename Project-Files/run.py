from controllerConfigTool import app
from controllerConfigTool.getInfoFromDatabase import areSettingsEmpty
from controllerConfigTool.programSettings import createDefaultSettings
from waitress import serve


if __name__ == '__main__':

    if areSettingsEmpty():
        # no settings detected, create test ones
        createDefaultSettings()


    serve(app, host='0.0.0.0', port=4345) #WAITRESS


    
