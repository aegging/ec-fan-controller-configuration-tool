# System Requirements

1. c.factory
2. Python 3
3. JetVent.ini file location
   If JetVent.ini file is in 'C:\Program Files\Fantech\JetVent\JetVent.ini' -- NO ACTION REQUIRED
   -- or -- If in other location , change value of variable 'JETVENT_INI_PATH' on line 3 of file configFile.py
4. Internet access

# Initial Configuration

Setup Database 
1. Update Database by running 'run-database-migration.bat' file


# START-APP

1. Run 'start-app.bat' to set up virtual environment and start app

# Review Admin Configuration

NOTE IP ADDRESS OF MACHINE
1. Go to http://xx.xx.xx.xx:8080/admin  (xx.xx.xx.xx is ip address of machine -- example: http://192.168.1.147:8080/admin)
