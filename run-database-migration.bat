rem py is calling my python3 variable
rem if you don't have python3 added to path, you must add
rem or you can hardcode the path, i.e. replace py w/ your complete python3 path



rem install virtual environment
py -m pip install --user virtualenv

IF not exist ec-fan-controller-config-tool.env\NUL py -m venv ec-fan-controller-config-tool.env

rem activate virtual environment
call "ec-fan-controller-config-tool.env\Scripts\activate.bat"
py -m pip install --upgrade pip
py -m pip install wheel

rem install dependencies
py -m pip install cryptography
ec-fan-controller-config-tool.env\Scripts\pip install -r requirements.txt

cd Project-Files
rem perform database migration
echo %CD%
set FLASK_APP=controllerConfigTool/__init__.py
flask db upgrade
