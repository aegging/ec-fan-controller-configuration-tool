rem py is calling my python3 variable
rem if you don't have python3 added to path, you must add
rem or you can hardcode the path, i.e. replace py w/ your complete python3 path



rem install virtual environment
py -m pip install --user virtualenv

IF not exist ec-fan-controller-config-tool.env\NUL py -m venv ec-fan-controller-config-tool.env

rem activate virtual environment
call "ec-fan-controller-config-tool.env\Scripts\activate.bat"

py -m pip install --upgrade pip
py -m pip install wheel
py -m pip install cryptography

rem install dependencies
ec-fan-controller-config-tool.env\Scripts\pip install -r requirements.txt

rem start application
echo Serving at hostIP:4345/salesorder/
"ec-fan-controller-config-tool.env\Scripts\python.exe" "Project-Files\run.py"